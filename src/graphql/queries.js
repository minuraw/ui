/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getChatHistory = /* GraphQL */ `
  query GetChatHistory(
    $member_id: String
    $limit: Int
    $offset: Int
    $conversation_id: String
  ) {
    getChatHistory(
      member_id: $member_id
      limit: $limit
      offset: $offset
      conversation_id: $conversation_id
    )
  }
`;
export const singlePost = /* GraphQL */ `
  query SinglePost($id: ID!) {
    singlePost(id: $id) {
      should_end_interaction
      response_map {
        responses {
          default {
            context_id
            work_item_id
            statement_type
            response_template {
              response_type
              items {
                id
                label
                priority
              }
              text
              delivery_status
              metadata {
                context {
                  concept_id
                  skill_id
                  skill_label
                }
                next_input {
                  ui_type
                  is_multiple_choice
                }
                min_date
                max_date
                max_num_of_images
                val_regex
                val_regex_err_message
                data_type
                min_value
                max_value
                initial_value
                max_length
                min_length
                skip_button_label
              }
              ui_elements {
                response {
                  response_type
                  text
                  delivery_status
                  metadata {
                    context {
                      concept_id
                      skill_id
                      skill_label
                    }
                    next_input {
                      ui_type
                      is_multiple_choice
                    }
                    min_date
                    max_date
                    max_num_of_images
                    val_regex
                    val_regex_err_message
                    data_type
                    min_value
                    max_value
                    initial_value
                    max_length
                    min_length
                    skip_button_label
                  }
                }
                external_response {
                  response_type
                  text
                  delivery_status
                  metadata {
                    context {
                      concept_id
                      skill_id
                      skill_label
                    }
                    next_input {
                      ui_type
                      is_multiple_choice
                    }
                    min_date
                    max_date
                    max_num_of_images
                    val_regex
                    val_regex_err_message
                    data_type
                    min_value
                    max_value
                    initial_value
                    max_length
                    min_length
                    skip_button_label
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
export const detectIntent = /* GraphQL */ `
  query DetectIntent($searchText: String!) {
    detectIntent(searchText: $searchText) {
      intent
      probability
      ambiguityLevel
    }
  }
`;
export const nlpSearchForInitialSymptoms = /* GraphQL */ `
  query NlpSearchForInitialSymptoms($initialSymptoms: SymptomSearchCriteria!) {
    nlpSearchForInitialSymptoms(initialSymptoms: $initialSymptoms) {
      id
      name
      commonName
      choiceId
    }
  }
`;
export const diagnose = /* GraphQL */ `
  query Diagnose($diagnoseData: DiagnoseCriteria) {
    diagnose(diagnoseData: $diagnoseData) {
      currentQuestion {
        questionType
        text
        items {
          id
          name
          choices {
            id
            label
          }
        }
      }
      probableConditions {
        id
        name
        commonName
        probability
      }
      hasEmergencyEvidence
      shouldStop
    }
  }
`;
export const pharmacies = /* GraphQL */ `
  query Pharmacies(
    $postalCode: String
    $city: String
    $address: String
    $pharmacyName: String
  ) {
    pharmacies(
      postalCode: $postalCode
      city: $city
      address: $address
      pharmacyName: $pharmacyName
    ) {
      name
      identifier {
        system
        value
      }
      telecom {
        system
        value
      }
      address {
        line
        postalCode
        state
        city
        country
        location {
          latitude
          longitude
        }
        specialties
      }
    }
  }
`;
export const helloWorld = /* GraphQL */ `
  query HelloWorld($consumer_key: String, $consumer_secret: String) {
    helloWorld(consumer_key: $consumer_key, consumer_secret: $consumer_secret)
  }
`;
export const getInitialQuestions = /* GraphQL */ `
  query GetInitialQuestions($category: String) {
    getInitialQuestions(category: $category) {
      id
      question
      type
      key
      endOfInteraction
      validate
      authenticate
      source
      action
    }
  }
`;
export const patientProfile = /* GraphQL */ `
  query PatientProfile($patientId: String, $email: String!, $phone: String) {
    patientProfile(patientId: $patientId, email: $email, phone: $phone) {
      patientId
      firstName
      lastName
      gender
      addressLineOne
      addressLineTwo
      addressLineThree
      city
      state
      zip
      dob
      phone
      email
    }
  }
`;
export const getConsultTimeslots = /* GraphQL */ `
  query GetConsultTimeslots(
    $consultId: String!
    $start: AWSDateTime
    $end: AWSDateTime
    $page: Int
  ) {
    getConsultTimeslots(
      consultId: $consultId
      start: $start
      end: $end
      page: $page
    ) {
      start
      end
      clinicians {
        id
        name
        npi
        license {
          state
          expiresAt
          licenseNumber
        }
      }
    }
  }
`;
export const getMember = /* GraphQL */ `
  query GetMember {
    getMember {
      memberId
      firstName
      lastName
      gender
      addressLineOne
      addressLineTwo
      addressLineThree
      city
      stateCode
      stateName
      zip
      dob
      contactChannelCode
      contactAddress
      loginChannelCode
      loginAddress
    }
  }
`;
export const getProductsPayment = /* GraphQL */ `
  query GetProductsPayment($productsPayment: ProductsPaymentRequest) {
    getProductsPayment(productsPayment: $productsPayment) {
      currency
      items {
        productId
        quantity
        metadata {
          provider
          protocol
          mode
        }
        price
      }
      clientSecret
      totalPrice
    }
  }
`;
export const getConversations = /* GraphQL */ `
  query GetConversations($memberId: String!) {
    getConversations(memberId: $memberId) {
      conversationId
      message
      date
    }
  }
`;
