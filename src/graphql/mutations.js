/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const post = /* GraphQL */ `
  mutation Post($postEvent: PostEvent) {
    post(postEvent: $postEvent) {
      should_end_interaction
      response_map {
        responses {
          default {
            context_id
            work_item_id
            statement_type
            response_template {
              response_type
              items {
                id
                label
                priority
              }
              text
              delivery_status
              metadata {
                context {
                  concept_id
                  skill_id
                  skill_label
                }
                next_input {
                  ui_type
                  is_multiple_choice
                }
                min_date
                max_date
                max_num_of_images
                val_regex
                val_regex_err_message
                data_type
                min_value
                max_value
                initial_value
                max_length
                min_length
                skip_button_label
              }
              ui_elements {
                response {
                  response_type
                  text
                  delivery_status
                  metadata {
                    context {
                      concept_id
                      skill_id
                      skill_label
                    }
                    next_input {
                      ui_type
                      is_multiple_choice
                    }
                    min_date
                    max_date
                    max_num_of_images
                    val_regex
                    val_regex_err_message
                    data_type
                    min_value
                    max_value
                    initial_value
                    max_length
                    min_length
                    skip_button_label
                  }
                }
                external_response {
                  response_type
                  text
                  delivery_status
                  metadata {
                    context {
                      concept_id
                      skill_id
                      skill_label
                    }
                    next_input {
                      ui_type
                      is_multiple_choice
                    }
                    min_date
                    max_date
                    max_num_of_images
                    val_regex
                    val_regex_err_message
                    data_type
                    min_value
                    max_value
                    initial_value
                    max_length
                    min_length
                    skip_button_label
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
export const externalMessage = /* GraphQL */ `
  mutation ExternalMessage($inputMessage: InputMessage) {
    externalMessage(inputMessage: $inputMessage) {
      memberId
      consultId
      displayText
      clientReferenceId
      eventType
      eventTime
      reason
      disposition
      fields
      messageId
      messageTime
      unreadCount
      lastReadAt
      conversationId
      messageTypeId
    }
  }
`;
export const putImage = /* GraphQL */ `
  mutation PutImage($name: String, $base64String: String!, $type: String) {
    putImage(name: $name, base64String: $base64String, type: $type) {
      url
    }
  }
`;
export const initialChat = /* GraphQL */ `
  mutation InitialChat($questionId: Int, $answer: String, $answerType: String) {
    initialChat(
      questionId: $questionId
      answer: $answer
      answerType: $answerType
    ) {
      questionId
      question
      answerType
      shouldEndInteraction
    }
  }
`;
export const saveChat = /* GraphQL */ `
  mutation SaveChat($messages: String!) {
    saveChat(messages: $messages)
  }
`;
