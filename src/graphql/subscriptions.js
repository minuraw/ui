/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onMessageReceived = /* GraphQL */ `
  subscription OnMessageReceived($memberId: String) {
    onMessageReceived(memberId: $memberId) {
      memberId
      consultId
      displayText
      clientReferenceId
      eventType
      eventTime
      reason
      disposition
      fields
      messageId
      messageTime
      unreadCount
      lastReadAt
      conversationId
      messageTypeId
    }
  }
`;
