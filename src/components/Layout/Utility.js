import React from 'react';
import { DynamicProductCard } from '../Cards';
import { SectionContainer } from '../Common';
import { Grid } from '@material-ui/core';

const Utility = ({ productGroup }) => {
  if (!productGroup) return null;

  return (
    <SectionContainer
      maxWidth="lg"
      color
      sectionTitle={productGroup.product_group_title}
      sectionSubtitle={productGroup.product_group_description}
    >
      <Grid container spacing={4}>
        {productGroup.products.map((product, index) => (
          <Grid item xs={12} sm={6} md={4} key={index}>
            <DynamicProductCard product={product} />
          </Grid>
        ))}
      </Grid>
    </SectionContainer>
  );
};

export { Utility };
