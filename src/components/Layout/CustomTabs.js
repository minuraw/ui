import { useState } from 'react';
import { Drawer, Tabs, Tab, makeStyles } from '@material-ui/core';

const useClasses = makeStyles((theme) => ({
  drawerPaper: {
    backgroundColor: theme.palette.grey[200],
    padding: '8px 0',
  },
  tabs: {
    '& .MuiButtonBase-root.MuiTab-root': {
      fontSize: '0.75rem',
    },
    '& .MuiTabs-indicator': {
      display: 'none',
    },
    '& .MuiTab-textColorInherit.Mui-selected': {
      backgroundColor: '#fff',
      borderRadius: theme.shape.borderRadius,
      border: '1px solid #ccc',
    },
    '& .MuiTabs-flexContainer': {
      marginLeft: theme.spacing(2),
    },
  },
}));

const CustomTabs = ({ setProductsVisible, tabData, handleClick }) => {
  const classes = useClasses();

  const [value, setValue] = useState(0);

  const handleChange = (e, newValue) => {
    setValue(newValue);
    handleClick();
  };

  return (
    <>
      <Drawer anchor="bottom" variant="permanent" PaperProps={{ className: classes.drawerPaper }}>
        <Tabs
          variant="scrollable"
          scrollButtons="auto"
          value={value}
          onChange={handleChange}
          className={classes.tabs}
        >
          {tabData.map((tab) => (
            <Tab
              key={tab.product_group_id}
              disableRipple
              label={tab.product_group_title}
              onClick={() => setProductsVisible(tab.product_group_id)}
            />
          ))}
        </Tabs>
      </Drawer>
    </>
  );
};

export { CustomTabs };
