import { useState, useContext } from 'react';
import Link from 'next/link';
import {
  Grid,
  Typography,
  ListItem,
  ListItemText,
  List,
  Drawer,
  Box,
  Button,
  makeStyles,
  Link as MuiLink,
  useMediaQuery,
} from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Badge from '@material-ui/core/Badge';
// import MenuIcon from '@material-ui/icons/Menu';
// import SearchIcon from '@material-ui/icons/Search';
import { LanguageToggle, StateSelector } from '../Common';
import { AuthHeaderProfile } from './AuthHeaderProfile';
import { Logo } from '../Logo';
import { Store as GlobalStore, useToggles } from '../../context';
import { FEATURE_TOGGLES } from '../../constants';

const CAPTIONS = {
  login: {
    'en-us': 'Login',
    'es-mx': 'Iniciar sesión',
  },
  over: {
    'en-us': 'Over 1 million users and growing...',
    'es-mx': 'Más de 1 millón de usuarios y creciendo...',
  },
};

const useClasses = makeStyles((theme) => ({
  drawer: {
    '& .MuiDrawer-paper': {
      backgroundColor: theme.palette.ggd.primary,
    },
  },
  drawerInner: {
    width: '260px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  logoContainer: {
    padding: '1rem',
  },
  link: {
    color: theme.palette.blue.dark,
    borderBottom: '3px solid transparent',
    paddingBottom: '2px',
    textDecoration: 'none',
    '&:hover': {
      borderBottomColor: theme.palette.secondary.main,
      cursor: 'pointer',
    },
  },
  searchBox: {
    backgroundColor: theme.palette.grey[100],
    borderRadius: theme.shape.borderRadius,
    padding: '2px',
  },
  searchButton: {
    backgroundColor: 'inherit',
  },
  mobileMenuIcon: {
    padding: 0,
  },
  iconContainer: {
    '& svg': {
      fill: theme.palette.ggd.light,
      marginRight: '1rem',
    },
  },
  item: {
    '& .MuiTypography-h6': {
      color: theme.palette.ggd.light,
      fontWeight: 500,
    },
    '& .MuiButton-label': {
      color: theme.palette.ggd.light,
    },
    '& svg': {
      fill: theme.palette.ggd.light,
    },
  },
  list: {
    '& .MuiAccordion-root:before': {
      display: 'none',
    },
    '& .MuiAccordionSummary-content': {
      alignItems: 'center',
    },
    '& .MuiSvgIcon-root': {
      fill: theme.palette.ggd.light,
    },
  },
  accordion: {
    backgroundColor: theme.palette.ggd.primary,
    '& .MuiTypography-h6': {
      color: theme.palette.ggd.light,
      fontWeight: 500,
    },
    '& .MuiBadge-badge': {
      backgroundColor: '#115F71',
      color: theme.palette.ggd.light,
      marginRight: '0.5rem',
    },
  },
  subItem: {
    '& .MuiTypography-h6': {
      color: theme.palette.ggd.light,
      fontSize: 16,
      fontWeight: 300,
      opacity: 0.5,
      paddingLeft: '2.5rem',
    },
  },
  bottomItem: {
    color: theme.palette.ggd.light,
    paddingTop: 0,
    paddingBottom: 0,
  },
  logo: {
    cursor: 'pointer',
  },
}));

const MobileNavigation = ({
  memberProfile,
  handleLogout = () => {},
  mainNavItems = [],
  subNavItems = [],
  bottomItems = [],
  headerType,
}) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const mediumScreen = useMediaQuery((theme) => theme.breakpoints.up('md'));

  const [showMobileNav, setShowMobileNav] = useState(false);

  const [expanded, setExpanded] = useState(false);

  const { isFeatureEnabled, isToggleReady } = useToggles();

  const showMainPage = isToggleReady && isFeatureEnabled(FEATURE_TOGGLES.SHOW_MAIN_PAGE);
  let logoUrl;
  if (showMainPage) {
    logoUrl = '/';
  } else {
    logoUrl = 'https://gogetdoc.com';
  }

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <>
      <Grid item sm={6} md={headerType !== 'chat' ? 6 : 4}>
        <Grid container alignItems="center" spacing={2}>
          {/* <Grid item>
            <IconButton onClick={() => setShowMobileNav(true)} className={classes.mobileMenuIcon}>
              <MenuIcon />
            </IconButton>
          </Grid> */}
          <Grid item>
            <Box style={{ marginTop: 7 }}>
              <Link href={logoUrl} passHref>
                <Box className={classes.logo}>
                  <Logo height="28px" color="#222525" />
                </Box>
              </Link>
            </Box>
          </Grid>
          {mediumScreen && headerType !== 'chat' && (
            <Grid item>
              <Typography variant="body2" color="textSecondary">
                {CAPTIONS.over[store.locale]}
              </Typography>
            </Grid>
          )}
        </Grid>
      </Grid>
      {/* {mediumScreen && headerType === 'chat' && (
        <Grid item sm={4}>
          <Box className={classes.searchBox}>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <Button size="small" className={classes.searchButton}>
                  <SearchIcon />
                </Button>
              </Grid>
              <Grid item xs>
                <Box pl={1}>
                  <InputBase placeholder="Search from Message" fullWidth />
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      )} */}
      <Grid item sm={6} md={4}>
        <Grid container justifyContent="flex-end" alignItems="center">
          <Grid item>
            <LanguageToggle />
          </Grid>
          <Grid item>
            {memberProfile ? (
              <AuthHeaderProfile memberData={memberProfile} handleLogout={handleLogout} />
            ) : (
              <Link href="/auth/login" passHref>
                <Button variant="contained" color="primary">
                  {CAPTIONS.login[store.locale]}
                </Button>
              </Link>
            )}
          </Grid>
        </Grid>
      </Grid>
      <Drawer
        open={showMobileNav}
        onClose={() => setShowMobileNav(false)}
        className={classes.drawer}
      >
        <Box className={classes.drawerInner}>
          <Box>
            <Box className={classes.logoContainer}>
              <Logo height="25px" color="#fff" />
            </Box>
            <List component="nav" className={classes.list}>
              {mainNavItems.map((item, index) =>
                item.subItems ? (
                  <Accordion
                    key={index}
                    expanded={expanded === `subItem${index}`}
                    onChange={handleChange(`subItem${index}`)}
                    className={classes.accordion}
                  >
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls={`subItem${index}-content`}
                      id={`subItem${index}--header`}
                    >
                      <Box className={classes.iconContainer}>{item.icon}</Box>
                      <Typography variant="h6">{item.label}</Typography>
                    </AccordionSummary>
                    {item.subItems.map((subItem, index) => (
                      <ListItem
                        button
                        key={index}
                        onClick={() => {
                          setShowMobileNav(false);
                        }}
                      >
                        <Link href={subItem.path} passHref>
                          <ListItemText className={classes.subItem}>
                            <Typography variant="h6">{subItem.label}</Typography>
                          </ListItemText>
                        </Link>
                        <Badge badgeContent={12} className={classes.badge} />
                      </ListItem>
                    ))}
                  </Accordion>
                ) : (
                  <ListItem
                    button
                    key={index}
                    onClick={() => {
                      setShowMobileNav(false);
                    }}
                  >
                    <Box className={classes.iconContainer}>{item.icon}</Box>
                    <Link href={item.path} passHref>
                      <ListItemText className={classes.item}>
                        <Typography variant="h6">{item.label}</Typography>
                      </ListItemText>
                    </Link>
                  </ListItem>
                ),
              )}
              <ListItem className={classes.item}>
                <StateSelector />
              </ListItem>
              <ListItem className={classes.item}>
                <LanguageToggle />
              </ListItem>
            </List>
          </Box>
          <List component="nav">
            {bottomItems.map((item, index) => (
              <ListItem
                button
                key={index}
                onClick={() => {
                  setShowMobileNav(false);
                }}
                className={classes.bottomItem}
              >
                <a target="_blank" rel="noreferrer" href={item.path} style={{ color: '#fff' }}>
                  <ListItemText>
                    <Typography variant="body2">{item.label}</Typography>
                  </ListItemText>
                </a>
              </ListItem>
            ))}
          </List>
        </Box>
      </Drawer>
    </>
  );
};

const DesktopNavigation = ({ memberProfile, handleLogout = () => {}, mainNavItems = [] }) => {
  const classes = useClasses();
  const { isFeatureEnabled, isToggleReady } = useToggles();

  const showMainPage = isToggleReady && isFeatureEnabled(FEATURE_TOGGLES.SHOW_MAIN_PAGE);
  let logoUrl;
  if (showMainPage) {
    logoUrl = '/';
  } else {
    logoUrl = 'https://gogetdoc.com';
  }

  return (
    <>
      <Grid item xs={3}>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <Box style={{ marginTop: 7 }}>
              <Link href={logoUrl} passHref>
                <Logo height="28px" color="#222525" />
              </Link>
            </Box>
          </Grid>
          <Grid item xs>
            <Typography variant="body2" color="textSecondary">
              Over 1 million users and growing...
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={6}>
        <Box textAlign="center">
          <Grid container spacing={4} justifyContent="center">
            {mainNavItems.map((item, index) => (
              <Grid item key={index}>
                <Typography variant="subtitle1">
                  <Link href={item.path} passHref>
                    <MuiLink className={classes.link} underline="none">
                      {item.label}
                    </MuiLink>
                  </Link>
                </Typography>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Grid>
      <Grid item xs={3}>
        <Grid container spacing={1} justifyContent="flex-end" alignItems="center">
          <Grid item>
            <StateSelector />
          </Grid>
          <Grid item>
            <LanguageToggle />
          </Grid>
          <Grid item>
            {memberProfile ? (
              <AuthHeaderProfile memberData={memberProfile} handleLogout={handleLogout} />
            ) : (
              <Link href="/auth/login" passHref>
                <Button variant="contained" color="primary">
                  Login
                </Button>
              </Link>
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { MobileNavigation, DesktopNavigation };
