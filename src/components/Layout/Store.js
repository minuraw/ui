import React from 'react';
import { DynamicProductCard } from '../Cards';
import { SectionContainer } from '../Common';
import { Grid } from '@material-ui/core';

const Store = ({ productGroup }) => {
  if (!productGroup) return null;

  return (
    <SectionContainer
      maxWidth="lg"
      color
      sectionTitle={productGroup.product_group_title}
      sectionSubtitle={productGroup.product_group_description}
      moreLink="/tests"
    >
      <Grid container spacing={4}>
        {productGroup.products.map((product, index) => (
          <Grid item xs={12} sm={6} md={3} key={index}>
            <DynamicProductCard product={product} />
          </Grid>
        ))}
      </Grid>
    </SectionContainer>
  );
};

export { Store };
