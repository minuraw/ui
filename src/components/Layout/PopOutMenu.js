import React, { useState, useRef, useContext } from 'react';
import { useRouter } from 'next/router';
import {
  Grid,
  Typography,
  MenuItem,
  MenuList,
  Paper,
  Box,
  Divider,
  Link,
  makeStyles,
  ClickAwayListener,
  IconButton,
  Popper,
  SvgIcon,
} from '@material-ui/core';
import { Store as GlobalStore } from '../../context';

const useClasses = makeStyles((theme) => ({
  paper: {
    borderRadius: '0 0 8px 8px',
    width: '240px',
  },
  popper: {
    zIndex: 9999,
  },
  divider: {
    backgroundColor: '#E2E2EA',
  },
  menuItem: {
    fontWeight: 500,
  },
  action: {
    fontWeight: 500,
    fontSize: '0.9375rem',
  },
  title: {
    fontWeight: 500,
    fontSize: '0.9375rem',
  },
  titleIcon: {
    fontSize: '1rem',
    marginTop: '3px',
  },
  menuDescription: {
    fontWeight: 400,
  },
}));

const PopOutMenu = ({
  menuItems = [],
  action,
  actionOnClick = () => {},
  title,
  titleIcon,
  children,
}) => {
  const classes = useClasses();
  const anchorRef = useRef(null);
  const router = useRouter();
  const { store } = useContext(GlobalStore);

  const [openMenu, setOpenMenu] = useState(false);

  const handleToggleMenu = () => {
    setOpenMenu((prevState) => !prevState);
  };

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpenMenu(false);
  };

  return (
    <>
      <IconButton onClick={handleToggleMenu} ref={anchorRef}>
        {children}
      </IconButton>
      <Popper
        open={openMenu}
        anchorEl={anchorRef.current}
        transition
        placement="bottom-end"
        className={classes.popper}
        modifiers={{
          offset: {
            enabled: true,
            offset: '0, 10',
          },
        }}
      >
        <Paper elevation={8} className={classes.paper}>
          <ClickAwayListener onClickAway={handleCloseMenu}>
            <MenuList>
              {(title || action) && (
                <Box px={2} mb={1}>
                  <Grid container justifyContent="space-between" alignItems="center">
                    {title && (
                      <Grid item>
                        <Grid container alignItems="center" spacing={1}>
                          <Grid item>
                            <Typography variant="body2" className={classes.title}>
                              {title}
                            </Typography>
                          </Grid>
                          {titleIcon && (
                            <Grid item>
                              <SvgIcon className={classes.titleIcon}>{titleIcon}</SvgIcon>
                            </Grid>
                          )}
                        </Grid>
                      </Grid>
                    )}
                    <Grid item>
                      <Link component="button" onClick={actionOnClick} underline="none">
                        <Typography variant="body2" className={classes.action}>
                          {action}
                        </Typography>
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
              )}
              {menuItems.map((item) => (
                <Box key={item.id}>
                  <Divider className={classes.divider} />
                  <MenuItem
                    dense
                    style={{ display: 'block' }}
                    onClick={() => router.push(item.path)}
                  >
                    <Typography color="textPrimary" className={classes.menuItem}>
                      {item.title[store.locale]}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      className={classes.menuDescription}
                      noWrap
                    >
                      {item.description[store.locale]}
                    </Typography>
                  </MenuItem>
                </Box>
              ))}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </>
  );
};

export { PopOutMenu };
