/* eslint-disable camelcase */
import React from 'react';
import { DynamicProductCard } from '../Cards';
import { SectionContainer } from '../Common';
import { Grid } from '@material-ui/core';


const filterMoreCareData = (productGroup) => {
  const title = productGroup.product_group_title;
  const titleDescription = productGroup.product_group_description || '';

  const products = [];

  productGroup.products.forEach((product) => {
    // Get visits
    const visits = product.product_metrics.find((metric) => metric.metric_id === 11)?.metric_value;
    const price = product.price_text;
    const name = product.name;
    const id = product.id;
    const display_type_id = product.display_type_id;
    const call_to_action_url = product.call_to_action_url;
    const call_to_action_open_in_new_window = product.call_to_action_open_in_new_window;

    // Push to products array
    products.push({
      visits,
      price,
      name,
      id,
      display_type_id,
      call_to_action_url,
      call_to_action_open_in_new_window,
    });
  });

  return { title, titleDescription, products };
};

const MoreCare = ({ productGroup }) => {
  const { title, titleDescription, products } = filterMoreCareData(productGroup);
  return (
    <SectionContainer maxWidth="lg" color sectionTitle={title} sectionSubtitle={titleDescription}>
      <Grid container spacing={4}>
        {products.map((product) => {
          return (
            <Grid item xs={12} sm={6} md={3} key={product.id}>
              <DynamicProductCard product={product} />
            </Grid>
          );
        })}
      </Grid>
    </SectionContainer>
  );
};

export { MoreCare };
