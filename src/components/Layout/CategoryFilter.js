import { useState, useContext } from 'react';
import { Select, MenuItem, FormControl } from '@material-ui/core';
import { Store } from '../../context';

const renderCategories = (categories) => {
  if (!Array.isArray(categories)) return '';
  return categories.map((category) => {
    return (
      <MenuItem key={category.id} value={category.id}>
        {category.description}
      </MenuItem>
    );
  });
};

const CategoryFilter = ({ categories, handleCategorySelect = () => {} }) => {
  const { store, setInStore } = useContext(Store);

  // We see if the categoryId is saved in the context, if so get is from there otherwise fallback to 0
  const categoryId = store.categoryId || 0;
  const [category, setCategory] = useState(categoryId);

  const handleChange = ({ target: { value } }) => {
    setCategory(value);
    // Stroe in the global store so that re-rendering does not reset the component state
    setInStore({ categoryId: value });
    handleCategorySelect(value);
  };

  return (
    <FormControl variant="outlined" size="small">
      <Select value={category} onChange={handleChange}>
        <MenuItem value={0}>All Categories</MenuItem>
        {renderCategories(categories)}
      </Select>
    </FormControl>
  );
};

export { CategoryFilter };
