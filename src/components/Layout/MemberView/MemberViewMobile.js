import { useState, useContext, useEffect } from 'react';
import { Store as GlobalStore } from '../../../context';
import { useRouter } from 'next/router';
import { v4 as uuidv4 } from 'uuid';
import { MemberProfileData, GGDLoader } from '../../Common';
import { Tabs, Tab, Divider, Box, Drawer, makeStyles, Typography } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { ChatListItem } from '../../Common/ChatListItem';
import { Conversation, Wallet, WalletCard, AppointmentsCard } from '../../Chat';
import { getChatDateTime, groupAppointmentsByStatus } from '../../../utils';

const CAPTIONS = {
  chatHistory: {
    'en-us': 'Start your first GoGetDoc chat!',
    'es-mx': '¡Comienza tu primer chat de GoGetDoc!',
  },
  appointmentsHistory: {
    'en-us': 'Book your first GoGetDoc visit!',
    'es-mx': '¡Reserva tu primera visita GoGetDoc!',
  },
  book: {
    'en-us': 'Book your first general visit',
    'es-mx': 'Reserve su primera visita general',
  },
};

const useClasses = makeStyles((theme) => ({
  container: {
    height: '90vh',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.ggd.gray,
    borderRight: '1px solid #F1F1F5',
  },
  containerHeader: {
    backgroundColor: theme.palette.ggd.gray,
  },
  profileContainer: {
    backgroundColor: theme.palette.ggd.gray,
  },
  tabContainer: {
    backgroundColor: theme.palette.ggd.gray,
  },
  tabs: {
    '&.MuiTabs-root': {
      minHeight: 0,
    },
    '& .MuiButtonBase-root.MuiTab-root': {
      fontSize: '1rem',
      fontFamily: 'Roboto',
      minWidth: 0,
      minHeight: '30px',
      padding: '2px 12px',
    },
    '& .MuiTabs-indicator': {
      display: 'none',
    },
    '& .MuiTab-textColorInherit.Mui-selected': {
      backgroundColor: '#171725',
      color: '#fff',
      borderRadius: '20px',
    },
    '& .MuiTabs-flexContainer': {
      marginLeft: theme.spacing(1),
    },
  },
  loaderContainer: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dot: {
    '&::before': {
      content: "'\\00B7'",
      padding: '0 0.1em',
      marginRight: '0.3rem',
      marginLeft: '0.3rem',
      fontWeight: 800,
    },
  },
}));

const MemberViewMobile = ({
  memberProfile: { memberSince, firstName, lastName, dob, phone },
  routerParams: {
    conversationId,
    sync,
    productId,
    skillId,
    skill,
    skillMode,
    messageId,
    provider,
    section,
    certificateId,
  },
  handleChangeConversation = () => {},
  tabData = [],
  conversations = [],
  updateConversation = () => {},
  handleTabChange = () => {},
  walletCertificates = [],
  loading = false,
  handleChangeWalletCertificate = () => {},
  appointments,
  selectedAppointment,
  handleChangeAppointment = () => {},
  updateChat = () => {},
  reactivateChat = () => {},
}) => {
  const classes = useClasses();

  const { store, setInStore } = useContext(GlobalStore);
  const router = useRouter();

  const [plannedAppointments, setPlannedAppointments] = useState([]);
  const [completedAppointments, setCompletedAppointments] = useState([]);

  useEffect(() => {
    router.pathname.includes('dashboard') && setInStore({ dashboard: true });
    return () => {
      router.pathname.includes('dashboard') && setInStore({ dashboard: false });
    };
  }, []);

  useEffect(() => {
    if (appointments) {
      const groupedAppointments = groupAppointmentsByStatus(appointments, {
        Cancelled: 'Finished',
      });

      setPlannedAppointments(groupedAppointments.Planned || []);
      setCompletedAppointments(groupedAppointments.Finished || []);
    }
  }, [appointments]);

  const [showTabContent, setShowTabContent] = useState(false);

  const handleChatItemClick = (conversation) => {
    setShowTabContent(true);
    handleChangeConversation(conversation);
  };

  const handleWalletItemClick = (certificate) => {
    setShowTabContent(true);
    handleChangeWalletCertificate(certificate);
  };

  const handleAppointmentItemClick = (appointment) => {
    setShowTabContent(true);
    handleChangeAppointment(appointment);
  };

  const handleCloseDrawer = () => {
    setShowTabContent(false);
  };

  return (
    <Box className={classes.container}>
      <Box className={classes.containerHeader}>
        <Box className={classes.profileContainer} p={2}>
          <MemberProfileData
            memberSince={memberSince}
            firstName={firstName}
            lastName={lastName}
            dob={dob}
            phoneNumber={phone && phone[0]}
          />
        </Box>
        <Divider />
        <Box p={1}>
          <Tabs
            value={section}
            onChange={(e, value) => handleTabChange(value)}
            variant="scrollable"
            scrollButtons="auto"
            className={classes.tabs}
          >
            {tabData.map((tab) => (
              <Tab
                label={tab.label[store.locale]}
                disabled={tab.disabled}
                value={tab.id}
                key={tab.id}
                style={{ marginRight: '20px' }}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
      </Box>
      <Box style={{ flex: 1, overflow: 'scroll' }}>
        {loading && (
          <Box p={3} className={classes.loaderContainer}>
            <GGDLoader />
          </Box>
        )}
        {!loading && section === 'chat' && (
          <Box p={2}>
            {conversations && conversations.length > 0 ? (
              conversations.map((conversation, index) => (
                <Box mb={2} key={`${conversation.conversationId}-${index}`}>
                  <ChatListItem
                    avatarUrl=""
                    consultant={
                      conversation.provider === 2 ? conversation.intent : conversation.description
                    }
                    visitType={
                      conversation.provider === 2 ? conversation.description : conversation.intent
                    }
                    timeStamp={getChatDateTime(conversation.date)}
                    attachments={false}
                    lastEncounterText={conversation.message}
                    onClick={() => handleChatItemClick(conversation)}
                    selected={conversation.conversationId === conversationId}
                  />
                </Box>
              ))
            ) : (
              <Box display="flex" justifyContent="center">
                <Typography variant="body2" color="textSecondary">
                  {CAPTIONS.chatHistory[store.locale]}
                </Typography>
              </Box>
            )}
          </Box>
        )}
        {!loading && section === 'wallet' && (
          <Box>
            <WalletCard
              isMobile
              certificateId={certificateId}
              seeOriginal={handleWalletItemClick}
              certificates={walletCertificates}
              updateChat={updateChat}
            />
          </Box>
        )}
        {!loading && section === 'appointments' && (
          <Box p={2}>
            {appointments && appointments.length ? (
              <>
                {Array.isArray(plannedAppointments) &&
                  plannedAppointments.map((appointment, idx) => (
                    <Box key={`${appointment.protocol}-${idx}`}>
                      <Box mb={2}>
                        <AppointmentsCard
                          protocol={appointment.protocol}
                          partner={appointment.partner}
                          videoLink={appointment.link}
                          conversationId={appointment.conversationId}
                          onSelectAppointment={() => handleAppointmentItemClick(appointment)}
                          isSelected={
                            selectedAppointment && selectedAppointment.id === appointment.id
                          }
                          startTime={appointment.scheduledStartTime}
                          status={appointment.encounterDescription}
                          receiptUrl={appointment.receiptUrl}
                          reactivateChat={reactivateChat}
                        />
                      </Box>
                      <Box mb={2}>
                        <Divider />
                      </Box>
                    </Box>
                  ))}
                {completedAppointments.length > 0 && (
                  <>
                    <Box mb={2} p={2} textAlign="center" style={{ backgroundColor: '#f2f2f2' }}>
                      <Typography variant="subtitle1" gutterBottom>
                        Past Appointments
                      </Typography>
                      <Typography variant="body2" color="textSecondary">
                        {completedAppointments.length} appointments{' '}
                        <span className={classes.dot}>
                          We <FavoriteIcon fontSize="inherit" /> {firstName} {lastName}
                        </span>
                      </Typography>
                    </Box>
                    {Array.isArray(completedAppointments) &&
                      completedAppointments.map((appointment, idx) => (
                        <Box key={`${appointment.protocol}-${idx}`}>
                          <Box mb={2}>
                            <AppointmentsCard
                              protocol={appointment.protocol}
                              partner={appointment.partner}
                              videoLink={appointment.link}
                              conversationId={appointment.conversationId}
                              onSelectAppointment={() => handleChangeAppointment(appointment)}
                              isSelected={
                                selectedAppointment && selectedAppointment.id === appointment.id
                              }
                              startTime={appointment.scheduledStartTime}
                              status={appointment.encounterDescription}
                              receiptUrl={appointment.receiptUrl}
                              reactivateChat={reactivateChat}
                            />
                          </Box>
                          <Box mb={2}>
                            <Divider />
                          </Box>
                        </Box>
                      ))}
                  </>
                )}
              </>
            ) : (
              <Box display="flex" justifyContent="center">
                <Typography variant="body2" color="textSecondary">
                  {CAPTIONS.appointmentsHistory[store.locale]}
                </Typography>
              </Box>
            )}
          </Box>
        )}
      </Box>
      <Drawer
        anchor="bottom"
        open={showTabContent}
        transitionDuration={300}
        PaperProps={{
          style: { position: 'relative' },
        }}
      >
        <Box>
          <Box>
            {section === 'chat' && conversationId && (
              <Conversation
                key={conversationId || uuidv4()}
                sync={sync}
                productId={productId}
                skillId={skillId}
                skill={skill}
                skillMode={skillMode}
                conversationId={conversationId}
                messageId={messageId}
                provider={provider}
                isAlreadyInitiatedChat={!!conversationId}
                handleCloseDrawer={handleCloseDrawer}
                updateConversation={updateConversation}
              />
            )}
            {section === 'wallet' && (
              <Wallet
                walletCertificates={walletCertificates}
                certificateId={certificateId}
                handleCloseDrawer={handleCloseDrawer}
                key={uuidv4()}
              />
            )}
            {section === 'appointments' && conversationId && (
              <Conversation
                key={conversationId || uuidv4()}
                sync={sync}
                productId={productId}
                skillId={skillId}
                skill={skill}
                skillMode={skillMode}
                conversationId={conversationId}
                messageId={messageId}
                provider={provider}
                isAlreadyInitiatedChat={!!conversationId}
                handleCloseDrawer={handleCloseDrawer}
                updateConversation={updateConversation}
              />
            )}
          </Box>
        </Box>
      </Drawer>
    </Box>
  );
};

export { MemberViewMobile };
