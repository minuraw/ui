import Link from 'next/link';
import { Button } from '@material-ui/core';

const MoreLink = ({ link }) => {
  return (
    <Link href={link} passHref>
      <Button data-test="see_more_button" variant="outlined" color="primary">
        See More
      </Button>
    </Link>
  );
};

export { MoreLink };
