import { MobileNavigation, DesktopNavigation } from '../Layout';
import { AppBar, Grid, makeStyles, useMediaQuery, Box } from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import PersonAddOutlinedIcon from '@material-ui/icons/PersonAddOutlined';
import FavoriteBorderOutlinedIcon from '@material-ui/icons/FavoriteBorderOutlined';
import TrackChangesOutlinedIcon from '@material-ui/icons/TrackChangesOutlined';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import { useAuth } from '../../context';

const MENU_ITEMS = [
  // {
  //   label: "Home",
  //   icon: <HomeOutlinedIcon />,
  //   path: "/",
  // },
  {
    label: 'Get Seen',
    icon: <PersonAddOutlinedIcon />,
    path: '/products',
    subItems: [
      {
        label: 'Chat',
        path: '/',
      },
      {
        label: 'Video',
        path: '/',
      },
    ],
  },
  {
    label: 'Get Tasks Done',
    icon: <CheckBoxOutlinedIcon />,
    path: '/',
  },
  {
    label: 'Get Tested',
    icon: <FavoriteBorderOutlinedIcon />,
    path: '/tests',
  },
  {
    label: 'Get Modern Care',
    icon: <TrackChangesOutlinedIcon />,
    path: '/providers',
  },
  // {
  //   label: "Account",
  //   icon: <PersonOutlineOutlinedIcon />,
  //   path: "/",
  // },
  // {
  //   label: "Log Out",
  //   icon: <ExitToAppOutlinedIcon />,
  //   path: "/",
  // },
];

const SUB_MENU_ITEMS = [
  {
    label: 'Login',
    primary: true,
    icon: <AccountCircleIcon />,
    badge: false,
    path: 'auth/login',
  },
];

const BOTTOM_ITEMS = [
  // {
  //   label: 'Collection Notice',
  //   path: '/',
  // },
  {
    label: 'Privacy Policy',
    path: 'https://gogetdoc.com/privacy',
  },
  {
    label: 'Terms of Service',
    path: 'https://gogetdoc.com/terms',
  },
  {
    label: 'Need help, click here',
    path: 'https://help.gogetdoc.com',
  },
];

const useClasses = makeStyles((theme) => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.grey[200]}`,
    backgroundColor: theme.palette.ggd.gray,
  },
  appBarChat: {
    position: 'sticky',
    top: 0,
    [theme.breakpoints.down('xs')]: {
      position: 'relative',
    },
  },
}));

const Header = ({ headerType = 'standard' }) => {
  const classes = useClasses();
  const largeScreen = useMediaQuery((theme) => theme.breakpoints.up('xl'));
  const { getMemberProfile, signOut } = useAuth();

  const memberProfile = getMemberProfile();

  const handleLogout = async () => {
    signOut();
  };

  return (
    <>
      {headerType === 'standard' && (
        <>
          <AppBar position="sticky" elevation={0} className={classes.appBar}>
            <Box p={2}>
              <Grid data-cy="login_button_container" container justifyContent="space-between" alignItems="center" spacing={1}>
                {largeScreen ? (
                  <DesktopNavigation
                    memberProfile={memberProfile}
                    handleLogout={handleLogout}
                    mainNavItems={MENU_ITEMS}
                    subNavItems={SUB_MENU_ITEMS}
                  />
                ) : (
                  <MobileNavigation
                    memberProfile={memberProfile}
                    handleLogout={handleLogout}
                    mainNavItems={MENU_ITEMS}
                    subNavItems={SUB_MENU_ITEMS}
                    bottomItems={BOTTOM_ITEMS}
                  />
                )}
              </Grid>
            </Box>
          </AppBar>
        </>
      )}
      {headerType === 'chat' && (
        <>
          <AppBar elevation={0} color="transparent" className={`${classes.appBar} ${classes.appBarChat}`}>
            <Box p={2}>
              <Grid container justifyContent="space-between" alignItems="center" spacing={1}>
                <MobileNavigation
                  memberProfile={memberProfile}
                  handleLogout={handleLogout}
                  mainNavItems={MENU_ITEMS}
                  subNavItems={SUB_MENU_ITEMS}
                  Items={SUB_MENU_ITEMS}
                  headerType={headerType}
                />
              </Grid>
            </Box>
          </AppBar>
        </>
      )}
    </>
  );
};

export { Header };
