import React, { useContext } from 'react';
import { Avatar } from '../Common';
import { PopOutMenu } from './PopOutMenu';
import { Grid, Typography, IconButton, makeStyles, useMediaQuery } from '@material-ui/core';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Store as GlobalStore } from '../../context';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

const useClasses = makeStyles((theme) => ({
  lightText: {
    color: theme.palette.grey[600],
  },
  memberName: {
    color: '#171725',
  },
}));

const CAPTIONS = {
  member: {
    'en-us': 'Member since',
    'es-mx': 'Miembro desde',
  },
  loggedIn: {
    'en-us': 'Logged in',
    'es-mx': 'Conectado',
  },
  logOut: {
    'en-us': 'Log out',
    'es-mx': 'Desconectarse',
  },
};

const MENU_ITEMS = [
  {
    id: 1,
    title: {
      'en-us': 'My account',
      'es-mx': 'Mi cuenta',
    },
    description: {
      'en-us': 'See all your account history',
      'es-mx': 'Ver todo el historial de su cuenta',
    },
    path: '/dashboard',
  },
];

const AuthHeaderProfile = ({ memberData, handleLogout = () => {} }) => {
  const classes = useClasses();
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.up('sm'));
  const { store } = useContext(GlobalStore);

  return (
    <>
      <Grid container spacing={1} alignItems="center">
        <Grid item>
          <IconButton disabled>
            <NotificationsNoneIcon />
          </IconButton>
        </Grid>
        <Grid item>
          <Grid container spacing={1} alignItems="center">
            <Grid item>
              {!smallScreen ? (
                <PopOutMenu
                  menuItems={MENU_ITEMS}
                  title={CAPTIONS.loggedIn[store.locale]}
                  action={CAPTIONS.logOut[store.locale]}
                  titleIcon={<CheckCircleOutlineIcon />}
                  actionOnClick={handleLogout}
                >
                  <Avatar firstName={memberData.firstName} lastName={memberData.lastName} small />
                </PopOutMenu>
              ) : (
                <Avatar firstName={memberData.firstName} lastName={memberData.lastName} small />
              )}
            </Grid>
            {smallScreen && (
              <>
                <Grid item>
                  <Typography variant="subtitle1" className={classes.memberName}>
                    {memberData.firstName} {memberData.lastName}
                  </Typography>
                  <Typography variant="body2" className={classes.lightText}>
                    {CAPTIONS.member[store.locale]} {memberData.memberSince}
                  </Typography>
                </Grid>
                <Grid item>
                  <PopOutMenu
                    menuItems={MENU_ITEMS}
                    title={CAPTIONS.loggedIn[store.locale]}
                    action={CAPTIONS.logOut[store.locale]}
                    titleIcon={<CheckCircleOutlineIcon />}
                    actionOnClick={handleLogout}
                  >
                    <ArrowDropDownIcon />
                  </PopOutMenu>
                </Grid>
              </>
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AuthHeaderProfile };
