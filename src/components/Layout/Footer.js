import Link from 'next/link';
import { Logo } from '../Logo';
import { InlineList } from '../Common';
import { DateTime } from 'luxon';
import {
  Divider,
  Grid,
  Typography,
  Box,
  List,
  ListItemText,
  Link as MuiLink,
  makeStyles,
} from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';

const useClasses = makeStyles((theme) => ({
  ul: {
    listStyle: 'none',
    '& li': {
      display: 'inline',
    },
  },
  lightText: {
    color: theme.palette.grey[600],
    fontWeight: 600,
  },
  mainFooterLink: {
    color: theme.palette.grey[600],
  },
}));

const GGD_LEGAL = 'GoGetDoc, LLC. All rights reserved';
const FOOTER_LINKS = [
  // {
  //   id: 1,
  //   label: 'Collection Notice',
  //   link: '/',
  // },
  {
    id: 2,
    label: 'Privacy Policy',
    link: 'https://gogetdoc.com/privacy',
  },
  {
    id: 3,
    label: 'Terms of Service',
    link: 'https://gogetdoc.com/terms',
  },
  // {
  //   id: 4,
  //   label: "We're Hiring",
  //   link: '/',
  // },
  {
    id: 5,
    label: 'Need help, click here',
    link: 'https://help.gogetdoc.com',
  },
];

const Footer = () => {
  const classes = useClasses();

  return (
    <>
      <Divider />
      <Box my={3}>
        <Logo height="25px" />
      </Box>
      <Box my={5}>
        <Grid container alignItems="flex-start" spacing={2}>
          <Grid item xs={6} sm={6} md={3}>
            <Typography variant="subtitle1">Go Get</Typography>
            <List disablePadding>
              <ListItemText>
                <Link href="/" passHref>
                  <MuiLink variant="body2" underline="none" className={classes.mainFooterLink}>
                    General Visit
                  </MuiLink>
                </Link>
              </ListItemText>
              <ListItemText>
                <Link href="/" passHref>
                  <MuiLink variant="body2" underline="none" className={classes.mainFooterLink}>
                    Vaccine Passport
                  </MuiLink>
                </Link>
              </ListItemText>
            </List>
          </Grid>
          <Grid item xs={6} sm={6} md={3}>
            <Typography variant="subtitle1">Partners</Typography>
            <List disablePadding>
              <ListItemText>
                <Link href="/" passHref>
                  <MuiLink variant="body2" underline="none" className={classes.mainFooterLink}>
                    Affiliate Program
                  </MuiLink>
                </Link>
              </ListItemText>
              <ListItemText>
                <Link href="/" passHref>
                  <MuiLink variant="body2" underline="none" className={classes.mainFooterLink}>
                    Use Wellpay
                  </MuiLink>
                </Link>
              </ListItemText>
            </List>
          </Grid>
          <Grid item xs={6} sm={6} md={3}>
            <Typography variant="subtitle1">About us</Typography>
          </Grid>
          <Grid item xs={6} sm={6} md={3}>
            <Typography variant="subtitle1">Support</Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box mt={3}>
        <Grid container alignItems="baseline" spacing={1}>
          <Grid item>
            <Typography variant="body2" className={classes.lightText}>
              &copy;{DateTime.now().toFormat('yyyy')} {GGD_LEGAL}. Made with{' '}
              <FavoriteIcon fontSize="inherit" /> in San Mateo, California.
            </Typography>
          </Grid>
          <Grid item>
            <InlineList items={FOOTER_LINKS} linkColor="primary" />
          </Grid>
        </Grid>
        <Box my={1}>
          <Typography variant="body2" className={classes.lightText}>
            GoGetDoc works to make its website accessible to all, including those with disabilities.
            If you are having difficulty accessing this website, please email us at{' '}
            <MuiLink color="inherit" href="mailto:support@gogetdoc.com">
              support@gogetdoc.com
            </MuiLink>{' '}
            so that we can provide you with the services you require through alternative means.
          </Typography>
        </Box>
        <Box>
          <Typography variant="body2" className={classes.lightText}>
            GoGetDoc offers access to laboratory testing for wellness monitoring, informational and
            educational use. With the exception of certain diagnostic test panels, list available
            here, the tests we offer access to are not intended to diagnose or treat disease. None
            of our tests are intended to be a substitute for seeking professional medical advice,
            help, diagnosis, or treatment. For regulatory reasons, our tests are not available in NY
            with the exception of COVID-19.
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export { Footer };
