import Image from 'next/image';
import { Grid } from '@material-ui/core';

const IMAGE_URL_BASE_PATH =
  'https://ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com/images';

const PARTNERS = [
  {
    name: 'Fortune',
    logo: '/marketingPartners/fortune@3x.png',
    height: 26,
    width: 107,
  },
  {
    name: 'Usa Today',
    logo: '/marketingPartners/usa_today.png',
    height: 36,
    width: 85.84,
  },
  {
    name: 'Forbes',
    logo: '/marketingPartners/forbes.png',
    height: 26.27,
    width: 101.22,
  },
  {
    name: 'CNBC',
    logo: '/marketingPartners/cnbc.png',
    height: 40,
    width: 53.81,
  },
  {
    name: 'The Wall Street Journal',
    logo: '/marketingPartners/the_wall_street_journal.png',
    height: 38.44,
    width: 64.7,
  },
];

const MarketingPartners = () => {
  return (
    <Grid container spacing={3} justifyContent="center" alignItems="center">
      {PARTNERS.map((partner) => (
        <Grid item key={partner.name}>
          <Image
            src={IMAGE_URL_BASE_PATH + partner.logo}
            height={partner.height}
            width={partner.width}
            alt={partner.name}
          />
        </Grid>
      ))}
    </Grid>
  );
};

export { MarketingPartners };
