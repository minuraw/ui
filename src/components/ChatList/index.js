import { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { Box, Card, CardContent, Typography } from '@material-ui/core';
import { getMemberConversations, eventListener, EVENTS } from '../../events';
import { CircularLoader } from '../Loader';

import { Conversation } from '../Chat/Conversation';
import { PROTOCOL_NAMES } from '../../constants/protocolNames';

const ChatList = ({
  start,
  sync,
  productId,
  skillId,
  skill,
  skillMode,
  conversationId,
  messageId,
  provider,
  redirectTo,
}) => {
  const [isDataLoading, setIsDataLoading] = useState(false);
  const [conversations, setConversations] = useState([]);

  const loadConversations = (error, conversations) => {
    if (!error) {
      setConversations(
        conversations.map((conversation) => {
          if (conversation.messageTypeId === 3) {
            conversation.message = 'Id image verification failed!';
          } else if (conversation.messageTypeId === 4) {
            conversation.message = 'Start visit.';
          } else if (conversation.messageTypeId === 5) {
            conversation.message = 'Your prescription received!';
          } else if (conversation.messageTypeId === 6) {
            conversation.message = 'Diagnosis information received!';
          } else if (conversation.messageTypeId === 7) {
            conversation.message = 'Here are your results.';
          } else if (conversation.messageTypeId === 10) {
            conversation.message = 'Your vax card registration was successful.';
          }
          return conversation;
        }),
      );
    }
    setIsDataLoading(false);
  };

  const handleConversationLoad = (conversation) => {
    const { conversationId, provider } = conversation;
    const routeParams = {
      conversationId,
      messageId: null,
      productId: null,
      provider,
      section: 'chat',
      skill: null,
      skillId: null,
      skillMode: null,
      start: false,
      sync: false,
    };
    redirectTo(routeParams, `/dashboard/chat/${conversationId}?provider=${provider}`);
  };

  useEffect(() => {
    setIsDataLoading(true);
    getMemberConversations(loadConversations);
  }, []);

  const handleChatNotificationReceived = (payload) => {
    if (!payload) return;

    const chatConversationId = payload.eventType;

    setConversations((chatListConversation) => {
      const matchingConversationIndex = chatListConversation.findIndex(
        (conversation) => conversation.conversationId === chatConversationId,
      );
      if (matchingConversationIndex !== -1 && payload.displayText) {
        const chatConversation = chatListConversation[matchingConversationIndex];
        // Remove the element from the array
        chatListConversation.splice(matchingConversationIndex, 1);
        // The date should be coming from the backend
        const newChatConversation = {
          ...chatConversation,
          message: payload.displayText,
          date: new Date(),
        };
        // Add it to the first element
        chatListConversation.splice(0, 0, newChatConversation);
      }
      return [...chatListConversation];
    });
  };

  useEffect(() => {
    eventListener(EVENTS.CHAT_NOTIFICATION_RECEIVED, handleChatNotificationReceived);
  }, []);

  if (isDataLoading) {
    return <CircularLoader displayText="Loading.." GGD/>;
  }

  return (
    <Box display="flex">
      <Box display="flex" flexDirection="column" minWidth={300} maxWidth={300}>
          {conversations.map((conversation) => {
            return (
              <Card
                style={{ cursor: 'pointer', margin: '3px' }}
                variant="outlined"
                key={conversation.conversationId}
                onClick={(e) => {
                  handleConversationLoad(conversation);
                }}
              >
                <CardContent>
                  <Typography variant="h5" component="h5">
                    {PROTOCOL_NAMES[conversation.description] || 'Clinical visit'}
                  </Typography>
                  <Typography color="textSecondary" style={{ fontSize: 14 }}>
                    {new Intl.DateTimeFormat('en-US', {
                      dateStyle: 'short',
                      timeStyle: 'short',
                    }).format(new Date(conversation.date))}
                  </Typography>
                  <Typography variant="body2" component="p">
                    {conversation.message}
                  </Typography>
                </CardContent>
              </Card>
            );
          })}
      </Box>
      <Box>
        <Conversation
          key={conversationId || uuidv4()}
          sync={sync}
          productId={productId}
          skillId={skillId}
          skill={skill}
          skillMode={skillMode}
          conversationId={conversationId}
          messageId={messageId}
          provider={provider}
          isAlreadyInitiatedChat={!!conversationId}
        />
      </Box>
    </Box>
  );
};

export default ChatList;
