import React, { useContext } from 'react';
import Link from 'next/link';
import { Container, Grid, Typography, Box, Link as MuiLink } from '@material-ui/core';
import { Logo } from '../Logo';
import { LanguageToggle } from '../Common';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  talk: {
    'en-us': 'Need help?',
    'es-mx': '¿Necesitas ayuda?',
  },
  linkText: {
    'en-us': `Visit the GoGetDoc support center`,
    'es-mx': 'Visite el centro de soporte de GoGetDoc',
  },
  helpLink: {
    'en-us': 'https://help.gogetdoc.com/en/',
    'es-mx': 'https://help.gogetdoc.com/es/',
  },
};

const LoginWrapper = ({ children }) => {
  const { store } = useContext(GlobalStore);
  return (
    <Container maxWidth="xl" style={{ backgroundColor: '#212121' }}>
      <Container maxWidth="xs">
        <Grid container direction="column" justifyContent="center" style={{ minHeight: '100vh' }}>
          <Grid item xs={12}>
            <Box textAlign="center" mb={4}>
              <Link href="https://www.gogetdoc.com" passHref>
                <a>
                  <Logo height="35px" color="#fff" />
                </a>
              </Link>
            </Box>
          </Grid>
          <Grid item xs={12}>
            {children}
          </Grid>
          <Box p={2}>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item xs>
                <Typography variant="body2" style={{ color: '#fff' }}>
                  {CAPTIONS.talk[store.locale]}{' '}
                  <MuiLink
                    href={CAPTIONS.helpLink[store.locale]}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {CAPTIONS.linkText[store.locale]}
                  </MuiLink>
                </Typography>
              </Grid>
              <Grid item>
                <LanguageToggle inverse />
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Container>
    </Container>
  );
};

export { LoginWrapper };
