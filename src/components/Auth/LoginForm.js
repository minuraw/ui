import React, { useContext, useState } from 'react';
import { Box, Button, CircularProgress, makeStyles } from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import { Store as GlobalStore } from '../../context';
import { INPUT_TYPES } from '../../components/Chat/constants';
import { PhoneNumberInput } from '../../components/Common/PhoneNumberInput';
import { DateInput } from '../../components/Common/DOBInput';
import { OtpInput } from '../../components/Chat/OtpInput';
import { getInputValidator } from '../../utils';

const CAPTIONS = {
  getStarted: {
    'en-us': 'Get started by entering your phone number',
    'es-mx': 'Comience ingresando su número de teléfono',
  },
  agree: {
    'en-us': 'By clicking "continue", you agree to GoGetDoc',
    'es-mx': 'Al hacer clic en "continuar", acepta los',
  },
  terms: {
    'en-us': 'Terms of Service',
    'es-mx': 'Términos de Servicios',
  },
  agree2: {
    'en-us': 'and acknowledge you have read the',
    'es-mx': 'de GoGetDoc Inc y reconoce que ha leído las',
  },
  policy: {
    'en-us': 'Privacy Policy.',
    'es-mx': 'Póliticas de Privacidad.',
  },
  agree3: {
    'en-us':
      'You also consent to receive calls or SMS messages, including by automated dialer, from GoGetDoc Inc and its affiliates to the number you provide for informational purposes including your appointments and results. A handling fee may apply for international users. You understand that you may opt out by texting',
    'es-mx':
      'También acepta recibir llamadas o mensajes SMS, incluso mediante un marcador automático, de GoGetDoc Inc y sus afiliados al número que proporcione con fines informativos, incluidas sus citas y resultados. Es posible que se aplique una tarifa de gestión para los usuarios internacionales. Usted comprende que puede optar por no participar enviando un mensaje de texto con',
  },
  stop: {
    'en-us': 'STOP',
    'es-mx': 'STOP',
  },
  to: {
    'en-us': 'to 877-280-2151',
    'es-mx': 'al 877-280-2151',
  },
  continue: {
    'en-us': 'Continue',
    'es-mx': 'Continuar',
  },
  cantLogin: {
    'en-us': `Can't login?`,
    'es-mx': '¿No puede iniciar sesión?',
  },
  signUp: {
    'en-us': 'Sign up for new user?',
    'es-mx': '¿Registrarse como usuario nuevo?',
  },
  security: {
    'en-us': 'For your security please confirm your date of birth.',
    'es-mx': 'Por su seguridad por favor confirme su fecha de nacimiento.',
  },
};

const useClasses = makeStyles((theme) => ({
  textField: {
    '& .MuiInputBase-root': {
      backgroundColor: theme.palette.grey[50],
    },
  },
  inputContainer: {
    border: '1px solid #E2E2E2',
    borderRadius: '1rem',
    padding: '0.5rem',
    marginTop: '1rem',
  },
}));

const LoginForm = ({ inputControl, inputType, handleResponse }) => {
  const { store } = useContext(GlobalStore);
  const [loading, setLoading] = useState(false);
  const classes = useClasses();

  const onSubmit = async (data) => {
    setLoading(true);
    const value = data[inputControl];
    // Check if there is a validator
    const validity = getInputValidator(inputType, null, store.locale);
    if (validity && value) {
      const validation = validity(value);
      if (!validation.valid) {
        setLoading(false);
        // If there is an error, return it so that errors object will be populated with it
        return {
          [inputControl]: validation.code,
        };
      } else {
        try {
          await handleResponse(value);
        } catch (e) {
          // Return submit error
          return { [inputControl]: e.message };
        } finally {
          setLoading(false);
        }
      }
    }
  };

  const renderUserInput = ({ input, error, inputType, handleResponse }) => {
    if (inputType === INPUT_TYPES.DATE_INPUT) return <DateInput input={input} error={error} />;
    if (inputType === INPUT_TYPES.PHONE_NUMBER)
      return <PhoneNumberInput input={input} error={error} />;
    if (inputType === INPUT_TYPES.OTP_INPUT)
      return (
        <Box m={2}>
          <OtpInput onSubmit={handleResponse} centered />
        </Box>
      );
    return null;
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit, submitErrors, dirtyFieldsSinceLastSubmit }) => {
        const identifierDirty = dirtyFieldsSinceLastSubmit[inputControl];
        const identifierSubmitErrors = submitErrors && submitErrors[inputControl];
        return (
          <form   
          data-cy="login_form"            
          onSubmit={handleSubmit}>
            <Field
              name={inputControl}
              render={({ input }) => (
                <Box data-cy="login_input"  className={inputType !== INPUT_TYPES.OTP_INPUT && classes.inputContainer}>
                  {renderUserInput({
                    input,
                    error: identifierDirty ? null : identifierSubmitErrors,
                    inputType,
                    handleResponse,
                  })}
                </Box>
              )}
            />
            {inputType !== INPUT_TYPES.OTP_INPUT && (
              <Box mt={2}>
                <Button data-cy="login_form_submit_button" type="submit" color="primary" fullWidth>
                  {loading ? (
                    <CircularProgress color="inherit" size={24} />
                  ) : (
                    CAPTIONS.continue[store.locale]
                  )}
                </Button>
              </Box>
            )}
          </form>
        );
      }}
    />
  );
};

export { LoginForm };
