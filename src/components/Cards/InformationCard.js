import { Paper, Box, Typography, Grid, Divider, makeStyles } from '@material-ui/core';

const useClasses = makeStyles((theme) => ({
  subtitle: {
    fontWeight: 500,
  },
}));

const InformationCard = ({ title, subtitle, topRight, content = [] }) => {
  const classes = useClasses();

  return (
    <Paper variant="outlined">
      <Box py={2}>
        <Box mb={content.length ? 3 : 0} px={2}>
          <Grid container justifyContent="space-between" alignItems="baseline" spacing={1}>
            <Grid item xs>
              <Typography variant="h5" gutterBottom>
                {title}
              </Typography>
              {subtitle && (
                <Typography color="textSecondary" variant="body2">
                  {subtitle}
                </Typography>
              )}
            </Grid>
            {topRight && (
              <Grid item>
                <Typography
                  color="textSecondary"
                  className={classes.subtitle}
                  style={{ marginTop: '3px' }}
                >
                  {topRight}
                </Typography>
              </Grid>
            )}
          </Grid>
        </Box>
        {content && Array.isArray(content) && content.length > 0 && (
          <Box mb={1}>
            {content.map((content, index) => (
              <Box key={index}>
                <Box my={2}>
                  <Divider variant="fullWidth" />
                </Box>
                <Box px={2}>
                  <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                    {content.left && (
                      <Grid item xs={3} sm={2}>
                        <Box textAlign="center">{content.left}</Box>
                      </Grid>
                    )}
                    {(content.title || content.secondaryText) && (
                      <Grid item xs={9} sm={10}>
                        {content.title && (
                          <Typography className={classes.subtitle}>{content.title}</Typography>
                        )}
                        {content.secondaryText && (
                          <Typography color="textSecondary" variant="body2">
                            {content.secondaryText}
                          </Typography>
                        )}
                      </Grid>
                    )}
                  </Grid>
                </Box>
              </Box>
            ))}
          </Box>
        )}
      </Box>
    </Paper>
  );
};

export { InformationCard };
