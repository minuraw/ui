import React from 'react';
import { Box, Divider, Card, CardContent, CardHeader } from '@material-ui/core';
import PropTypes from 'prop-types';

const StandardCard = ({ title, children, footer, disableFooterDivider = true }) => {
  return (
    <Card variant="outlined">
      {title && (
        <>
          <CardHeader title={title} align="center" />
          <Divider />
        </>
      )}
      <CardContent>{children}</CardContent>
      {footer && (
        <>
          {!disableFooterDivider && <Divider />}
          <Box p={2}>{footer}</Box>
        </>
      )}
    </Card>
  );
};

StandardCard.propTypes = {
  title: PropTypes.string,
  children: PropTypes.element.isRequired,
  footer: PropTypes.element,
};

export { StandardCard };
