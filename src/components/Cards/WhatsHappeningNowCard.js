import { useEffect } from 'react';
import { UpdateCard } from './UpdateCard';
import { SatisfactionSelector } from '../Common/SatisfactionSelector';
import {
  AsyncChatComplete,
  AsyncChatCompleteMoreClinicalReactions,
  AsyncChatCompleteNoRequisites,
  AsyncChatCompleteNoTreatment,
  AsyncOrderConfirmed,
  AsyncPostVisitAmendment,
  AsyncReviewCompleted,
  AsyncReviewStarted,
  PrescriptionInstructionsSent,
  SyncOrderConfirmed,
  SyncVisitComplete,
  SyncVisitStarted,
  SyncNotesComplete,
  SyncNotesAmended,
  SyncVisitCancelledClinicallyIrrelevant,
  SyncVisitCancelledByUser,
  SyncVisitEndedByProvider,
  SyncVisitEndedByMemberCanRejoin,
  SyncVisitEndedByMemberCantRejoin,
  SyncVisitCancelledDontMeetRequirements,
  SyncVisitRescheduled,
  SyncVisitPhotoRequest,
  SyncBeforeStartVisit,
} from './ConsultUpdateContent';
import { formatPhoneNumberUS, getDayDateMonthTime } from '../../utils';
import { SURVEY_SOURCES } from '../Chat/constants';

import { logEvent } from '../../events';

const CARD_TITLE = {
  'Appointment Cancellation': 'What happened',
  Finished: 'What happened',
  'Referred Out': 'What happened',
  'Patient No Show': 'What happened',
  'Insufficient Patient Identification': 'We need your help',
};

const getContactType = (arr, type) => arr.find((item) => item.type === type);

// Temp. solution until pharmacy display component is completed
const formatPharmacyAddress = (obj) => {
  let addressParts = [];

  const addressKeys = [
    { key: 'name', separator: ', ' },
    { key: 'addressLineOne', separator: ', ' },
    { key: 'addressLineTwo', separator: ', ' },
    { key: 'addressLineThree', separator: ', ' },
    { key: 'city', separator: ', ' },
    { key: 'state' },
    { key: 'postalCode' },
  ];

  addressKeys.forEach((addressKey) => {
    if (obj[addressKey.key])
      addressParts = [...addressParts, `${obj[addressKey.key]}${addressKey.separator || ' '}`];
  });

  return addressParts;
};

const getStatus = (status, disposition, dispositionReason) => {
  let derivedStatus;

  if (status === 'Finished') {
    if (disposition === 'Diagnosed') {
      derivedStatus = 'showPrescription';
    } else if (disposition === 'Referred Out') {
      derivedStatus = 'showReferOut';
    } else if (disposition === 'Patient No Show') {
      derivedStatus = 'showPatientNoShow';
    }
  } else if (status === 'Cancelled') {
    if (
      disposition === 'Appointment Cancellation' &&
      dispositionReason === 'Patient cancelled the appointment'
    ) {
      derivedStatus = 'showCancelledByUser';
    } else if (disposition === 'Appointment Cancellation' && dispositionReason === 'Other') {
      derivedStatus = 'showPatientNoShow';
    }
  } else if (disposition === 'Insufficient Patient Identification') {
    derivedStatus = 'showPhotoIDRequest';
  }

  return derivedStatus;
};

const renderContent = (
  status,
  disposition,
  progress,
  mode,
  additionalData,
  cancelAppointment,
  rescheduleAppointment,
  initiateNewChat,
) => {
  const {
    contact = [],
    pharmacy = {},
    scheduledStartTime = '',
    intent = 'appointment',
    dispositionReason = '',
  } = additionalData;

  const phone = getContactType(contact, 'phone') || '';
  const email = getContactType(contact, 'email') || '';

  const appointmentTime = scheduledStartTime ? getDayDateMonthTime(scheduledStartTime) : {};
  const formattedPhone = formatPhoneNumberUS(phone.value);
  const formattedPharmacyAddress = formatPharmacyAddress(pharmacy);
  const appointmentName = intent;
  const newStatus = disposition ? getStatus(status, disposition, dispositionReason) : status;

  if (mode === 'async') {
    switch (status) {
      case 'Planned':
        return (
          <AsyncOrderConfirmed
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
          />
        );
      case 'In Progress':
        return (
          <AsyncReviewStarted
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
          />
        );
      case 'Finished':
        return <AsyncChatComplete progress={progress} pharmacy={formattedPharmacyAddress} />;
      case 'complete_no_requisites':
        return <AsyncChatCompleteNoRequisites progress={progress} />;
      case 'complete_more_clinical_reactions':
        return <AsyncChatCompleteMoreClinicalReactions progress={progress} />;
      case 'complete_no_treatment':
        return <AsyncChatCompleteNoTreatment progress={progress} />;
      case 'post_visit_ammendment':
        return <AsyncPostVisitAmendment progress={progress} pharmacy={formattedPharmacyAddress} />;
      case 'review_completed':
        return (
          <AsyncReviewCompleted
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
          />
        );
      default:
        return null;
    }
  } else if (mode === 'sync') {
    switch (newStatus) {
      case 'Planned':
        return (
          <SyncOrderConfirmed
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
            appointmentTime={appointmentTime}
            appointmentName={appointmentName}
            scheduledStartTime={scheduledStartTime}
            rescheduleAppointment={rescheduleAppointment}
          />
        );
      case 'In Progress':
        return (
          <SyncVisitStarted
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
            appointmentName={appointmentName}
          />
        );
      case 'Finished':
        return (
          <SyncVisitComplete
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
          />
        );
      case 'showPrescription':
        return (
          <PrescriptionInstructionsSent
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
          />
        );
      case 'sync_notes_complete':
        return (
          <SyncNotesComplete
            progress={progress}
            phone={formattedPhone}
            email={email.value}
            pharmacy={formattedPharmacyAddress}
          />
        );
      case 'sync_notes_amended':
        return <SyncNotesAmended progress={progress} />;
      case 'showReferOut':
        return <SyncVisitCancelledClinicallyIrrelevant progress={progress} />;
      case 'showCancelledByUser':
        return <SyncVisitCancelledByUser progress={progress} initiateNewChat={initiateNewChat} />;
      case 'sync_before_start_visit':
        return (
          <SyncBeforeStartVisit
            progress={progress}
            pharmacy={formattedPharmacyAddress}
            rescheduleAppointment={rescheduleAppointment}
            cancelAppointment={cancelAppointment}
          />
        );
      case 'showPatientNoShow':
        return <SyncVisitEndedByProvider initiateNewChat={initiateNewChat} />;
      case 'sync_visit_ended_can_rejoin':
        return <SyncVisitEndedByMemberCanRejoin phone={formattedPhone} email={email.value} />;
      case 'sync_visit_ended_cant_rejoin':
        return <SyncVisitEndedByMemberCantRejoin phone={formattedPhone} email={email.value} />;
      case 'sync_visit_cancelled_dont_meet_requirements':
        return <SyncVisitCancelledDontMeetRequirements initiateNewChat={initiateNewChat} />;
      case 'sync_visit_rescheduled':
        return <SyncVisitRescheduled rescheduleAppointment={rescheduleAppointment} />;
      case 'showPhotoIDRequest':
        return (
          <SyncVisitPhotoRequest
            pharmacy={formattedPharmacyAddress}
            phone={formattedPhone}
            email={email.value}
            rescheduleAppointment={rescheduleAppointment}
          />
        );
      default:
        return null;
    }
  } else {
    return null;
  }
};

const getSurveySourceIdByStatus = (status) => {
  switch (status) {
    case 'Planned':
      return SURVEY_SOURCES.SYNC_VISIT_CONSULT_CREATED;
    case 'Finished':
      return SURVEY_SOURCES.SYNC_VISIT_CONSULT_COMPLETED;
    case 'Cancelled':
      return SURVEY_SOURCES.SYNC_VISIT_CONSULT_CANCELLED;
    default:
      return 0;
  }
};

const WhatsHappeningNowCard = ({
  status,
  disposition,
  progress,
  mode,
  additionalData = {},
  scrollDown = () => {},
  reactivateConversation = () => {},
  initiateNewChat = () => {},
}) => {
  const cancelAppointment = () => {
    reactivateConversation(false, 'CANCEL');
  };

  const rescheduleAppointment = () => {
    reactivateConversation(false, 'RESCHEDULE');
  };

  const cardContent = renderContent(
    status,
    disposition,
    progress,
    mode,
    additionalData,
    cancelAppointment,
    rescheduleAppointment,
    initiateNewChat,
  );
  const getCardTitle = (status) => CARD_TITLE[status] || "What's happening now";

  const surveySourceId = getSurveySourceIdByStatus(status);

  // this one logs if there are no content in what's happening card
  useEffect(() => {
    // if no card content push the changes to DataDog
    if (!cardContent) {
      logEvent(null, {
        level: 'error',
        message: 'NO_CONTENT_IN_WHATS_HAPPENING_CARD',
        context: {
          status,
          disposition,
          mode,
          additionalData,
        },
      });
    }
  }, [cardContent]);

  return (
    <>
      {(status || disposition) && mode && (
        <>
          {cardContent && (
            <UpdateCard
              title={getCardTitle(disposition || status)}
              cancelAppointment={cancelAppointment}
              rescheduleAppointment={rescheduleAppointment}
              initiateNewChat={initiateNewChat}
            >
              {cardContent}
            </UpdateCard>
          )}
          <SatisfactionSelector
            surveySourceId={surveySourceId}
            conversationId={additionalData.conversationId}
            associatedExternalId={additionalData.externalEncounterId}
            scrollDown={scrollDown}
          />
        </>
      )}
    </>
  );
};

export { WhatsHappeningNowCard };
