import React, {useState, useEffect} from 'react';
import { StandardCard } from './StandardCard';
import { Grid, Box, IconButton, SvgIcon, makeStyles } from '@material-ui/core';
import { DataGrid, MapboxMap } from '../Common';
import { Marker } from 'react-map-gl';
import Geocoding from '@mapbox/mapbox-sdk/services/geocoding';
import LocalPharmacyIcon from '@material-ui/icons/LocalPharmacy';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIcon from '@material-ui/icons/Phone';

const DATA_MAPPING = {
  name: {
    label: <LocalPharmacyIcon color="primary" fontSize="small" m={1} />,
    renderFn: (name) => name,
  },
  address: {
    label: <LocationOnIcon color="primary" fontSize="small" m={1} />,
    renderFn: (address) =>
      Object.keys(address)
        .map((key) => (key === 'line' ? address[key].join(', ') : address[key]))
        .join(', '),
  },
  telecom: {
    label: <PhoneIcon color="primary" fontSize="small" />,
    renderFn: (telecom) =>
      telecom
        .filter((result) => result.system === 'phone')
        .map((result) => result.value)
        .join(', '),
  },
};

const DEFAULT_MAP_CONFIG = {
  latitude: 32.779167,
  longitude: -96.808891,
  zoom: 8,
};

const useClasses = makeStyles((theme) => ({
  mapContainer: {
    position: 'relative',
    height: '150px',
  },
  iconButton: {
    position: 'relative',
  },
  locationPin: {
    fill: theme.palette.primary.main,
  },
}));


const PharmacyCard = ({ pharmacy, footer }) => {
  const classes = useClasses();
  const [mapConfig, setMapConfig] = useState(DEFAULT_MAP_CONFIG);

  const geocodingClient = Geocoding({
    accessToken: process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN,
  });

  useEffect(() => {
    geocodingClient
      .forwardGeocode({
        query: Object.values(pharmacy.address).join(','),
        limit: 1,
      })
      .send()
      .then((response) => {
        const { body } = response;
        const { features = [] } = body;
        const [location] = features;
        if (location && Array.isArray(location.center)) {
          const [lng, lat] = location.center;
          setMapConfig({
            latitude: lat,
            longitude: lng,
          });
        }
      });
  }, []);


  return (
    <Box mb={2}>
      <StandardCard footer={footer}>
        <Grid container direction="row" justifyContent="center" alignItems="flex-start">
          <Grid item xs={12} md={8}>
            <DataGrid data={pharmacy} dataMapping={DATA_MAPPING} />
          </Grid>
          <Grid item xs={12} md={4}>
            <MapboxMap {...mapConfig} scrollZoom={false}>
              <Marker
                key="marker-location"
                latitude={mapConfig.latitude}
                longitude={mapConfig.longitude}
                offsetTop={-24}
                offsetLeft={-28}
              >
                <IconButton className={classes.iconButton}>
                  <SvgIcon fontSize="large">
                    <svg
                      className={classes.locationPin}
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 25 35"
                    >
                      <path d="M12.5 0C5.59 0 0 5.627 0 12.584c0 7.496 7.893 17.832 11.143 21.769a1.765 1.765 0 002.732 0C17.107 30.416 25 20.08 25 12.583 25 5.628 19.41 0 12.5 0z" />
                      <ellipse cx="12.5" cy="12.583" rx="8.553" ry="8.61" fill="#fff" />
                    </svg>
                  </SvgIcon>
                </IconButton>
              </Marker>
            </MapboxMap>
          </Grid>
        </Grid>
      </StandardCard>
    </Box>
  );
};

export { PharmacyCard };
