import { ClinicalCard, ServiceCard, UtilityCard, StoreCard } from './';

const UTILITY_METRIC1 = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
const UTILITY_METRIC2 = [13, 14];

const getProvidersDisplayName = (providers) =>
  providers && Array.isArray(providers) && providers.length > 0
    ? providers.map((provider) => provider.name).join(', ')
    : '';

const DynamicProductCard = ({ product }) => {
  switch (product.display_type_id) {
    case 1:
      return (
        <ClinicalCard
          goGetDocPlus={product.member_only}
          visitType={product.name}
          provider={getProvidersDisplayName(product.providers)}
          price={product.price_text}
          deliveryType={product.service_delivery_channel}
          productDescription={product.description}
          providerThumbnails={product.providers.map((p) => p.image)}
          providerWaitTime={product.product_metrics.find((m) => m.metric_id === 1)}
          duration={product.product_metrics.find((m) => m.metric_id === 2)}
          minAge={product.min_age}
          maxAge={product.max_age}
          additionalText={product.additional_text}
          actionUrl={product.call_to_action_url}
          actionOpenInNewWindow={product.call_to_action_open_in_new_windows}
        />
      );
    case 2:
      return (
        <UtilityCard
          productId={product.id}
          title={product.name}
          description={product.description}
          metric1={product.product_metrics.find((m) => UTILITY_METRIC1.includes(m.metric_id))}
          metric2={product.product_metrics.find((m) => UTILITY_METRIC2.includes(m.metric_id))}
          cost={product.price_text}
          actionUrl={product.call_to_action_url}
          actionOpenInNewWindow={product.call_to_action_open_in_new_windows}
        />
      );
    case 3:
    case 4:
      return (
        <StoreCard
          title={product.name}
          type={product.description}
          price={product.price_text}
          provider={getProvidersDisplayName(product.providers)}
          rating={product.rating}
          thumbnail={product.product_image_url}
          additionalText={product.additional_text}
          actionUrl={product.call_to_action_url}
          actionOpenInNewWindow={product.call_to_action_open_in_new_windows}
        />
      );
    case 5:
      return (
        <ServiceCard
          visits={product.visits}
          name={product.name}
          price={product.price}
          actionUrl={product.call_to_action_url}
          actionOpenInNewWindow={product.call_to_action_open_in_new_windows}
        />
      );
  }

  return '';
};

export { DynamicProductCard };
