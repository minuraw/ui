import React, { useEffect, useState } from 'react'
import { Box, Grid, Typography, makeStyles, useMediaQuery } from '@material-ui/core';
import { DynamicProductCard } from './DynamicProductCard';
import { getProducts } from '../../services';
import { GGDLoader } from '../Common/GGDLoader';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { Logo } from '../../components/Logo';

const useStyles = makeStyles({
  greyColor: {
    backgroundColor: '#f7f7f7',
  },
  whiteColor: {
    backgroundColor: '#fff',
  },
  lightText: {
    color: '#a0a0a0',
  },
  dot: {
    fontSize: '5px',
    position: 'relative',
    transform: 'translateY(-50%)',
    marginRight: '7px',
    marginLeft: '7px',
  },
});

const SpecificProductCard = ({productGroupId, productId, powered}) => {
  const [productGroup, setProductGroup] = useState();
  const [product, setProduct] = useState();
  const classes = useStyles();
  const largeScreen = useMediaQuery((theme) => theme.breakpoints.up('sm'));

  useEffect(() => {
    getProductGroups();
  }, []);

  const getProductGroups = async () => {
    const productGroups = await getProducts({ product_group_id:  parseInt(productGroupId) });
    setProductGroup(productGroups[0]);
    const product = (productGroups[0].products = productGroups[0].products.filter(
      (p) => p.id === parseInt(productId),
    ));
    setProduct(product[0]);
  };

  if (!product)
    return (
      <Box display="flex" justifyContent="center">
        <GGDLoader />
      </Box>
    );

  return (
    <Box>
      <Box my={1}>
        <Grid container alignItems="center">
          <Grid item>
            <Typography variant="h5">{productGroup.product_group_title}</Typography>
          </Grid>
          {productGroup.product_group_title && largeScreen && (
            <Grid item className={classes.lightText}>
              <Typography variant="body2">
                <FiberManualRecordIcon className={classes.dot} />
                {productGroup.product_group_description}
              </Typography>
            </Grid>
          )}
        </Grid>
      </Box>
      <Grid container>
        <Grid
          item
          xs={12}
          md={5}
          style={{ boxShadow: '0 0 10px rgb(0 0 0 / 20%)', borderRadius: '1rem' }}
        >
          <DynamicProductCard product={product} />
          {powered &&
            <Box my={1} mr={1}>
              <Grid container alignItems="center" justifyContent='flex-end' spacing={1}>
                <Grid item>
                  <Typography variant="body2">Powered by</Typography>
                </Grid>
                <Grid item>
                  <Box display="flex" alignItems="center">
                    <Logo height="15px" />
                  </Box>
                </Grid>
              </Grid>
            </Box>
          }
        </Grid>
      </Grid>
    </Box>
  );
};

export { SpecificProductCard };
