import { Box, Grid, Typography, makeStyles } from '@material-ui/core';
import { CallMade } from '@material-ui/icons';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { Store as GlobalStore } from '../../context';

const useStyles = makeStyles((theme) => ({
  visitsIcon: {
    color: '#6fbf73',
  },
  price: {
    color: theme.palette.secondary.main,
  },
}));

const ServiceCard = ({ name, price, visits, actionUrl = '#', actionOpenInNewWindow = false }) => {
  const classes = useStyles();
  const router = useRouter();
  const { setInStore } = useContext(GlobalStore);

  const actionHandler = () => {
    setInStore(
      {
        conversationId: null,
        messageId: null,
      },
      true,
    );

    if (actionOpenInNewWindow) {
      window.open(actionUrl);
    } else {
      router.push(actionUrl);
    }
  };

  return (
    <Grid container spacing={2} onClick={actionHandler}>
      <Grid item xs={10}>
        <Typography variant="body2" gutterBottom>
          {name}
        </Typography>
        <Box>
          <Typography style={{ fontSize: '0.7rem', color: '#a0a0a0' }}>
            <CallMade fontSize="inherit" color="action" className={classes.visitsIcon} /> {visits}{' '}
            Visits
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={2}>
        <Typography variant="h6" className={classes.price}>
          {price}
        </Typography>
      </Grid>
    </Grid>
  );
};

export { ServiceCard };
