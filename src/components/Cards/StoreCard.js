import { Card, CardMedia, Typography, Box, Link, makeStyles, Grid } from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { Store as GlobalStore } from '../../context';

const useStyles = makeStyles((theme) => ({
  greyColor: {
    backgroundColor: '#f7f7f7',
  },
  whiteColor: {
    backgroundColor: '#fff',
  },
  lightText: {
    color: '#a0a0a0',
  },
  price: {
    color: theme.palette.blue.main,
  },
}));

const StoreCard = ({
  title,
  type,
  price,
  provider,
  rating,
  thumbnail,
  additionalText,
  actionUrl = '#',
  actionOpenInNewWindow = false,
}) => {
  const classes = useStyles();
  const router = useRouter();
  const { setInStore } = useContext(GlobalStore);

  const actionHandler = () => {
    setInStore(
      {
        conversationId: null,
        messageId: null,
      },
      true,
    );

    if (actionOpenInNewWindow) {
      window.open(actionUrl);
    } else {
      router.push(actionUrl);
    }
  };

  return (
    <Link underline="none" onClick={actionHandler}>
      <Card elevation={0} style={{ height: '100%' }}>
        <CardMedia component="img" image={thumbnail} style={{ maxHeight: '100px' }} />
        <Box p={2} mt={1} textAlign="left">
          <Typography variant="h6" gutterBottom>
            {title}
          </Typography>
          <Typography variant="subtitle1" className={classes.lightText}>
            {type}
          </Typography>
          <Box my={2}>
            <Grid container justifyContent="space-between">
              <Grid item>
                <Typography variant="h6" className={classes.price}>
                  {price}
                </Typography>
              </Grid>
              <Grid item>
                <Rating name="read-only" value={rating} readOnly size="small" />
              </Grid>
            </Grid>
          </Box>
          <Typography variant="body2">
            {additionalText && <span className={classes.lightText}>{additionalText}</span>}
            {!additionalText && provider && (
              <span className={classes.lightText}>Delivered by {provider}</span>
            )}
          </Typography>
        </Box>
      </Card>
    </Link>
  );
};

export { StoreCard };
