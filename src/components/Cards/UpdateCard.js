import { useState, useRef, useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import {
  Paper,
  Box,
  Typography,
  Grid,
  Divider,
  IconButton,
  makeStyles,
  Popper,
  ClickAwayListener,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';
import NotificationsOutlinedIcon from '@material-ui/icons/NotificationsOutlined';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import EventIcon from '@material-ui/icons/Event';
import PlayCircleFilledWhiteOutlinedIcon from '@material-ui/icons/PlayCircleFilledWhiteOutlined';

const useClasses = makeStyles({
  alertIcon: {
    transform: 'rotate(-20deg)',
    color: '#3DD598',
  },
  menuIcon: {
    color: '#fff',
    minWidth: 0,
    marginRight: '5px',
  },
  menuText: {
    color: '#fff',
  },
  menu: {
    backgroundColor: '#222525',
  },
  popper: {
    zIndex: 1300,
  },
});

const MenuOptions = ({ children, cancelAppointment, rescheduleAppointment, initiateNewChat }) => {
  const classes = useClasses();
  const anchorRef = useRef(null);
  // const locale = getLocale();
  const { store, setInStore } = useContext(GlobalStore);

  const [openMenu, setOpenMenu] = useState(false);

  const handleToggleMenu = () => {
    setOpenMenu((prevState) => !prevState);
  };

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpenMenu(false);
  };
  
  const startVisit = (e) => {
    handleCloseMenu(e)
    setInStore({ showVideo: true });
  }

  return (
    <>
      <IconButton onClick={handleToggleMenu} ref={anchorRef} aria-label="Chat options">
        {children}
      </IconButton>
      <Popper
        open={openMenu}
        anchorEl={anchorRef.current}
        transition
        placement="bottom-end"
        className={classes.popper}
        modifiers={{
          offset: {
            enabled: true,
            offset: '0, 2',
          },
        }}
      >
        <Paper elevation={3} className={classes.menu}>
          <ClickAwayListener onClickAway={handleCloseMenu}>
            <MenuList dense aria-haspopup="true">
              {store.startVisit ? (
                <MenuItem onClick={(e) => startVisit(e)} className={classes.menuText}>
                  <ListItemIcon className={classes.menuIcon}>
                    <PlayCircleFilledWhiteOutlinedIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText className={classes.menuItem}>Start visit</ListItemText>
                </MenuItem>
              ) : store.showRescheduleCancel ? (
                <Box>
                  <MenuItem onClick={rescheduleAppointment} className={classes.menuText}>
                    <ListItemIcon className={classes.menuIcon}>
                      <EventIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText className={classes.menuItem}>Reschedule visit</ListItemText>
                  </MenuItem>
                  <MenuItem onClick={cancelAppointment} className={classes.menuText}>
                    <ListItemIcon className={classes.menuIcon}>
                      <CancelOutlinedIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText className={classes.menuItem}>Cancel visit</ListItemText>
                  </MenuItem>
                </Box>
              ) : (
                <MenuItem onClick={initiateNewChat} className={classes.menuText}>
                  <ListItemIcon className={classes.menuIcon}>
                    <EventIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText className={classes.menuItem}>Schedule new visit</ListItemText>
                </MenuItem>
              )}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </>
  );
};

const UpdateCard = ({
  title,
  subtitle,
  children,
  showAlert = true,
  showMenu = true,
  cancelAppointment,
  rescheduleAppointment,
  initiateNewChat,
}) => {
  const classes = useClasses();

  return (
    <Paper variant="outlined">
      <Box py={2}>
        <Box mb={2} px={2}>
          <Grid container justifyContent="space-between" alignItems="center" spacing={1}>
            <Grid item xs>
              <Typography variant="h5">{title}</Typography>
              {subtitle && (
                <Typography variant="body2" style={{ marginTop: '0.35em' }}>
                  {subtitle}
                </Typography>
              )}
            </Grid>
            {(showAlert || showMenu) && (
              <Grid item>
                <Grid container spacing={1} alignItems="center">
                  {showAlert && (
                    <Grid item>
                      <IconButton disabled>
                        <NotificationsOutlinedIcon className={classes.alertIcon} />
                      </IconButton>
                    </Grid>
                  )}
                  {showMenu && (
                    <Grid item>
                      <MenuOptions
                        cancelAppointment={cancelAppointment}
                        rescheduleAppointment={rescheduleAppointment}
                        initiateNewChat={initiateNewChat}
                      >
                        <MoreHorizIcon />
                      </MenuOptions>
                    </Grid>
                  )}
                </Grid>
              </Grid>
            )}
          </Grid>
          <Box my={2}>
            <Divider />
          </Box>
          {children}
        </Box>
      </Box>
    </Paper>
  );
};

export { UpdateCard };
