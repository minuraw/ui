import { Paper, Typography, Button, Chip, Grid, Box, Avatar, makeStyles } from '@material-ui/core';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { Store as GlobalStore } from '../../context';

const useClasses = makeStyles((theme) => ({
  lightText: {
    color: '#a0a0a0',
    fontWeight: '400',
  },
  dot: {
    fontSize: '5px',
    position: 'relative',
    transform: 'translateY(-50%)',
    marginRight: '3px',
    marginLeft: '3px',
  },
  metricDescription: {
    textTransform: 'uppercase',
    color: '#a0a0a0',
  },
  metricValue: {
    color: theme.palette.blue.dark,
  },
  productDescription: {
    color: theme.palette.blue.dark,
  },
  price: {
    color: theme.palette.blue.dark,
  },
  deliveryType: {
    color: theme.palette.blue.dark,
  },
}));

// TODO - make this db driven
const getButtonCaption = (url) => {
  if (url.includes('async')) {
    return 'Start Chat';
  }
  return 'Schedule';
};

const ClinicalCard = ({
  goGetDocPlus,
  visitType,
  price,
  deliveryType,
  provider,
  productDescription,
  providerThumbnails,
  providerWaitTime,
  duration,
  minAge,
  maxAge,
  additionalText,
  actionUrl = '#',
  actionOpenInNewWindow = false,
}) => {
  const classes = useClasses();
  const router = useRouter();
  const { setInStore } = useContext(GlobalStore);

  const ageRange = (minAge, maxAge) => {
    if (maxAge === null && minAge === null) return 'All ages';

    // eslint-disable-next-line no-sequences
    switch ((minAge, maxAge)) {
      case !minAge && maxAge:
        return `Up to ${maxAge}`;
      case minAge && !maxAge:
        return `From ${minAge}`;
      default:
        return `${minAge} - ${maxAge}`;
    }
  };

  const actionHandler = () => {
    setInStore(
      {
        conversationId: null,
        messageId: null,
      },
      true,
    );

    if (actionOpenInNewWindow) {
      window.open(actionUrl);
    } else {
      router.push(actionUrl);
    }
  };

  return (
    <Paper style={{ height: '100%' }}>
      <Box p={3} style={{ height: '100%' }}>
        <Grid
          data-test="clinical_card_continer"
          container
          direction="column"
          justifyContent="space-between"
          style={{ height: '100%' }}
        >
          <Grid item>
            {goGetDocPlus === '1' && (
              <Box mb={2}>
                <Chip label="GoGetDoc+" color="primary" />
              </Box>
            )}
            <Grid
              container
              direction="row"
              justifyContent="space-between"
              alignItems="flex-start"
              spacing={1}
            >
              <Grid item xs>
                <Typography variant="h5" gutterBottom>
                  {visitType}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="h3" className={classes.price}>
                  {price}
                </Typography>
              </Grid>
            </Grid>
            <Box mt={1}>
              <Typography variant="subtitle1" className={classes.deliveryType}>
                {deliveryType}{' '}
                {provider && (
                  <span className={classes.lightText}>
                    <FiberManualRecordIcon className={classes.dot} /> Delivered by {provider}
                  </span>
                )}
              </Typography>
            </Box>
            <Box my={3}>
              <Typography variant="body2" className={classes.productDescription}>
                {productDescription}
              </Typography>
            </Box>
            <Box my={3}>
              <Grid container spacing={1} alignItems="center">
                <Grid item>
                  <AvatarGroup max={4}>
                    {providerThumbnails.map((url, index) => (
                      <Avatar src={url} key={index} />
                    ))}
                  </AvatarGroup>
                </Grid>
                <Grid item xs>
                  <Typography variant="body2" color="textSecondary">
                    {additionalText}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item>
            <Box mb={3}>
              <Grid container justifyContent="space-between">
                <Grid item>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    className={classes.metricDescription}
                  >
                    {providerWaitTime?.metric_description}
                  </Typography>
                  <Typography variant="subtitle1" className={classes.metricValue}>
                    {providerWaitTime?.metric_value}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    className={classes.metricDescription}
                  >
                    Age
                  </Typography>
                  <Typography variant="subtitle1" className={classes.metricValue}>
                    {ageRange(minAge, maxAge)}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    className={classes.metricDescription}
                  >
                    {duration?.metric_description}
                  </Typography>
                  <Typography variant="subtitle1" className={classes.metricValue}>
                    {duration?.metric_value}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item>
            {/* <Grid container spacing={2}> */}
            {/* <Grid item xs={12} sm={12} md={6}> */}
            {/* TODO - Decide the functionality of this button. */}
            {/* <Button fullWidth variant="outlined" onClick={actionHandler}>
                  Schedule
                </Button>
              </Grid> */}
            {/* <Grid item>
                
              </Grid> */}
            {/* </Grid> */}
            <Button fullWidth color="primary" onClick={actionHandler}>
              {getButtonCaption(actionUrl)}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export { ClinicalCard };
