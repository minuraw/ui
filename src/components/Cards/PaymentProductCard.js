import { Box, Typography, Divider, Grid } from '@material-ui/core';

const PaymentProductCard = ({
  name,
  priceText,
  serviceDeliveryChannel,
  providerText,
  price,
  convenienceFee,
  minAmount,
}) => {
  const showConvenienceFee = convenienceFee !== 0;

  return (
    <Box p={1}>
      <Typography variant="body2" gutterBottom>
        <strong>{name}</strong>
      </Typography>
      <Typography variant="body2">Delivered by {providerText}</Typography>
      <Box mt={2}>
        {showConvenienceFee ? (
          <>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <Typography variant="body2">{providerText}:</Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2">${price - convenienceFee}</Typography>
              </Grid>
            </Grid>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <Typography variant="body2">Convenience fee:</Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2">${convenienceFee}</Typography>
              </Grid>
            </Grid>
            <Box my={1}>
              <Divider style={{ backgroundColor: '#999' }} />
            </Box>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <Typography variant="body2">Total:</Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2">
                  <strong>{priceText}</strong>
                </Typography>
              </Grid>
            </Grid>
          </>
        ) : (
          <Typography variant="body2">
            {minAmount ? `Only $${minAmount} for our members` : `${priceText} for our members`}
          </Typography>
        )}
      </Box>
    </Box>
  );
};

export { PaymentProductCard };
