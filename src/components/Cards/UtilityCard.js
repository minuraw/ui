import { Paper, Grid, Button, Typography, Box, makeStyles } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import { FreeForPlusMembers } from '../Common';
import StarIcon from '@material-ui/icons/Star';

const useStyles = makeStyles((theme) => ({
  greyColor: {
    backgroundColor: '#f7f7f7',
  },
  whiteColor: {
    backgroundColor: '#fff',
  },
  subTitle: {
    color: '#a17700',
    textTransform: 'uppercase',
  },
  metricValue: {
    color: theme.palette.blue.dark,
  },
  featured: {
    color: '#3dd598',
  },
}));

const UtilityCard = ({
  productId,
  title,
  description,
  metric1,
  metric2,
  cost,
  actionUrl = '#',
  actionOpenInNewWindow = false,
}) => {
  const classes = useStyles();
  const router = useRouter();
  const { setInStore } = useContext(GlobalStore);

  const actionHandler = () => {
    setInStore(
      {
        conversationId: null,
        messageId: null,
      },
      true,
    );

    if (actionOpenInNewWindow) {
      window.open(actionUrl);
    } else {
      router.push(actionUrl);
    }
  };

  return (
    <Paper style={{ height: '100%' }}>
      <Box py={3} px={2} style={{ height: '100%' }}>
        <Grid
          data-id="dynamic_utility_card_container"
          container
          direction="column"
          justifyContent="space-between"
          style={{ height: '100%' }}
        >
          <Grid item>
            <Grid container spacing={1} justifyContent="space-between" alignItems="center">
              <Grid item>
                <Typography variant="h5">{title}</Typography>
              </Grid>
              {productId === 9 && (
                <Grid item>
                  <Grid container>
                    <StarIcon fontSize="small" style={{ color: '#3dd598' }} />
                    <Typography variant="h6" className={classes.featured}>
                      New
                    </Typography>
                  </Grid>
                </Grid>
              )}
            </Grid>
            <Box my={2}>
              <Typography variant="body2">{description}</Typography>
            </Box>
          </Grid>
          <Grid item>
            <Box mb={2}>
              <Grid container justifyContent="space-between">
                <Grid item>
                  <Typography variant="subtitle2" gutterBottom className={classes.subTitle}>
                    {metric1?.metric_description}
                  </Typography>
                  <Typography variant="subtitle1" className={classes.metricValue}>
                    {metric1?.metric_value}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
            <Box mb={1}>
              <Grid container justifyContent="space-between" spacing={3}>
                {metric2 && (
                  <Grid item xs={4}>
                    <Typography variant="subtitle2" gutterBottom className={classes.subTitle}>
                      {metric2?.metric_description}
                    </Typography>
                    <Typography variant="subtitle1" className={classes.metricValue}>
                      {metric2?.metric_value}
                    </Typography>
                  </Grid>
                )}
                <Grid item xs={8}>
                  <Typography variant="subtitle2" gutterBottom className={classes.subTitle}>
                    Cost
                  </Typography>
                  <Typography variant="subtitle1" className={classes.metricValue}>
                    {cost === 'Free For + Members' ? <FreeForPlusMembers /> : cost}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item>
            <Button color="primary" fullWidth onClick={actionHandler}>
              Get Yours
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export { UtilityCard };
