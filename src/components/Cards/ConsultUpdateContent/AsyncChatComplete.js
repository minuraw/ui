import { PrimaryContent } from './PrimaryContent';
import { TimeToActionInformation } from './TimeToActionInformation';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const AsyncChatComplete = ({ progress, pharmacy }) => {
  return (
    <>
      <PrimaryContent
        title="You did great! Our certified provider has completed the diagnosis from your visit."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                The provider is working on next steps including prescriptions, referrals, education,
                and test orders. (possibly show steps from backend that are specific to the user)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation text="Time to final visit notes is" time="15 minutes" />
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncChatComplete };
