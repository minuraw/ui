import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { NotificationInformation } from './NotificationInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { Grid } from '@material-ui/core';

const SyncVisitEndedByMemberCantRejoin = ({ phone, email }) => {
  return (
    <>
      <PrimaryContent title="Your visit has ended. Your certified provider is finalizing the visit notes." />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <TimeToActionInformation text="Time to final visit notes is" time="15 minutes" />
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send will include visit notes from your provider."
          />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitEndedByMemberCantRejoin };
