import { useEffect, useState } from 'react';
import { SyncBeforeStartVisit } from './SyncBeforeStartVisit';
import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { Grid, Typography, Link } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import EventIcon from '@material-ui/icons/Event';

const TIME_BEFORE_APPOINTMENT = 15; // in minutes

const minutesBeforeAppointmentStarts = (scheduledStartTimeInMS) =>
  (scheduledStartTimeInMS - new Date().getTime()) / 60000;

const isAppointmentAboutToStart = (scheduledStartTimeInMS) =>
  minutesBeforeAppointmentStarts(scheduledStartTimeInMS) < TIME_BEFORE_APPOINTMENT;

const TitleText = ({ appointmentName, appointmentTime }) => {
  const { day = '', ordinalDate = '', month = '', time = '' } = appointmentTime;

  const appointmentTimeExists = day && ordinalDate && month && time;

  return (
    <span>
      You're good to go! Your {appointmentName} is scheduled{' '}
      {appointmentTimeExists && `for ${day}, ${month} ${ordinalDate} at ${time}.`}
    </span>
  );
};

const SyncOrderConfirmed = ({
  progress,
  phone,
  email,
  pharmacy,
  appointmentTime,
  appointmentName,
  scheduledStartTime,
  rescheduleAppointment,
}) => {
  const [showBeforeStartVisitContent, setShowBeforeStartVisitContent] = useState(
    isAppointmentAboutToStart(scheduledStartTime),
  );
  const [minutesToAppointment, setMinutesToAppointment] = useState(
    minutesBeforeAppointmentStarts(scheduledStartTime),
  );

  const scheduledStartTimeInMS = new Date(scheduledStartTime).getTime();

  useEffect(() => {
    const timer = setInterval(() => {
      if (isAppointmentAboutToStart(scheduledStartTimeInMS)) {
        setShowBeforeStartVisitContent(true);
        setMinutesToAppointment(minutesBeforeAppointmentStarts(scheduledStartTimeInMS));
      }
    }, 1000);

    return function cleanup() {
      clearInterval(timer);
    };
  });

  return showBeforeStartVisitContent ? (
    <SyncBeforeStartVisit
      appointmentName={appointmentName}
      pharmacy={pharmacy}
      minutesToAppointment={minutesToAppointment}
    />
  ) : (
    <>
      <PrimaryContent
        title={<TitleText appointmentName={appointmentName} appointmentTime={appointmentTime} />}
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                You'll get an email that includes an invite.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation
            text="You can join"
            time="10 minutes"
            extraText="before the appointment time"
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EventIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Reschedule your appointment by clicking{' '}
                <Link onClick={rescheduleAppointment}>here</Link>. Make sure to reschedule at least
                24 hours before your appointment.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="Updates may include if there are any changes to your appointment."
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Prescriptions will be sent to: {pharmacy}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { SyncOrderConfirmed };
