import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const AsyncOrderConfirmed = ({ progress, phone, pharmacy, email }) => {
  return (
    <>
      <PrimaryContent
        title="We're currently working to connect you with a highly trained and certified provider. The first thing they'll do is review your answers to our questions."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <TimeToActionInformation text="A provider will review in as few as" time="30 minutes" />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                The average review can take <strong>30 minutes</strong>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send is when your provider completes your review. Further updates will follow!"
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncOrderConfirmed };
