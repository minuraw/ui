import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const AsyncReviewCompleted = ({ progress, phone, email, pharmacy }) => {
  return (
    <>
      <PrimaryContent
        title="The provider is ready to start the chat visit. Be on the lookout for updates from the provider."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <TimeToActionInformation text="The average provider response time is" time="2 hours" />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                Chat visits are usually complete once there's a diagnosis. Then the provider will
                get you what you need. This can include prescriptions, referrals, education, and
                test orders.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send is when your provider is ready to chat."
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncReviewCompleted };
