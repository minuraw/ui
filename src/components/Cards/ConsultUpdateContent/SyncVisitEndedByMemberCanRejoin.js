import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { NotificationInformation } from './NotificationInformation';
import { Grid } from '@material-ui/core';

const SyncVisitEndedByMemberCanRejoin = ({ phone, email }) => {
  return (
    <>
      <PrimaryContent title="Your visit is still in session. Please rejoin. Then we'll complete your visit." />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <TimeToActionInformation
            text="Your provider will wait"
            time="x minutes"
            extraText="for you to rejoin"
          />
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send will include your provider's notes from your visit."
          />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitEndedByMemberCanRejoin };
