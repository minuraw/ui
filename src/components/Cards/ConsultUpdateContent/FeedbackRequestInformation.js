import { Grid, Typography } from '@material-ui/core';
import RateReviewOutlinedIcon from '@material-ui/icons/RateReviewOutlined';

const FeedbackRequestInformation = () => {
  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <RateReviewOutlinedIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          You deserve the best in healthcare. Please provide feedback on your
          experience.
        </Typography>
      </Grid>
    </Grid>
  );
};

export { FeedbackRequestInformation };
