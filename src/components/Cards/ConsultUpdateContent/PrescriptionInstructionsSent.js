import { PrimaryContent } from './PrimaryContent';
import { TimeToActionInformation } from './TimeToActionInformation';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { NotificationInformation } from './NotificationInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { Grid, Typography } from '@material-ui/core';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const PrescriptionInstructionsSent = ({ email, phone, progress, pharmacy }) => {
  return (
    <>
      <PrimaryContent
        title="You did great! Your visit is complete. Your certified provider is finalizing visit notes."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid item>
            <FeedbackRequestInformation />
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation
            text="Time to final visit notes is"
            time="15 minutes"
            extraText="after your visit"
          />
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send will include the provider's visit notes."
          />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
      </Grid>
    </>
  );
};

export { PrescriptionInstructionsSent };
