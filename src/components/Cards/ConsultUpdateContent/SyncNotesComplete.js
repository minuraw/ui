import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { NotificationInformation } from './NotificationInformation';
import { Grid, Typography } from '@material-ui/core';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import { TimeToActionInformation } from './TimeToActionInformation';

const SyncNotesComplete = ({ progress, phone, email, pharmacy }) => {
  return (
    <>
      <PrimaryContent
        title="You did great! Your visit is complete. Your certified provider is finalizing visit notes."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Prescriptions will be sent to: {pharmacy}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation
            text="Time to final visit notes is"
            time="15 minutes"
            extraText="after your visit"
          />
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="If there are important updates, we'll send you a notification."
          />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncNotesComplete };
