import { Grid, Typography } from '@material-ui/core';
import AccessTimeOutlinedIcon from '@material-ui/icons/AccessTimeOutlined';

const TimeToActionInformation = ({ text = '', time = '', extraText = '' }) => {
  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <AccessTimeOutlinedIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          {text} <strong>{time}</strong> {extraText}
        </Typography>
      </Grid>
    </Grid>
  );
};

export { TimeToActionInformation };
