import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { Grid, Typography } from '@material-ui/core';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import EnhancedEncryptionOutlinedIcon from '@material-ui/icons/EnhancedEncryptionOutlined';

const AsyncChatCompleteMoreClinicalReactions = ({ progress }) => {
  return (
    <>
      <PrimaryContent
        title="Based on the information available, the provider recommends a different care plan. Let's explore our options."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocalHospitalOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                In this case, we typically recommend starting with a SymptomBot check (link) or a
                General Visit (link) to get the care you need.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EnhancedEncryptionOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                You can view the history of all your GoGetDoc care visits in your secure visit
                Vault. (link)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncChatCompleteMoreClinicalReactions };
