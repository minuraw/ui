import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { RestartInformation } from './RestartInformation';
import { Grid, Link } from '@material-ui/core';

const TitleText = ({ initiateNewChat }) => {
  return (
    <span>
      Your visit has been canceled. Click <Link onClick={initiateNewChat}>here</Link> to get started
      if you want to schedule a new visit.
    </span>
  );
};

const SyncVisitCancelledByUser = ({ initiateNewChat }) => {
  return (
    <>
      <PrimaryContent title={<TitleText initiateNewChat={initiateNewChat} />} />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <RestartInformation />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation overrideText="You can view the history of all your GoGetDoc care visits in your secure visit Vault." />
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitCancelledByUser };
