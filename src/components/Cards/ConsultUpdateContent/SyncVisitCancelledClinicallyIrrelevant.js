import { PrimaryContent } from './PrimaryContent';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { RestartInformation } from './RestartInformation';
import { Grid } from '@material-ui/core';

const TitleText = () => {
  return (
    <span>
      Based on the information available, the provider determined that this visit is not clinically
      recommended. Let's explore a different care option.
    </span>
  );
};

const SyncVisitCancelledClinicallyIrrelevant = ({ progress }) => {
  return (
    <>
      <PrimaryContent title={<TitleText />} progress={progress} />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <RestartInformation />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitCancelledClinicallyIrrelevant };
