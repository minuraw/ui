import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import { Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const AsyncPostVisitAmendment = ({ progress, phone, email, pharmacy }) => {
  return (
    <>
      <PrimaryContent
        title="Our certified provider has updated the notes from your visit."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                You now have access to the provider's orders. This includes your prescriptions,
                referrals, education, and test orders.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="We'll send notifications if there are any further amendments."
          />
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncPostVisitAmendment };
