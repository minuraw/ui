import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import EventIcon from '@material-ui/icons/Event';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import { Grid, Typography, Link } from '@material-ui/core';

const TitleText = () => {
  return <span>Success! Your visit has been rescheduled to (new date and time)</span>;
};

const SyncVisitRescheduled = ({ rescheduleAppointment }) => {
  return (
    <>
      <PrimaryContent title={<TitleText />} />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EventIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Click <Link onClick={rescheduleAppointment}>here</Link> to get started if you want
                to reschedule.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid item>
            <Grid container justifyContent="space-between" spacing={2}>
              <Grid item>
                <CheckCircleOutlineOutlinedIcon />
              </Grid>
              <Grid item xs>
                <Typography color="textPrimary">
                  You'll receive a reschedule confirmation email that includes an invite.
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <NotificationInformation additionalText="Updates may include if there are any changes to your appointment." />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitRescheduled };
