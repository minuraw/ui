import { getLocale } from '../../../services';
import { Grid, Typography } from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

const CAPTIONS = {
  before: {
    'en-us': 'Before proceeding,',
    'es-mx': 'Antes de continuar,',
  },
  medicalEmergency: {
    'en-us':
      'if you are having a medical emergency - please stop now and call 911 or your local emergency number.',
    'es-mx':
      'si tiene una emergencia médica, deténgase ahora y llame al 911 o al número de emergencia local.',
  },
  pregnant: {
    'en-us': 'You should also not proceed if you are pregnant or under the age of 18.',
    'es-mx': 'Tampoco debe continuar si está embarazada o es menor de 18 años.',
  },
};

const EmergencyInformation = () => {
  const locale = getLocale();

  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <ErrorOutlineIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          {CAPTIONS.before[locale]} <strong>{CAPTIONS.medicalEmergency[locale]}</strong>{' '}
          {CAPTIONS.pregnant[locale]}
        </Typography>
      </Grid>
    </Grid>
  );
};

export { EmergencyInformation };
