import { Grid, Typography } from '@material-ui/core';
import SmsOutlinedIcon from '@material-ui/icons/SmsOutlined';

const NotificationInformation = ({ phone = '', email = '', additionalText = '' }) => {
  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <SmsOutlinedIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          We will send you SMS on <strong>{phone}</strong> and email updates to{' '}
          <strong>{email}</strong>. {additionalText}
        </Typography>
      </Grid>
    </Grid>
  );
};

export { NotificationInformation };
