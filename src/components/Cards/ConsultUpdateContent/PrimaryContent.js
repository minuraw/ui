import { Typography, CircularProgress, Box, makeStyles, Grid, Divider } from '@material-ui/core';
import { useEffect, useState } from 'react';

const useClasses = makeStyles((theme) => ({
  circularProgress: {
    color: theme.palette.blue.main,
    '& .MuiCircularProgress-circleDeterminate': {
      fill: theme.palette.ggd.mediumGray,
    },
  },
}));

const ProgressWithPercentage = ({ percentageComplete }) => {
  const classes = useClasses();
  const [progressBar, setProgressBar] = useState();

  useEffect(() => {
    const delay = setTimeout(() => {
      setProgressBar(percentageComplete);
    }, 2000);

    return () => clearTimeout(delay);
  }, [percentageComplete]);

  return (
    <Box sx={{ position: 'relative', display: 'inline-flex' }}>
      <CircularProgress
        variant="determinate"
        value={progressBar}
        size={30}
        className={classes.circularProgress}
      />
    </Box>
  );
};

const PrimaryContent = ({ title, progress }) => {
  return (
    <>
      <Grid container justifyContent="space-between" alignItems="center" spacing={1}>
        {progress && (
          <Grid item>
            <Box style={{ marginTop: '4px', marginRight: '3px' }}>
              <ProgressWithPercentage percentageComplete={progress} />
            </Box>
          </Grid>
        )}
        <Grid item xs>
          <Typography style={{ fontWeight: 500 }}>{title}</Typography>
        </Grid>
      </Grid>
      <Box my={2}>
        <Divider />
      </Box>
    </>
  );
};

export { PrimaryContent };
