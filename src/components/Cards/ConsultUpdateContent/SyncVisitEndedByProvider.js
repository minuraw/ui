import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { Grid, Link, Typography } from '@material-ui/core';
import EventIcon from '@material-ui/icons/Event';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';

const SyncVisitEndedByProvider = ({ initiateNewChat }) => {
  return (
    <>
      <PrimaryContent title="We weren't able to connect for your visit." />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <ChatOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                If you believe this is an error, please start a support chat: (support link)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation overrideText="You can view the history of all your GoGetDoc care visits in your secure visit Vault." />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EventIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Click <Link onClick={initiateNewChat}>here</Link> to get started if you want to
                schedule a new visit.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitEndedByProvider };
