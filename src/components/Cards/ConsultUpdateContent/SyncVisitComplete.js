import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { NotificationInformation } from './NotificationInformation';
import { Grid, Typography } from '@material-ui/core';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const TitleText = () => {
  return (
    <span>
      You did great! Your (type) visit is complete. Your certified provider is finalizing visit
      notes.
    </span>
  );
};

const SyncVisitComplete = ({ progress, pharmacy, phone, email }) => {
  return (
    <>
      <PrimaryContent title={<TitleText />} progress={progress} />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Prescriptions will be sent to: {pharmacy}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation
            text="Time to final visit notes is"
            time="15 minutes"
            extraText="after your visit"
          />
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send will include the provider's visit notes."
          />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitComplete };
