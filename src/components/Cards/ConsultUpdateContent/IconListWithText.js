import {
  Grid,
  Typography,
  Box,
  Divider,
  useMediaQuery,
  SvgIcon,
  makeStyles,
} from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';

const useClasses = makeStyles((theme) => ({
  icon: {
    fontSize: '2rem',
  },
}));

const IconListWithText = ({ title, icons = [] }) => {
  const classes = useClasses();
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  return (
    <>
      <Grid container justifyContent="space-between" spacing={2}>
        <Grid item>
          <CheckCircleOutlineOutlinedIcon />
        </Grid>
        <Grid item xs>
          <Box mb={2}>
            <Typography color="textPrimary" gutterBottom variant="body2">
              {title}
            </Typography>
          </Box>
          <Grid container spacing={2}>
            {icons.map((icon, index) => (
              <Grid item key={`${index}-${icon.title}`}>
                <Box textAlign={smallScreen ? 'left' : 'center'}>
                  <SvgIcon className={classes.icon}>{icon.icon}</SvgIcon>
                </Box>
                <Typography variant="body2" align={smallScreen ? 'left' : 'center'}>
                  {icon.title}
                </Typography>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
      <Box mt={2}>
        <Divider />
      </Box>
    </>
  );
};

export { IconListWithText };
