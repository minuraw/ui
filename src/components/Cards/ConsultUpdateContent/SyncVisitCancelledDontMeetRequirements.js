import { PrimaryContent } from './PrimaryContent';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { RestartInformation } from './RestartInformation';
import { Grid, Typography, Link } from '@material-ui/core';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';

const SyncVisitCancelledDontMeetRequirements = ({ initiateNewChat }) => {
  return (
    <>
      <PrimaryContent title="Based on the information provided, the provider determined that this visit is the right care path" />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <RestartInformation />
        </Grid>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocalHospitalOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Click <Link onClick={initiateNewChat}>here</Link> to get started if you want to
                schedule a new visit.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitCancelledDontMeetRequirements };
