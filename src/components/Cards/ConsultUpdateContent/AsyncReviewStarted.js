import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import { TimeToActionInformation } from './TimeToActionInformation';
import { Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const AsyncReviewStarted = ({ progress, phone, email, pharmacy }) => {
  return (
    <>
      <PrimaryContent
        title="The provider is reviewing your answers to our questions. Your visit will start when they complete the review."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                The average review can take <strong>30 minutes</strong>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation
            text="Your chat will start in approximately"
            time="(countdown timer?)"
          />
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="The next notification we'll send is when your provider is ready to chat."
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncReviewStarted };
