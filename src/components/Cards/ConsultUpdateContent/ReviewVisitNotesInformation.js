import { Grid, Typography, Link } from '@material-ui/core';
import RemoveRedEyeOutlinedIcon from '@material-ui/icons/RemoveRedEyeOutlined';
import { useRouter } from 'next/router';

const ReviewVisitNotesInformation = ({ overrideText = '' }) => {
  const router = useRouter();

  const navigateToAppointments = () => {
    router.push('/dashboard/appointments');
  };

  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <RemoveRedEyeOutlinedIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          {overrideText || "To review your visit notes, we'll securely keep them in your vault."}{' '}
          Click <Link onClick={navigateToAppointments}>here</Link> to access your GoGetDoc Vault.
        </Typography>
      </Grid>
    </Grid>
  );
};

export { ReviewVisitNotesInformation };
