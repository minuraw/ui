import { Grid, Typography } from '@material-ui/core';
import ReplayOutlinedIcon from '@material-ui/icons/ReplayOutlined';

const RestartInformation = () => {
  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <ReplayOutlinedIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          If you believe this is an error, please restart this chat. Ensure you answer all questions
          as accurately as possible.
        </Typography>
      </Grid>
    </Grid>
  );
};

export { RestartInformation };
