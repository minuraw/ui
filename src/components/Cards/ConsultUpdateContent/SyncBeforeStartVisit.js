import { useContext } from 'react'
import { Store as GlobalStore } from '../../../context';
import { PrimaryContent } from './PrimaryContent';
import { TimeToActionInformation } from './TimeToActionInformation';
import { Grid, Typography, Link } from '@material-ui/core';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';

const TitleText = ({ appointmentName, minutesToAppointment }) => {
  return (
    <span>
      {minutesToAppointment > 0
        ? `Your ${appointmentName} starts in ${Math.ceil(minutesToAppointment)} minute${
            minutesToAppointment > 1 ? 's' : ''
          }`
        : 'Your appointment has started'}
    </span>
  );
};

const SyncBeforeStartVisit = ({
  progress,
  pharmacy,
  appointmentName,
  minutesToAppointment,
  rescheduleAppointment,
  cancelAppointment,
}) => {
  const { setInStore } = useContext(GlobalStore);
  return (
    <>
      <PrimaryContent
        title={
          <TitleText
            appointmentName={appointmentName}
            minutesToAppointment={minutesToAppointment}
          />
        }
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocalHospitalOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                You can join your appointment by clicking{' '}
                <Link style={{ cursor: 'pointer' }} onClick={() => setInStore({ showVideo: true })}>here</Link>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CancelOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                <Link onClick={rescheduleAppointment}>Click here</Link> to reschedule your visit.{' '}
                <Link onClick={cancelAppointment}>Click here</Link> if you need to cancel.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Prescriptions will be sent to: {pharmacy}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Please have your ID ready for this visit.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <TimeToActionInformation text="The average visit lasts" time="10 minutes" />
        </Grid>
      </Grid>
    </>
  );
};

export { SyncBeforeStartVisit };
