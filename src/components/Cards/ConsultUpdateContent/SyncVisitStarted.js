import { PrimaryContent } from './PrimaryContent';
import { TimeToActionInformation } from './TimeToActionInformation';
import { Grid, Typography } from '@material-ui/core';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';

const TitleText = ({ appointmentName }) => {
  return <span>Your {appointmentName} has started. Join your provider!</span>;
};

const VisitLengthTitle = ({ appointmentName }) => <span>The average {appointmentName} lasts</span>;

const SyncVisitStarted = ({ progress, pharmacy, appointmentName }) => {
  return (
    <>
      <PrimaryContent title={<TitleText appointmentName={appointmentName} />} progress={progress} />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <TimeToActionInformation
            text={<VisitLengthTitle appointmentName={appointmentName} />}
            time="10 minutes"
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">Prescriptions will be sent to: {pharmacy}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocalHospitalOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                You can join your appointment by clicking here (link)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CancelOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                Reschedule or cancel your appointment by clicking here (link). There may be a fee
                associated with rescheduling less than 24 hours before your visit.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <ChatOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                If you are having difficulties joining your visit, get support here: (support link)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitStarted };
