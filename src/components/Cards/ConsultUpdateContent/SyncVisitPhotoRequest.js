import { PrimaryContent } from './PrimaryContent';
import { NotificationInformation } from './NotificationInformation';
import EventIcon from '@material-ui/icons/Event';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import { Grid, Link, Typography } from '@material-ui/core';

const TitleText = () => {
  return (
    <span>
      Your provider has reviewed your information and needs more information.
    </span>
  );
};

const SyncVisitPhotoRequest = ({ pharmacy, phone, email, rescheduleAppointment }) => {
  return (
    <>
      <PrimaryContent title={<TitleText />} />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EventIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Reschedule your appointment by clicking{' '}
                <Link onClick={rescheduleAppointment}>here</Link>. Make sure to reschedule at least
                24 hours before your appointment.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <NotificationInformation
            phone={phone}
            email={email}
            additionalText="Updates may include if there are any changes to your appointment."
          />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocationOnOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Prescriptions will be sent to: {pharmacy}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { SyncVisitPhotoRequest };
