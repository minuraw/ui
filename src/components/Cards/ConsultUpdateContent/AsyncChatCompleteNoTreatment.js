import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { RestartInformation } from './RestartInformation';
import { Grid, Typography } from '@material-ui/core';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import EnhancedEncryptionOutlinedIcon from '@material-ui/icons/EnhancedEncryptionOutlined';

const AsyncChatCompleteNoTreatment = ({ progress }) => {
  return (
    <>
      <PrimaryContent
        title="Based on the information available, the provider determined that a (flow purpose goes here) is not clinically recommended. Let's explore a different care option."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <RestartInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocalHospitalOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                In this case, we typically recommend starting with a SymptomBot check (link) or a
                General Visit (link) to get the care you need.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EnhancedEncryptionOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                You can view the history of all your GoGetDoc care visits in your secure visit
                Vault. (link)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncChatCompleteNoTreatment };
