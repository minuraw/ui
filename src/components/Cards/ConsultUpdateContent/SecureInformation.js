import { getLocale } from '../../../services';
import { Grid, Typography } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

const CAPTIONS = {
  secureText: {
    'en-us':
      'All health-related information that you share here is stored in your secure 256bit encrypted HIPAA compliant vault.',
    'es-mx':
      'Toda la información relacionada con la salud que comparte aquí se almacena en su bóveda segura cifrada de 256 bits que cumple con la HIPAA.',
  },
};

const SecureInformation = () => {
  const locale = getLocale();

  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item>
        <LockOutlinedIcon />
      </Grid>
      <Grid item xs>
        <Typography color="textPrimary" variant="body2">
          {CAPTIONS.secureText[locale]}
        </Typography>
      </Grid>
    </Grid>
  );
};

export { SecureInformation };
