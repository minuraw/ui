import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { ReviewVisitNotesInformation } from './ReviewVisitNotesInformation';
import { Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';

const SyncNotesAmended = ({ progress }) => {
  return (
    <>
      <PrimaryContent
        title="Our certified provider has updated the notes from your visit."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <ReviewVisitNotesInformation />
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <CheckCircleOutlineOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary" variant="body2">
                Your provider may update your notes to add to your diagnosis. The provider may also
                make changes to your prescription, order a test, or share educational materials.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { SyncNotesAmended };
