import { PrimaryContent } from './PrimaryContent';
import { FeedbackRequestInformation } from './FeedbackRequestInformation';
import { Grid, Typography } from '@material-ui/core';
import ReplayOutlinedIcon from '@material-ui/icons/ReplayOutlined';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import EnhancedEncryptionOutlinedIcon from '@material-ui/icons/EnhancedEncryptionOutlined';

const AsyncChatCompleteNoRequisites = ({ progress }) => {
  return (
    <>
      <PrimaryContent
        title="Based on the information available, we are missing the (missing value goes here) required to complete this visit. Let's explore our options."
        progress={progress}
      />
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <ReplayOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                If you believe this is an error, please restart this chat and provide the (missing
                requirement).
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <LocalHospitalOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                If you don't have the (missing requirement), we typically recommend starting with a
                SymptomBot check (link) or a General Visit (link) to get the care you need.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <FeedbackRequestInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <EnhancedEncryptionOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textPrimary">
                You can view the history of all your GoGetDoc care visits in your secure visit
                Vault. (link)
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { AsyncChatCompleteNoRequisites };
