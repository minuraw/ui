import { useState, useEffect, useContext } from 'react';
import { PaymentSlider, Payment } from '../Common';
import { useAuth, Store as GlobalStore } from '../../context';
import { parsePhoneNumber } from 'libphonenumber-js';
import { makeStyles } from '@material-ui/core';
import { PROVIDER_DESCRIPTION } from '../../constants/providers';
import { COUNTRIES_ISO_CODES } from '../../utils';

 const FREE_PHONE_COUNTRIES = [
   COUNTRIES_ISO_CODES.US,
   COUNTRIES_ISO_CODES.PR,
   COUNTRIES_ISO_CODES.AS,
   COUNTRIES_ISO_CODES.GU,
   COUNTRIES_ISO_CODES.MP,
   COUNTRIES_ISO_CODES.VI,
 ];


const CAPTIONS = {
  choose: {
    'en-us': 'Choose your own contribution',
    'es-mx': 'Seleccione su propia contribución',
  },
  freeService: {
    'en-us':
      'is a free service for our members. We trust that if we bring you value, you will support us fairly.',
    'es-mx':
      'es un servicio gratuito para nuestros miembros. Confiamos en que si le aportamos valor, nos apoyará de manera justa.',
  },
  nonFreeService1: {
    'en-us': 'is only',
    'es-mx': 'cuesta solo',
  },
  nonFreeService2: {
    'en-us':
      'for international members. We trust that if we bring you value, you will support us fairly.',
    'es-mx':
      'para miembros internacionales. Confiamos en que si le aportamos valor, nos apoyará de manera justa.',
  },
  mexicans: {
    'en-us':
      'for mexican members. We trust that if we bring you value, you will support us fairly.',
    'es-mx':
      'para miembros mexicanos. Confiamos en que si le aportamos valor, nos apoyará de manera justa.',
  },
  mostPeople: {
    'en-us': 'of people contribute more than',
    'es-mx': 'de las personas contribuyen más que',
  },
  chose: {
    'en-us': 'I chose not to contribute to',
    'es-mx': 'Elegí no contribuir a',
  },
};

const useClasses = makeStyles({
  highlight: {
    backgroundColor: '#e6f1ed',
    padding: '2px',
    fontWeight: 500,
  },
});

const ContributionText = ({ locale }) => {
  const classes = useClasses();

  return (
    <span>
      <span className={classes.highlight}>68.57%</span> {CAPTIONS.mostPeople[locale]}{' '}
      <span className={classes.highlight}>$5</span>
    </span>
  );
};

const SelectPayment = ({ handlePaymentCompletion, productId, provider }) => {
  const [selectedPaymentAmount, setSelectedPaymentAmount] = useState(null);
  const [defaultAmount, setDefaultAmount] = useState(5);
  const [minAmount, setMinAmount] = useState(0);
  const [value, setValue] = useState(5);
  const [phoneNumberCountry, setPhoneNumberCountry] = useState();
    
  const { getMemberProfile } = useAuth();
  const memberProfile = getMemberProfile();
  const { store } = useContext(GlobalStore);

  useEffect(() => {
    if (memberProfile && memberProfile.phone && memberProfile.phone[0]) {
      const phoneNumber = `+${memberProfile.phone[0].replace(/\D/g, '')}`;
      const { country } = parsePhoneNumber(phoneNumber);
      setPhoneNumberCountry(country);
      if (provider === 4 || provider === 5) {
        if (!FREE_PHONE_COUNTRIES.includes(country)) {
          if (country === 'MX') {
            setValue(5);
            setDefaultAmount(5);
            setMinAmount(5);
          } else {
            setValue(1.99);
            setDefaultAmount(1.99);
            setMinAmount(1.99);
          }
        }
      } else {
        setValue(2);
        setDefaultAmount(2);
        setMinAmount(0);
      }
    }
  }, []);

  const handlePaymentSliderSubmit = (value) => {
    if (value === 0) {
      handlePaymentCompletion(`${CAPTIONS.chose[store.locale]} ${PROVIDER_DESCRIPTION[provider]}`);
      return;
    }
    setSelectedPaymentAmount(value);
  };

  const handlePaymentSuccess = (paymentToken) => {
    handlePaymentCompletion(paymentToken);
  };

  return (
    <>
      {!selectedPaymentAmount && (
        <PaymentSlider
          title={CAPTIONS.choose[store.locale]}
          secondaryText={`
            ${PROVIDER_DESCRIPTION[provider]}
            ${
              minAmount === 0
                ? CAPTIONS.freeService[store.locale]
                : `${CAPTIONS.nonFreeService1[store.locale]} $${minAmount} ${
                    phoneNumberCountry === 'MX'
                      ? CAPTIONS.mexicans[store.locale]
                      : CAPTIONS.nonFreeService2[store.locale]
                  }`
            }
          `}
          contributionText={minAmount === 0 ? <ContributionText locale={store.locale} /> : null}
          value={value}
          setValue={setValue}
          defaultAmount={defaultAmount}
          minAmount={minAmount}
          maxAmount={10}
          onSubmit={handlePaymentSliderSubmit}
        />
      )}
      {selectedPaymentAmount > 0 && (
        <Payment
          price={selectedPaymentAmount}
          productId={productId}
          handlePaymentSuccess={handlePaymentSuccess}
          provider={provider}
          minAmount={minAmount}
        />
      )}
    </>
  );
};

export default SelectPayment;
