import { useContext } from 'react'
import { Box, Grid, Typography, makeStyles, Link } from '@material-ui/core';
import { MessageGroup } from '../Common';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import { Store as GlobalStore } from '../../context';


const CAPTIONS = {
  success: {
    'en-us': 'Success! Your VaxYes record was successfully created.',
    'es-mx': '¡Éxito! Su registro de VaxYes se creó correctamente.',
  },
  thanks: {
    'en-us': 'Thank you!',
    'es-mx': '¡Gracias!',
  },
  secure: {
    'en-us': 'Secure your VaxYes card in your digital wallet.',
    'es-mx': 'Asegure su tarjeta VaxYes en su billetera digital.',
  },
  safely: {
    'en-us':
      'Safely start verifying your vaccine record across the country and around the world. We’ll be automatically verifying your submission and will update you via email and SMS.',
    'es-mx':
      'Comience a verificar de forma segura su registro de vacunas en todo el país y en todo el mundo. Verificaremos automáticamente su envío y lo actualizaremos por correo electrónico y SMS.',
  },
  share: {
    'en-us':
      'Share this unique link with family and friends so they can get their vaccine cards too:',
    'es-mx':
      'Comparta este enlace único con familiares y amigos para que ellos también puedan obtener sus tarjetas de vacunas:',
  },
};

const useStyles = makeStyles((theme) => ({
  container: {
    border: 'solid 1px rgba(0,0,0,0.1)',
  },
  iconContainer: {
    backgroundColor: theme.palette.secondary.main,
    textAlign: 'center',
  },
  icon: {
    color: theme.palette.ggd.light,
    fontSize: 50,
    marginTop: 16,
  },
  link: {
    color: theme.palette.blue.main,
    fontWeight: 500,
  },
  title: {
    fontWeight: 500,
  },
  shareText: {
    fontWeight: 500,
  },
}));

const VaxYesSuccessCard = ({ msgId }) => {
  const classes = useStyles();
  const { store } = useContext(GlobalStore);

  return (
    <Box my={3}>
      <MessageGroup
        messages={[CAPTIONS.success[store.locale]]}
        messageId={msgId}
      />
      <Box my={3} className={classes.container}>
        <Grid container>
          <Grid item xs={3} sm={2} className={classes.iconContainer}>
            <CheckBoxOutlinedIcon className={classes.icon} />
          </Grid>
          <Grid item xs={9} sm={10}>
            <Box p={2}>
              <Typography className={classes.link}>{CAPTIONS.thanks[store.locale]}</Typography>
              <Typography color="textPrimary" className={classes.title}>
                {CAPTIONS.secure[store.locale]}
              </Typography>
              <Box my={2}>
                <Typography color="textSecondary">
                  {CAPTIONS.safely[store.locale]}
                </Typography>
              </Box>
              <Typography color="textPrimary" className={classes.shareText}>
                {CAPTIONS.share[store.locale]}{' '}
                <Link
                  href="https://gogetdoc.com/vaxyes"
                  target="_blank"
                  rel="noreferrer"
                  style={{ wordWrap: 'break-word' }}
                >
                  https://gogetdoc.com/vaxyes
                </Link>
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default VaxYesSuccessCard;
