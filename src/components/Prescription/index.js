import { useState, useEffect } from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import LibraryBooksOutlinedIcon from '@material-ui/icons/LibraryBooksOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

import { MessageGroup } from '../Common';
import { InformationCard } from '../Cards';

import { getConsultationPrescriptionInformation } from '../../events';

const TITLE = 'Prescription Details';
const SUBTITLE = 'Here is the summary of your prescription we sent to your pharmacy.';

const prescriptionNumber = (prescriptions, index) =>
  // Only show number of prescriptions if there's more than one
  prescriptions.length > 1 ? `${index + 1} of ${prescriptions.length}` : null;

const Pharmacy = ({ pharmacy }) => {
  if (!pharmacy) {
    return <span>N/A</span>;
  }

  return (
    <>
      <span>
        <strong>{pharmacy.name}</strong>
      </span>
      <br />
      <span>{pharmacy.addressLineOne}</span>
      <br />
      <span>{`${pharmacy.city}, ${pharmacy.state} ${pharmacy.postalCode} ${
        pharmacy.country || 'USA'
      }`}</span>
    </>
  );
};

const Prescriptions = ({ msgId, consultId }) => {
  const [isDataLoading, setIsDataLoading] = useState(true);

  const [prescriptionInfo, setPrescriptionInfo] = useState([]);
  const [pharmacyInfo, setPharmacyInfo] = useState(null);
  const [hasError, setHasError] = useState(false);

  const handlePrescriptionLoad = (error, data) => {
    setIsDataLoading(false);
    if (error) {
      setHasError(true);
      return;
    }
    setPharmacyInfo(data.pharmacy);
    setPrescriptionInfo(data.prescriptions || []);
  };

  useEffect(() => {
    // load prescription data here
    getConsultationPrescriptionInformation(handlePrescriptionLoad, { consultId });
  }, []);

  if (isDataLoading) {
    return (
      <Box textAlign="center">
        <CircularProgress />
      </Box>
    );
  }

  if (hasError) {
    return 'Something went wrong. Please refresh the page';
  }

  if (Array.isArray(prescriptionInfo) && prescriptionInfo.length === 0) {
    return '';
  }

  return (
    <Box my={3}>
      <MessageGroup messages={['Here is your Prescription']} messageId={msgId} />
      {Array.isArray(prescriptionInfo) &&
        prescriptionInfo.length > 0 &&
        prescriptionInfo.map((p, index) => (
          <Box my={4} key={`${p.medicationName}-${index}`}>
            <InformationCard
              title={TITLE}
              subtitle={SUBTITLE}
              topRight={prescriptionNumber(prescriptionInfo, index)}
              content={[
                {
                  left: <LocalHospitalOutlinedIcon />,
                  title: 'Medication',
                  secondaryText: p.medicationName,
                },
                {
                  left: <LibraryBooksOutlinedIcon />,
                  title: 'Instructions',
                  secondaryText: p.patientInstructions,
                },
                {
                  left: <LocationOnOutlinedIcon />,
                  title: 'Pharmacy Details',
                  secondaryText: <Pharmacy pharmacy={pharmacyInfo} />,
                },
              ]}
            />
          </Box>
        ))}
    </Box>
  );
};

export default Prescriptions;
