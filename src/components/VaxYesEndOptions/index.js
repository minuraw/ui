import { useState, useEffect, useContext } from 'react';
import { Box, Button, Grid } from '@material-ui/core';
import { getVaxYesBoosterCertificateInfo } from '../../events';
import { useRouter } from 'next/router';
import { GGDLoader } from '../Common/GGDLoader';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  view: {
    'en-us': 'View Card',
    'es-mx': 'Ver tarjeta',
  },
  update: {
    'en-us': 'Update Certificate',
    'es-mx': 'Actualizar Certificado',
  },
  booster: {
    'en-us': 'Add Booster',
    'es-mx': 'Agregar Refuerzo',
  },
};

const VaxYesEndOptions = ({ updateChat = () => {}, isBooster = false }) => {
  const [isDataLoading, setIsDataLoading] = useState(true);
  const [isBoosterCertificateAvailable, setIsBoosterCertificateAvailable] = useState(false);
  const { store } = useContext(GlobalStore);

  const router = useRouter();

  const handleDeleteClick = () => {
    updateChat();
  };
  const handleVaxYesBoosterCertInfoLoad = (err, { isBoosterCertAvailable = false } = {}) => {
    setIsDataLoading(false);
    if (!err) {
      setIsBoosterCertificateAvailable(isBoosterCertAvailable);
      return;
    }
    setIsBoosterCertificateAvailable(false);
  };

  useEffect(() => {
    getVaxYesBoosterCertificateInfo(handleVaxYesBoosterCertInfoLoad);
  }, []);

  const handleAddBoosterRecordButtonClick = () => {
    // TODO: Remove the 5 hard code
    router.push('/chat/start/vax-yes-booster/5');
  };

  const handleViewCardClick = () => {
    const certId = isBooster ? 'booster' : 'covid';
    router.push(`/dashboard/wallet/${certId}`);
  };

  if (isDataLoading) {
    return (
      <Box align="center">
        <GGDLoader />
      </Box>
    );
  }
  return (
    <Grid container justifyContent="center" spacing={2}>
      <Grid item>
        <Button color="secondary" variant="contained" onClick={handleViewCardClick}>
          {CAPTIONS.view[store.locale]}
        </Button>
      </Grid>

      <Grid item>
        <Button color="secondary" variant="contained" onClick={handleDeleteClick}>
          {CAPTIONS.update[store.locale]}
        </Button>
      </Grid>

      {!isBooster && !isBoosterCertificateAvailable && (
        <Grid item>
          <Button color="secondary" variant="contained" onClick={handleAddBoosterRecordButtonClick}>
            {CAPTIONS.booster[store.locale]}
          </Button>
        </Grid>
      )}
    </Grid>
  );
};

export default VaxYesEndOptions;
