import { useEffect } from 'react';

import { useRouter } from 'next/router';

import toast, { Toaster } from 'react-hot-toast';

import { eventListener, removeEventListener, EVENTS } from '../../events';

const Notifications = () => {
  const router = useRouter();

  const handleAppNotificationReceived = (notification) => {
    const { error, text, Component, options = {} } = notification;
    // if error is available show it as an error
    if (error) {
      toast.error(
        (t) => {
          return (
            <div
              // remove toast when click on it
              onClick={() => {
                toast.dismiss(t.id);
              }}
            >
              {error}
            </div>
          );
        },
        { ...options },
      );
      return;
    }
    if (text) {
      toast.success(
        (t) => {
          return (
            <div
              onClick={() => {
                toast.dismiss(t.id);
              }}
            >
              {text}
            </div>
          );
        },
        { ...options },
      );
    }
    if (Component) {
      toast.custom(<Component />, { ...options });
    }
  };

  useEffect(() => {
    const listenerFnc = eventListener(
      EVENTS.APP_NOTIFICATION_RECEIVED,
      handleAppNotificationReceived,
    );
    return () => {
      removeEventListener(EVENTS.APP_NOTIFICATION_RECEIVED, listenerFnc);
    };
  }, []);

  // At initial component load, check if there are any pending notifications to be shown
  useEffect(() => {
    if (router.isReady) {
      const pendingNotifications = window.localStorage.getItem('notifications');
      if (pendingNotifications) {
        let notifications = JSON.parse(pendingNotifications);
        const duration = notifications?.options?.duration;
        // toast component requires, duration === Infinity to keep the toast displayed always
        // but the session storage cannot save Infinity, therefore -1 is used to indicate the Infinity
        if (duration === -1) {
          notifications = {
            ...notifications,
            options: { ...notifications.options, duration: Infinity },
          };
        }
        handleAppNotificationReceived(notifications);
        // clear pending notifications
        window.localStorage.removeItem('notifications');
      }
    }
  }, [router]);

  return <Toaster />;
};
export default Notifications;
