import { GGDLoader } from '../../components/Common';
import { Paper, CircularProgress, Box } from '@material-ui/core';

const CircularLoader = ({ displayText = null, GGD = false, height = '100vh' }) => {
  return (
    <Paper
      style={{
        height,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {GGD ? <GGDLoader /> : <CircularProgress />}
      {displayText && <Box m={5}>{displayText}</Box>}
    </Paper>
  );
};

export { CircularLoader };
