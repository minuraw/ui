import React, { useCallback, useState } from 'react';
import { Form, Field } from 'react-final-form';
import { string, object } from 'yup';
import {
  Checkbox,
  FormControlLabel,
  Grid,
  Box,
  TextField,
  InputAdornment,
  makeStyles,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import { Search } from '@material-ui/icons';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import { useSnackbar } from 'notistack';
import { getPharmacies } from '../../connectors/bff-connector';
import { useGeolocation, useYupValidation } from '../../hooks';
import { GGDLoader } from '../Common/GGDLoader';
import { SecureSubmitButton } from '../Chat/SecureSubmitButton';

const useClasses = makeStyles((theme) => ({
  textField: {
    '& .MuiInputLabel-root': {
      color: theme.palette.grey[500],
      top: '1rem',
      paddingLeft: '1.6rem',
      pointerEvents: 'none',
      zIndex: 1,
    },
    '& .MuiInputLabel-shrink': {
      transform: 'translate(-12.5px, -13px) scale(0.75)',
      borderRadius: 0,
    },
  },
  input: {
    backgroundColor: theme.palette.ggd.light,
    color: theme.palette.grey[600],
    padding: '1rem',
    borderRadius: theme.shape.borderRadius,
  },
  searchIcon: {
    fill: theme.palette.grey[500],
    fontSize: 30,
  },
  inputLabelNoShrink: {
    transform: 'translate(28px, 24px) scale(1)',
  },
  checkboxLabel: {
    '& .MuiFormControlLabel-label': {
      fontSize: '0.8rem',
    },
  },
}));

const validationSchema = object({
  zip: string()
    .required()
    .matches(/^[0-9]+$/, 'Zip code must be only digits')
    .min(5, 'Zip code must be exactly 5 digits')
    .max(5, 'Zip code must be exactly 5 digits')
    .label('Zip code'),
});

const PharmacySearchForm = ({ onSuccess }) => {
  const { enqueueSnackbar } = useSnackbar();
  const [loading, setLoading] = useState(false);
  const [checked, setChecked] = useState(false);
  const { getLocation, loading: locationLoading } = useGeolocation();
  const { validate } = useYupValidation();

  const classes = useClasses();

  const gqlGetPharmacy = useCallback(async (params) => {
    try {
      setLoading(true);
      const pharmacies = await getPharmacies(params);
      setLoading(false);
      onSuccess(pharmacies);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  const validateForm = async (data) => {
    const errors = await validate(validationSchema)(data);

    if (errors) return errors;

    onSubmit(data);
  };

  const onSubmit = (data) => {
    return gqlGetPharmacy({ pharmacyName: data.name, city: data.city, postalCode: data.zip });
  };

  const handleGetLocation = async (e, form) => {
    try {
      setChecked(e.target.checked);
      if (e.target.checked) {
        const { features } = await getLocation();

        const [postcode] = features;

        if (postcode?.text) {
          form.mutators.setZip(postcode.text);
        } else {
          throw new Error('Your zip code was not found. Please enter it manually.');
        }
      } else {
        form.mutators.setZip();
      }
    } catch (error) {
      let { msg = error.message } = error;
      if (error.code === 1) {
        msg = 'Location is disabled from browser or system preferences. Please switch location on';
      }
      enqueueSnackbar(msg, { variant: 'error' });
    }
  };

  return (
    <Form
      onSubmit={validateForm}
      mutators={{
        setZip: (args, state, utils) => {
          utils.changeValue(state, 'zip', () => args[0] || '');
        },
      }}
      render={({ handleSubmit, form, submitErrors: errors = {} }) => (
        <Grid container spacing={2} direction="column">
          <Grid item xs={12}>
            <Box display="flex" alignItems="center" justifyContent="space-between">
              <Typography variant="h6">Search pharmacy</Typography>
              {!loading && (
                <FormControlLabel
                  control={
                    <Checkbox
                      size="small"
                      checked={checked}
                      onChange={(e) => handleGetLocation(e, form)}
                    />
                  }
                  className={classes.checkboxLabel}
                  label="Use my current location"
                  color="secondary"
                  disabled={locationLoading}
                />
              )}
            </Box>
          </Grid>
          <Grid item xs={12}>
            {loading ? (
              <Box display="flex" justifyContent="center">
                <GGDLoader />
              </Box>
            ) : (
              <Grid container spacing={1} direction="row">
                <Grid item xs={12}>
                  <form onSubmit={handleSubmit}>
                    <Field
                      name="zip"
                      render={({ input }) => (
                        <TextField
                          {...input}
                          label="Zip Code"
                          fullWidth
                          error={!!errors.zip}
                          helperText={errors.zip}
                          className={classes.textField}
                          InputProps={{
                            disableUnderline: true,
                            className: classes.input,
                            startAdornment: (
                              <InputAdornment position="start">
                                {locationLoading ? (
                                  <CircularProgress size={14} />
                                ) : (
                                  <LocationOnOutlinedIcon className={classes.searchIcon} />
                                )}
                              </InputAdornment>
                            ),
                          }}
                          InputLabelProps={{
                            shrink: input.value,
                            className: input.value ? undefined : classes.inputLabelNoShrink,
                          }}
                        />
                      )}
                    />
                    <Field
                      name="name"
                      render={({ input }) => (
                        <TextField
                          {...input}
                          label="Pharmacy Name (Optional)"
                          fullWidth
                          className={classes.textField}
                          InputProps={{
                            disableUnderline: true,
                            className: classes.input,
                            startAdornment: (
                              <InputAdornment position="start">
                                <Search className={classes.searchIcon} />
                              </InputAdornment>
                            ),
                          }}
                          InputLabelProps={{
                            shrink: input.value,
                            className: input.value ? undefined : classes.inputLabelNoShrink,
                          }}
                        />
                      )}
                    />
                    <Box display="flex" justifyContent="flex-end">
                      <Box mt={3} className={classes.btnContainer}>
                        <SecureSubmitButton onClick={handleSubmit} />
                      </Box>
                    </Box>
                  </form>
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
      )}
    />
  );
};

export { PharmacySearchForm };
