import { useState, useEffect } from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import LibraryBooksOutlinedIcon from '@material-ui/icons/LibraryBooksOutlined';
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined';
import { MessageGroup } from '../Common';

import { getConsultationDiagnosisInformation } from '../../events';
import { InformationCard } from '../Cards';

const TITLE = 'Diagnosis';
const SUBTITLE = 'Here are the notes from your visit which your clinician has provided.';

const Diagnosis = ({ msgId, consultId }) => {
  const [isDataLoading, setIsDataLoading] = useState(true);

  const [diagnosisInfo, setDiagnosisInfo] = useState([]);
  const [patientInstruction, setPatientInstruction] = useState('');
  const [hasError, setHasError] = useState(false);

  const handleDiagnosisLoad = (error, data) => {
    setIsDataLoading(false);
    if (error) {
      setHasError(true);
      return;
    }
    setPatientInstruction(data.diagnosis?.patientInstructions);
    setDiagnosisInfo(data.diagnosis?.conditions || []);
  };

  useEffect(() => {
    // load prescription data here
    getConsultationDiagnosisInformation(handleDiagnosisLoad, { consultId });
  }, []);

  if (isDataLoading) {
    <Box textAlign="center">
      <CircularProgress />
    </Box>;
  }

  if (hasError) {
    return 'Something went wrong. Please refresh the page';
  }

  const generateContent = (diagnosisInfo = []) => {
    let content = [
      {
        left: <LibraryBooksOutlinedIcon />,
        secondaryText: patientInstruction || 'No instructions from your clinician.',
      },
    ];
    Array.isArray(diagnosisInfo) &&
      diagnosisInfo
        .filter((condition) => condition.code)
        .forEach((condition) => {
          content = [
            ...content,
            {
              left: <LocalHospitalOutlinedIcon />,
              title: 'Condition',
              secondaryText: condition.code,
            },
          ];
        });

    return content;
  };

  return (
    <Box my={3}>
      <MessageGroup messages={['Here is your diagnosis.']} messageId={msgId} />
      <Box my={4}>
        <InformationCard
          title={TITLE}
          subtitle={SUBTITLE}
          content={generateContent(diagnosisInfo)}
        />
      </Box>
    </Box>
  );
};

export default Diagnosis;
