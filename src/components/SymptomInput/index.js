import { useState, useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import { ActionDrawerWrapper } from '../Chat/ActionDrawerWrapper';
import { AutoFill } from '../Common';
import { getSymptomSuggestions } from '../../events';
import { makeStyles } from '@material-ui/core';
import { ErrorOutline } from '@material-ui/icons';

const CAPTIONS = {
  next: {
    'en-us': 'Next',
    'es-mx': 'Siguiente',
  },
  accuracyText: {
    'en-us': 'Entering more than one symptom greatly improves accuracy',
    'es-mx': 'Ingresar más de un síntoma mejora en gran medida la precisión',
  },
};

const useClasses = makeStyles((theme) => ({
  infoIcon: {
    color: theme.palette.brightGreen.main,
  },
}));

const InfoIcon = () => {
  const classes = useClasses();

  return <ErrorOutline className={classes.infoIcon} />;
};

const SymptomInput = ({ onClickNext = () => {}, gender, age, maxResults }) => {
  const [selectedSymptoms, setSelectedSymptoms] = useState([]);
  const { store } = useContext(GlobalStore);

  const handleWordSelect = (symptoms) => {
    const values = symptoms.map((symptom) => symptom.value);
    setSelectedSymptoms(values);
  };

  const getSuggestedWords = (handlerFunc, payload) => {
    return getSymptomSuggestions(handlerFunc, {
      ...payload,
      gender,
      age,
      maxResults,
    });
  };
  return (
    <ActionDrawerWrapper
      secondaryText={{
        text: CAPTIONS.accuracyText[store.locale],
        icon: <InfoIcon />,
      }}
      buttonDisabled={selectedSymptoms.length === 0}
      buttonLabel={CAPTIONS.next[store.locale]}
      onClick={() => onClickNext(selectedSymptoms)}
    >
      <AutoFill getSuggestedWords={getSuggestedWords} handleWordSelect={handleWordSelect} />
    </ActionDrawerWrapper>
  );
};

export default SymptomInput;
