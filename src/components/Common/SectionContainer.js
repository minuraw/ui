import {
  Container,
  makeStyles,
  Box,
  Typography,
  Tooltip,
  Grid,
  useMediaQuery,
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { CategoryFilter, MoreLink } from '../Layout';

const useStyles = makeStyles({
  greyColor: {
    backgroundColor: '#f7f7f7',
  },
  whiteColor: {
    backgroundColor: '#fff',
  },
  lightText: {
    color: '#a0a0a0',
  },
  dot: {
    fontSize: '5px',
    position: 'relative',
    transform: 'translateY(-50%)',
    marginRight: '7px',
    marginLeft: '7px',
  },
});

const SectionContainer = ({
  maxWidth = 'md',
  children,
  color,
  sectionTitle,
  sectionSubtitle,
  tooltipText,
  filterCategories,
  handleCategorySelect,
  moreLink = '',
}) => {
  const classes = useStyles();
  const largeScreen = useMediaQuery((theme) => theme.breakpoints.up('sm'));

  return (
    <Box className={color ? classes.greyColor : classes.whiteColor}>
      <Container maxWidth={maxWidth}>
        {sectionTitle && (
          <Box pt={9}>
            <Grid container alignItems="center" justifyContent="space-between">
              <Grid item>
                <Grid container alignItems="baseline">
                  <Grid item>
                    <Typography variant="h6">{sectionTitle}</Typography>
                  </Grid>
                  {sectionSubtitle && largeScreen && (
                    <Grid item className={classes.lightText}>
                      <Typography variant="body2">
                        <FiberManualRecordIcon className={classes.dot} />
                        {sectionSubtitle}
                      </Typography>
                    </Grid>
                  )}
                </Grid>
              </Grid>
              {(moreLink || filterCategories) && (
                <Grid item>
                  <Grid container spacing={2} alignItems="center">
                    {moreLink && (
                      <Grid item>
                        <MoreLink data-id="see_more_link_dashboard" link={moreLink} />
                      </Grid>
                    )}
                    {filterCategories && handleCategorySelect && largeScreen && (
                      <Grid item>
                        <CategoryFilter
                          categories={filterCategories}
                          handleCategorySelect={handleCategorySelect}
                        />
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              )}
            </Grid>
          </Box>
        )}
        <Box py={4}>{children}</Box>
      </Container>
      {tooltipText && (
        <span>
          <Tooltip title={tooltipText} placement="top">
            <InfoIcon style={{ fontSize: '1rem', color: '#a0a0a0' }} />
          </Tooltip>
        </span>
      )}
    </Box>
  );
};

export { SectionContainer };
