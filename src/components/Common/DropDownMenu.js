import React from 'react';
import { Box, Typography, FormControl, InputLabel, Select, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  form: {
    minWidth: 300,
  },
  label: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    maxWidth: '80%',
    overflow: 'hidden',
  },
}));

const DropDownMenu = ({ item = {}, items, onClick = () => {} }) => {
  const classes = useStyles();
  const { question } = item;

  return (
    <Box display="flex" flexWrap="wrap" justifyContent="center">
      <FormControl variant="outlined" className={classes.form}>
        <InputLabel id="select-helper-label" className={classes.label}>{question}</InputLabel>
        <Select labelId="select-helper-label" id="select-helper" label={question}>
          {items.map((item) => (
            <Box m={1} key={item.id} onClick={() => onClick(item)}>
              <Typography style={{ cursor: 'pointer' }}>{item.label}</Typography>
            </Box>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};

export { DropDownMenu };
