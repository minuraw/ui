import React, { useState, useEffect } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import ReactMapGL, { NavigationControl } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

const useClasses = makeStyles((theme) => ({
  mapContainer: {
    borderRadius: theme.shape.borderRadius,
    '& .mapboxgl-map': {
      borderRadius: theme.shape.borderRadius,
    },
    '& .mapboxgl-ctrl-bottom-right, .mapboxgl-ctrl-bottom-left': {
      display: 'none',
    },
    '& .mapboxgl-ctrl-group': {
      background: 'rgb(0,0,0,0.2)',
    },
  },
  navigationControl: {
    position: 'absolute',
    right: '2rem',
    top: '0.5rem',
  },
}));

const MapboxMap = ({
  latitude = 32.895367,
  longitude = -96.788849,
  zoom = 11,
  children,
  scrollZoom = true,
  controls = false,
}) => {
  const classes = useClasses();
  const [viewport, setViewport] = useState({});

  useEffect(() => {
    setViewport((viewport) => ({ ...viewport, latitude, longitude, zoom }));
  }, [latitude, longitude, zoom]);

  return (
    <Box className={classes.mapContainer}>
      <ReactMapGL
        mapboxApiAccessToken={process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN}
        {...viewport}
        height="4rem"
        width="4rem"
        mapStyle="mapbox://styles/mapbox/streets-v11"
        scrollZoom={scrollZoom}
        onViewportChange={(viewport) => {
          setViewport(viewport);
        }}
      >
        {children}
        {controls && (
          <Box className={classes.navigationControl}>
            <NavigationControl />
          </Box>
        )}
      </ReactMapGL>
    </Box>
  );
};

export { MapboxMap };
