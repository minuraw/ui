const toBase64 = (file) =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line no-undef
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.onerror = (error) => reject(error);
  });

const stringToColor = (string) => {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = '#';

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.substr(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
};

const getFileSizeFromBase64 = (base64Data) => {
  if (!base64Data) {
    return 0;
  }
  return getReadableFileSize(base64Data.length * 0.75); // not 100% true. but will sufficient as an estimate
};

const getReadableFileSize = (valueInBytes) => {
  if (isNaN(valueInBytes)) {
    return '';
  }
  if (valueInBytes < 1024) {
    return valueInBytes + ' bytes';
  } else if (valueInBytes < 1048576) {
    return (valueInBytes / 1024).toFixed(1) + ' kB';
  } else if (valueInBytes < 1073741824) {
    return (valueInBytes / 1048576).toFixed(2) + ' MB';
  } else {
    return (valueInBytes / 1073741824).toFixed(1) + ' GB';
  }
};

export { toBase64, stringToColor, getReadableFileSize, getFileSizeFromBase64 };
