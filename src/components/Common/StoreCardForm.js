import React, { useState, useContext, useEffect } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import styles from './PaymentForm.module.css';
import { SecureSubmitButton } from '../Chat';
import { Grid, Link, makeStyles, Box, Typography, Select, MenuItem } from '@material-ui/core';
import CreditCardIcon from '@material-ui/icons/CreditCard';
// import { CONVERSATION_PROVIDER_ID } from '../../constants/providers';
import { Store as GlobalStore } from '../../context';
import { GGDLoader } from './GGDLoader';
import {
  getSavedCards,
  notifyAppNotification,
  setupNewCard,
  updateCardDetails,
} from '../../events';
import { nonEmptyArray } from '../../utils';
// import * as console from 'console';

const CAPTIONS = {
  byClicking: {
    'en-us': 'By clicking pay, I acknowledge that I have reviewed and electronically agree to',
    'es-mx': 'Al hacer clic en pagar, reconozco que he revisado y acepto electrónicamente los',
  },
  terms: {
    'en-us': 'Terms and Conditions',
    'es-mx': 'Términos y condiciones',
  },
  and: {
    'en-us': 'and',
    'es-mx': 'y las',
  },
  policy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Políticas de Privacidad',
  },
  confirm: {
    'en-us': 'Confirm',
    'es-mx': 'Confirmar',
  },
  payment: {
    'en-us': 'Payment successful.',
    'es-mx': 'Pago exitoso.',
  },
};

const cardStyle = {
  hidePostalCode: true,
  iconStyle: 'solid',
  iconColor: '#000',
  style: {
    base: {
      color: '#32325d',
      fontFamily: 'Arial, sans-serif',
      fontSmoothing: 'antialiased',
      fontSize: '16px',
      '::placeholder': {
        color: '#32325d',
      },
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a',
    },
  },
};

const useClasses = makeStyles((theme) => ({
  link: {
    color: theme.palette.blue.main,
  },
  errorContainer: {
    height: '14px',
  },
  termsText: {
    fontSize: '0.875rem',
  },
  saveCardsContainer: {
    backgroundColor: theme.palette.ggd.light,
    borderRadius: '0.6rem',
    padding: '0.3rem 0.8rem',
  },
  select: {
    marginTop: '2px',
    '& :focus': {
      backgroundColor: 'transparent',
    },
  },
}));

const getFormattedCardNumber = (cardBrand, lastFourDigits) => {
  return (
    <Typography variant="body2" color="textPrimary">
      {cardBrand} ending in {lastFourDigits}
    </Typography>
  );
};

const StoreCardForm = ({
  totalPrice,
  handlePaymentSuccess = () => {},
  provider,
  productId,
  paymentInfo = {},
}) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const [succeeded, setSucceeded] = useState(false);
  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState('');
  const [loading, setLoading] = useState(true);
  const [showAddCardForm, setShowAddCardForm] = useState(false);
  const [selectedCardIndex, setSelectedCardIndex] = useState(0);
  const [storedCards, setStoredCards] = useState([]);
  const [disabled, setDisabled] = useState(true);
  const stripe = useStripe();
  const elements = useElements();

  // const isVaxYesPayment = provider === CONVERSATION_PROVIDER_ID.VAX_YES;

  const handleChange = async (event) => {
    // Listen for changes in the CardElement
    // and display any errors as the customer types their card details
    setDisabled(event.empty);
    setError(event.error ? event.error.message : '');
  };

  const handleCardSelect = (event) => {
    setSelectedCardIndex(event.target.value);
  };

  const handleSuccess = (paymentSourceId) => {
    const paymentData = {
      msg: 'Your card information has been securely stored.',
      amount: totalPrice,
      paymentSourceId,
      productId,
    };
    handlePaymentSuccess(paymentData);
  };

  const handleSubmitSelectedCard = () => {
    const card = storedCards[selectedCardIndex];
    handleSuccess(card.payment_source_id);
  };

  const newCardSetupHandler = async (error, data) => {
    if (error) {
      setError(`Payment failed. Please try again.`);
      setProcessing(false);
    } else {
      const { clientSecret, paymentSourceId } = data;

      const payload = await stripe.confirmCardSetup(clientSecret, {
        payment_method: {
          card: elements.getElement(CardElement),
        },
      });

      if (payload.error) {
        setError(`Payment failed ${payload.error.message}`);
        setProcessing(false);
      } else {
        updateCardDetails(
          (e, data) => {
            if (e) {
              setError(`Payment failed. Please try again.`);
              setProcessing(false);
              return;
            }
            setError(null);
            setProcessing(false);
            setSucceeded(true);
            handleSuccess(paymentSourceId);
          },
          {
            paymentMethodId: payload?.setupIntent?.payment_method,
            intentId: payload?.setupIntent?.id,
          },
        );
      }
    }
  };

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    setProcessing(true);
    setupNewCard(newCardSetupHandler);
  };

  useEffect(() => {
    getSavedCards((error, data) => {
      setLoading(false);
      if (error) {
        notifyAppNotification(null, {
          error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.',
        });
      } else if (nonEmptyArray(data)) {
        const cards = data.filter(
          (card) => card.payment_card_last4 && card.payment_card_last4.length > 3,
        );
        setStoredCards(cards);
        return;
      }
      setShowAddCardForm(true);
    }, {});
  }, []);

  if (loading) {
    return (
      <Box display="flex" justifyContent="center">
        <GGDLoader />
      </Box>
    );
  }

  const AgreeComponent = () => {
    return (
      <Typography color="textSecondary" className={classes.termsText}>
        {CAPTIONS.byClicking[store.locale]}{' '}
        <Link
          href="https://gogetdoc.com/terms"
          target="_blank"
          rel="noopener noreferrer"
          className={classes.link}
        >
          {CAPTIONS.terms[store.locale]}{' '}
        </Link>{' '}
        {CAPTIONS.and[store.locale]}{' '}
        <Link
          href="https://gogetdoc.com/privacy"
          target="_blank"
          rel="noopener noreferrer"
          className={classes.link}
        >
          {CAPTIONS.policy[store.locale]}
        </Link>
        .
      </Typography>
    );
  };

  if (!showAddCardForm && storedCards.length > 0) {
    return (
      <>
        <Box className={classes.saveCardsContainer}>
          <Grid container alignItems="center" justifyContent="space-between">
            <Grid item>
              <Grid container spacing={1} alignItems="center">
                <Grid item>
                  <CreditCardIcon fontSize="small" style={{ marginTop: '4px' }} />
                </Grid>
                <Grid item xs>
                  <Select
                    id="saved-cards-select"
                    defaultValue={1}
                    value={selectedCardIndex}
                    onChange={handleCardSelect}
                    disableUnderline
                    className={classes.select}
                  >
                    {storedCards.map((card, i) => (
                      <MenuItem key={card.payment_source_id} value={i}>
                        {getFormattedCardNumber(
                          card.payment_card_brand.toUpperCase(),
                          card.payment_card_last4,
                        )}
                      </MenuItem>
                    ))}
                  </Select>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs>
              <Box textAlign="right">
                <Link component="button" underline="none" onClick={() => setShowAddCardForm(true)}>
                  <Typography variant="body2">New Card</Typography>
                </Link>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Box mt={3}>
          <AgreeComponent />
        </Box>
        <Box mt={4}>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <SecureSubmitButton
                onClick={handleSubmitSelectedCard}
                label={
                  processing ? <div className={styles.spinner} /> : CAPTIONS.confirm[store.locale]
                }
              />
            </Grid>
          </Grid>
        </Box>
      </>
    );
  }

  return (
    <div className={styles.form}>
      <CardElement className={styles.input} options={cardStyle} onChange={handleChange} />
      <Box className={classes.errorContainer}>
        {error && (
          <Typography variant="body2" color="error">
            {error}
          </Typography>
        )}
      </Box>
      <Box mt={1}>
        <AgreeComponent />
      </Box>
      <Box mt={4}>
        <Grid container alignItems="center" justifyContent="space-between">
          <Grid item>
            {storedCards.length > 0 && (
              <Link component="button" underline="none" onClick={() => setShowAddCardForm(false)}>
                <Typography variant="body2">Saved Cards</Typography>
              </Link>
            )}
          </Grid>
          <Grid item>
            <SecureSubmitButton
              onClick={handleSubmit}
              disabled={processing || disabled || succeeded}
              label={
                processing ? <div className={styles.spinner} /> : CAPTIONS.confirm[store.locale]
              }
            />
          </Grid>
        </Grid>
      </Box>
      {succeeded && CAPTIONS.payment[store.locale]}
    </div>
  );
};

export { StoreCardForm };
