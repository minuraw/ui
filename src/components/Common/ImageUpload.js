import React from 'react';
import PropTypes from 'prop-types';
import { toBase64 } from './utils';
import { useSnackbar } from 'notistack';

const ImageUpload = ({ buttonText = 'Choose image', onUploadImage = () => {} }) => {
  const { enqueueSnackbar } = useSnackbar();

  const handleChange = async (event) => {
    if (event.target.files && event.target.files[0]) {
      const image = event.target.files[0];

      if (!image) return;
      const base64Image = await toBase64(image).catch((error) => Error(error));

      if (base64Image instanceof Error)
        return enqueueSnackbar('Error uploading image', { variant: 'error' });

      onUploadImage(base64Image, image.name, image.type);
    }
  };

  return (
    <>
      <div>
        <h4 style={{ textAlign: 'center' }}>Select Image</h4>
        <input type="file" name="myImage" onChange={handleChange} />
      </div>
    </>
  );
};

ImageUpload.propTypes = {
  buttonText: PropTypes.string,
};

export { ImageUpload };
