// import Link from 'next/link';
import { Box, Typography, Link as MuiLink, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  ul: {
    listStyle: 'none',
    margin: 0,
    padding: 0,
    '& li': {
      display: 'inline',
    },
    '& li+li': {
      paddingLeft: theme.spacing(1),
      marginLeft: theme.spacing(1),
    },
    '&:hover': {
      cursor: 'pointer',
    },
  },
  link: {
    textDecoration: 'none',
  },
  linkText: {
    color: theme.palette.blue.main,
  },
}));

const InlineList = ({ items }) => {
  const classes = useStyles();

  if (!Array.isArray(items)) return null;

  return (
    <Box>
      <Typography component="ul" className={classes.ul}>
        {items.map((item, index) => (
          <Typography component="li" variant="body2" key={index} className={classes.linkText}>
              <MuiLink
                target="_blank"
                rel="noreferrer"
                href={item.link}
                color="inherit"
                underline="none"
                className={classes.link}
              >
                {item.label}
              </MuiLink>
          </Typography>
        ))}
      </Typography>
    </Box>
  );
};

export { InlineList };
