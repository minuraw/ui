import React, { useEffect, useState } from 'react';
// eslint-disable-next-line import/no-named-default
import { default as ReactCamera, FACING_MODES } from 'react-html5-camera-photo';
import { Box, makeStyles, CircularProgress, Typography, Button, useMediaQuery } from '@material-ui/core';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CenterFocusStrongIcon from '@material-ui/icons/CenterFocusStrong';
// import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import CameraSwitchIcon from '@material-ui/icons/Cached';
import 'react-html5-camera-photo/build/css/index.css';
import { Alert } from '@material-ui/lab';
import { v4 as uuidv4 } from 'uuid';

const useClasses = makeStyles((theme) => ({
  cameraContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
    minWidth: '80vw',
    borderRadius: '1rem',
    [theme.breakpoints.up('sm')]: {
      width: '70%',
      minWidth: '30vw',
    },
    margin: 'auto',
    '& video': {
      width: '100%',
      borderRadius: '1rem',
    },
    '& img': {
      position: 'relative',
      top: '0.1rem',
      width: '100%',
      borderRadius: '1rem',
    },
    '& #container-circles': {
      zIndex: 300,
      position: 'absolute',
      width: '100%',
      height: '100%',
      left: 0,
      bottom: 0,
    },
    '& #inner-circle': {
      display: 'none',
    },
    '& #outer-circle': {
      cursor: 'pointer',
      left: 0,
      right: 0,
      bottom: 14,
      margin: 'auto',
      width: '3rem',
      height: '3rem',
      backgroundColor: 'transparent',
    },
  },
  loaderContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '30vh',
  },
  alertContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '30vh',
  },
  lastphoto: {
    maxWidth: '100%',
  },
  selfieMode: {
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    top: 10,
    left: 10,
    zIndex: 100,
    backgroundColor: theme.palette.primary.main,
    padding: '0.3rem 1rem',
    borderRadius: '0.6rem',
  },
  selfieText: {
    color: '#fff',
  },
  selfieIcon: {
    fill: '#fff',
    fontSize: 15,
    margin: '0 0.3rem 0 0',
  },
  selfieAnimation: {
    animationName: '$blinker',
    animationDuration: '1s',
    animationTimingFunction: 'linear',
    animationIterationCount: 'infinite',
  },
  '@keyframes blinker': {
    from: { opacity: 1 },
    to: { opacity: 0 },
  },
  captureBtnIconContainer: {
    width: 'fit-content',
    cursor: 'pointer',
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    margin: 'auto',
    zIndex: 200,
  },
  captureBtnIcon: {
    fill: theme.palette.grey[700],
    backgroundColor: '#fff',
    borderRadius: '50%',
    width: '3rem',
    height: '3rem',
    padding: '0.5rem',
  },
  // settingsBtnIconContainer: {
  //   zIndex: 300,
  //   backgroundColor: '#fff',
  //   borderRadius: '50%',
  //   minWidth: 0,
  //   padding: '0.5rem',
  //   position: 'absolute',
  //   left: 10,
  //   bottom: 12,
  //   '&:hover': {
  //     backgroundColor: theme.palette.grey[700],
  //     '& $settingsBtnIcon': {
  //       fill: '#fff',
  //     },
  //   },
  //   '&:active': {
  //     transform: 'scale(0.9)',
  //   },
  //   transition: '0.2s',
  // },
  // settingsBtnIcon: {
  //   fontSize: 30,
  //   fill: theme.palette.grey[700],
  // },
  switchCameraBtn: {
    zIndex: 400,
    border: '1px solid #fff',
    backgroundColor: 'transparent',
    borderRadius: '50%',
    minWidth: 0,
    padding: '0.3rem',
    position: 'absolute',
    right: 10,
    top: 12,
    '&:hover': {
      backgroundColor: 'transparent',
    },
    '&:active': {
      transform: 'scale(0.9)',
    },
    transition: '0.2s',
  },
  switchCameraBtnIcon: {
    fontSize: 20,
    fill: theme.palette.background.default,
    backgroundColor: 'transparent',
  },
  switchFrontCameraAnimation: {
    animationName: '$turnRear',
    animationDuration: '0.3s',
    animationTimingFunction: 'linear',
  },
  '@keyframes turnRear': {
    from: { transform: 'rotateY(0deg)' },
    to: { transform: 'rotateY(180deg)' },
  },
  switchRearCameraAnimation: {
    animationName: '$turnFront',
    animationDuration: '0.3s',
    animationTimingFunction: 'linear',
  },
  '@keyframes turnFront': {
    from: { transform: 'rotateY(0deg)' },
    to: { transform: 'rotateY(-180deg)' },
  },
}));

const CAMERA_ACCESS_NOT_CHECKED = 'not checked';
const CAMERA_ACCESS_CHECKED_NO_ACCESS = 'no access';
const CAMERA_ACCESS_CHECKED_SUCCESS = 'success';

const Camera = ({ onTakePhoto }) => {
  const classes = useClasses();
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));
  const [hasCameraAccess, setHasCameraAccess] = useState(CAMERA_ACCESS_NOT_CHECKED);
  const [frontCamera, setFrontCamera] = useState(true)

  useEffect(() => {
    const getMedia = async () => {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          video: true,
        });

        if (stream.active && stream.id) setHasCameraAccess(CAMERA_ACCESS_CHECKED_SUCCESS);
      } catch (err) {
        setHasCameraAccess(CAMERA_ACCESS_CHECKED_NO_ACCESS);
      }
    };

    getMedia();
  }, []);

  return (
    <Box
      className={`${classes.cameraContainer} ${
        smallScreen
          ? frontCamera
            ? classes.switchFrontCameraAnimation
            : classes.switchRearCameraAnimation
          : null
      }`}
      style={{ backgroundColor: hasCameraAccess !== CAMERA_ACCESS_CHECKED_SUCCESS && '#000' }}
    >
      {hasCameraAccess === CAMERA_ACCESS_NOT_CHECKED && (
        <Box className={classes.loaderContainer}>
          <CircularProgress />
        </Box>
      )}
      {hasCameraAccess === CAMERA_ACCESS_CHECKED_SUCCESS && (
        <>
          <Box className={classes.selfieMode}>
            <FiberManualRecordIcon className={`${classes.selfieIcon} ${classes.selfieAnimation}`} />
            <Typography variant="h6" className={classes.selfieText}>
              {frontCamera ? 'Selfie' : 'Rear'}
            </Typography>
          </Box>

          {smallScreen && (
            <Button
              className={classes.switchCameraBtn}
              onClick={() => setFrontCamera(!frontCamera)}
            >
              <CameraSwitchIcon className={classes.switchCameraBtnIcon} />
            </Button>
          )}

          <ReactCamera
            className={classes.camera}
            onTakePhotoAnimationDone={(dataUri) => onTakePhoto(dataUri, uuidv4(), 'image/png')}
            idealFacingMode={frontCamera ? FACING_MODES.USER : FACING_MODES.ENVIRONMENT}
            isImageMirror={false}
          />

          <Box className={classes.captureBtnIconContainer}>
            <CenterFocusStrongIcon className={classes.captureBtnIcon} />
          </Box>

          {/* <Button className={classes.settingsBtnIconContainer}>
            <SettingsOutlinedIcon className={classes.settingsBtnIcon} />
          </Button> */}
        </>
      )}
      {hasCameraAccess === CAMERA_ACCESS_CHECKED_NO_ACCESS && (
        <Box className={classes.alertContainer}>
          <Alert color="info">We cannot access your camera.</Alert>
        </Box>
      )}
    </Box>
  );
};

export { Camera };
