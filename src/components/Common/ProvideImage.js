/* eslint-disable no-undef */
import { useState, useRef } from 'react';
import { Camera } from './Camera';
import {
  Box,
  Link,
  Grid,
  Typography,
  makeStyles,
  useMediaQuery,
  ButtonBase,
  Paper,
  SvgIcon,
} from '@material-ui/core';
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined';
import AddAPhotoOutlinedIcon from '@material-ui/icons/AddAPhotoOutlined';
import { toBase64 } from './utils';
import Image from 'next/image';
import { getLocale } from '../../services';

import { getFileUploadUrl, notifyAppNotification } from '../../events';
import { GGDLoader } from './GGDLoader';
import { ActionDrawerWrapper } from '../Chat';

const CAPTIONS = {
  dragAndDropImage: {
    'en-us': 'Drag and drop your image here or browse',
    'es-mx': 'Arrastra y suelta tu imagen aquí o navega.',
  },
  uploadOrPhoto: {
    'en-us': 'Take a picture or upload a photo',
    'es-mx': 'Toma una foto o sube una foto',
  },
  uploadYourImage: {
    'en-us': 'Upload File',
    'es-mx': 'Subir archivo',
  },
  supportTypes: {
    'en-us': 'Supports JPEG, or PNG Files up to 5MB',
    'es-mx': 'Admite archivos JPEG o PNG de hasta 5MB',
  },
  takePicture: {
    'en-us': 'Take a Picture',
    'es-mx': 'Toma una foto',
  },
  uploadFile: {
    'en-us': 'Or upload a file instead',
    'es-mx': 'O sube un archivo en su lugar',
  },
  uploadAnother: {
    'en-us': 'Make sure your image is clear. Try another?',
    'es-mx': 'Subir otro',
  },
  allEncrypted: {
    'en-us': 'All photos are encrypted and visible to authorized personnel only.',
    'es-mx': 'Todas las fotos están encriptadas y solo son visibles para el personal autorizado.',
  },
  continue: {
    'en-us': 'Continue',
    'es-mx': 'Continuar',
  },
  imageOversize: {
    'en-us': 'Image must be up to 20MB',
    'es-mx': 'La imagen debe ser de hasta 20MB',
  },
  uploadError: {
    'en-us': 'Upload error. Please try again.',
    'es-mx': 'Error al Subir. Inténtalo de nuevo.',
  },
};

const useClasses = makeStyles((theme) => ({
  link: {
    color: theme.palette.blue.main,
  },
  image: {
    objectFit: 'contain',
  },
  actionButton: {
    width: '100%',
    display: 'block',
    textAlign: 'center',
  },
  icon: {
    fontSize: '2.5rem',
    color: theme.palette.blue.main,
  },
  buttonCaption: {
    fontSize: '1rem',
  },
}));

const PaperButton = ({ buttonLabel, icon, onClick = () => {} }) => {
  const classes = useClasses();

  return (
    <ButtonBase disableRipple className={classes.actionButton} onClick={onClick}>
      <Paper variant="outlined">
        <Box p={1}>
          {icon && (
            <Box p={1}>
              <SvgIcon className={classes.icon}>{icon}</SvgIcon>
            </Box>
          )}
          <Typography variant="subtitle1" className={classes.buttonCaption}>
            {buttonLabel}
          </Typography>
        </Box>
      </Paper>
    </ButtonBase>
  );
};

const ProvideImage = ({ handleImage = () => {}, uploadDisabled = false, imageKey }) => {
  const [showCamera, setShowCamera] = useState(false);
  const [image, setImage] = useState('');
  const [name, setName] = useState('');
  const [type, setType] = useState('');
  const [oversize, setOversize] = useState(false);
  const [isUploading, setIsUploading] = useState(false);

  const fileInputRef = useRef();

  const classes = useClasses();
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  const locale = getLocale();

  const handleShowImageUpload = () => {
    setShowCamera(false);
  };

  const handleShowCamera = () => {
    setOversize(false);
    setShowCamera(true);
  };

  // const dropHandler = (event) => {
  //   event.preventDefault();
  //   if (event.dataTransfer.items) {
  //     const files = [];
  //     for (let i = 0; i < event.dataTransfer.items.length; i++) {
  //       if (event.dataTransfer.items[i].kind === 'file') {
  //         files.push(event.dataTransfer.items[i].getAsFile());
  //       }
  //     }
  //     if (files.length > 0) {
  //       handleInputFiles(files);
  //     }
  //   } else {
  //     if (event.dataTransfer.files.length > 0) {
  //       handleInputFiles(event.dataTransfer.files);
  //     }
  //   }
  // };

  // const onDragOver = (event) => {
  //   event.preventDefault();
  // };

  const tryAnother = () => {
    setImage('');
    setName('');
    setType('');
    if (fileInputRef && fileInputRef.current) {
      fileInputRef.current.value = '';
    }
  };

  const handleFileChange = async (event) => {
    if (event.target.files && event.target.files.length > 0) {
      handleInputFiles(event.target.files);
    }
  };

  const handleInputFiles = async (files) => {
    for (let i = 0; i < files.length; i++) {
      const file = files[i];

      if (!file) {
        break;
      }

      if ((file.size / 1024 / 1024).toFixed(1) > 20) {
        setOversize(true);
        break;
      }

      if (file.type.match(/image|(jpg|jpeg|png|gif)$/)) {
        const base64Image = await toBase64(file).catch((error) => Error(error));

        if (base64Image instanceof Error) {
          throw base64Image;
        }
        setOversize(false);
        setImage(base64Image);
        setName(file.name);
        setType(file.type);
      }
    }
  };

  const handleBrowse = () => {
    fileInputRef.current.click();
  };

  const handleCameraPhoto = (photo, name, type) => {
    console.log(photo);
    if (((photo.length - 'data:image/png;base64,'.length) / 1024 / 1024).toFixed(1) > 20) {
      setOversize(true);
    } else {
      setOversize(false);
      setImage(photo);
      setName(name + '.png');
      setType(type);
    }
  };

  // return a promise that resolves with a File instance
  const URLtoFile = (url, filename, mimeType) => {
    return fetch(url)
      .then(function (res) {
        return res.arrayBuffer();
      })
      .then(function (buf) {
        return new File([buf], filename, { type: mimeType });
      });
  };

  const promisifyFileUpload = (url, file) => {
    return new Promise((resolve, reject) => {
      const uploadFileToURL = new XMLHttpRequest();
      uploadFileToURL.open('PUT', url, true);

      uploadFileToURL.onload = () => {
        if (uploadFileToURL.status >= 200 && uploadFileToURL.status < 300) {
          resolve();
        } else {
          reject(new Error('File upload error'));
        }
      };
      uploadFileToURL.send(file);
    });
  };

  const uploadFileToUrl = (data) => {
    setIsUploading(true);
    const { url, location } = data;
    (async function () {
      try {
        const file = await URLtoFile(image, name, type);

        await promisifyFileUpload(url, file);
        handleImage(image, name, type, location);
      } catch (err) {
        // error occurred
        notifyAppNotification(null, {
          error: CAPTIONS.uploadError[locale],
        });
        tryAnother();
      } finally {
        setIsUploading(false);
      }
    })();
  };

  const handleFileUploadLinkLoad = (err, data) => {
    setIsUploading(false);
    if (err) {
      // error occurred
      notifyAppNotification(null, {
        error: CAPTIONS.uploadError[locale],
      });
      tryAnother();
    } else {
      uploadFileToUrl(data);
    }
  };

  const handleUploadImage = () => {
    setIsUploading(true);
    if (!imageKey) {
      handleImage(image, name, type);
    } else {
      getFileUploadUrl(handleFileUploadLinkLoad, { filePath: imageKey });
    }
  };

  if (isUploading) {
    return (
      <Box display="flex" justifyContent="center">
        <GGDLoader />
      </Box>
    );
  }

  return (
    <ActionDrawerWrapper
      secondaryText={{ text: CAPTIONS.allEncrypted[locale] }}
      buttonLabel={CAPTIONS.continue[locale]}
      buttonDisabled={!image}
      onClick={handleUploadImage}
    >
      <Grid container spacing={2} justifyContent="center">
        {!showCamera && !image && (
          <>
            {smallScreen ? (
              <Grid item xs={12} sm={4}>
                <PaperButton
                  buttonLabel={CAPTIONS.uploadOrPhoto[locale]}
                  icon={<AddAPhotoOutlinedIcon />}
                  onClick={handleBrowse}
                />
              </Grid>
            ) : (
              <>
                <Grid item xs={6} sm={4}>
                  <PaperButton
                    buttonLabel={CAPTIONS.takePicture[locale]}
                    icon={<AddAPhotoOutlinedIcon />}
                    onClick={handleShowCamera}
                  />
                </Grid>
                <Grid item xs={6} sm={4}>
                  <PaperButton
                    buttonLabel={CAPTIONS.uploadYourImage[locale]}
                    icon={<CloudUploadOutlinedIcon />}
                    onClick={handleBrowse}
                  />
                </Grid>
              </>
            )}
          </>
        )}
      </Grid>
      <Box mt={smallScreen ? 1 : 2}>
        <Grid container direction="column" justifyContent="center" alignItems="center" spacing={3}>
          {showCamera && !image && (
            <Grid item>
              <Camera
                onTakePhoto={(photo, name, type) => {
                  handleCameraPhoto(photo, name, type);
                }}
              />
              {(!smallScreen || (!uploadDisabled && smallScreen)) && (
                <Box mt={1} mb={2}>
                  <Typography variant="body2" align="center">
                    <Link
                      component="button"
                      className={classes.link}
                      onClick={handleShowImageUpload}
                    >
                      {CAPTIONS.uploadFile[locale]}
                    </Link>
                  </Typography>
                </Box>
              )}
            </Grid>
          )}
          {image && (
            <Grid item>
              <Paper variant="outlined">
                <Box p={2}>
                  <Image
                    src={image}
                    alt="Preview"
                    width={420}
                    height={240}
                    className={classes.image}
                  />
                </Box>
              </Paper>
              <Box mt={1} mb={2}>
                <Typography variant="body2" align="center">
                  <Link component="button" onClick={tryAnother} className={classes.link}>
                    {CAPTIONS.uploadAnother[locale]}
                  </Link>
                </Typography>
              </Box>
            </Grid>
          )}
          {oversize && (
            <Box display="flex" justifyContent="center">
              <Typography variant="body2" style={{ color: 'red' }}>
                {CAPTIONS.imageOversize[locale]}
              </Typography>
            </Box>
          )}
        </Grid>
      </Box>
      <input
        ref={fileInputRef}
        style={{ display: 'none' }}
        type="file"
        name="myImage"
        onChange={handleFileChange}
      />
    </ActionDrawerWrapper>
  );
};

export { ProvideImage };
