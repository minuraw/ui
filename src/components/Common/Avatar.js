import { stringToColor } from './utils';
import { Avatar as MuiAvatar, makeStyles } from '@material-ui/core';

const useClasses = makeStyles((theme) => ({
  small: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    fontSize: '0.9rem',
    paddingTop: '2px',
  },
}));

const stringAvatar = (name) => {
  const wordsInName = name.split(' ').filter((namePart) => namePart);
  if (wordsInName.length === 0) return;

  let avatarText = wordsInName[0][0];
  if (wordsInName.length > 1) {
    avatarText += wordsInName[1][0];
  }

  return {
    style: {
      backgroundColor: stringToColor(name),
    },
    children: avatarText,
  };
};

const Avatar = ({ src, firstName = '', lastName = '', small }) => {
  const classes = useClasses();

  return (
    <>
      <MuiAvatar
        src={src}
        {...stringAvatar(`${firstName} ${lastName}`)}
        className={small ? classes.small : ''}
      />
    </>
  );
};

export { Avatar };
