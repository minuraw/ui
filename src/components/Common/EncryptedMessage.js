import { Box, makeStyles } from '@material-ui/core';
import { getLocale } from '../../services';
import LockIcon from '@material-ui/icons/Lock';

const CAPTIONS = {
  message: {
    'en-us': 'Messages are end-to-end encrypted. Click to learn more.',
    'es-mx': 'Los mensajes están cifrados de extremo a extremo. Pulsa para más información.',
  },
  secure_link: {
    'en-us': 'https://help.gogetdoc.com/en/articles/5461766-is-this-secure',
    'es-mx': 'https://help.gogetdoc.com/es/articles/5461766-is-this-secure',
  },
};

const useClasses = makeStyles((theme) => ({
  container: {
    margin: '1rem',
    [theme.breakpoints.up('sm')]: {
      margin: '1rem auto',
    },
    width: 'fit-content',
    backgroundColor: '#FFF5C4',
    boxShadow: '0 1px 1px rgb(0 0 0 / 10%)',
    padding: '0.4rem 0.8rem',
    borderRadius: '0.5rem',
    cursor: 'pointer',
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
  },
  lockIcon: {
    fontSize: '0.8rem',
    color: theme.palette.text.primary,
    marginRight: '0.2rem',
  },
}));

const EncryptedMessage = () => {
  const classes = useClasses();
  const locale = getLocale();

  return (
    <a className={classes.link} href={CAPTIONS.secure_link[locale]} target="_blank" rel="noopener noreferrer">
        <Box className={classes.container}>
            <LockIcon className={classes.lockIcon} />
            {CAPTIONS.message[locale]}
        </Box>
    </a>
  );
};

export { EncryptedMessage };