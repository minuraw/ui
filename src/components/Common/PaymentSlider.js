import { useContext } from 'react'
import { Slider, withStyles, Typography, Box, Grid } from '@material-ui/core';
import { SecureSubmitButton } from '../Chat';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  next: {
    'en-us': 'Next',
    'es-mx': 'Siguiente',
  },
};

const CustomSlider = withStyles({
  root: {
    height: 6,
  },
  thumb: {
    height: 18,
    width: 18,
    marginTop: -6,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 6,
    borderRadius: 4,
  },
  rail: {
    height: 6,
    borderRadius: 4,
  },
})(Slider);

const PaymentSlider = ({
  title,
  secondaryText,
  contributionText,
  value,
  setValue = () => {},
  defaultAmount,
  minAmount,
  maxAmount,
  onSubmit = () => {},
}) => {
  const { store } = useContext(GlobalStore);

  return (
    <Box p={2} style={{ overflowX: 'hidden' }} textAlign="center">
      {(title || secondaryText) && (
        <Box mb={3}>
          {title && (
            <Typography gutterBottom variant="h5">
              {title}
            </Typography>
          )}
          {secondaryText && <Typography>{secondaryText}</Typography>}
        </Box>
      )}
      <Box mb={2}>
        <Typography variant="h5">${value}</Typography>
      </Box>
      <CustomSlider
        data-cy="payment_slider"
        key={`slider-${defaultAmount}`}
        defaultValue={defaultAmount}
        min={minAmount}
        max={maxAmount}
        step={1}
        onChange={(e, value) => setValue(value)}
        aria-label="Payment slider"
        aria-valuetext="Current amount"
        getAriaLabel={() => `$${value}`}
      />
      {contributionText && (
        <Box my={2}>
          <Typography>{contributionText}</Typography>
        </Box>
      )}
      <Box mt={5}>
        <Grid container justifyContent="flex-end">
          <Grid item>
            <SecureSubmitButton
              label={CAPTIONS.next[store.locale]}
              onClick={() => onSubmit(value)}
            />
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export { PaymentSlider };
