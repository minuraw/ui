import React, { useEffect, useState, useContext } from 'react';
import { Button, Menu, MenuItem, makeStyles } from '@material-ui/core';
import LanguageOutlinedIcon from '@material-ui/icons/LanguageOutlined';
import { setLocale } from '../../services';
import { isServerSide } from '../../utils';
import { Store as GlobalStore } from '../../context';

const LANGUAGES = [
  {
    key: 'en-us',
    label: 'English',
  },
  {
    key: 'es-mx',
    label: 'Español',
  },
];

const LANGUAGE_SHORT = {
  'en-us': 'EN',
  'es-mx': 'SP',
};

const useClasses = makeStyles((theme) => ({
  inverse: {
    color: theme.palette.common.white,
  },
}));

const LanguageToggle = ({ inverse = false }) => {
  const { setInStore } = useContext(GlobalStore);
  const classes = useClasses();

  const locale = isServerSide() ? 'en-us' : window.localStorage.getItem('locale') || 'en-us';

  const [language, setLanguage] = useState(LANGUAGE_SHORT[locale]);
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    setInStore({ locale: locale }, true);
  }, []);

  const handleOpenMenu = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleToggleLanguage = (e, lang) => {
    setLanguage(LANGUAGE_SHORT[lang]);
    setLocale(lang);
    setInStore({ locale: lang }, true);
    handleCloseMenu();
  };

  return (
    <>
      <Button
        disableRipple
        size="small"
        startIcon={<LanguageOutlinedIcon />}
        onClick={handleOpenMenu}
        style={{ backgroundColor: 'transparent' }}
        className={inverse ? classes.inverse : null}
      >
        {language.toUpperCase()}
      </Button>
      <Menu keepMounted anchorEl={anchorEl} open={!!anchorEl} onClose={handleCloseMenu}>
        {LANGUAGES.map((language) => (
          <MenuItem key={language.key} onClick={(e) => handleToggleLanguage(e, language.key)} dense>
            {language.label}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export { LanguageToggle };
