import {
  Grid,
  Box,
  Typography,
  Chip,
  makeStyles,
  CircularProgress,
  Button,
  InputBase,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { useCallback, useState } from 'react';
import { detectIntent } from '../../events';
import { navigateToNewChat } from '../../utils';
import { getLocale } from '../../services';

import { useRouter } from 'next/router';

const CAPTIONS = {
  suggestion: {
    'en-us': 'Suggestion:',
    'es-mx': 'Sugerencia:',
  },
};

const INTENT_SKILL_MAP = {
  AcneProtocol: {
    skill: 'acne',
    productId: 1,
  },
  MigraineRefillsProtocol: {
    skill: 'migraine_refills',
    productId: 1,
  },
};

const SEARCH_SUGGESTIONS = [
  { label: 'Acne', skill: 'acne', sync: false, productId: 1 },
  { label: 'Migraine Refills', skill: 'migraine_refills', sync: false, productId: 1 },
  { label: 'Migraine Refills Video', skill: 'migraine_refills', sync: true, productId: 1 },
];

const useStyles = makeStyles((theme) => ({
  chip: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.primary.main,
  },
  searchBox: {
    backgroundColor: theme.palette.grey[100],
    borderRadius: theme.shape.borderRadius,
  },
}));

const IntentSearch = ({ suggestions = false }) => {
  const classes = useStyles();
  const router = useRouter();
  const locale = getLocale();

  const [searchText, setSearchText] = useState('');
  const [loading, setLoading] = useState(false);

  const changeHandler = useCallback((event) => {
    setSearchText(event.target.value);
  }, []);

  const detectIntentHandler = useCallback(
    (error, intent) => {
      if (error) {
        setLoading(false);
        return;
      }
      if (intent === 'Unknown') {
        setLoading(false);
      } else if (intent === 'SymptomCheck') {
        setLoading(false);
      } else if (INTENT_SKILL_MAP[intent]) {
        // this is a valid protocol
        navigateToNewChat(
          router,
          INTENT_SKILL_MAP[intent].productId,
          INTENT_SKILL_MAP[intent].skill,
          false,
        );
      } else {
        // invalid protocol
      }
    },
    [router],
  );

  const searchHandler = useCallback(() => {
    if (!searchText) return;

    setLoading(true);
    detectIntent(detectIntentHandler, { searchText });
  }, [searchText, detectIntentHandler]);

  const keyPressHandler = useCallback(
    (e) => {
      if (e.which === 13) {
        // Enter key press
        searchHandler();
      }
    },
    [searchHandler],
  );

  return (
    <>
      <Box p={2} className={classes.searchBox}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item xs>
            <Box pr={2}>
              {loading ? (
                <CircularProgress size="1rem" thickness={6} />
              ) : (
                <InputBase
                  placeholder={locale === 'es-mx' ? 'Ingrese sus síntomas o consultas...' : 'Enter your symptoms or questions...'}
                  fullWidth
                  onChange={changeHandler}
                  onKeyPress={keyPressHandler}
                />
              )}
            </Box>
          </Grid>
          <Grid item>
            <Button color="primary" size="small" onClick={searchHandler} disabled={loading}>
              <SearchIcon />
            </Button>
          </Grid>
        </Grid>
      </Box>
      {suggestions && (
        <Box mt={3}>
          <Grid container alignItems="center" justifyContent="center" spacing={1}>
            <Grid item xs={4} sm={3}>
              <Typography variant="body1" color="textSecondary">
                {CAPTIONS.suggestion[locale]}
              </Typography>
            </Grid>
            {SEARCH_SUGGESTIONS.map((suggestion, index) => (
              <Grid item key={index}>
                <Chip
                  label={suggestion.label}
                  onClick={() => {
                    navigateToNewChat(
                      router,
                      suggestion.productId,
                      suggestion.skill,
                      suggestion.sync,
                    );
                  }}
                  className={classes.chip}
                />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}
    </>
  );
};

export { IntentSearch };
