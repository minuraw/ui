import { useEffect, useState, useContext } from 'react';
import { PaymentForm, GGDLoader } from '../Common';
import { PaymentProductCard } from '../Cards';
import { getProductPaymentInfo, verifyMemberPayment } from '../../events';
import {
  Typography,
  makeStyles,
  Tooltip,
  Box,
  IconButton,
  ClickAwayListener,
  useMediaQuery,
  Zoom,
} from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  error: {
    'en-us': 'Your payment could not be processed. Please refresh the page. Visit the GoGetDoc help center if this error persists.',
    'es-mx': 'Su pago no pudo ser procesado. Actualice la página. Visite el centro de ayuda de GoGetDoc si este error persiste.',
  },
  complete: {
    'en-us': 'Complete your',
    'es-mx': 'Complete su pago de',
  },
};

const useClasses = makeStyles((theme) => ({
  paymentText: {
    fontSize: '1.5rem',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1rem',
    },
    color: '#171725',
  },
  link: {
    color: '#0000FF',
  },
  tooltipIcon: {
    fontSize: '1.1rem',
    [theme.breakpoints.down('sm')]: {
      marginBottom: '0.1rem',
      fontSize: '1rem',
    },
  },
}));

const getProviderText = (product) => {
  const { providerNames } = product || {};
  if (!providerNames) return '';
  const listOfProviderNames = providerNames.join(',');
  return listOfProviderNames || 'GoGetDoc';
};

const Payment = ({
  handlePaymentSuccess = () => {},
  productId,
  sync,
  skill,
  price = 0,
  convenienceFee = 0,
  provider,
  minAmount,
}) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const [isDataLoading, setIsDataLoading] = useState(true);
  const [paymentInfo, setPaymentInfo] = useState(null);
  const [error, setError] = useState(null);
  const [showTooltip, setShowTooltip] = useState(false);

  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));

  const handlePaymentLoading = (error, paymentInfo) => {
    setIsDataLoading(false);
    if (!error) {
      setPaymentInfo(paymentInfo);
      return;
    }
    setError(error);
  };

  useEffect(() => {
    setIsDataLoading(true);
    getProductPaymentInfo(handlePaymentLoading, { productId, sync, protocol: skill, price });
  }, []);

  const handleTooltipClick = () => setShowTooltip((currentState) => !currentState);

  if (error) {
    return <>{CAPTIONS.error[store.locale]}</>;
  }
  if (isDataLoading || !paymentInfo) {
    return (
      <Box align="center">
        <GGDLoader />
      </Box>
    );
  }

  const handleTooltipOpen = () => {
    if (smallScreen) return;

    setShowTooltip(true);
  };

  const handleTooltipClose = () => {
    if (smallScreen) return;

    setShowTooltip(false);
  };

  const onPaymentSuccess = (payload) => {
    handlePaymentSuccess(payload);
    // if the payment id is available verify the member payment
    if (paymentInfo.paymentId) {
      verifyMemberPayment(null, { paymentId: paymentInfo.paymentId });
    }
  };

  const { totalPrice, items } = paymentInfo;

  return (
    <Box p={1}>
      <Box mb={3}>
        <Typography variant="body1" className={classes.paymentText}>
          {CAPTIONS.complete[store.locale]} <strong>${totalPrice}</strong>
          {store.locale === 'en-us' && ' payment'}{' '}
          <span>
            <ClickAwayListener onClickAway={() => setShowTooltip(false)}>
              <Tooltip
                title={items.map((product) => {
                  return (
                    <PaymentProductCard
                      key={product.productId}
                      {...product}
                      providerText={getProviderText(product)}
                      convenienceFee={convenienceFee}
                      minAmount={minAmount}
                    />
                  );
                })}
                placement="top"
                open={showTooltip}
                arrow
                onOpen={handleTooltipOpen}
                onClose={handleTooltipClose}
                enterTouchDelay={500}
                TransitionComponent={Zoom}
              >
                <IconButton onClick={handleTooltipClick} label="Payment information">
                  <ErrorOutlineIcon className={classes.tooltipIcon} />
                </IconButton>
              </Tooltip>
            </ClickAwayListener>
          </span>
        </Typography>
      </Box>
      <Box>
        <PaymentForm
          {...paymentInfo}
          handlePaymentSuccess={onPaymentSuccess}
          provider={provider}
          paymentInfo={paymentInfo}
        />
      </Box>
    </Box>
  );
};

export { Payment };
