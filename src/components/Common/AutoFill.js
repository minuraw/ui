import { useEffect, useState, useCallback, useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import {
  TextField,
  CircularProgress,
  Chip,
  Box,
  Grid,
  InputAdornment,
  makeStyles,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import { Search } from '@material-ui/icons';

import { FixedSizeList as List } from 'react-window';

const useClasses = makeStyles((theme) => ({
  textField: {
    '& .MuiInputLabel-root': {
      color: theme.palette.grey[500],
      top: '0.5rem',
      paddingLeft: '1.5rem',
    },
  },
  input: {
    backgroundColor: theme.palette.ggd.light,
    color: theme.palette.grey[600],
    padding: '0.5rem 1rem',
    borderRadius: '1rem',
  },
  searchIcon: {
    fill: theme.palette.grey[500],
    fontSize: 20,
  },
  listContainer: {
    top: '-0.75rem',
    backgroundColor: theme.palette.ggd.light,
  },
  chip: {
    borderRadius: '20px',
    borderColor: theme.palette.grey[300],
    backgroundColor: '#fff',
    color: '#000',
    maxWidth: '80vw',
  },
}));

const CAPTIONS = {
  search: {
    'en-us': 'Search symptoms, e.g. headache',
    'es-mx': 'Buscar por síntoma, ej.: dolor de cabeza',
  },
  results: {
    'en-us': 'No results. Try using another word or phrase.',
    'es-mx': 'No hay resultados. Intenta usar otra palabra o frase.',
  },
};

const AutoFill = ({ getSuggestedWords, handleWordSelect }) => {
  // Suggested words are expected to be in the form of
  // word = { id, label, value }
  // id = unique identifier
  // label = value shown in the UI
  // value = any object that is expected to pass around as meta data, this is untouched by the component
  const [suggestedWords, setSuggestedWords] = useState([]);
  const [filledWord, setFilledWord] = useState('');

  const [dataLoading, setDataLoading] = useState(false);

  const [searchedWord, setSearchedWord] = useState('');

  const [selectedWords, setSelectedWords] = useState([]);

  const classes = useClasses();

  const { store } = useContext(GlobalStore);

  useEffect(() => {
    // If the searched word does not eqaul to result available word, search again
    if (filledWord !== searchedWord) getAutoFilledWords(searchedWord);
    // eslint-disable-next-line no-use-before-define
  }, [getAutoFilledWords, searchedWord, filledWord]);

  useEffect(() => {
    // When selected words change notify the parenet
    handleWordSelect(selectedWords);
  }, [selectedWords]);

  const handleSuggestedWordsLoad = (error, { filledWord, suggestedWords }) => {
    setDataLoading(false);
    if (error) {
      setFilledWord(filledWord);
      setSuggestedWords([]);
      return;
    }
    setFilledWord(filledWord);
    setSuggestedWords(suggestedWords);
  };

  const getAutoFilledWords = useCallback(
    (word) => {
      // Check if the data loading
      if (!dataLoading) {
        setDataLoading(true);
        getSuggestedWords(handleSuggestedWordsLoad, { searchText: word });
      }
    },
    [dataLoading, getSuggestedWords],
  );

  const handleTextInput = (e) => {
    const value = e.target.value || '';
    setSearchedWord(value);
  };

  const handleSuggestedWordClick = (word) => {
    setSelectedWords((words) => [...words, word]);
    setSearchedWord('');
    setFilledWord('');
    setSuggestedWords([]);
  };

  const handleSelectedWordDelete = (word) =>
    setSelectedWords((currentWords) =>
      currentWords.filter((currentWord) => currentWord.id !== word.id),
    );

  const showSuggestedWords = !dataLoading && suggestedWords.length !== 0;
  const showNoResults = !dataLoading && suggestedWords.length === 0 && !!filledWord;
  const showSelectedWords = !dataLoading && suggestedWords.length === 0;

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Grid container>
          <Grid item xs={12}>
            <TextField
              fullWidth
              onChange={handleTextInput}
              value={searchedWord}
              placeholder={CAPTIONS.search[store.locale]}
              InputProps={{
                disableUnderline: true,
                className: classes.input,
                endAdornment: !dataLoading ? (
                  <InputAdornment position="end">
                    <Search className={classes.searchIcon} />
                  </InputAdornment>
                ) : (
                  <CircularProgress size={20} />
                ),
              }}
            />
          </Grid>
          <Grid item xs={12}>
            {showSuggestedWords && (
              <List
                height={200}
                itemCount={suggestedWords.length}
                itemSize={46}
                className={classes.listContainer}
              >
                {({ index, style }) => {
                  const { label, id } = suggestedWords[index];
                  return (
                    <ListItem
                      style={style}
                      button
                      key={id}
                      onClick={() => handleSuggestedWordClick(suggestedWords[index])}
                    >
                      <ListItemText>{label}</ListItemText>
                    </ListItem>
                  );
                }}
              </List>
            )}
          </Grid>
          <Grid item xs={12}>
            <Box m={1}>{showNoResults && CAPTIONS.results[store.locale]}</Box>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={1}>
          {showSelectedWords &&
            selectedWords.map((word, index) => {
              return (
                <Grid item key={`${word.id}-${index}`}>
                  <Chip
                    variant="outlined"
                    label={word.label}
                    className={classes.chip}
                    onDelete={() => {
                      handleSelectedWordDelete(word);
                    }}
                  />
                </Grid>
              );
            })}
        </Grid>
      </Grid>
    </Grid>
  );
};

export { AutoFill };
