import React, { useContext, useState, useEffect } from 'react';
import { TextField, makeStyles } from '@material-ui/core';
import MaskedInput from 'react-text-mask';
import { Store as GlobalStore } from '../../context';

const useClasses = makeStyles(() => ({
  input: {
    fontSize: '1.3rem',
    fontWeight: 500,
  },
}));


const MaskedInputField = (props) => {
  // Make sure mask, place holeChar, showMask and other related props inside the props
  const { inputRef, ...other } = props;
  return (
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
    />
  );
};

const DateInput = ({ input, error }) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const [placeholder, setPlaceholder] = useState('MM/DD/YYYY')

  useEffect(() => {
     setPlaceholder(store.locale === 'es-mx' ? 'DD/MM/YYYY' : 'MM/DD/YYYY')
  }, [store.locale])

  return (
    <TextField
      {...input}
      autoFocus
      InputProps={{
        inputComponent: MaskedInputField,
        disableUnderline: true,
        className: classes.input,
      }}
      inputProps={{
        // mm/dd/yyyy
        mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
        showMask: false,
        placeholder: placeholder,
        guide: false,
      }}
      error={!!error}
      helperText={error || ''}
      fullWidth
      type="tel"
    />
  );
};

export { DateInput };
