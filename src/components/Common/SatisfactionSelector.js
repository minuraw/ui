import React, { useState, useRef, useEffect, useCallback, useContext } from 'react';
import { Box, makeStyles, TextField, Grid, Link } from '@material-ui/core';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import { Form, Field } from 'react-final-form';
// import { string, object } from 'yup';
// import { useYupValidation } from '../../hooks';
import { SecureSubmitButton } from '../Chat/SecureSubmitButton';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { getSurveyStatus, postSurvey } from '../../events';
import { v4 as uuidv4 } from 'uuid';
import { MessageGroup } from '../Common/MessageGroup';
import { Store as GlobalStore } from '../../context';

// const validationSchema = object({
//   feedback: string().required().trim().label('Feedback'),
// });

const CAPTIONS = {
  back: {
    'en-us': 'Back',
    'es-mx': 'Volver',
  },
  send: {
    'en-us': 'Send',
    'es-mx': 'Enviar',
  },
  skip: {
    'en-us': 'Skip',
    'es-mx': 'Omitir',
  },
  how: {
    'en-us': 'How are we doing?',
    'es-mx': '¿Cómo lo estamos haciendo?',
  },
  feedback: {
    'en-us': 'What would you tell a friend about your experience today?',
    'es-mx': '¿Qué le dirías a un amigo sobre tu experiencia de hoy?',
  },
  thanks: {
    'en-us': 'Thank you for taking the time to share.',
    'es-mx': 'Gracias por tomarse el tiempo de compartir.',
  },
  placeholder: {
    'en-us': 'Please send us your feedback',
    'es-mx': 'Por favor envíenos sus comentarios',
  },
};

const useStyles = makeStyles((theme) => ({
  container: {
    '& .slick-track': {
      display: 'flex',
      alignItems: 'center',
    },
    '& .slick-slide div': {
      outline: 'none',
    },
  },
  iconContainer: {
    cursor: 'pointer',
    border: '1px solid',
    borderColor: theme.palette.text.secondary,
    borderRadius: '1.3rem',
    padding: '0.7rem 0.8rem 0.4rem 0.8rem',
    margin: '0.2rem',
    transition: '0.3s',
    '&:hover': {
      transition: '0.3s',
      transform: 'scale(1.1)',
      backgroundColor: theme.palette.text.secondary,
      '& $icon': {
        color: theme.palette.ggd.light,
      },
    },
    '&:active': {
      transition: '0.3s',
      transform: 'scale(1.3)',
      backgroundColor: theme.palette.secondary.main,
    },
  },
  iconSelected: {
    borderColor: theme.palette.ggd.light,
    backgroundColor: theme.palette.secondary.main,
    '& $icon': {
      color: '#fff',
    },
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  icon: {
    fontSize: '1.5rem',
    color: theme.palette.text.secondary,
  },
}));

const settings = {
  dots: false,
  infinite: false,
  arrows: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  draggable: false,
  accessibility: false,
  swipe: false,
};

const SatisfactionSelector = ({ surveySourceId, conversationId, associatedExternalId, scrollDown = () => {} }) => {
  const classes = useStyles();
  const [selected, setSelected] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [isSending, setIsSending] = useState(false)
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [inputText, setInputText] = useState('')
  // const { validate } = useYupValidation();
  const sliderRef = useRef();
  const { store } = useContext(GlobalStore);

  useEffect(() => {
    getSurveyStatus(handleSurveyStatusLoad, {
      conversation_id: conversationId,
      surveySourceId,
    });
  }, [surveySourceId, conversationId]);

  const SATISFACTION = [
    {
      id: 1,
      value: 'Dissatisfied',
      icon: <SentimentVeryDissatisfiedIcon className={classes.icon} />,
      feedback: CAPTIONS.feedback[store.locale],
      thanks: CAPTIONS.thanks[store.locale],
    },
    {
      id: 2,
      value: 'Satisfied',
      icon: <SentimentSatisfiedAltIcon className={classes.icon} />,
      feedback: CAPTIONS.feedback[store.locale],
      thanks: CAPTIONS.thanks[store.locale],
    },
  ];

  const handleSurveyStatusLoad = useCallback((_, data) => {
    if (Array.isArray(data) && data.length === 0) {
      // no previous data
      setIsLoading(false);
    }
  }, []);

  // const validateForm = async (data) => {
  //   const errors = await validate(validationSchema)(data);

  //   if (errors) return errors;

  //   onSubmit({ ...data });
  // };

  const onSubmit = () => {
    setIsSending(true);
    scrollDown();
    postSurvey(handlePostSurvey, {
      surveySourceId,
      score: selected,
      scoringScale: 2,
      conversationId,
      associatedExternalId,
      explanation: inputText,
    });
  };

  const handlePostSurvey = useCallback((_, data) => {
    setIsSubmitted(true);
  }, []);

  const gotoNext = (sel) => {
    setSelected(sel);
    sliderRef.current.slickNext();
    scrollDown()
  };

  const gotoPrev = () => {
    setSelected()
    sliderRef.current.slickPrev();
  };
  
  if (!surveySourceId || isLoading || isSubmitted) {
    return '';
  }

  return (
    <>
      <Box className={classes.container}>
        <Box mt={3}>
          <MessageGroup
            messages={[CAPTIONS.how[store.locale]]}
            messageId={uuidv4()}
            messageTime={new Date()}
          />
          {selected &&
            <Box my={3}>
              <MessageGroup
                messages={[SATISFACTION.find((obj) => obj.id === selected)?.feedback]}
                messageId={uuidv4()}
                messageTime={new Date()}
              />
            </Box>
          }
          {isSending &&
            <Box my={5}>
              <MessageGroup
                messages={[SATISFACTION.find((obj) => obj.id === selected)?.thanks]}
                messageId={uuidv4()}
                messageTime={new Date()}
              />
            </Box>
          }
        </Box>
        {!isSending &&
          <Slider {...settings} ref={sliderRef}>
            <div>
              <Box display="flex" justifyContent="center">
                {SATISFACTION.map((satisfaction) => (
                  <Box
                    key={satisfaction.id}
                    className={`${classes.iconContainer} ${
                      selected === satisfaction.id && classes.iconSelected
                    }`}
                    onClick={() => gotoNext(satisfaction.id)}
                  >
                    {satisfaction.icon}
                  </Box>
                ))}
              </Box>
            </div>
            <div>
              <Grid container justifyContent="center" alignItems="center" my={1}>
                <Grid item xs={12}>
                  <Box px={6}>
                    <Form
                      onSubmit={onSubmit}
                      render={({ handleSubmit, submitErrors: errors = {} }) => {
                        return (
                          <form onSubmit={handleSubmit}>
                            <Box mb={1}>
                              <Field
                                name="feedback"
                                render={({ input }) => (
                                  <TextField
                                    {...input}
                                    value={inputText}
                                    placeholder={CAPTIONS.placeholder[store.locale]}
                                    fullWidth
                                    margin="none"
                                    variant="outlined"
                                    error={!!errors.feedback}
                                    helperText={errors.feedback}
                                    onChange={(e) => setInputText(e.target.value)}
                                  />
                                )}
                              />
                            </Box>
                            <Grid container alignItems="center" justifyContent="space-between">
                              <Grid item>
                                <Link
                                  onClick={gotoPrev}
                                  underline="none"
                                  style={{ cursor: 'pointer' }}
                                >
                                  {CAPTIONS.back[store.locale]}
                                </Link>
                              </Grid>
                              <Grid item>
                                <SecureSubmitButton
                                  label={
                                    inputText
                                      ? CAPTIONS.send[store.locale]
                                      : CAPTIONS.skip[store.locale]
                                  }
                                  onClick={handleSubmit}
                                />
                              </Grid>
                            </Grid>
                          </form>
                        );
                      }}
                    />
                  </Box>
                </Grid>
              </Grid>
            </div>
          </Slider>
        }
      </Box>
    </>
  );
};

export { SatisfactionSelector };
