import React, { useState, useEffect, useContext } from 'react';
import {
  useStripe,
  PaymentRequestButtonElement,
} from '@stripe/react-stripe-js';
import { Store as GlobalStore } from '../../context';

const numeral = require('numeral');

const PaymentProvider = ({
  clientSecret = {},
  paymentInfo = {},
  handlePaymentSuccess = () => {},
  setError = () => {},
  setProcessing = () => {},
  setSucceeded = () => {},
}) => {
    
  const stripe = useStripe();
  const { store } = useContext(GlobalStore);
  const [paymentRequest, setPaymentRequest] = useState(null);

  useEffect(() => {
    if (stripe && paymentInfo) {
      const totalPrice = numeral(paymentInfo.totalPrice);
      const amount = totalPrice.multiply(100);
      const pr = stripe.paymentRequest({
        country: 'US',
        currency: paymentInfo.currency,
        total: {
          label: paymentInfo.items[0].name,
          amount: amount.value(),
        },
      });
      // Check the availability of the Payment Request API.
      pr.canMakePayment().then((result) => {
        if (result) {
          setPaymentRequest(pr);
        }
      });
    }
  }, [stripe]);

  useEffect(() => {
    if (paymentRequest) {
      paymentRequest.on('paymentmethod', async (ev) => {
        // Confirm the PaymentIntent without handling potential next actions (yet).
        const { paymentIntent, error: confirmError } = await stripe.confirmCardPayment(
          clientSecret,
          { payment_method: ev.paymentMethod.id },
          { handleActions: false },
        );

        if (confirmError) {
          // Report to the browser that the payment failed, prompting it to
          // re-show the payment interface, or show an error message and close
          // the payment interface.
          ev.complete('fail');
          handleSubmit(confirmError);
        } else {
          // Report to the browser that the confirmation was successful, prompting
          // it to close the browser payment method collection interface.
          ev.complete('success');
          // Check if the PaymentIntent requires any actions and if so let Stripe.js
          // handle the flow. If using an API version older than "2019-02-11"
          // instead check for: `paymentIntent.status === "requires_source_action"`.
          if (paymentIntent.status === 'requires_action') {
            // Let Stripe.js handle the rest of the payment flow.
            const { error } = await stripe.confirmCardPayment(clientSecret);
            if (error) {
              // The payment failed -- ask your customer for a new payment method.
              handleSubmit(error);
            } else {
              handleSubmit();
            }
          } else {
            handleSubmit();
          }
        }
      });
    }
  }, [paymentRequest]);

  const handleSubmit = (error) => {
    setProcessing(true);
    if (error) {
      setError(`Payment failed ${error}`);
      setProcessing(false);
    } else {
      setError(null);
      setProcessing(false);
      setSucceeded(true);
      handlePaymentSuccess(
        store.locale === 'es-mx'
          ? `Pagué $${paymentInfo.totalPrice} a GoGetDoc`
          : `I paid $${paymentInfo.totalPrice} to GoGetDoc`,
      );
    }
  };

  return paymentRequest && <PaymentRequestButtonElement options={{ paymentRequest }} />;
};

export { PaymentProvider }
