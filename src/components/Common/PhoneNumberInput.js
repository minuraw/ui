import React, { useState } from 'react';
import { Box, makeStyles, TextField } from '@material-ui/core';
import InputMask from 'react-input-mask';
import dynamic from 'next/dynamic';
import { COUNTRIES_ISO_CODES } from '../../utils';
const MuiPhoneNumber = dynamic(import('material-ui-phone-number'), { ssr: false });

const EXCLUDED_PHONE_COUNTRIES = [COUNTRIES_ISO_CODES.IR, COUNTRIES_ISO_CODES.CU, COUNTRIES_ISO_CODES.SD, COUNTRIES_ISO_CODES.SY, COUNTRIES_ISO_CODES.KP];

const useClasses = makeStyles((theme) => ({
  phoneNumberInput: {
    '& fieldset': {
      border: 'none',
    },
    '& input': {
      fontSize: '1.3rem',
      fontWeight: 500,
      color: theme.palette.text.primary,
      padding: '0.2rem',
    },
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    '& .Mui-error': {
      margin: 0
    }
  }
}));


const PhoneNumberInput = ({ input, error }) => {
  const classes = useClasses();
  const [country, setCountry] = useState('+1')

  const handleChange = (e) => {
    input.onChange(country + e.target.value);
  }

  return (
    <Box className={classes.container}>
      <style type="text/css">
        {`@media (max-width: 600px) {
          #country-menu .MuiMenu-paper {
            top: auto !important;
            bottom: 1rem;
          }
        }
      `}
      </style>
      <MuiPhoneNumber
        defaultCountry={COUNTRIES_ISO_CODES.US.toLowerCase()}
        excludeCountries={EXCLUDED_PHONE_COUNTRIES.map((countryCode) => countryCode.toLowerCase())}
        disableAreaCodes
        countryCodeEditable={false}
        data-cy="input-login-phone"
        variant="outlined"
        className={classes.phoneNumberInput}
        onChange={(country) => setCountry(country)}
        inputProps={{
          style: { display: 'none' },
        }}
        style={{ marginRight: '1rem' }}
      />
      <InputMask
        mask={country === '+1' ? '(999) 999-9999' : '999999999999'}
        maskChar=" "
        onChange={(e) => handleChange(e)}
      >
        {() => (
          <TextField
            id="phone-number"
            type="tel"
            variant="outlined"
            autoFocus
            fullWidth
            error={!!error}
            helperText={error || ''}
            className={classes.phoneNumberInput}
            inputClass={classes.container}
          />
        )}
      </InputMask>
    </Box>
  );
};

export { PhoneNumberInput };
