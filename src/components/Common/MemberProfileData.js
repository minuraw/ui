import { useContext } from 'react'
import { Avatar } from '../Common';
import { getFormattedDateTimeString, formatPhoneNumberUS } from '../../utils';
import { Grid, Paper, Typography, makeStyles, Box } from '@material-ui/core';
import { Store as GlobalStore } from '../../context';

const LINE_HEIGHT = 1.2;

const CAPTIONS = {
  member: {
    'en-us': 'Member since',
    'es-mx': 'Miembro desde',
  },
};

const useClasses = makeStyles((theme) => ({
  memberText: {
    color: '#A17700',
    fontWeight: 500,
    lineHeight: LINE_HEIGHT,
  },
  userName: {
    fontWeight: 500,
    lineHeight: LINE_HEIGHT,
  },
  dobPhone: {
    lineHeight: LINE_HEIGHT,
  },
  dot: {
    '&::before': {
      content: "'\\00B7'",
      padding: '0 0.1em',
      marginRight: '0.3rem',
      marginLeft: '0.3rem',
      fontWeight: 800,
    },
  },
}));

const MemberProfileData = ({ memberSince, firstName, lastName, dob, phoneNumber }) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);

  return (
    <Paper>
      <Box p={2}>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <Avatar firstName={firstName} lastName={lastName} small/>
          </Grid>
          <Grid item xs>
            <Typography className={classes.userName}>
              {firstName} {lastName}
            </Typography>
            <Typography className={classes.memberText}>
              {CAPTIONS.member[store.locale]} {memberSince}
            </Typography>
            <Typography color="textSecondary" className={classes.dobPhone}>
              { getFormattedDateTimeString(dob, 'short') }
              <span className={classes.dot}>{formatPhoneNumberUS(phoneNumber)}</span>
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export { MemberProfileData };
