import React, { useState } from 'react';
import { Button } from '@material-ui/core';

import { StandardCard } from '../Cards';
import { CheckboxList } from './';

const MultipleChoiceList = ({
  options,
  allowMultiple = true,
  initialValues,
  RenderActions,
  onChange = () => {},
  onSubmit = () => {},
  optionsSelectedData = {},
}) => {
  const [currentValues, setCurrentValues] = useState(initialValues);

  return (
    <>
      <StandardCard
        footer={
          <Button color="primary" fullWidth onClick={() => onSubmit(currentValues)}>
            Submit
          </Button>
        }
      >
        <CheckboxList
          allowMultiple={allowMultiple}
          options={options}
          initialValues={initialValues}
          RenderActions={RenderActions}
          onChange={(values) => {
            setCurrentValues(values);
            onChange(values);
          }}
        />
      </StandardCard>
    </>
  );
};

export { MultipleChoiceList };
