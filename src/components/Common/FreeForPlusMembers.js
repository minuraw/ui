import { Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  plus: { fontSize: '0.9em', color: '#A17700' },
});

const FreeForPlusMembers = ({ fontStyle = 'inherit' }) => {
  const classes = useStyles();

  return (
    <Typography variant={fontStyle}>
      Free For{' '}
      <Typography component="span" className={classes.plus}>
        <strong>+</strong>
      </Typography>{' '}
      Members
    </Typography>
  );
};

export { FreeForPlusMembers };
