import { Avatar } from '../Common';
import { limitText } from '../../utils';
import { Paper, Grid, Box, Typography, makeStyles, ButtonBase } from '@material-ui/core';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const LINE_HEIGHT = 1.2;

const useClasses = makeStyles((theme) => ({
  title: {
    color: theme.palette.blue.main,
    fontWeight: 500,
    lineHeight: LINE_HEIGHT,
  },
  subTitle: {
    fontWeight: 500,
    lineHeight: LINE_HEIGHT,
  },
  timeStamp: {
    fontSize: '14px',
  },
  lastEncounter: {
    color: '#333',
  },
  icon: {
    fontSize: '1rem',
    stroke: '#92929D',
    strokeWidth: '1px',
    transform: 'rotate(40deg)',
  },
  iconGrid: {
    width: '49px',
  },
  itemButton: {
    width: '100%',
    display: 'block',
    textAlign: 'left',
  },
  itemButtonPaperSelected: {
    backgroundColor: 'rgba(30,117,255,0.1)',
  },
  itemButtonPaper: {
    '&:hover': {
      backgroundColor: 'rgba(30,117,255,0.1)',
    },
  },
}));

const formatVisitType = (visitType) => {
  if (visitType === 'VaxYes' || visitType === 'CoVaxYes') {
    return 'Covid-19';
  }
  if (visitType === 'VaxYesBooster') {
    return 'Covid-19 Booster';
  }
  return visitType;
};

const ChatListItem = ({
  selected = false,
  avatarUrl,
  consultant,
  visitType,
  timeStamp,
  attachments = false,
  lastEncounterText,
  onClick = () => {},
}) => {
  const classes = useClasses();

  return (
    <ButtonBase disableRipple onClick={onClick} className={classes.itemButton}>
      <Paper className={selected ? classes.itemButtonPaperSelected : classes.itemButtonPaper}>
        <Box p={2}>
          <Grid container direction="column" spacing={1}>
            <Grid item>
              <Grid container justifyContent="space-between" spacing={1}>
                <Grid item>
                  <Avatar src={avatarUrl} firstName={consultant} />
                </Grid>
                <Grid item xs>
                  <Typography className={classes.title}>{consultant}</Typography>
                  <Typography className={classes.subTitle}>{formatVisitType(visitType)}</Typography>
                </Grid>
                <Grid item>
                  <Typography color="textSecondary" className={classes.timeStamp}>
                    {timeStamp}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container justifyContent="space-between" alignItems="center" spacing={1}>
                <Grid item className={classes.iconGrid} align="center">
                  {attachments && <AttachFileIcon color="textSecondary" className={classes.icon} />}
                </Grid>
                <Grid item xs>
                  <Typography color="textPrimary" className={classes.lastEncounter}>
                    {limitText(lastEncounterText, 100)}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    </ButtonBase>
  );
};

export { ChatListItem };
