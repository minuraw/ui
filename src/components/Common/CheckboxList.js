import React, { useState, useEffect } from 'react';
import { Box, IconButton, makeStyles, Grid, Divider, Typography } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  iconChecked: {
    color: theme.palette.primary.main,
  },
}));

const CheckboxList = ({
  options,
  allowMultiple = false,
  initialValues,
  RenderActions,
  onChange = () => {},
  optionsSelectedData = {},
}) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [selectedValues, setSelectedValues] = useState(() => initialValues || []);

  useEffect(() => {
    onChange(selectedValues);
  }, [selectedValues, onChange]);

  /**
   * Handles click
   *
   * @param {*} value
   */
  const handleClick = (value) => {
    if (allowMultiple) {
      if (selectedValues.includes(value)) {
        setSelectedValues((values) => values.filter((item) => item !== value));
      } else {
        setSelectedValues((values) => [...values, value]);
      }
    } else {
      setSelectedValues((values) => (value !== values ? [value] : []));
    }
  };

  /**
   * Handles showing selected items
   *
   * @param {*} value
   */
  const isSelected = (value) => selectedValues.includes(value);

  if (!Array.isArray(selectedValues)) return null;

  return (
    <>
      <Box>
        {options.map((option, index) => (
          <React.Fragment key={option.value}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs>
                <Typography>
                  {option.label_translation_key ? t(option.label_translation_key) : option.label}
                </Typography>
              </Grid>
              <Grid item>
                <IconButton
                  edge="end"
                  onClick={() => handleClick(option.value)}
                  data-cy={`checkbox-list-button-${option.value}`}
                >
                  {isSelected(option.value) ? (
                    <CheckCircleIcon
                      data-cy="checkboxcard-item-checked"
                      className={classes.iconChecked}
                    />
                  ) : (
                    <AddCircleIcon data-cy="checkbox-item-unchecked" color="disabled" />
                  )}
                </IconButton>
              </Grid>
              {isSelected(option.value) && optionsSelectedData[option.value] && (
                <Grid item xs={12}>
                  <Box pb={2}>{optionsSelectedData[option.value]}</Box>
                </Grid>
              )}
            </Grid>
            {index + 1 < options.length && <Divider />}
          </React.Fragment>
        ))}
      </Box>
      {RenderActions && <RenderActions selectedValues={selectedValues} />}
    </>
  );
};

CheckboxList.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      label_translation_key: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
    }),
  ).isRequired,
  initialValues: PropTypes.array,
  onChange: PropTypes.func,
  allowMultiple: PropTypes.bool,
  RenderActions: PropTypes.func,
};

export { CheckboxList };
