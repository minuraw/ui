import { useState } from 'react';
import { Button, Menu, MenuItem } from '@material-ui/core';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';

const STATES = [
  {
    key: 'CA',
    label: 'California',
  },
  {
    key: 'FL',
    label: 'Florida',
  },
];

const StateSelector = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [usState, setUsState] = useState('State');

  const handleOpenMenu = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleSelectState = (e, state) => {
    setUsState(state);
    handleCloseMenu();
  };

  return (
    <>
      <Button
        disableRipple
        size="small"
        startIcon={<LocationOnOutlinedIcon />}
        onClick={handleOpenMenu}
        style={{ backgroundColor: 'transparent' }}
      >
        {usState}
      </Button>
      <Menu anchorEl={anchorEl} open={!!anchorEl} onClose={handleCloseMenu}>
        {STATES.map((state) => (
          <MenuItem dense onClick={(e) => handleSelectState(e, state.label)} key={state.key}>
            {state.label}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export { StateSelector };
