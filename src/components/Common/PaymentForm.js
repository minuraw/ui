import React, { useState, useContext } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import styles from './PaymentForm.module.css';
import { SecureSubmitButton } from '../Chat';
import { Grid, Link, makeStyles, Box, Typography } from '@material-ui/core';
import { PaymentProvider } from './PaymentProvider';
// import { CONVERSATION_PROVIDER_ID } from '../../constants/providers';
import { Store as GlobalStore } from '../../context';

import { LOGGER } from '../../utils';

const CAPTIONS = {
  byClicking: {
    'en-us': 'By clicking pay, I acknowledge that I have reviewed and electronically agree to',
    'es-mx': 'Al hacer clic en pagar, reconozco que he revisado y acepto electrónicamente los',
  },
  terms: {
    'en-us': 'Terms and Conditions',
    'es-mx': 'Términos y condiciones',
  },
  and: {
    'en-us': 'and',
    'es-mx': 'y las',
  },
  policy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Políticas de Privacidad',
  },
  pay: {
    'en-us': 'Pay',
    'es-mx': 'Pagar',
  },
  payment: {
    'en-us': 'Payment successful.',
    'es-mx': 'Pago exitoso.',
  },
};

const cardStyle = {
  hidePostalCode: true,
  style: {
    base: {
      color: '#32325d',
      fontFamily: 'Arial, sans-serif',
      fontSmoothing: 'antialiased',
      fontSize: '16px',
      '::placeholder': {
        color: '#32325d',
      },
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a',
    },
  },
};

const useClasses = makeStyles((theme) => ({
  link: {
    color: theme.palette.blue.main,
  },
  errorContainer: {
    height: '14px',
  },
  termsText: {
    fontSize: '0.875rem',
  },
}));

export default function CheckoutForm({
  clientSecret,
  totalPrice,
  handlePaymentSuccess = () => {},
  provider,
  paymentInfo = {},
}) {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const [succeeded, setSucceeded] = useState(false);
  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState('');
  const [disabled, setDisabled] = useState(true);
  const stripe = useStripe();
  const elements = useElements();

  // const isVaxYesPayment = provider === CONVERSATION_PROVIDER_ID.VAX_YES;

  const handleChange = async (event) => {
    // Listen for changes in the CardElement
    // and display any errors as the customer types their card details
    setDisabled(event.empty);
    setError(event.error ? event.error.message : '');
  };

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    setProcessing(true);

    const payload = await stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: elements.getElement(CardElement),
      },
    });

    // Log the payment related information to DD
    LOGGER.info('[Payment]-Payment-info', { payload, provider, paymentInfo });

    if (payload.error) {
      setError(`Payment failed ${payload.error.message}`);
      setProcessing(false);
    } else {
      setError(null);
      setProcessing(false);
      setSucceeded(true);
      handlePaymentSuccess(
        store.locale === 'es-mx'
          ? `Pagué $${totalPrice} a GoGetDoc`
          : `I paid $${totalPrice} to GoGetDoc`,
      );
    }
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      <CardElement className={styles.input} options={cardStyle} onChange={handleChange} />
      <PaymentProvider
        clientSecret={clientSecret}
        paymentInfo={paymentInfo}
        handlePaymentSuccess={handlePaymentSuccess}
        setError={setError}
        setProcessing={setProcessing}
        setSucceeded={setSucceeded}
      />
      <Box className={classes.errorContainer}>
        {error && (
          <Typography variant="body2" color="error">
            {error}
          </Typography>
        )}
      </Box>
      <Box mt={1}>
        <Typography color="textSecondary" className={classes.termsText}>
          {CAPTIONS.byClicking[store.locale]}{' '}
          <Link
            href="https://gogetdoc.com/terms"
            target="_blank"
            rel="noopener noreferrer"
            className={classes.link}
          >
            {CAPTIONS.terms[store.locale]}{' '}
          </Link>{' '}
          {CAPTIONS.and[store.locale]}{' '}
          <Link
            href="https://gogetdoc.com/privacy"
            target="_blank"
            rel="noopener noreferrer"
            className={classes.link}
          >
            {CAPTIONS.policy[store.locale]}
          </Link>
          .
        </Typography>
      </Box>
      <Box mt={4}>
        <Grid container justifyContent="flex-end">
          <Grid item>
            <SecureSubmitButton
              disabled={processing || disabled || succeeded}
              label={
                processing ? (
                  <div className={styles.spinner} />
                ) : (
                  `${CAPTIONS.pay[store.locale]} $${totalPrice}`
                )
              }
            />
          </Grid>
        </Grid>
      </Box>
      {succeeded && CAPTIONS.payment[store.locale]}
    </form>
  );
}
