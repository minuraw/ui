import React from 'react';
import { Box } from '@material-ui/core';
import { MessageGroup } from '../Common';
import parser from 'html-react-parser';
import { UserImagePreview } from './UserImagePreview';

const QuestionWithImage = ({ imagePath, question, msgId, chatTime, scrollDown }) => {

  return (
    <>
      <Box my={3}>
        <MessageGroup
          messages={question.map((q) => {
            return parser(q || '');
          })}
          messageId={msgId}
          messageTime={chatTime}
          scrollDown={scrollDown}
        />
      </Box>
      <UserImagePreview imagePath={imagePath}/>
    </>
  );
};

export { QuestionWithImage };
