import { ButtonBase, Link as MuiLink, makeStyles } from '@material-ui/core';

const useClasses = makeStyles({
  buttonLink: {
    verticalAlign: 'baseline',
    letterSpacing: 'inherit',
    fontSize: 'inherit',
    fontWeight: 'inherit',
  },
});

const ButtonLink = ({
  label,
  onClick = () => {},
  underline,
  color = 'inherit',
  disabled = false,
}) => {
  const classes = useClasses();

  return (
    <MuiLink
      color={color}
      variant="body2"
      component={ButtonBase}
      underline={underline ? 'always' : 'none'}
      onClick={onClick}
      className={classes.buttonLink}
      disabled={disabled}
    >
      {label}
    </MuiLink>
  );
};

export { ButtonLink };
