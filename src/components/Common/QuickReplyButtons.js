import React from 'react';
import { Box, Button, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  button: {
    textTransform: 'none',
    padding: '0.5rem 1.5rem',
    minWidth: '8rem',
    [theme.breakpoints.up('sm')]: {
      '&:hover': {
        backgroundColor: theme.palette.secondary.main,
      },
    },
  },
  btnSelected: {
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },
  },
}));


const QuickReplyButtons = ({ item = {}, items, onClick = () => {} }) => {
  const classes = useStyles();

  function getKeyByValue(object, value) {
    return Object.keys(object).find((key) => object[key] === value);
  }

  const getClass = (option) => {
    const selected = item && getKeyByValue(item, true);
    if (selected && selected === option.key) {
      return classes.btnSelected
    } else {
      return null;
    }
  };

  return (
    <Box display="flex" flexWrap="wrap" justifyContent="center">
      {items.map((item) => (
        <Box m={1} key={item.id}>
          <Button
            tabIndex="-1"
            className={`${classes.button} ${getClass(item)}`}
            color="secondary"
            size="small"
            variant="contained"
            onClick={() => onClick(item)}
          >
            {item.label}
          </Button>
        </Box>
      ))}
    </Box>
  );
};

QuickReplyButtons.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ),
};

export { QuickReplyButtons };
