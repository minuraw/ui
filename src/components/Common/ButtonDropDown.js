/* eslint-disable react/jsx-handler-names */
import React, { useState, useRef } from 'react';
import {
  makeStyles,
  Typography,
  useMediaQuery,
  ButtonGroup,
  Button,
  Popper,
  ClickAwayListener,
  Paper,
  MenuList,
  MenuItem,
  ListItemText,
  ListItemIcon,
  Link
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

const useClasses = makeStyles((theme) => ({
  popper: {
    zIndex: 9999,
  },
  paper: {
    backgroundColor: theme.palette.ggd.black,
    borderRadius: '8px',
    margin: '0.05rem 0',
    '& .MuiList-padding': {
      padding: 0,
    },
  },
  btnGroup: {
    backgroundColor: theme.palette.secondary.main,
  },
  menuItem: {
    color: '#fff',
  },
  icon: {
    color: '#fff',
    minWidth: 0,
    marginRight: '10px',
  },
}));

const ButtonDropDown = ({ menuCTA, menuItems }) => {
  const classes = useClasses();
  const anchorRef = useRef(null);
  const [openMenu, setOpenMenu] = useState(false);
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  const handleToggleMenu = () => {
    setOpenMenu((prevState) => !prevState);
  };

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpenMenu(false);
  };

  const ListItem = ({item}) => {
    return (
      <>
        <ListItemIcon className={classes.icon}>{item.icon}</ListItemIcon>
        <ListItemText className={classes.menuItem}>{item.text}</ListItemText>
      </>
    )
  }
  
  return (
    <ButtonGroup
      className={classes.btnGroup}
      disableElevation
      size={smallScreen ? 'small' : 'large'}
    >
      <Button onClick={handleToggleMenu} ref={anchorRef} aria-label="Visit options">
        <Typography style={{ color: 'white' }} variant="subtitle1">
          {menuCTA}
        </Typography>
        {openMenu ? (
          <ArrowDropUpIcon style={{ color: 'white' }} />
        ) : (
          <ArrowDropDownIcon style={{ color: 'white' }} />
        )}
      </Button>
      <Popper
        open={openMenu}
        anchorEl={anchorRef.current}
        transition
        placement="bottom-end"
        className={classes.popper}
      >
        <Paper elevation={3} className={classes.paper}>
          <ClickAwayListener onClickAway={handleCloseMenu}>
            <MenuList dense aria-haspopup="true">
              {menuItems.map((item, index) =>
                item.link ? (
                  <Link
                    key={index}
                    href={item.link}
                    target="_blank"
                    rel="noopener noreferrer"
                    underline="none"
                  >
                    <MenuItem>
                      <ListItem item={item} />
                    </MenuItem>
                  </Link>
                ) : (
                  <MenuItem key={index} onClick={item.handler}>
                    <ListItem item={item} />
                  </MenuItem>
                ),
              )}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </ButtonGroup>
  );
};

export { ButtonDropDown };
