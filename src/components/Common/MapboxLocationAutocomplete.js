import React, { useState, useEffect } from 'react';
import { Autocomplete } from '@material-ui/lab';
import { TextField, InputAdornment, Paper, makeStyles, Box } from '@material-ui/core';
import Geocoding from '@mapbox/mapbox-sdk/services/geocoding';
import LocationOnIconOutlined from '@material-ui/icons/LocationOnOutlined';
import debounce from 'lodash.debounce';
import { COUNTRIES_ISO_CODES } from '../../constants';

const useClasses = makeStyles((theme) => ({
  container: {
    '& .MuiAutocomplete-popper': {
      position: 'relative',
    },
  },
  textField: {
    '& .MuiInputLabel-root': {
      color: theme.palette.grey[500],
      top: '0.5rem',
      paddingLeft: '1.5rem',
    },
    '& .MuiInputLabel-shrink': {
      transform: 'translate(0, -3.5px) scale(0.75)',
      borderRadius: 0,
    },
    zIndex: 1,
  },
  input: {
    backgroundColor: theme.palette.ggd.light,
    color: theme.palette.grey[600],
    padding: '0.5rem 1.5rem',
    borderRadius: '1rem',
    zIndex: -1,
  },
  dropdown: {
    position: 'relative',
    top: '-1rem',
    backgroundColor: theme.palette.ggd.light,
    color: theme.palette.grey[600],
    fontSize: 16,
    borderRadius: '0 0 1rem 1rem',
    marginBottom: '1rem',
    minHeight: '10rem',
  },
  searchIcon: {
    fill: theme.palette.grey[500],
    fontSize: 20,
  },
  inputLabelNoShrink: {
    transform: 'translate(28px, 24px) scale(1)',
  },
}));

const GEOCODE_DEFAULTS = {
  limit: 5,
  countries: [COUNTRIES_ISO_CODES.US],
  autocomplete: true,
};

const MapboxLocationAutocomplete = ({
  onUpdateLocation,
  geocodeConfig = {},
  accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN,
  label,
}) => {
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState([]);

  const classes = useClasses();

  const geocodingClient = Geocoding({
    accessToken,
  });

  useEffect(() => {
    if (!open) setOptions([]);
  }, [open]);

  /**
   * Handle search location
   */
  const handleSearchLocation = debounce((location) => {
    if (!location) return;

    geocodingClient
      .forwardGeocode({
        query: location,
        ...GEOCODE_DEFAULTS,
        ...geocodeConfig,
      })
      .send()
      .then((response) => {
        const { body } = response;
        const { features = [] } = body;

        setOptions(features);
      });
  }, 500);

  const handleUpdateLocation = (location) => {
    onUpdateLocation(location);
  };

  return (
    <Box className={classes.container}>
      <Autocomplete
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
        onChange={(e, value) => value && handleUpdateLocation(value)}
        getOptionSelected={(option, value) => option.place_name === value.place_name}
        getOptionLabel={(option) => option.place_name}
        options={options}
        filterOptions={(options) => options}
        onInputChange={(e) => e.target.value && handleSearchLocation(e.target.value)}
        noOptionsText="Type your address"
        PaperComponent={({ children }) => <Paper className={classes.dropdown}>{children}</Paper>}
        disablePortal
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            className={classes.textField}
            InputProps={{
              ...params.InputProps,
              disableUnderline: true,
              className: classes.input,
              endAdornment: null,
              startAdornment: (
                <InputAdornment position="start">
                  <LocationOnIconOutlined className={classes.searchIcon} />
                </InputAdornment>
              ),
            }}
            InputLabelProps={{
              shrink: open,
              className: open ? undefined : classes.inputLabelNoShrink,
            }}
          />
        )}
      />
    </Box>
  );
};

export { MapboxLocationAutocomplete };
