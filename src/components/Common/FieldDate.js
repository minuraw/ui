import React from 'react';
import { Field } from 'react-final-form';
import { TextField } from '@material-ui/core';
import { DateTime } from 'luxon';
import NumberFormat from 'react-number-format';

const FieldDate = ({ name, label, locale = 'en-GB', error = '', inputProps = {} }) => {
  return (
    <Field
      name={name}
      parse={(value) => {
        const dt = DateTime.fromFormat(value, 'D', {
          locale,
        });

        return dt.isValid ? dt.toISODate() : value;
      }}
      format={(value) => {
        if (value && value.includes('-')) {
          const dt = DateTime.fromISO(value);

          if (dt.isValid) {
            return dt.toFormat('D', { locale });
          }
        }
        return value;
      }}
      render={({ input }) => (
        <NumberFormat
          {...input}
          label={label}
          error={!!error}
          helperText={error}
          customInput={TextField}
          {...inputProps}
        />
      )}
    />
  );
};

export { FieldDate };
