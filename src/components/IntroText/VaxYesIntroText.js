import { UpdateCard } from '../Cards/UpdateCard';
import { getLocale } from '../../services';
import { Grid, Typography, makeStyles, Link } from '@material-ui/core';
import {
  IconListWithText,
  SecureInformation,
  TimeToActionInformation,
} from '../Cards/ConsultUpdateContent';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import AssignmentIndOutlinedIcon from '@material-ui/icons/AssignmentIndOutlined';
import FeaturedPlayListOutlinedIcon from '@material-ui/icons/FeaturedPlayListOutlined';

const CAPTIONS = {
  title: {
    'en-us': 'Thanks for choosing VaxYes',
    'es-mx': 'Gracias por elegir VaxYes',
  },
  secondaryText: {
    'en-us': 'The new way to securely save, and carry all vaccination information on your phone.',
    'es-mx':
      'La nueva forma de guardar de forma segura y llevar toda la información de vacunación en su teléfono.',
  },
  thingsYouNeed: {
    'en-us': 'Things you will need',
    'es-mx': 'Cosas que necesitarás',
  },
  id: {
    'en-us': 'Government ID',
    'es-mx': 'Identificación del gobierno',
  },
  vaccineCard: {
    'en-us': 'Vaccine Card',
    'es-mx': 'Tarjeta de vacuna',
  },
  timeToComplete: {
    'en-us': 'Average time to complete',
    'es-mx': 'Tiempo promedio para completar',
  },
  byClicking: {
    'en-us': 'By clicking “I Agree”, you agree to our ',
    'es-mx': 'Al hacer clic en "Acepto", acepta nuestros ',
  },
  terms: {
    'en-us': 'Terms',
    'es-mx': 'términos',
  },
  and: {
    'en-us': ' and ',
    'es-mx': ' y ',
  },
  privacyPolicy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Política de privacidad',
  },
  iHereby: {
    'en-us': `. I hereby attest under the penalties of perjury that I was, or will be, fully vaccinated
                for COVID-19 with an FDA- or WHO-authorized vaccine— meaning that two weeks had passed, or
                will have passed, since my final dose of the authorized vaccine— at this time.`,
    'es-mx': `. Por la presente certifico, bajo pena de perjurio, que fui, o estaré, completamente vacunado 
                contra COVID-19 con una vacuna autorizada por la FDA o la OMS, lo que significa que habían 
                pasado dos semanas desde mi última dosis de la vacuna autorizada en este momento.`,
  },
  handlingFee: {
    'en-us': `A handling fee may apply for international users. You understand if you don’t want to
                receive these messages you can reply STOP to opt out of receiving texts.`,
    'es-mx': `Es posible que se aplique una tarifa de gestión para los usuarios.
                Si no desea recibir estos mensajes, puede responder PARE para optar por no recibir mensajes de texto.`,
  },
};

const useClasses = makeStyles({
  legalText: {
    fontSize: '0.875rem',
  },
});

const VaxYesIntroText = () => {
  const classes = useClasses();
  const locale = getLocale();

  return (
    <UpdateCard
      title={CAPTIONS.title[locale]}
      subtitle={CAPTIONS.secondaryText[locale]}
      showAlert={false}
      showMenu={false}
    >
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <IconListWithText
            title={CAPTIONS.thingsYouNeed[locale]}
            icons={[
              {
                title: CAPTIONS.id[locale],
                icon: <AssignmentIndOutlinedIcon />,
              },
              {
                title: CAPTIONS.vaccineCard[locale],
                icon: <FeaturedPlayListOutlinedIcon />,
              },
            ]}
          />
        </Grid>
        <Grid item>
          <TimeToActionInformation text={CAPTIONS.timeToComplete[locale]} time="5 minutes" />
        </Grid>
        <Grid item>
          <SecureInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <AssignmentOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textSecondary" gutterBottom className={classes.legalText}>
                {CAPTIONS.byClicking[locale]}
                <Link href="https://gogetdoc.com/terms" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.terms[locale]}
                </Link>
                {CAPTIONS.and[locale]}
                <Link href="https://gogetdoc.com/privacy" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.privacyPolicy[locale]}
                </Link>
                {CAPTIONS.iHereby[locale]}
              </Typography>
              <Typography color="textSecondary" className={classes.legalText}>
                {CAPTIONS.handlingFee[locale]}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </UpdateCard>
  );
};

export { VaxYesIntroText };
