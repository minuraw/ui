import { getLocale } from '../../services';
import { UpdateCard } from '../Cards/UpdateCard';
import {
  EmergencyInformation,
  IconListWithText,
  SecureInformation,
  TimeToActionInformation,
} from '../Cards/ConsultUpdateContent';
import { Grid, Typography, Link } from '@material-ui/core';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import AssignmentIndOutlinedIcon from '@material-ui/icons/AssignmentIndOutlined';
import PhotoCameraOutlinedIcon from '@material-ui/icons/PhotoCameraOutlined';

const CAPTIONS = {
  title: {
    'en-us': "You're scheduling a General Care Visit with GoGetDoc",
    'es-mx': 'Está programando una visita de atención general con GoGetDoc',
  },
  secondaryText: {
    'en-us': 'The most convenient way to get high-quality care online.',
    'es-mx': 'La forma más conveniente de obtener atención de alta calidad en línea.',
  },
  averageTime: {
    'en-us': 'Average time to book an appointment is',
    'es-mx': 'El tiempo medio para reservar una cita es',
  },
  thingsYouNeed: {
    'en-us': 'Things you will need',
    'es-mx': 'Cosas que necesitarás',
  },
  photo: {
    'en-us': 'Recent photo or take a selfie',
    'es-mx': 'Foto reciente o tomar una selfie',
  },
  id: {
    'en-us': 'Government ID',
    'es-mx': 'Identificación del gobierno',
  },
  byClicking: {
    'en-us': `By clicking 'I Agree' you agree to the`,
    'es-mx': `Al dar click en 'Estoy de acuerdo' usted expresa estar de acuerdo con los`,
  },
  terms: {
    'en-us': 'Terms of Use',
    'es-mx': 'Términos de Uso',
  },
  and: {
    'en-us': 'and',
    'es-mx': 'y las',
  },
  privacy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Políticas de Privacidad',
  },
};

const GeneralVisitIntroText = () => {
  const locale = getLocale();

  return (
    <UpdateCard
      title={CAPTIONS.title[locale]}
      subtitle={CAPTIONS.secondaryText[locale]}
      showAlert={false}
      showMenu={false}
    >
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <IconListWithText
            title={CAPTIONS.thingsYouNeed[locale]}
            icons={[
              {
                title: CAPTIONS.photo[locale],
                icon: <PhotoCameraOutlinedIcon />,
              },
              {
                title: CAPTIONS.id[locale],
                icon: <AssignmentIndOutlinedIcon />,
              },
            ]}
          />
        </Grid>
        <Grid item>
          <TimeToActionInformation text={CAPTIONS.averageTime[locale]} time="3 minutes" />
        </Grid>
        <Grid item>
          <SecureInformation />
        </Grid>
        <Grid item>
          <EmergencyInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <AssignmentOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textSecondary" variant="body2">
                {CAPTIONS.byClicking[locale]}{' '}
                <Link href="https://gogetdoc.com/terms" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.terms[locale]}
                </Link>{' '}
                {CAPTIONS.and[locale]}{' '}
                <Link href="https://gogetdoc.com/privacy" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.privacy[locale]}
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </UpdateCard>
  );
};

export { GeneralVisitIntroText };
