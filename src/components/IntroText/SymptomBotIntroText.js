import { UpdateCard } from '../Cards/UpdateCard';
import { getLocale } from '../../services';
import {
  EmergencyInformation,
  SecureInformation,
  TimeToActionInformation,
} from '../Cards/ConsultUpdateContent';
import { Grid, Typography, makeStyles, Link } from '@material-ui/core';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';

const CAPTIONS = {
  title: {
    'en-us': 'Thanks for choosing SymptomBot',
    'es-mx': 'Gracias por elegir SymptomBot',
  },
  secondaryTest: {
    'en-us': 'Your smart symptom checking companion.',
    'es-mx': 'Su compañero inteligente para la verificación de síntomas.',
  },
  timeToComplete: {
    'en-us': 'Average time to get results is',
    'es-mx': 'El tiempo medio para obtener resultados es',
  },
  seconds: {
    'en-us': 'seconds',
    'es-mx': 'segundos',
  },
  byClicking: {
    'en-us': `By clicking 'I Agree' you agree to the`,
    'es-mx': `Al dar click en 'Estoy de acuerdo' usted expresa estar de acuerdo con los`,
  },
  terms: {
    'en-us': 'Terms of Use',
    'es-mx': 'Términos de Uso',
  },
  and: {
    'en-us': 'and',
    'es-mx': 'y las',
  },
  privacy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Políticas de Privacidad',
  },
};

const useClasses = makeStyles({
  legalText: {
    fontSize: '0.875rem',
  },
});

const SymptomBotIntroText = () => {
  const classes = useClasses();
  const locale = getLocale();

  return (
    <UpdateCard
      title={CAPTIONS.title[locale]}
      subtitle={CAPTIONS.secondaryTest[locale]}
      showAlert={false}
      showMenu={false}
    >
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <TimeToActionInformation
            text={CAPTIONS.timeToComplete[locale]}
            time={`90 ${CAPTIONS.seconds[locale]}`}
          />
        </Grid>
        <Grid item>
          <SecureInformation />
        </Grid>
        <Grid item>
          <EmergencyInformation />
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <AssignmentOutlinedIcon />
            </Grid>
            <Grid item xs>
              <Typography color="textSecondary" className={classes.legalText}>
                {CAPTIONS.byClicking[locale]}{' '}
                <Link href="https://gogetdoc.com/terms" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.terms[locale]}
                </Link>{' '}
                {CAPTIONS.and[locale]}{' '}
                <Link href="https://gogetdoc.com/privacy" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.privacy[locale]}
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </UpdateCard>
  );
};

export { SymptomBotIntroText };
