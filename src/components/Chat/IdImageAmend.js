import React, { useState } from 'react';
import { Box, Typography, Button } from '@material-ui/core';

import { ProvideImage } from '../Common';

const IdImageAmend = ({ options = [], onSubmit = () => {}, messageId }) => {
  const [faceImage, setFaceImage] = useState(null);
  const [idImage, setIdImage] = useState(null);

  const handleFaceImageUpload = (image, name, type) => {
    setFaceImage({
      image,
      name,
      type,
    });
  };

  const handleIdImageUpload = (image, name, type) => {
    setIdImage({
      image,
      name,
      type,
    });
  };
  return (
    <>
      <Box>
        {faceImage == null && (
          <Box m={3} textAlign="center">
            <Typography variant="h5" component="h6">
              Please upload clear photo of your face.
            </Typography>
            <ProvideImage handleImage={handleFaceImageUpload} />
          </Box>
        )}
        {faceImage != null && idImage == null && (
          <Box m={3} textAlign="center">
            <Typography variant="h5" component="h6">
              Please upload clear photo of your identification.
            </Typography>
            <ProvideImage handleImage={handleIdImageUpload} />
          </Box>
        )}
        {faceImage != null && idImage != null && (
          <Box m={3} textAlign="center">
            <Typography variant="h5" component="h6">
              Confirm to re-submit images.
            </Typography>
            <Button
              color="primary"
              fullWidth
              onClick={() => onSubmit(faceImage, idImage, messageId)}
            >
              Re-Submit
            </Button>
          </Box>
        )}
      </Box>
    </>
  );
};

export { IdImageAmend };
