import React, { useContext } from 'react';
import { Box, makeStyles, Typography, useMediaQuery, Paper } from '@material-ui/core';
import { getFormattedDateTimeString } from '../../utils';
import { ChatHeader } from '../Chat/ChatHeader';
import { LogoIconGGD } from '../Logo';
import Image from 'next/image';
import { PROTOCOL_NAMES } from '../../constants/protocolNames';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  itemsWallet: {
    'en-us': 'There are no certificates in your wallet.',
    'es-mx': 'No hay certificados en su cartera.',
  },
  uploaded: {
    'en-us': 'uploaded on',
    'es-mx': 'cargada el',
  },
};

const useClasses = makeStyles((theme) => ({
  bodyRestricted: {
    overflow: 'hidden',
    height: '100%',
  },
  container: {
    [theme.breakpoints.down('xs')]: {
      minHeight: '100vh',
      maxHeight: '100vh',
    },
    [theme.breakpoints.up('sm')]: {
      height: '90vh',
    },
    '@media (min-height:944px)': {
      height: '93vh',
    },
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.ggd.gray,
  },
  scrollBar: {
    flex: 1,
    overflowY: 'scroll',
    '&::-webkit-scrollbar': {
      width: '0.2rem',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.2)',
      borderRadius: '0.1rem',
    },
  },
  chatHeader: {
    position: 'sticky',
    top: 0,
    backgroundColor: '#fff',
    zIndex: 1,
  },
  imgContainer: {
    width: 'fit-content',
    backgroundColor: theme.palette.background.default,
    padding: '1rem',
    margin: '1rem',
    borderRadius: '1rem',
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
    },
  },
  date: {
    color: theme.palette.text.primary,
  },
}));

const Wallet = ({ walletCertificates = [], certificateId, handleCloseDrawer = () => {} }) => {
  const classes = useClasses();
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));
  const { store } = useContext(GlobalStore);
  const blurImage =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mMsVAYAAQsAlgeKyEAAAAAASUVORK5CYII=';

  const selectedCertificate = walletCertificates.find(
    (certificate) => certificate.id === certificateId,
  );
  let items = [];
  if (selectedCertificate) {
    items = selectedCertificate.images;
  }

  return (
    <Box className={classes.container}>
      <Box className={smallScreen ? classes.chatHeader : ''}>
        <ChatHeader
          avatarUrl={{ url: '', image: <LogoIconGGD color="#fff" height="30px" /> }}
          chatHeader={PROTOCOL_NAMES['vax-yes']}
          // reloadChat={reloadChat}
          // handleDeleteChat={handleDeleteChat}
          // enableDelete={enableDelete}
          handleCloseDrawer={handleCloseDrawer}
        />
      </Box>
      <Box className={classes.scrollBar}>
        {items.length > 0 ? (
          items.map((item) => (
            <Box p={2} key={item.id}>
              <Paper>
                <Box p={2}>
                  <Box mb={2}>
                    <Typography color="textSecondary" gutterBottom>
                      {item.description}
                      {', '}
                      {CAPTIONS.uploaded[store.locale]}{' '}
                      <span className={classes.date}>
                        { getFormattedDateTimeString(item.uploadedOn, 'long', true) }
                      </span>
                    </Typography>
                  </Box>
                  <Image
                    src={item.url}
                    placeholder="blur"
                    blurDataURL={blurImage}
                    width={460}
                    height={360}
                    alt="Wallet Image"
                    objectFit="contain"
                  />
                </Box>
              </Paper>
            </Box>
          ))
        ) : (
          <Box display="flex" justifyContent="center" m={2}>
            <Typography variant="body2" color="textSecondary">
              {CAPTIONS.itemsWallet[store.locale]}
            </Typography>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export { Wallet };
