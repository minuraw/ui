import { Box, makeStyles } from '@material-ui/core';
import { useCallback, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { ChatHOC } from './ChatHOC';
import { useAuth } from '../../context';

const useClasses = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.ggd.gray,
  },
}));

const Conversation = ({
  sync,
  productId,
  groupId,
  skillId,
  skill,
  skillMode,
  conversationId,
  messageId,
  provider,
  handleCloseDrawer = () => {},
  updateConversation = () => {},
  directLink = false,
  isUpdate = false,
  ssoData = {},
}) => {
  const { isAuthenticated, signIn, getMemberProfile, getMemberId } = useAuth();

  const classes = useClasses();

  const [reloadKey, setReloadKey] = useState(false);

  const generateConversationId = () => {
    const memberId = getMemberId();
    if (memberId) {
      return `${memberId}-${uuidv4()}`;
    } else {
      return null;
    }
  };

  const reloadChat = useCallback(() => {
    setReloadKey(uuidv4());
  }, []);

  return (
    <Box className={classes.container} >
      <ChatHOC
        key={reloadKey}
        signIn={signIn}
        getMemberId={getMemberId}
        isAuthenticated={isAuthenticated}
        getMemberProfile={getMemberProfile}
        reloadChat={reloadChat}
        sync={sync}
        productId={productId}
        groupId={groupId}
        skillId={skillId}
        skill={skill}
        skillMode={skillMode}
        messageId={messageId}
        provider={provider}
        handleCloseDrawer={handleCloseDrawer}
        updateConversation={updateConversation}
        generateConversationId={generateConversationId}
        conversationId={conversationId}
        directLink={directLink}
        isUpdate={isUpdate}
        ssoData={ssoData}
      />
    </Box>
  );
};

export { Conversation };
