import React, { useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import { Box, makeStyles, Typography } from '@material-ui/core';

import Link from 'next/link';

import { ChatHeader } from '../Chat/ChatHeader';
import { LogoIconGGD } from '../Logo';
import { isServerSide } from '../../utils';

const CAPTIONS = {
  appointment: {
    'en-us': 'Select an appointment to view details.',
    'es-mx': 'Seleccione una cita para ver los detalles.',
  },
};

const useClasses = makeStyles((theme) => ({
  bodyRestricted: {
    overflow: 'hidden',
    height: '100%',
  },
  container: {
    height: '100vh',
    [theme.breakpoints.up('sm')]: {
      height: '90vh',
    },
    '@media (min-height:944px)': {
      height: '93vh',
    },
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.ggd.gray,
  },
  scrollBar: {
    flex: 1,
    overflowY: 'scroll',
    '&::-webkit-scrollbar': {
      width: '0.2rem',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.2)',
      borderRadius: '0.1rem',
    },
  },
  imgContainer: {
    width: 'fit-content',
    backgroundColor: theme.palette.background.default,
    padding: '1rem',
    margin: '1rem',
    borderRadius: '1rem',
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
    },
  },
  imgTitle: {
    marginRight: '0.3rem',
    color: theme.palette.text.secondary,
  },
}));

const Appointment = ({ appointment }) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);

  const { link, encounterDescription, partner, protocol } = appointment || {};

  let encodedLink = link;
  if (link && !isServerSide()) {
    encodedLink = window.btoa(link);
  }
  return (
    <Box className={classes.container}>
      <ChatHeader
        avatarUrl={{ url: '', image: <LogoIconGGD color="#fff" height="30px" /> }}
        chatHeader="Appointment"
      />
      <Box className={classes.scrollBar}>
        {appointment ? (
          <Box>
            <Box>{encounterDescription}</Box>
            <Box>{partner}</Box>
            <Box>{protocol}</Box>
            <Box>
              <Link href={`/visit?token=${encodedLink}`} passHref>
                Start Here
              </Link>
            </Box>
          </Box>
        ) : (
          <Box display="flex" justifyContent="center" m={2}>
            <Typography variant="body2">{CAPTIONS.appointment[store.locale]}</Typography>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export { Appointment };
