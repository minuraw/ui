import React from 'react';
import { Form, Field } from 'react-final-form';
import {
  Button,
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { FieldDate } from '../Common';

const formatDob = (dob) => {
  try {
    const temp = dob.split('/');
    if (temp.length === 3) {
      return temp[2] + '-' + temp[0] + '-' + temp[1];
    } else {
      return dob;
    }
  } catch (err) {
    return dob;
  }
};

const ProfileEditForm = ({
  initialValues = {},
  onUpdateSuccess = () => {},
  buttonText = 'Save',
}) => {
  const onSubmit = async (data) => {
    data.dob = formatDob(data.dob);
    onUpdateSuccess(data);
  };

  return (
    <>
      <Typography paragraph>
        To connect you with a doctor quickly, we just need a little information from you.
      </Typography>
      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        render={({ handleSubmit, submitErrors: errors = {} }) => (
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2} direction="column">
              <Grid item xs>
                <Field
                  name="first_name"
                  render={({ input }) => (
                    <TextField {...input} label="First Name" variant="outlined" fullWidth />
                  )}
                />
              </Grid>
              <Grid item xs>
                <Field
                  name="last_name"
                  render={({ input }) => (
                    <TextField {...input} label="Last Name" variant="outlined" fullWidth />
                  )}
                />
              </Grid>
              <Grid item xs>
                <FieldDate
                  name="dob"
                  label="Date Of Birth"
                  locale="en-GB"
                  error={errors.dob}
                  inputProps={{
                    variant: 'outlined',
                    fullWidth: true,
                    format: '##/##/####',
                  }}
                />
              </Grid>
              <Grid item xs>
                <Field
                  name="gender"
                  render={({ input }) => (
                    <FormControl variant="outlined" fullWidth>
                      <InputLabel id="choose-gender">Gender</InputLabel>
                      <Select {...input} labelId="choose-gender" label="Gender">
                        <MenuItem value="Male">Male</MenuItem>
                        <MenuItem value="Female">Female</MenuItem>
                        <MenuItem value="Other"> Other </MenuItem>
                        <MenuItem value="Prefer not to say"> Prefer not to say </MenuItem>
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item xs>
                <Field
                  name="phone"
                  render={({ input }) => (
                    <TextField {...input} label="Contact phone" variant="outlined" fullWidth />
                  )}
                />
              </Grid>
              <Grid item xs>
                <Button type="submit" fullWidth color="primary">
                  {buttonText}
                </Button>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </>
  );
};

ProfileEditForm.propTypes = {
  onUpdateSuccess: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
};

export { ProfileEditForm };
