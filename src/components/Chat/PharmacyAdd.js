import React, { useEffect, useState, useContext, useRef } from 'react';
import { PharmacySearchForm } from '../../components/Forms';
import { Box, Typography, makeStyles, Link } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import { AddressCard } from '../Cards';
import { Store as GlobalStore } from '../../context';
import { SecureSubmitButton } from '../Chat/SecureSubmitButton';

const CAPTIONS = {
  back: {
    'en-us': 'Back',
    'es-mx': 'Volver',
  },
  noPharmacies: {
    'en-us': 'No pharmacies found.',
    'es-mx': 'No se han encontrado farmacias',
  },
};

const useClasses = makeStyles((theme) => ({
  cardsContainer: {
    maxHeight: '20rem',
    overflowX: 'scroll',
  },
  separatorTop: {
    position: 'relative',
    top: '1rem',
    width: '100%',
    height: '1.2rem',
  },
  separatorBottom: {
    position: 'relative',
    bottom: '1rem',
    width: '100%',
    height: '1.2rem',
  },
  link: {
    cursor: 'pointer',
    color: theme.palette.blue.main,
    marginRight: '0.3rem',
  },
}));

const PharmacyAdd = ({ onSelect = () => {} }) => {
  const [pharmaciesAvailable, setPharmaciesAvailable] = useState();
  const [selectedPharmacy, setSelectedPharmacy] = useState(null);
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const anchorRef = useRef(null);
  /**
   * This functionality is not available at the moment, please un-comment in a future release.
   */

  const handleSuccess = (pharmacies) => {
    if (Array.isArray(pharmacies)) setPharmaciesAvailable(pharmacies);
  };

  const getPharmacyId = (pharmacy) => {
    const [dosespotId] = pharmacy.identifier.filter(
      (id) => id.system === 'urn:arena:system:dosespot',
    );

    return dosespotId?.value;
  };

  const PAGE_SIZE = 10;
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    setPageNumber(1);
    pharmaciesAvailable && setTotalPages(Math.ceil(pharmaciesAvailable.length / PAGE_SIZE));
  }, [pharmaciesAvailable]);

  useEffect(() => anchorRef && anchorRef.current && anchorRef.current.scrollTo(0, 0), [pageNumber]);

  const paginate = (array, pageNumber) => {
    return array.slice((pageNumber - 1) * PAGE_SIZE, pageNumber * PAGE_SIZE);
  };

  const handleChange = (event, value) => {
    setPageNumber(value);
  };

  const handleBack = () => {
    setSelectedPharmacy();
    setPharmaciesAvailable();
  };

  return (
    <>
      {!pharmaciesAvailable && <PharmacySearchForm onSuccess={handleSuccess} />}
      {pharmaciesAvailable && (
        <Box>
          {pharmaciesAvailable.length ? (
            <>
              <Typography variant="h6">Select a pharmacy</Typography>
              <Box className={classes.separatorTop} />
              <Box className={classes.cardsContainer} ref={anchorRef}>
                {paginate(pharmaciesAvailable, pageNumber).map((pharmacy) => {
                  const pharmacyId = getPharmacyId(pharmacy);

                  return (
                    <Box
                      my={1}
                      key={pharmacyId}
                      onClick={() => setSelectedPharmacy(pharmacy)}
                      style={{ cursor: 'pointer' }}
                    >
                      <AddressCard
                        address={pharmacy}
                        selected={
                          selectedPharmacy && pharmacyId === getPharmacyId(selectedPharmacy)
                        }
                        outline={false}
                      />
                    </Box>
                  );
                })}
                <Box display="flex" justifyContent="center" mb={2}>
                  <Pagination
                    count={totalPages}
                    page={pageNumber}
                    color="primary"
                    onChange={handleChange}
                  />
                </Box>
              </Box>
              <Box className={classes.separatorBottom} />
              <Box display="flex" alignItems="center" justifyContent="space-between">
                <Link onClick={handleBack} className={classes.link}>
                  {CAPTIONS.back[store.locale]}
                </Link>
                <Box>
                  <SecureSubmitButton
                    disabled={!selectedPharmacy}
                    onClick={() => onSelect(selectedPharmacy)}
                  />
                </Box>
              </Box>
            </>
          ) : (
            <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
              <Typography>{CAPTIONS.noPharmacies[store.locale]}</Typography>
              <Box m={2}>
                <Link onClick={handleBack} className={classes.link}>
                  {CAPTIONS.back[store.locale]}
                </Link>
              </Box>
            </Box>
          )}
        </Box>
      )}
    </>
  );
};

export { PharmacyAdd };
