import { useState } from 'react';
import { ActionDrawerWrapper } from './ActionDrawerWrapper';
import { Box, Typography, Link, Divider } from '@material-ui/core';

// cannot generate dynamically since template differs. (some has links - should finalize)
const initialInputData = [
  {
    rejected: false,
    accepted: false,
  },
  {
    rejected: false,
    accepted: false,
  },
  {
    rejected: false,
    accepted: false,
  },
];

const Consents = ({ consents = [], onSubmit = () => {} }) => {
  const [inputData, setInputData] = useState(initialInputData);

  const acceptHandler = () => {
    for (let i = 0; i < inputData.length; i++) {
      inputData[i].rejected = false;
      inputData[i].accepted = true;
    }
    setInputData([...inputData]);
    onSubmit(consents);
  };

  return (
    <ActionDrawerWrapper
      title="Ok, almost done. Please review the consents below:"
      buttonLabel="Agree To All"
      onClick={acceptHandler}
      secondaryText={{ text: <Link href="">Decline and exit</Link> }}
    >
      <Typography variant="body2">
        I have read and accept Wheel&apos;s
        <Link
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.wheel.com/legal/wheel-provider-group-terms-of-use"
        >
          {' '}
          Terms of Use
        </Link>
        ,
        <Link
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.wheel.com/legal/privacy-policy"
        >
          {' '}
          Privacy Policy
        </Link>
        ,
        <Link
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.wheel.com/legal/notice-of-privacy-practices"
        >
          {' '}
          Notice of Privacy Practices
        </Link>{' '}
        and
        <Link target="_blank" rel="noopener" href="/legal/telehealth-informed-consent">
          {' '}
          Telehealth Informed Consent
        </Link>
      </Typography>
      <Box my={2}>
        <Divider />
      </Box>
      <Typography variant="body2">
        I agree to the{' '}
        <Link target="_blank" rel="noopener noreferrer" href="/legal/hipaa-media-authorization">
          {' '}
          GoGetDoc HIPAA Marketing Authorization
        </Link>
      </Typography>
      <Box my={2}>
        <Divider />
      </Box>
      <Typography variant="body2">
        I agree to the processing of my health information for the purpose of performing the
        interview.
      </Typography>
    </ActionDrawerWrapper>
  );
};

export { Consents };
