import React, { useContext } from 'react';
import { Card, Box, makeStyles, Typography } from '@material-ui/core';
import { CovidCard } from './CovidCard';
import { MedicalInsuranceCard } from './MedicalInsuranceCard';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  certificates: {
    'en-us': 'Add your first GoGetDoc digital card!',
    'es-mx': '¡Agregue su primera tarjeta digital GoGetDoc!',
  },
};

const useClasses = makeStyles((theme) => ({
  cardContainer: {
    margin: '1rem',
    padding: '1rem',
    border: '1px solid',
    borderColor: theme.palette.ggd.darkGray,
    backgroundColor: theme.palette.ggd.gray,
    borderRadius: '1rem',
    boxShadow: 'none',
  },
  btnMenu: {
    minWidth: 'fit-content',
    height: '0',
    backgroundColor: 'transparent',
    padding: 0,
    color: '#999',
    fontSize: '1rem',
    fontWeight: '600',
  },
}));

// DO NOT REMOVE THIS (reference for insurance card)- lasantha
// const certificates = [
//   {
//     id: 2,
//     type: 'medical',
//     name: 'Medical Insurance Digital Card',
//     payors: [
//       {
//         id: 1,
//         payor: 'Anthem (65432)',
//         name: 'Primary Payor',
//         plan: 'Silver PPO',
//         groupId: '476507',
//         memberId: 'JQY026M95901',
//       },
//       {
//         id: 2,
//         payor: 'Test (65432)',
//         name: 'Secondary Payor',
//         plan: 'Silver PPO',
//         groupId: '111111',
//         memberId: 'ABC026M95901',
//       },
//     ],
//     creationDate: 'Jun 20 2021',
//   },
// ];

const WalletCard = ({
  certificates = [],
  seeOriginal = () => {},
  certificateId,
  handleCloseDrawer = () => {},
  updateChat = () => {},
}) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);

  const hasBooster = !!certificates.find((cert) => cert.type === 'booster');

  return certificates.length ? (
    certificates.map((certificate) => (
      <Card
        className={classes.cardContainer}
        key={certificate.id}
        style={certificateId === certificate.id ? { backgroundColor: '#e4edfb' } : {}}
      >
        {(certificate.type === 'covid' || certificate.type === 'booster') && (
          <CovidCard
            selected={certificateId === certificate.id}
            seeOriginal={seeOriginal}
            certificate={certificate}
            hasBooster={hasBooster}
            updateChat={updateChat}
          />
        )}
        {/* MedicalInsuranceCard needs to be updated - add name, buttons etc. */}
        {certificate.type === 'medical' && <MedicalInsuranceCard certificate={certificate} />}
      </Card>
    ))
  ) : (
    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '1rem' }}>
      <Typography variant="body2" color="textSecondary">
        {CAPTIONS.certificates[store.locale]}
      </Typography>
    </Box>
  );
};

export { WalletCard };
