import { useState, useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import { Box, Grid, Typography, LinearProgress, makeStyles } from '@material-ui/core';
import { MessageGroup, SectionContainer } from '../Common';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { DynamicProductCard, InformationCard } from '../Cards';
import { SatisfactionSelector } from '../Common/SatisfactionSelector';
import { SURVEY_SOURCES } from './constants';

const generalVisitProduct = {
  id: 108,
  name: 'General Visit',
  description:
    "We can help treat simple medical conditions like skin conditions, women's health, allergies, reflux and more.",
  active: true,
  price_text: '$29',
  product_image_url: 'https://s3.envato.com/files/331373446/2.jpg',
  rank: 0,
  rating: 1,
  category_id: 1,
  created_on: '2021-07-21T00:00:00.000Z',
  max_age: 65,
  member_only: true,
  min_age: 18,
  additional_text: 'Over 4000 trusted providers online',
  service_delivery_channel_id: 2,
  service_delivery_channel: 'Video',
  modified_on: '2021-07-21T00:00:00.000Z',
  product_group_description: "Great healthcare shouldn't be expensive.",
  display_type_id: 1,
  product_group_id: 1,
  product_group_rank: 0,
  product_group_title: 'Get a Medical Provider',
  display_type_description: 'Clinical - Detailed, Metrics',
  display_type_is_active: true,
  display_type_rank: 0,
  call_to_action_text: 'Schedule',
  call_to_action_url: '/chat/start/108/standard-intake/sync',
  call_to_action_display_type: 1,
  call_to_action_open_in_new_window: false,
  product_metrics: [
    {
      metric_id: 1,
      metric_description: 'Wait Time',
      metric_rank: 0,
      metric_value: '2 minutes',
    },
    {
      metric_id: 2,
      metric_description: 'Duration',
      metric_rank: 1,
      metric_value: '10 minutes',
    },
  ],
  providers: [
    {
      id: 1,
      name: 'Wheel, Inc.',
      image: 'https://pbs.twimg.com/profile_images/1427645616360939535/81D31erW_400x400.jpg',
      description:
        "We can help treat simple medical conditions like skin conditions, women's health, allergies, reflux and more.",
      active: true,
      rank: 0,
    },
  ],
};

const useClasses = makeStyles((theme) => ({
  progressBar: {
    borderRadius: '5px',
  },
}));

const CAPTIONS = {
  here: {
    'en-us': 'Here are your results.',
    'es-mx': 'Aquí están sus resultados.',
  },
  results: {
    'en-us': 'Results',
    'es-mx': 'Resultados',
  },
  please: {
    'en-us':
      'Please note that the list below may not be complete and is provided solely for information purposes and is not a qualified medical opinion.',
    'es-mx':
      'Por favor, tenga en cuenta que la lista siguiente puede no estar completa y es provista únicamente con fines informativos y no es una opinión médica calificada.',
  },
  noResults: {
    'en-us': 'No results available.',
    'es-mx': 'Sin resultados disponibles.',
  },
  unfortunately: {
    'en-us': 'Your answers did not produce a result. Please try again or visit the GoGetDoc help center.',
    'es-mx': 'Sus respuestas no produjeron ningún resultado. Vuelva a intentarlo o visite el centro de ayuda de GoGetDoc.',
  },
  evidenceLevel: {
    Strong: {
      'en-us': 'Strong',
      'es-mx': 'Fuerte',
    },
    Moderate: {
      'en-us': 'Moderate',
      'es-mx': 'Moderada',
    },
    Low: {
      'en-us': 'Low',
      'es-mx': 'Baja',
    },
  },
  evidence: {
    'en-us': 'evidence',
    'es-mx': 'evidencia',
  },
  recommendation: {
    'en-us': 'Recommendation',
    'es-mx': 'Recomendación',
  },
  goTo: {
    'en-us': 'Go to the nearest emergency department',
    'es-mx': 'Vaya al servicio de urgencias más cercano',
  },
  symptoms: {
    'en-us':
      'Your symptoms appear serious and may require urgent care. If you can’t get to an emergency department, please call an ambulance.',
    'es-mx':
      'Sus síntomas parecen serios y pueden requerir atención urgente. Si no puede llegar al departamento de emergencias, llame a una ambulancia.',
  },
  alarming: {
    'en-us': 'Alarming symptoms',
    'es-mx': 'Síntomas alarmantes',
  },
};

const SymptomCheckResults = ({
  msgId,
  messages,
  results = {},
  conversationId = '',
  showSatisfactionSelector = false,
  scrollDown = () => {},
}) => {
  const classes = useClasses();
  const [showClinicalCards] = useState(false);
  const { store } = useContext(GlobalStore);

  const probableConditions = results.probableConditions || [];

  let conditions = probableConditions.filter((condition) => condition.evidenceLevel === 'Strong');
  if (conditions.length === 0) {
    conditions = probableConditions.filter((condition) => condition.evidenceLevel === 'Moderate');
  }
  if (conditions.length === 0) {
    conditions = probableConditions.filter((condition) => condition.evidenceLevel === 'Low');
  }

  const isEmergency = results.hasEmergencyEvidence || false;
  const symptoms = (results.seriousSymptoms || [])
    .filter((symptom) => symptom.isEmergency)
    .map((symptom) => symptom.commonName);

  return (
    <>
      <Box my={3}>
        <MessageGroup messages={messages || [CAPTIONS.here[store.locale]]} messageId={msgId} />
        {isEmergency && (
          <Box
            my={3}
            style={{
              marginLeft: '45px',
              border: 'solid 1px rgba(0,0,0,0.1)',
              display: 'flex',
              flexDirection: 'column',
            }}
            data-cy="emergency_results"
          >
            <Grid container wrap="nowrap">
              <Grid item style={{ width: 100, backgroundColor: '#FFC200', textAlign: 'center' }}>
                <ErrorOutlineIcon style={{ color: 'white', fontSize: 60, marginTop: 16 }} />
              </Grid>
              <Grid item style={{ marginLeft: 16, padding: 8, width: '70%' }}>
                <Typography
                  variant="body2"
                  style={{ color: '#0062FF', margin: '20px 0px 0px 0px', fontSize: 14 }}
                >
                  {CAPTIONS.recommendation[store.locale]}
                </Typography>
                <Typography variant="h5">{CAPTIONS.goTo[store.locale]}</Typography>
                <Typography
                  variant="body2"
                  style={{ color: '#92929D', margin: '20px 0px', fontSize: 14 }}
                >
                  {CAPTIONS.symptoms[store.locale]}
                </Typography>
                <Typography
                  variant="subtitle1"
                  style={{ color: '#171725', margin: '20px 0px 0px 0px', fontSize: 14 }}
                >
                  {CAPTIONS.alarming[store.locale]}
                </Typography>
                {symptoms &&
                  symptoms.map((symptom) => (
                    <Typography
                      key={symptom}
                      variant="body2"
                      style={{ color: '#171725', margin: '8px 0px', fontSize: 14 }}
                    >
                      {symptom}
                    </Typography>
                  ))}
              </Grid>
            </Grid>
          </Box>
        )}
        <Box mt={4} data-cy="symptombot_results">
          {conditions && conditions.length > 0 ? (
            <InformationCard
              title={CAPTIONS.results[store.locale]}
              subtitle={CAPTIONS.please[store.locale]}
              content={conditions.map((condition) => ({
                left: (
                  <LinearProgress
                    variant="determinate"
                    value={condition.probability * 100}
                    className={classes.progressBar}
                  />
                ),
                title: condition.commonName,
                secondaryText: `${CAPTIONS.evidenceLevel[condition.evidenceLevel][store.locale]} ${
                  CAPTIONS.evidence[store.locale]
                }`,
              }))}
            />
          ) : (
            <InformationCard
              title={CAPTIONS.noResults[store.locale]}
              subtitle={CAPTIONS.unfortunately[store.locale]}
            />
          )}
        </Box>
      </Box>
      {showClinicalCards && (
        <Box mb={2} data-cy="clinical_cards">
          <SectionContainer color>
            <Typography variant="h5" paragraph>
              Get Seen.
            </Typography>
            <Grid container justifyContent="center">
              <Grid item xs={12} sm={8}>
                <DynamicProductCard product={generalVisitProduct} />
              </Grid>
            </Grid>
          </SectionContainer>
        </Box>
      )}
      {showSatisfactionSelector && (
        <SatisfactionSelector
          surveySourceId={
            isEmergency
              ? SURVEY_SOURCES.SYMPTOMBOT_EMERGENCY_REFEROUT
              : SURVEY_SOURCES.SYMPTOMBOT_DIAGNOSED
          }
          conversationId={conversationId}
          associatedExternalId={null}
          scrollDown={scrollDown}
        />
      )}
    </>
  );
};

export { SymptomCheckResults };
