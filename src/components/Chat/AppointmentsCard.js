import React, { useState, useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import {
  Box,
  makeStyles,
  Typography,
  Grid,
  Link,
  ClickAwayListener,
  Tooltip,
  Zoom,
  useMediaQuery,
  Paper,
  Chip,
} from '@material-ui/core';
import EventRoundedIcon from '@material-ui/icons/EventRounded';
import PlayCircleOutlineRoundedIcon from '@material-ui/icons/PlayCircleOutlineRounded';
import ChatBubbleOutlineRoundedIcon from '@material-ui/icons/ChatBubbleOutlineRounded';
import { getDateFromDateTimeIso, getTimeFromDateTimeIso } from '../../utils';

const MODIFICATION_DISABLED_DURATION = 2 * 60 * 60 * 1000;

const APPOINTMENT_STATUS = {
  PLANNED: 'Planned',
  IN_PROGRESS: 'In Progress',
  FINISHED: 'Finished',
  EnteredInError: 'Entered in Error',
  Cancelled: 'Cancelled',
};

const useClasses = makeStyles((theme) => ({
  title: {
    fontWeight: 500,
  },
  icon: {
    color: theme.palette.text.secondary,
  },
  consultant: {
    fontSize: '0.875rem',
  },
  time: {
    fontSize: '0.875rem',
    fontWeight: 500,
  },
  link: {
    fontWeight: 600,
    fontSize: '0.875rem',
    color: theme.palette.blue.main,
  },
  selected: {
    backgroundColor: 'rgba(30,117,255,0.1)',
    borderRadius: theme.shape.borderRadius,
  },
}));

const AppointmentsCard = ({
  protocol,
  partner,
  videoLink,
  onSelectAppointment = () => {},
  appointmentType = 'video',
  conversationId,
  startTime,
  isSelected = false,
  status,
  receiptUrl,
  reactivateChat = () => {},
}) => {
  const classes = useClasses();
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const [showTooltip, setShowTooltip] = useState(false);
  const { setInStore } = useContext(GlobalStore);

  const date = getDateFromDateTimeIso(startTime);
  const time = getTimeFromDateTimeIso(startTime);

  const isModificationAllowed =
    new Date(startTime).getTime() - new Date().getTime() > MODIFICATION_DISABLED_DURATION &&
    (status === APPOINTMENT_STATUS.PLANNED || status === APPOINTMENT_STATUS.IN_PROGRESS);

  const showJoinVisit =
    status === APPOINTMENT_STATUS.PLANNED || status === APPOINTMENT_STATUS.IN_PROGRESS;

  const showReceiptLink =
    status === APPOINTMENT_STATUS.FINISHED ||
    status === APPOINTMENT_STATUS.PLANNED ||
    status === APPOINTMENT_STATUS.IN_PROGRESS;

  const enableReceiptTooltip =
    status === APPOINTMENT_STATUS.PLANNED ||
    status === APPOINTMENT_STATUS.IN_PROGRESS ||
    (status === APPOINTMENT_STATUS.FINISHED && !receiptUrl);

  const receiptTooltip =
    status === APPOINTMENT_STATUS.FINISHED && !receiptUrl
      ? 'Your receipt is not available at this time. Please contact support for assistance.'
      : 'The receipt will become available after the visit is completed.';

  const handleStartVisit = (ev) => {
    ev.stopPropagation();
    setInStore({ showVideo: true })
    onSelectAppointment()
  };

  const handleReschedule = (ev) => {
    ev.stopPropagation();
    reactivateChat(conversationId, 'RESCHEDULE');
  };

  const handleCancel = (ev) => {
    ev.stopPropagation();
    reactivateChat(conversationId, 'CANCEL');
  };

  const handleReceiptClick = (ev) => {
    ev.stopPropagation();
    if (enableReceiptTooltip) {
      setShowTooltip(true);
      return;
    }
    window.open(receiptUrl, '_ blank');
  };

  const handleTooltipOpen = () => {
    if (smallScreen) return;

    setShowTooltip(true);
  };

  const handleTooltipClose = () => {
    if (smallScreen) return;

    setShowTooltip(false);
  };

  return (
    <Paper>
      <Box p={2}>
        <Grid
          container
          spacing={4}
          className={isSelected ? classes.selected : ''}
          onClick={onSelectAppointment}
        >
          <Grid item>
            <Grid container spacing={1}>
              <Grid item>
                <EventRoundedIcon fontSize="small" className={classes.icon} />
              </Grid>
              <Grid item>
                {date && (
                  <Typography color="textPrimary" className={classes.title}>
                    {date}
                  </Typography>
                )}
                {time && (
                  <Typography color="textSecondary" className={classes.time}>
                    {time.toUpperCase()}
                  </Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs>
            <Grid container spacing={1}>
              <Grid item>
                {appointmentType === 'video' && (
                  <PlayCircleOutlineRoundedIcon fontSize="small" className={classes.icon} />
                )}
                {appointmentType === 'chat' && (
                  <ChatBubbleOutlineRoundedIcon fontSize="small" className={classes.icon} />
                )}
              </Grid>
              <Grid item xs>
                <Typography color="textPrimary">{protocol}</Typography>
                <Typography color="textSecondary" className={classes.consultant}>
                  Provided by {partner}
                </Typography>
                <Box mt={3}>
                  <Grid container spacing={3} alignItems="center">
                    {showJoinVisit && (
                      <Grid item>
                        <Link
                          component="button"
                          className={classes.link}
                          onClick={(event) => handleStartVisit(event, videoLink)}
                        >
                          Join Visit
                        </Link>
                      </Grid>
                    )}
                    {isModificationAllowed && (
                      <>
                        <Grid item>
                          <Link
                            onClick={handleReschedule}
                            component="button"
                            className={classes.link}
                          >
                            Reschedule
                          </Link>
                        </Grid>
                        <Grid item>
                          <Link onClick={handleCancel} component="button" className={classes.link}>
                            Cancel
                          </Link>
                        </Grid>
                      </>
                    )}
                    {showReceiptLink && (
                      <ClickAwayListener onClickAway={() => setShowTooltip(false)}>
                        <Tooltip
                          title={receiptTooltip}
                          placement="top"
                          open={showTooltip && enableReceiptTooltip}
                          onOpen={handleTooltipOpen}
                          onClose={handleTooltipClose}
                          arrow
                          enterTouchDelay={500}
                          TransitionComponent={Zoom}
                        >
                          <Grid item>
                            <Link
                              component="button"
                              className={classes.link}
                              onClick={handleReceiptClick}
                            >
                              Receipt
                            </Link>
                          </Grid>
                        </Tooltip>
                      </ClickAwayListener>
                    )}
                  </Grid>
                  <Box mt={2}>
                    {status !== APPOINTMENT_STATUS.PLANNED && (
                      <Chip label={status} variant="outlined" />
                    )}
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export { AppointmentsCard };
