import React, { useContext, useState, useEffect } from 'react';
import {
  Box,
  makeStyles,
  Typography,
  Grid,
} from '@material-ui/core';
import { SecureSubmitButton } from '../Chat/SecureSubmitButton';
import { setInStorage, clearFromStorage } from '../../utils';
import { Store as GlobalStore } from '../../context';
import { GGDLoader, ButtonDropDown } from '../Common';
import EventIcon from '@material-ui/icons/Event';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { Outlook, Gmail, Apple, Yahoo } from '../../images'
import { getCalenderLink } from '../../events';

const CAPTIONS = {
  changeVisit: {
    'en-us': 'Change Visit',
    'es-mx': 'Modificar Visita',
  },
  cancel: {
    'en-us': 'Cancel',
    'es-mx': 'Cancelar',
  },
  reschedule: {
    'en-us': 'Reschedule',
    'es-mx': 'Reprogramar',
  },
  addToCalendar: {
    'en-us': 'Add to Calendar',
    'es-mx': 'Agregar al Calendario',
  },
};

const useClasses = makeStyles((theme) => ({
  videoContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0,
  },
  video: {
    width: '100%',
    height: '48vh',
    [theme.breakpoints.up('sm')]: {
      height: '76vh',
    },
  },
  joined: {
    height: '88vh',
  },
}));

const renderRescheduleCancel = (
  showRescheduleCancel,
  store,
  handleRescheduleClick,
  handleCancelClick,
  calendarLinks
) => {

  const changeVisitItems = [
    {
      icon: <EventIcon fontSize="small" />,
      text: CAPTIONS.reschedule[store.locale],
      handler: handleRescheduleClick,
    },
    {
      icon: <HighlightOffIcon fontSize="small" />,
      text: CAPTIONS.cancel[store.locale],
      handler: handleCancelClick,
    },
  ];

  const addToCalendarItems = [
    {
      icon: <Outlook />,
      text: 'Outlook',
      link: calendarLinks?.o365InviteLink,
    },
    {
      icon: <Gmail />,
      text: 'Google',
      link: calendarLinks?.googleInviteLink,
    },
    {
      icon: <Apple />,
      text: 'Apple',
      link: calendarLinks?.icsLink,
    },
    {
      icon: <Yahoo />,
      text: 'Yahoo',
      link: calendarLinks?.yahooInviteLink,
    },
  ];

  if (showRescheduleCancel) {
    return (
      <Grid container justifyContent="center" spacing={1}>
        <Grid item>
          <ButtonDropDown
            menuCTA={CAPTIONS.addToCalendar[store.locale]}
            menuItems={addToCalendarItems}
          />
        </Grid>
        <Grid item>
          <ButtonDropDown
            menuCTA={CAPTIONS.changeVisit[store.locale]}
            menuItems={changeVisitItems}
          />
        </Grid>
      </Grid>
    );
  }
  return '';
};

const VideoVisit = ({ answer, reactivateConversation = () => {}, question, conversationId}) => {
  const { error, link, showRescheduleCancel } = answer;
  let  data  = question?.data;
  if(!data){
    data = {};
  }
  const classes = useClasses();
  const { store, setInStore } = useContext(GlobalStore);
  const [isLoading, setIsLoading] = useState(true);
  const [isJoined, setIsJoined] = useState(false);
  const [ calendarLinks, setCalendarLinks] = useState()

  const handleCancelClick = () => {
    reactivateConversation(false, 'CANCEL');
  };

  const handleRescheduleClick = () => {
    reactivateConversation(false, 'RESCHEDULE');
  };

  const loadCalendarLinks = (error, data) => {
    if (!error) {
      setCalendarLinks(data)
    }
  }

  useEffect(() => {
    setInStore({ showVideo: false });
    setInStore({ startVisit: !error });
    setInStore({ showRescheduleCancel: showRescheduleCancel });
  }, []);

  useEffect(() => getCalenderLink(loadCalendarLinks, {convId: conversationId}), []);

  useEffect(
    () => {
      window.addEventListener('message', (event) => {
        // IMPORTANT: check the origin of the data!
        if (event.origin.startsWith(process.env.NEXT_PUBLIC_VIDEO_VISIT_URL)) {
          // The data was sent from your site.
          // Data sent with postMessage is stored in event.data:
          setIsJoined(event.data.join);
          if (event.data.close === false) setInStore({ showVideo: event.data.close});
        }
      })
    }, []);

  useEffect(() => {
    // set the extend session as true
    setInStorage('extend_session', store.video);
    return function cleanup() {
      clearFromStorage('extend_session');
    };
  }, [store.video]);

  return (
    <>
      {error ? (
        renderRescheduleCancel(
          showRescheduleCancel,
          store,
          handleRescheduleClick,
          handleCancelClick,
          calendarLinks,
        )
      ) : link ? (
        <Box>
          {!store.showVideo ? (
            <Box m={1}>
              <SecureSubmitButton label="Start Visit" onClick={() => setInStore({showVideo: true})} centered />
            </Box>
          ) : (
            <Box className={classes.videoContainer}>
              {isLoading && (
                <Box>
                  <GGDLoader />
                </Box>
              )}
              <iframe
                src={link}
                title="Your visit"
                className={`${classes.video} ${isJoined && classes.joined}`}
                style={isLoading ? { display: 'none' } : { display: 'block' }}
                allow="camera; microphone"
                frameBorder="0"
                onLoad={() => setIsLoading(false)}
              />
            </Box>
          )}
        </Box>
      ) : data.hasEmergency ? (
        <Box display="flex" justifyContent="center">
          <Typography variant="subtitle1">
            Please be seen at a local emergency department immediately or call 9-1-1.
          </Typography>
        </Box>
      ) : (
        <Box display="flex" justifyContent="center">
          <Typography variant="subtitle1">
            Sorry! we are having troubles connecting you to the video visit. Please contact our
            support at support@gogetdoc.com.
          </Typography>
        </Box>
      )}
    </>
  );
};

export { VideoVisit };
