import { Box, Typography, Grid, SvgIcon, makeStyles } from '@material-ui/core';
import { SecureSubmitButton } from './SecureSubmitButton';

const useClasses = makeStyles({
  secondaryTextIcon: {
    fontSize: '1rem',
    marginTop: '1px',
  },
});

const ActionDrawerWrapper = ({
  title,
  topRight,
  children,
  secondaryText = {},
  buttonLabel,
  buttonDisabled,
  onClick,
  noButton = false,
}) => {
  const classes = useClasses();

  return (
    <>
      {title && (
        <Box mb={3}>
          <Grid container alignItems="center" justifyContent="space-between" spacing={2}>
            <Grid item>
              <Typography variant="h4">{title}</Typography>
            </Grid>
            {topRight && <Grid item>{topRight}</Grid>}
          </Grid>
        </Box>
      )}
      <Box mb={3}>{children}</Box>
      {(secondaryText || !noButton) && (
        <Grid container alignItems="center" justifyContent="flex-end" spacing={1}>
          {secondaryText && (
            <Grid item xs>
              <Grid container alignItems="flex-start" spacing={1}>
                {secondaryText.icon && (
                  <Grid item>
                    <SvgIcon className={classes.secondaryTextIcon}>{secondaryText.icon}</SvgIcon>
                  </Grid>
                )}
                <Grid item xs>
                  <Typography variant="body2" color="textSecondary">
                    {secondaryText.text}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
          {!noButton && (
            <Grid item>
              <SecureSubmitButton label={buttonLabel} disabled={buttonDisabled} onClick={onClick} />
            </Grid>
          )}
        </Grid>
      )}
    </>
  );
};

export { ActionDrawerWrapper };
