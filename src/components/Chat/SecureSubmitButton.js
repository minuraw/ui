import { useContext } from 'react'
import { Button, Grid, makeStyles } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Store as GlobalStore } from '../../context';

const useClasses = makeStyles((theme) => ({
  icon: {
    color: theme.palette.grey[500],
  },
  buttonLabel: {
    textTransform: 'Capitalize',
    '& .MuiButton-label': {
      padding: '5px 10px',
    },
  },
}));

const SecureSubmitButton = ({
  label = 'Next',
  color = 'secondary',
  onClick = () => {},
  centered = false,
  disabled,
}) => {
  const classes = useClasses();
  const { store } = useContext(GlobalStore);

  return (
    <Grid container alignItems="center" justifyContent={centered ? 'center' : 'flex-start'}>
      <Grid item style={{ marginRight: 10 }}>
        <LockOutlinedIcon className={classes.icon} />
      </Grid>
      <Grid item>
        <Button
          type="submit"
          size="small"
          color={color}
          variant="contained"
          disabled={disabled}
          onClick={onClick}
          className={classes.buttonLabel}
        >
          {label || (store.locale === 'es-mx' ? 'Enviar mensaje' : 'Send message')}
        </Button>
      </Grid>
    </Grid>
  );
};

export { SecureSubmitButton };
