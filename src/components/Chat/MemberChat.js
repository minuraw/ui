/* eslint-disable no-undef */
import { useRef, useCallback, useEffect, useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import { Drawer, TextField, Grid, Box, Button, makeStyles, useMediaQuery } from '@material-ui/core';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Link from 'next/link';
import parser from 'html-react-parser';

import {
  QuickReplyButtons,
  MessageGroup,
  TypingIndicator,
  ProvideImage,
  Payment,
  UserAnswerGroup,
  PharmacyPreview,
} from '../Common';
import { Consents } from './Consents';
import { MultiSelectQuickReply } from './MultiSelectQuickReply';
import { YesNoDontKnow } from './YesNoDontKnow';
import { OtpInput } from './OtpInput';
import { PharmacyAdd } from './PharmacyAdd';
import { PhoneNumberInput } from '../Common/PhoneNumberInput';
import { DateInput } from '../Common/DOBInput';
import Appointment from '../Appointment';
import { Field, Form } from 'react-final-form';
import { INPUT_TYPES, CONVERSATION_MESSAGE_TYPES } from './constants';
import { stripHtml } from 'string-strip-html';
import { AddressSearch } from './AddressSearch';
import { SecureSubmitButton } from './SecureSubmitButton';
import { IdImageAmend } from './IdImageAmend';
import Prescription from '../Prescription';
import Diagnosis from '../Diagnosis';
import SymptomInput from '../SymptomInput';
import { SymptomCheckResults } from './SymptomCheckResults';
import Agreement from '../Agreements';
import { ChatHeader } from './ChatHeader';
import SelectPayment from '../SelectPayment';
import VaxYesSuccessCard from '../VaxYesSuccessCard';
import VaxYesEndOptions from '../VaxYesEndOptions';
import { VideoVisit } from './VideoVisit';
import { CONVERSATION_PROVIDER_ID } from '../../constants/providers';
import { EncryptedMessage } from '../Common/EncryptedMessage';
import { DropDownMenu } from '../Common/DropDownMenu';
import { WhatsHappeningNowCard } from '../Cards/WhatsHappeningNowCard';
import { SpecificProductCard } from '../Cards/SpecificProductCard';
import { OffSessionPayment } from '../Common/OffSessionPayment';
import { QuestionWithImage } from '../Common/QuestionWithImage';

const SYMPTOM_CHECKER_APPEAR_LIST = ['rei'];

const INPUT = {
  USER_TEXT_INPUT: 'USER_TEXT_INPUT',
};

const renderExternalLink = ({ link, label, page, msgId }) => {
  // get the token from the URL
  const videoLink = new URL(link);
  const token = videoLink.searchParams.get('token');
  return (
    <Grid container spacing={2} justifyContent="center" data-messageId={msgId}>
      <Grid item>
        <Link href={{ pathname: page, query: { token } }} passHref>
          <a
            target="_blank"
            rel="noopener noreferrer"
            style={{ color: '#FFFFFF', textDecoration: 'none' }}
          >
            <SecureSubmitButton label={label} />
          </a>
        </Link>
      </Grid>
    </Grid>
  );
};

const renderIdImageReUpload = ({ question, resubmitImages, msgId }) => {
  return (
    <Box my={3}>
      <MessageGroup messages={question.map((q) => stripHtml(q || '').result)} messageId={msgId} />
      <IdImageAmend onSubmit={resubmitImages} messageId={msgId} />
    </Box>
  );
};

const renderLoading = () => {
  return (
    <Box my={3}>
      <MessageGroup messages={[<TypingIndicator key="typing" />]} />
    </Box>
  );
};

const RenderTextMessage = ({ question, msgId, chatTime, scrollDown, isLast }) => {
  return (
    <Box my={3}>
      <MessageGroup
        messages={question.map((q) => {
          return parser(q || '');
        })}
        messageId={msgId}
        messageTime={chatTime}
        scrollDown={scrollDown}
        isLast={isLast}
      />
    </Box>
  );
};

const renderQuestion = (
  conversationId,
  question,
  isLoading,
  {
    msgId,
    msgTypeId,
    resubmitImages,
    text,
    createdOn,
    dateTime,
    additionalData = {},
    data = {},
    metadata = {},
    display = [],
  } = {},
  groupId,
  scrollDown,
  reactivateConversation,
  initiateNewChat,
  isLast,
) => {
  if (isLoading) {
    return renderLoading();
  }

  if (msgTypeId === CONVERSATION_MESSAGE_TYPES.QUESTION_WITH_DISPLAY_TYPE_ID) {
    const questionData = JSON.parse(question);
    question = questionData.text;
    display = questionData.display;
  }

  // if question is undefined, show nothing
  if (!question) return '';
  if (!Array.isArray(question)) {
    question = [question];
  }

  const chatTime = createdOn || dateTime;

  let displayContent = '';
  let questionContent = '';

  if (display && display.length > 0) {
    display.forEach((displayItem) => {
      switch (displayItem.type) {
        case INPUT_TYPES.SYMPTOM_CHECKER_OUTPUT:
          displayContent = <SymptomCheckResults msgId={msgId} results={displayItem} />;
          break;
        default:
          displayContent = (
            <RenderTextMessage
              question={question}
              msgId={msgId}
              chatTime={chatTime}
              scrollDown={scrollDown}
              isLast={isLast}
            />
          );
      }
    });
  }

  switch (msgTypeId) {
    case CONVERSATION_MESSAGE_TYPES.VAX_YES_SUCCESS_OUTPUT:
      questionContent = (
        <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
          <VaxYesSuccessCard msgId={msgId} />
          {!SYMPTOM_CHECKER_APPEAR_LIST.includes(groupId) && (
            <SpecificProductCard productGroupId={2} productId={9} />
          )}
        </Box>
      );
      break;
    case CONVERSATION_MESSAGE_TYPES.IMAGE_RE_UPLOAD_TYPE_ID:
      questionContent = renderIdImageReUpload({ question, resubmitImages, msgId });
      break;
    case CONVERSATION_MESSAGE_TYPES.EXTERNAL_LINK_MESSAGE_TYPE_ID:
      // return renderExternalLink({ link: text.value, label: 'Start Visit', msgId, page: '/visit' });
      questionContent = null;
      break;
    case CONVERSATION_MESSAGE_TYPES.PRESCRIPTION_TYPE_ID:
      questionContent = <Prescription consultId={text.value} msgId={msgId} />;
      break;
    case CONVERSATION_MESSAGE_TYPES.DIAGNOSIS_TYPE_ID:
      questionContent = <Diagnosis consultId={text.value} msgId={msgId} />;
      break;
    case CONVERSATION_MESSAGE_TYPES.SYMPTOM_CHECK_RESULTS_TYPE_ID:
      questionContent = (
        <SymptomCheckResults
          msgId={msgId}
          results={JSON.parse(question[0])}
          conversationId={conversationId}
          showSatisfactionSelector
          scrollDown={scrollDown}
        />
      );
      break;
    case CONVERSATION_MESSAGE_TYPES.MEMBER_AGREEMENT_TYPE:
      questionContent = <Agreement {...additionalData} />;
      break;
    case CONVERSATION_MESSAGE_TYPES.QUESTION_WITH_IMAGE_TYPE_ID:
      questionContent = (
        <>
          <QuestionWithImage
            imagePath={metadata.imagePath}
            question={question}
            msgId={msgId}
            chatTime={chatTime}
            scrollDown={scrollDown}
          />
        </>
      );
      break;
    case CONVERSATION_MESSAGE_TYPES.END_OF_CONSULT_WHATS_HAPPENING_NOW_TYPE_ID:
      questionContent = (
        <WhatsHappeningNowCard
          status={data.status}
          disposition={data.disposition}
          progress={data.progress}
          mode={data.mode}
          additionalData={data}
          scrollDown={scrollDown}
          reactivateConversation={reactivateConversation}
          initiateNewChat={initiateNewChat}
        />
      );
      break;
    case CONVERSATION_MESSAGE_TYPES.PHARMACY_VIEW_TYPE_ID:
      questionContent = (
        <>
          <RenderTextMessage
            question={question}
            msgId={msgId}
            chatTime={chatTime}
            scrollDown={scrollDown}
            isLast={isLast}
          />
          <Box ml={6}>
            <PharmacyPreview pharmacy={additionalData?.pharmacy} />
          </Box>
        </>
      );
      break;
    default:
      questionContent = (
        <RenderTextMessage
          question={question}
          msgId={msgId}
          chatTime={chatTime}
          scrollDown={scrollDown}
          isLast={isLast}
        />
      );
  }

  return (
    <>
      {displayContent} {questionContent}
    </>
  );
};

const TYPES_ALLOWED_WITHOUT_ANSWER = [
  INPUT_TYPES.IMAGE_INPUT,
  INPUT_TYPES.VAXYES_PAYMENT_INPUT,
  INPUT_TYPES.PHARMACY_INPUT,
  INPUT_TYPES.PAYMENT_INPUT,
  INPUT_TYPES.ADDRESS_SEARCH_INPUT,
  INPUT_TYPES.OFF_SESSION_PAYMENT_INPUT,
];

const renderAnswers = (
  type,
  answers,
  handleResponse,
  metadata,
  question,
  updateChat,
  provider,
  reactivateConversation,
  conversationId
) => {
  if (!answers && !TYPES_ALLOWED_WITHOUT_ANSWER.includes(type)) {
    return '';
  }
  if (type === INPUT_TYPES.SINGLE_CHOICE_INPUT) {
    if (answers.length > 6) {
      return (
        <DropDownMenu
          item={question}
          items={answers.map((a, i) => ({ id: i, label: a.label }))}
          onClick={(item) => handleResponse(answers[item.id])}
        />
      );
    } else {
      return (
        <QuickReplyButtons
          items={answers.map((a, i) => ({ id: i, label: a.label }))}
          onClick={(item) => handleResponse(answers[item.id])}
        />
      );
    }
  }

  if (type === INPUT_TYPES.MULTIPLE_CHOICE_INPUT) {
    return (
      <MultiSelectQuickReply
        key={answers[0].value}
        items={answers}
        options={question.options}
        isOptional={question.isOptional}
        onSubmit={(options) =>
          handleResponse(options.filter((option) => option.yes || option.no || option.idk))
        }
      />
    );
  }
  if (type === INPUT_TYPES.CONSENTS_INPUT) {
    return <Consents consents={answers} onSubmit={handleResponse} />;
  }
  if (type === INPUT_TYPES.VAXYES_PAYMENT_INPUT) {
    return (
      <SelectPayment handlePaymentCompletion={handleResponse} {...metadata} provider={provider} />
    );
  }
  if (type === INPUT_TYPES.PHARMACY_INPUT) {
    return <PharmacyAdd onSelect={handleResponse} />;
  }
  if (type === INPUT_TYPES.IMAGE_INPUT || type === INPUT_TYPES.SMART_CARD_INPUT) {
    // TODO - As UI team is still working on the SMART_CARD_INPUT component I have replaced it with the image upload component.
    // please use SMART_CARD_INPUT separately.
    const { imageKey } = question.metadata || {};
    return (
      // TODO Image upload needs to be handled in the component
      <ProvideImage
        key={new Date().getTime()}
        handleImage={(base64String, name, type, location) => {
          handleResponse({
            base64String,
            name,
            type,
            location,
          });
        }}
        imageKey={imageKey}
      />
    );
  }

  if (type === INPUT_TYPES.APPOINTMENT_TIMESLOT_INPUT) {
    return <Appointment {...answers} onClick={handleResponse} />;
  }
  if (type === INPUT_TYPES.PAYMENT_INPUT) {
    return <Payment handlePaymentSuccess={handleResponse} {...metadata} convenienceFee={5} />;
  }
  if (type === INPUT_TYPES.OFF_SESSION_PAYMENT_INPUT) {
    return <OffSessionPayment handlePaymentSuccess={handleResponse} {...metadata} />;
  }
  if (type === INPUT_TYPES.ADDRESS_SEARCH_INPUT) {
    return <AddressSearch inputAddress={answers} onSubmit={handleResponse} />;
  }
  if (type === INPUT_TYPES.EXTERNAL_VIDEO_VISIT) {
    return renderExternalLink(answers);
  }
  if (type === INPUT_TYPES.YES_NO_DONT_KNOW_SELECT) {
    return <YesNoDontKnow id={answers[0].id || 0} onSubmit={handleResponse} />;
  }
  if (type === INPUT_TYPES.SYMPTOM_CHECKER_INPUT) {
    return (
      <SymptomInput
        onClickNext={handleResponse}
        age={answers.age || 35}
        gender={answers.gender || 'male'}
        maxResults={10}
      />
    );
  }
  if (type === INPUT_TYPES.AGREEMENT_INPUT) {
    return (
      <QuickReplyButtons
        items={[{ id: 1, label: answers }]}
        onClick={(item) => handleResponse(item)}
      />
    );
  }
  if (type === INPUT_TYPES.END_OF_VAX_YES_INPUT) {
    return <VaxYesEndOptions updateChat={updateChat} />;
  }
  if (type === INPUT_TYPES.END_OF_GGD_CONSULT_INPUT) {
    return (
      <VideoVisit
        answer={answers}
        reactivateConversation={reactivateConversation}
        question={question}
        conversationId={conversationId}
      />
    );
  }
};

const renderUserInput = ({
  question,
  input,
  type,
  disabled,
  handleSubmit,
  error,
  key,
  extraSmallScreen,
  loading,
  store,
}) => {
  const alreadyHasCtaButton =
    type === INPUT_TYPES.OTP_INPUT ||
    type === INPUT_TYPES.IMAGE_INPUT ||
    type === INPUT_TYPES.ADDRESS_SEARCH_INPUT ||
    type === INPUT_TYPES.PHARMACY_INPUT ||
    type === INPUT_TYPES.VAXYES_PAYMENT_INPUT ||
    type === INPUT_TYPES.OFF_SESSION_PAYMENT_INPUT ||
    type === INPUT_TYPES.PAYMENT_INPUT;

  return (
    <Grid container spacing={2} alignItems="center">
      <Grid item xs>
        {type === INPUT_TYPES.DATE_INPUT && !loading && (
          <DateInput key={key} input={input} error={error} />
        )}
        {type === INPUT_TYPES.PHONE_NUMBER && !loading && (
          <PhoneNumberInput key={key} input={input} error={error} />
        )}
        {type === INPUT_TYPES.OTP_INPUT && (
          <OtpInput
            onSubmit={(e) => {
              input.onChange(e);
              handleSubmit();
            }}
            disabled={disabled}
          />
        )}
        {(type === INPUT_TYPES.TEXT_INPUT || type === INPUT_TYPES.NUMBER_INPUT) && (
          <TextField
            {...input}
            autoFocus
            InputProps={{ disableUnderline: true, style: { fontSize: '1.3rem', fontWeight: 500 } }}
            disabled={disabled}
            fullWidth
            error={!!error}
            helperText={error || ''}
            key={key}
            placeholder={
              question && question.key === 'memberEmail'
                ? 'example@domain.com'
                : store.locale === 'es-mx'
                ? 'Ingrese su respuesta aquí'
                : 'Enter your response here'
            }
          />
        )}
      </Grid>
      {type !== INPUT_TYPES.OTP_INPUT && (
        <Grid item>
          {extraSmallScreen && !alreadyHasCtaButton ? (
            <Button
              color="secondary"
              aria-label="Submit"
              disabled={!input.value}
              onClick={handleSubmit}
            >
              <KeyboardArrowRightIcon />
            </Button>
          ) : (
            !alreadyHasCtaButton && (
              <SecureSubmitButton onClick={handleSubmit} disabled={!input.value} />
            )
          )}
        </Grid>
      )}
    </Grid>
  );
};

const renderUserAnswer = ({
  answer,
  msgId,
  createdOn,
  dateTime,
  msgTypeId,
  updateMessage,
  enableUpdate,
}) => {
  const chatTime = createdOn || dateTime;

  return (
    <UserAnswerGroup
      alignRight
      msgTypeId={msgTypeId}
      messages={[stripHtml(answer.value || '').result]}
      messageId={msgId}
      messageTime={chatTime}
      updateMessage={updateMessage}
      enableUpdate={enableUpdate}
    />
  );
};

const MemberChat = ({
  question,
  chatHistory = [],
  handleResponse = () => {},
  isDataLoading = false,
  memberAvatarUrl = '',
  inputValidator,
  resubmitImages = () => {},
  chatHeader = '',
  reloadChat = () => {},
  handleDeleteChat = () => {},
  enableDelete = false,
  enableResume = false,
  handleCloseDrawer = () => {},
  updateMessage = () => {},
  provider,
  redirectToChatView = () => {},
  reactivateConversation = () => {},
  conversationId,
  initiateNewChat = () => {},
}) => {
  const mainContainer = useRef();
  const chatBox = useRef();
  const chatList = useRef();
  const extraSmallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  const { store } = useContext(GlobalStore);

  // get the key from the question or a random number, this will make sure input components are unique at each rendering
  const key = question.key || Math.random();
  const answers = question.answers;
  const onSubmit = async (data, form) => {
    const value = data[INPUT.USER_TEXT_INPUT];
    // Check if there is a validator
    if (inputValidator) {
      const validity = inputValidator(value);
      if (!validity.valid) {
        // If there is an error, return it so that errors object will be populated with it
        return {
          [INPUT.USER_TEXT_INPUT]: validity.code,
        };
      }
    }

    handleResponse(value, question, question.type, question.endOfQuestions);
    form && form.reset();
    scrollDown();
  };

  const handleAnswerInput = (value) => {
    handleResponse(value, question, question.type, question.endOfQuestions);
    scrollDown();
  };

  const scrollDown = () => {
    chatList &&
      chatList.current &&
      chatBox &&
      chatBox.current &&
      chatBox.current.scrollTo(0, chatList.current.clientHeight);
    window.innerWidth < 600 &&
      store.dashboard &&
      mainContainer &&
      mainContainer.current &&
      mainContainer.current.scrollIntoView(false);
    (chatHistory.length || Array.isArray(question.question)) &&
      window.innerWidth < 600 &&
      !store.dashboard &&
      mainContainer &&
      mainContainer.current &&
      window.scrollTo(0, mainContainer.current.clientHeight);
    // Set half-second Timeout for Android devices because sets clientHeight before open the keyboard
    setTimeout(
      () =>
        chatHistory.length &&
        window.innerWidth < 600 &&
        !store.dashboard &&
        mainContainer &&
        mainContainer.current &&
        window.scrollTo(0, mainContainer.current.clientHeight),
      500,
    );
  };

  useEffect(() => scrollDown(), [chatList && chatList.current && chatList.current.clientHeight]);

  useEffect(() => {
    !store.dashboard &&
      !smallScreen &&
      document.querySelector('body').classList.add(classes.bodyRestricted);
    return () => {
      !store.dashboard &&
        !smallScreen &&
        document.querySelector('body').classList.remove(classes.bodyRestricted);
    };
  });

  const useClasses = makeStyles((theme) => ({
    bodyRestricted: {
      overflow: 'hidden',
      height: '100%',
    },
    scrollBar: {
      flex: 1,
      overflowY: 'scroll',
      [theme.breakpoints.down('xs')]: {
        overflowY: 'initial',
      },
      '&::-webkit-scrollbar': {
        width: '0.2rem',
      },
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: 'rgba(0,0,0,.2)',
        borderRadius: '0.1rem',
      },
    },
    containerWidth: {
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: '#fff',
      maxWidth: '100%',
      margin: 'auto',
      [theme.breakpoints.up('sm')]: {
        maxWidth: '600px',
      },
      [theme.breakpoints.up('lg')]: {
        maxWidth: '50%',
      },
    },
    containerHeight: {
      height: '90vh',
      [theme.breakpoints.down('xs')]: {
        height: 'initial',
        minHeight: '100vh',
      },
      '@media (min-height:944px)': {
        height: '93vh',
      },
    },
    dashboardContainer: {
      maxWidth: '100%',
      [theme.breakpoints.down('xs')]: {
        minHeight: '100vh',
        maxHeight: 'none',
      },
      [theme.breakpoints.up('sm')]: {
        height: '90vh',
      },
      '@media (min-height:944px)': {
        height: '93vh',
      },
    },
    drawerContainer: {
      maxWidth: '1024px',
      width: '100%',
      margin: 'auto',
      overflow: 'scroll',
    },
    chatHeaderMobile: {
      position: 'sticky',
      top: '-4.8rem',
      backgroundColor: '#fff',
      zIndex: 1,
    },
    btnIcon: {
      backgroundColor: 'transparent',
      padding: '0',
    },
    arrowIcon: {
      color: theme.palette.primary.main,
    },
    paperDrawer: {
      position: 'sticky',
      bottom: 0,
      backgroundColor: theme.palette.ggd.mediumGray,
    },
  }));

  const classes = useClasses();

  const updateChat = useCallback(() => {
    handleDeleteChat(false);
  }, [handleDeleteChat]);

  const DashboardCallToActionButtons = useCallback(
    ({ questionType, answers, reactivateConversation, conversationId }) => {
      if (enableResume) {
        return (
          <Box p={2}>
            <QuickReplyButtons
              items={[{ id: 1, label: store.locale === 'es-mx' ? 'Reanudar' : 'Resume' }]}
              onClick={() => redirectToChatView()}
            />
          </Box>
        );
      } else {
        if (provider === CONVERSATION_PROVIDER_ID.VAX_YES) {
          return (
            <Box p={2}>
              <VaxYesEndOptions updateChat={updateChat} />
            </Box>
          );
        }
        if (provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER) {
          return (
            <Box p={2}>
              <VaxYesEndOptions updateChat={updateChat} isBooster />
            </Box>
          );
        }
        if (questionType === INPUT_TYPES.END_OF_GGD_CONSULT_INPUT) {
          return (
            <Box p={2}>
              <VideoVisit
                answer={answers}
                reactivateConversation={reactivateConversation}
                conversationId={conversationId}
              />
            </Box>
          );
        }
        return null;
      }
    },
    [enableResume, provider, redirectToChatView, updateChat],
  );

  const isLastMessage = (history, index) => {
    if (index === history.length - 1) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <Box
      ref={mainContainer}
      className={`${classes.containerWidth} ${
        store.dashboard ? classes.dashboardContainer : classes.containerHeight
      }`}
    >
      <Box data-cy="chat_header_container" className={smallScreen ? classes.chatHeaderMobile : ''}>
        <ChatHeader
          chatHeader={chatHeader}
          reloadChat={reloadChat}
          handleDeleteChat={handleDeleteChat}
          enableDelete={enableDelete}
          handleCloseDrawer={handleCloseDrawer}
          provider={provider}
        />
      </Box>
      <Box
        data-cy="chat_container"
        ref={chatBox}
        p={store.dashboard && !smallScreen ? 5 : 2}
        className={classes.scrollBar}
      >
        <EncryptedMessage />
        <Box data-cy="chat_list" ref={chatList}>
          {chatHistory.length > 0 &&
            chatHistory.map((history, index) => {
              const isLast = isLastMessage(chatHistory, index);
              return (
                <Box key={index}>
                  {history.isQuestion.value &&
                    renderQuestion(
                      conversationId,
                      history.text.value,
                      false,
                      {
                        ...history,
                        resubmitImages,
                      },
                      question.additionalData.groupId,
                      scrollDown,
                      reactivateConversation,
                      initiateNewChat,
                      isLast,
                    )}
                  {!history.isQuestion.value && (
                    <Box data-cy="chat_answers" my={3}>
                      {renderUserAnswer({
                        answer: history.text,
                        avatarUrl: memberAvatarUrl,
                        ...history,
                        updateMessage,
                        enableUpdate: isLast && history.isEditable,
                      })}
                    </Box>
                  )}
                </Box>
              );
            })}
          {renderQuestion(
            conversationId,
            question.question,
            isDataLoading,
            question,
            question.additionalData.groupId,
            scrollDown,
            reactivateConversation,
            initiateNewChat,
          )}
        </Box>
      </Box>
      <Drawer
        data-cy="chat_user_input_drawer"
        open
        anchor="bottom"
        variant="permanent"
        style={{ position: 'sticky', bottom: 0 }}
        PaperProps={{
          className: classes.paperDrawer,
        }}
      >
        {!store.dashboard || smallScreen ? (
          !isDataLoading &&
          question.type !== INPUT_TYPES.SYMPTOM_CHECKER_OUTPUT &&
          store.showInput && (
            <>
              <Box p={2} className={classes.drawerContainer}>
                <Box>
                  {renderAnswers(
                    question.type,
                    question.answers,
                    handleAnswerInput,
                    question.additionalData,
                    question,
                    updateChat,
                    provider,
                    reactivateConversation,
                    conversationId,
                  )}
                </Box>
                {!answers && (
                  <Form
                    onSubmit={onSubmit}
                    render={({ handleSubmit }) => (
                      <form onSubmit={handleSubmit}>
                        <Field
                          name={INPUT.USER_TEXT_INPUT}
                          render={({ input, meta: { dirtySinceLastSubmit, submitError } }) =>
                            renderUserInput({
                              question,
                              input,
                              type: question.type,
                              disabled: !!answers || question.disabled || isDataLoading,
                              handleSubmit,
                              // We are validating at submit stage, if there are any such error show them as long as the field is not dirty
                              error: dirtySinceLastSubmit ? null : submitError,
                              key,
                              extraSmallScreen,
                              loading: isDataLoading,
                              store,
                            })
                          }
                        />
                      </form>
                    )}
                  />
                )}
              </Box>
            </>
          )
        ) : (
          <DashboardCallToActionButtons
            questionType={question.type}
            answers={answers}
            reactivateConversation={reactivateConversation}
            conversationId={conversationId}
          />
        )}
      </Drawer>
    </Box>
  );
};

export { MemberChat };
