import { useState, useContext, useRef } from 'react';
import { useRouter } from 'next/router';
import { BotAvatar } from './BotAvatar';
import {
  Typography,
  Box,
  Grid,
  Divider,
  IconButton,
  useMediaQuery,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Popper,
  Paper,
  ClickAwayListener,
  MenuList,
  makeStyles,
} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import ContactSupportOutlinedIcon from '@material-ui/icons/ContactSupportOutlined';
// import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
// import { grey } from '@material-ui/core/colors';
import ReplayIcon from '@material-ui/icons/Replay';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import LockIcon from '@material-ui/icons/Lock';
import { Store as GlobalStore } from '../../context';
import { getLocale } from '../../services';

const BOT_NAME = 'Amy';

const useClasses = makeStyles((theme) => ({
  popper: {
    zIndex: 9999,
  },
  paper: {
    backgroundColor: '#222525',
    borderRadius: '8px',
  },
  menuItem: {
    color: '#fff',
  },
  icon: {
    color: '#fff',
    minWidth: 0,
    marginRight: '5px',
  },
  providerText: {
    color: theme.palette.grey[300],
  },
  chatHeader: {
    position: 'sticky',
    top: '0',
    backgroundColor: '#fff',
    zIndex: 1,
  },
  chatHeaderDashboard: {
    position: 'sticky',
    top: 0,
    backgroundColor: '#fff',
    zIndex: 1,
  },
  headerTitle: {
    color: '#44444F',
    '@media (max-width:360px)': {
      fontSize: '0.8rem',
    },
  },
  lockIcon: {
    marginLeft: '-0.2rem',
    fontSize: '1rem',
    color: theme.palette.text.secondary,
    animationName: '$blinker',
    animationDuration: '1s',
    animationTimingFunction: 'linear',
    animationIterationCount: 'infinite',
  },
  '@keyframes blinker': {
    '0%': { transform: 'scale(1)' },
    '50%': { transform: 'scale(1.2)' },
    '100%': { transform: 'scale(1)' },
  },
}));

const CAPTIONS = {
  helpCenter: {
    'en-us': 'Visit help center',
    'es-mx': 'Visite el centro de ayuda',
  },
  goToHome: {
    'en-us': 'Home',
    'es-mx': 'Hogar',
  },
  goToDashboard: {
    'en-us': 'My account',
    'es-mx': 'Mi cuenta',
  },
  encrypted: {
    'en-us': 'Your Secure Assistant',
    'es-mx': 'Tu asistente segura',
  },
};

const ChatOptions = ({ children, itemOnClick, itemDisabled }) => {
  const classes = useClasses();
  const anchorRef = useRef(null);
  const locale = getLocale();
  const router = useRouter();

  const [openMenu, setOpenMenu] = useState(false);

  const handleToggleMenu = () => {
    setOpenMenu((prevState) => !prevState);
  };

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpenMenu(false);
  };

  return (
    <>
      <IconButton onClick={handleToggleMenu} ref={anchorRef} aria-label="Chat options">
        {children}
      </IconButton>
      <Popper
        open={openMenu}
        anchorEl={anchorRef.current}
        transition
        placement="bottom-end"
        className={classes.popper}
        modifiers={{
          offset: {
            enabled: true,
            offset: '0, 2',
          },
        }}
      >
        <Paper elevation={3} className={classes.paper}>
          <ClickAwayListener onClickAway={handleCloseMenu}>
            <MenuList dense aria-haspopup="true">
              {/* <MenuItem onClick={itemOnClick} disabled={itemDisabled}>
                <ListItemIcon className={classes.icon}>
                  <DeleteForeverOutlinedIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText className={classes.menuItem}>Delete and start over</ListItemText>
              </MenuItem> */}
              <MenuItem onClick={() => router.push('/')} className={classes.menuItem}>
                <ListItemIcon className={classes.icon}>
                  <HomeOutlinedIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText className={classes.menuItem}>
                  {CAPTIONS.goToHome[locale]}
                </ListItemText>
              </MenuItem>
              <MenuItem onClick={() => router.push('/dashboard/chat')} className={classes.menuItem}>
                <ListItemIcon className={classes.icon}>
                  <AccountCircleOutlinedIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText className={classes.menuItem}>
                  {CAPTIONS.goToDashboard[locale]}
                </ListItemText>
              </MenuItem>
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </>
  );
};

const ChatHeader = ({
  chatHeader = '',
  reloadChat = () => {},
  handleDeleteChat = () => {},
  enableDelete = false,
  handleCloseDrawer = () => {},
  provider = '',
}) => {
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));
  const classes = useClasses();
  const { store } = useContext(GlobalStore);
  const locale = getLocale();

  const handleDeleteClick = () => {
    handleDeleteChat(true);
  };

  return (
    <>
      <Box
        className={
          smallScreen && !store.dashboard ? classes.chatHeader : classes.chatHeaderDashboard
        }
      >
        <Box p={2}>
          <Box display="flex" justifyContent="space-between" alignItems="center">
            {store.dashboard && smallScreen ? (
              <IconButton onClick={handleCloseDrawer}>
                <ArrowBackIosIcon />
              </IconButton>
            ) : null}
            <Box>
              <Typography variant="body1" className={classes.headerTitle}>
                {chatHeader}
              </Typography>
            </Box>
            <Box display="flex">
              <IconButton
                onClick={() => window.open('https://help.gogetdoc.com', '_ blank')}
                title="Contact customer support"
              >
                <ContactSupportOutlinedIcon />
              </IconButton>
              <IconButton
                onClick={reloadChat}
                style={{ marginRight: 4 }}
                aria-label="refresh"
                title="Refresh the conversation"
              >
                <ReplayIcon />
              </IconButton>
              <ChatOptions itemOnClick={handleDeleteClick} itemDisabled={!enableDelete}>
                <MoreHorizIcon />
              </ChatOptions>
            </Box>
          </Box>
        </Box>
        <Divider />
      </Box>
      <Box style={{ padding: '14px', backgroundColor: '#fff', zIndex: 1 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Grid container spacing={1}>
              <Grid item>
                <BotAvatar />
              </Grid>
              <Grid item>
                <Grid container direction="column">
                  <Grid item xs={12}>
                    <Typography variant="body1" color="textPrimary">
                      {BOT_NAME}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Box display="flex">
                      <LockIcon className={classes.lockIcon} />
                      <span style={{ marginLeft: '0.2rem', color: '#92929D' }}>
                        {CAPTIONS.encrypted[locale]}
                      </span>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          {provider === 2 && (
            <Grid item>
              <Typography variant="subtitle2" className={classes.providerText} align="right">
                Powered by{smallScreen && <br />} Infermedica
              </Typography>
            </Grid>
          )}
        </Grid>
      </Box>
      <Divider />
    </>
  );
};

export { ChatHeader };
