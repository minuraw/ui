import React, { useState } from 'react';
import { SecureSubmitButton } from '../Chat';
import {
  Box,
  Button,
  Typography,
  FormControlLabel,
  Checkbox,
  Paper,
  Table,
  TableRow,
  TableContainer,
  TableCell,
  TableBody,
  Grid,
  makeStyles,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const useClasses = makeStyles((theme) => ({
  container: {
    minHeight: '25vh',
    [theme.breakpoints.up('sm')]: {
      minHeight: '30vh',
    },
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  tableContainer: {
    padding: '0.3rem 0',
  },
  tableCellMulti: {
    display: 'table-row',
    padding: 0,
    margin: 0,
    borderBottom: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'table-cell',
      padding: '0.5rem 1rem',
      borderBottom: '1px solid rgba(224, 224, 224, 1)',
    },
    '& .MuiGrid-container': {
      borderBottom: '1px solid rgba(224, 224, 224, 1)',
      marginBottom: '1rem',
      width: 'calc(100vw - 2rem)',
      justifyContent: 'flex-start',
      [theme.breakpoints.up('sm')]: {
        marginBottom: '0',
        borderBottom: 'none',
        width: '100%',
        justifyContent: 'flex-end',
      },
    },
  },
  tableCellSingle: {
    padding: '0 1rem',
  },
}));

const GreenCheckbox = withStyles((theme) => ({
  root: {
    '&$checked': {
      color: theme.palette.primary.main,
    },
  },
  checked: {},
}))((props) => <Checkbox color="default" {...props} />);

const MultiSelect = ({
  items = [{ id: 1, label: 'Sample item for multi select' }],
  options = [{ key: 'yes', value: '' }], // { key: "yes", value: "Yes" }, { key: "no", value: "No" }, { key: "idk", value: "Don't know" }
  requiredAll = false,
  requiredOne = true,
  isOptional = false,
  onSubmit = () => {},
}) => {
  const classes = useClasses();

  const getClass = (options) => {
    if (options) {
      if (options.length > 1) {
        return classes.tableCellMulti;
      } else {
        return classes.tableCellSingle;
      }
    }
    return null;
  };

  const [inputData, setInputData] = useState(
    items.map((item) => {
      const newItem = { ...item };
      options.forEach((option) => {
        newItem[option.key] = false;
      });
      return newItem;
    }),
  );

  const changeHandler = (e, option, item) => {
    const index = inputData.findIndex((x) => x === item);
    options.forEach((option) => {
      inputData[index][option.key] = false;
    });
    inputData[index][option.key] = e.target.checked;
    setInputData([...inputData]);
  };

  const isNextButtonDisabled = () => {
    if (requiredAll || requiredOne) {
      for (let i = 0; i < inputData.length; i++) {
        let selected = false;
        options.forEach((option) => {
          selected ||= inputData[i][option.key];
        });
        if (selected && requiredOne && !requiredAll) {
          return false;
        }
        if (!selected && requiredAll) {
          return true;
        }
      }
      if (requiredOne && !requiredAll) {
        return true;
      }
    }
    return false;
  };

  const PAGE_SIZE = 3;
  const TOTAL_PAGES = Math.ceil(inputData.length / PAGE_SIZE);
  const [pageNumber, setPageNumber] = useState(1);

  const paginate = (array, pageNumber) => {
    return array.slice((pageNumber - 1) * PAGE_SIZE, pageNumber * PAGE_SIZE);
  };

  const nextHandler = () => {
    onSubmit(inputData);
  };

  return (
    <Box className={classes.container}>
      <Box>
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table aria-label="multi-select-table">
            <TableBody>
              {paginate(inputData, pageNumber).map((item) => (
                <TableRow key={item.id}>
                  <TableCell component="th" scope="row" className={getClass(options)}>
                    <Typography variant="subtitle1">{item.label}</Typography>
                  </TableCell>
                  <TableCell component="td" className={getClass(options)}>
                    <Grid container justifyContent="flex-end">
                      {options.map((option, optionIndex) => (
                        <Grid item key={option.key + optionIndex}>
                          <Box>
                            <FormControlLabel
                              control={
                                <GreenCheckbox
                                  style={{ marginRight: 2 }}
                                  edge="end"
                                  onChange={(e) => changeHandler(e, option, item)}
                                  checked={item[option.key]}
                                  inputProps={{ 'aria-labelledby': option.key + optionIndex }}
                                />
                              }
                              label={option.value}
                            />
                          </Box>
                        </Grid>
                      ))}
                    </Grid>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <Box mt={3}>
        <Grid container justifyContent="flex-end">
          <Grid item style={{ display: 'flex' }}>
            {pageNumber > 1 && (
              <Button style={{ marginRight: '1rem' }} onClick={() => setPageNumber(pageNumber - 1)}>
                Back
              </Button>
            )}
            {pageNumber < TOTAL_PAGES && (
              <Button color="secondary" onClick={() => setPageNumber(pageNumber + 1)}>
                Next
              </Button>
            )}
            {pageNumber === TOTAL_PAGES && (
              <>
                {isOptional && (
                  <Button color="secondary" onClick={() => onSubmit([])}>
                    Skip
                  </Button>
                )}
                <SecureSubmitButton
                  label="Submit"
                  disabled={isNextButtonDisabled()}
                  onClick={nextHandler}
                />
              </>
            )}
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export { MultiSelect };
