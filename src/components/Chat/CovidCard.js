import React, { useState, useRef, useContext } from 'react';
import ReactCardFlip from 'react-card-flip';
import {
  Box,
  Grid,
  Button,
  makeStyles,
  Typography,
  Paper,
  IconButton,
  Popper,
  ClickAwayListener,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
  ButtonGroup,
  FormControlLabel,
} from '@material-ui/core';
import SystemUpdateAltOutlinedIcon from '@material-ui/icons/SystemUpdateAltOutlined';
import CachedIcon from '@material-ui/icons/Cached';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useRouter } from 'next/router';
import { isAndroid } from 'react-device-detect';
import { getWalletPass } from '../../events';
import Image from 'next/image';
import { getFormattedDateTimeString } from '../../utils';
import { CustomSwitch } from '../Common';
// import AddToAndroidPay from '../../images/Save_to_Google_Pay_dark_US-UK.svg';
import { Store as GlobalStore } from '../../context';

const CAPTIONS = {
  download: {
    'en-us': 'Download Pass',
    'es-mx': 'Descargar Cert',
  },
  update: {
    'en-us': 'Update record',
    'es-mx': 'Actualizar registro',
  },
  date: {
    'en-us': 'DATE CREATED',
    'es-mx': 'FECHA DE CREACIÓN',
  },
  available: {
    'en-us': 'Pass image not available',
    'es-mx': 'Imagen de certificado no disponible',
  },
  details: {
    'en-us': 'Details',
    'es-mx': 'Detalles',
  },
  addBooster: {
    'en-us': 'Add Booster',
    'es-mx': 'Agregar refuerzo',
  },
  doseVaccine: {
    'en-us': 'VACCINE',
    'es-mx': 'VACUNA',
  },
  doseDate: {
    'en-us': 'DATE',
    'es-mx': 'FECHA',
  },
  doseLotNumber: {
    'en-us': 'LOT NUMBER',
    'es-mx': 'NÚMERO DE LOTE',
  },
  doseVerification: {
    'en-us': 'VERIFICATION LEVEL',
    'es-mx': 'NIVEL DE VERIFICACIÓN',
  },
};

const useClasses = makeStyles((theme) => ({
  popper: {
    zIndex: 9999,
  },
  paper: {
    backgroundColor: '#000',
    borderRadius: '8px',
    margin: '0.05rem 0',
    '& .MuiList-padding': {
      padding: 0,
    },
  },
  menuItem: {
    color: '#fff',
  },
  icon: {
    color: '#fff',
    minWidth: 0,
    marginRight: '10px',
  },
  label: {
    fontWeight: 500,
  },
  formControl: {
    marginRight: 0,
  },
  imgCertificate: {
    borderRadius: '0.6rem',
  }
}));

const addToAppleWallet =
  'https://ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com/images/Add_to_Apple_Wallet_rgb_US-UK 1_no_outline.svg';

const CardOptions = ({
  children,
  topMenu,
  handleDownloadCard,
  downloadEnabled,
  hasBooster,
  certificateType,
  handleAddBooster,
  handleUpdateRecord,
}) => {
  const classes = useClasses();
  const anchorRef = useRef(null);
  const { store } = useContext(GlobalStore);

  const [openMenu, setOpenMenu] = useState(false);

  const handleToggleMenu = () => {
    setOpenMenu((prevState) => !prevState);
  };

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpenMenu(false);
  };

  return (
    <>
      <IconButton onClick={handleToggleMenu} ref={anchorRef} aria-label="Chat options">
        {children}
      </IconButton>
      <Popper
        open={openMenu}
        anchorEl={anchorRef.current}
        transition
        placement="bottom-end"
        className={classes.popper}
      >
        <Paper elevation={3} className={classes.paper}>
          <ClickAwayListener onClickAway={handleCloseMenu}>
            <MenuList dense aria-haspopup="true">
              {topMenu && downloadEnabled && (
                <MenuItem onClick={handleDownloadCard}>
                  <ListItemIcon className={classes.icon}>
                    <SystemUpdateAltOutlinedIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText className={classes.menuItem}>
                    {CAPTIONS.download[store.locale]}
                  </ListItemText>
                </MenuItem>
              )}
              {!topMenu && !hasBooster && certificateType !== 'booster' && (
                <MenuItem onClick={handleAddBooster} style={{ borderBottom: '1px solid #92929D' }}>
                  <ListItemIcon className={classes.icon}>
                    <AddBoxOutlinedIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText className={classes.menuItem}>
                    {CAPTIONS.addBooster[store.locale]}
                  </ListItemText>
                </MenuItem>
              )}
              {!topMenu && (
                <MenuItem onClick={handleUpdateRecord}>
                  <ListItemIcon className={classes.icon}>
                    <CachedIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText className={classes.menuItem}>
                    {CAPTIONS.update[store.locale]}
                  </ListItemText>
                </MenuItem>
              )}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </>
  );
};

const Dose = (props) => {
  const classes = useClasses();

  const { brand, dose, date, lotNumber, verificationLevel } = props.dose;
  const { store } = props;

  return (
    <Box data-cy="covid_card_dose_container" p={2}>
      <Box mb={1}>
        <Typography className={classes.label}>{dose}</Typography>
      </Box>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Typography variant="body2" color="textSecondary" className={classes.label}>
            {CAPTIONS.doseVaccine[store.locale]}
          </Typography>
          <Typography>{brand}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="body2" color="textSecondary" className={classes.label}>
            {CAPTIONS.doseDate[store.locale]}
          </Typography>
          <Typography>{getFormattedDateTimeString(date, 'short')}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="body2" color="textSecondary" className={classes.label}>
            {CAPTIONS.doseLotNumber[store.locale]}
          </Typography>
          <Typography>{lotNumber}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="body2" color="textSecondary" className={classes.label}>
            {CAPTIONS.doseVerification[store.locale]}
          </Typography>
          <Typography>{verificationLevel}</Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

const CovidCard = ({
  certificate,
  seeOriginal = () => {},
  hasBooster = false,
  updateChat = () => {},
}) => {
  const router = useRouter();
  const classes = useClasses();

  const [isFlipped, setIsFlipped] = useState(false);
  const { store } = useContext(GlobalStore);

  const addBooster = (e) => {
    e.stopPropagation();
    // TODO: 5 is hard-coded, this should be removed
    router.push('/chat/start/vax-yes-booster/5');
  };

  const downloadPassImage = (e, downloadUrl) => {
    e.stopPropagation();
    try {
      const link = document.createElement('a');
      link.href = downloadUrl;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (err) {
      console.error(err);
    }
  };

  const generateWalletPass = (e, type) => {
    e.stopPropagation();
    getWalletPass(downloadWalletPass, { type });
  };

  const downloadWalletPass = (_, walletPass) => {
    const passUrls = walletPass.pass_urls || [];
    const downloadNext = (i) => {
      if (i >= passUrls.length) {
        return;
      }
      const a = document.createElement('a');
      a.href = passUrls[i];
      a.target = '_parent';
      (document.body || document.documentElement).appendChild(a);
      a.click();
      // Delete the temporary link.
      a.parentNode.removeChild(a);
      // Download the next file with a small timeout. The timeout is necessary
      // for IE, which will otherwise only download the first file.
      setTimeout(() => {
        downloadNext(i + 1);
      }, 1000);
    };
    // Initiate the first download.
    downloadNext(0);
  };

  const handleDownload = (e, downloadUrl, certificateType) => {
    isAndroid ? downloadPassImage(e, downloadUrl) : generateWalletPass(e, certificateType);
  };

  const enableCertificateOptions = certificate.passImage;

  return (
    <Grid data-cy="covid_card_container" container direction="column" spacing={2}>
      <Grid item>
        <Grid container justifyContent="space-between" spacing={1} alignItems="center">
          <Grid item xs>
            <Typography variant="subtitle1" style={{ color: '#696974' }}>
              {certificate.name}
            </Typography>
          </Grid>
          <Grid item>
            <Grid item>
              <ButtonGroup style={{ backgroundColor: '#000' }} disableElevation size="small">
                <Button
                  data-cy = "covid_certificate_download_button"
                  onClick={(e) =>
                    handleDownload(e, certificate.passImage.downloadUrl, certificate.type)
                  }
                  style={{ color: '#fff', borderRight: '1px solid #92929D' }}
                >
                  {!isAndroid ? (
                    <Image
                      src={addToAppleWallet}
                      width={96}
                      height={30}
                      alt="Add to Apple Wallet"
                    />
                  ) : (
                    'Download'
                  )}
                </Button>
                {enableCertificateOptions && (
                  <CardOptions
                    topMenu
                    handleDownloadCard={(e) =>
                      downloadPassImage(e, certificate.passImage.downloadUrl)
                    }
                    downloadEnabled={certificate.passImage}
                  >
                    <ArrowDropDownIcon data-cy="vaccine_card_arrow_dropdown" style={{ color: 'white' }} />
                  </CardOptions>
                )}
              </ButtonGroup>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <ReactCardFlip data-cy="covid_card_flipper" isFlipped={isFlipped}>
          <Box style={{ backgroundColor: '#fff', borderRadius: '1rem' }}>
            {certificate.doses.map((dose) => (
              <Box key={dose.id}>
                <Dose dose={dose} store={store} key="front" />
              </Box>
            ))}
          </Box>
          <Box align="center" key="back">
            <Image
              data-cy="covid-card_image"
              src={certificate.passImage.imageUrl}
              width={800}
              height={640}
              alt="Pass Image"
              objectFit="contain"
              className={classes.imgCertificate}
            />
          </Box>
        </ReactCardFlip>
      </Grid>
      <Grid item style={{paddingTop: 0}}>
        <Grid container justifyContent="space-between" alignItems="center" spacing={1}>
          <Grid item>
            <Typography variant="subtitle2" color="textSecondary">
              {CAPTIONS.date[store.locale]}
            </Typography>
            <Typography variant="body2" color="textSecondary">
              {getFormattedDateTimeString(certificate.creationDate, 'short')}
            </Typography>
          </Grid>
          <Grid item>
            <Grid container spacing={1} alignItems="center">
              {certificate.passImage && (
                <Grid item>
                  <Box align="right" p={1}>
                    <FormControlLabel
                      control={
                        <CustomSwitch
                          data-cy="covid_card_switch"
                          checked={isFlipped}
                          onChange={() => setIsFlipped((current) => !current)}
                        />
                      }
                      className={classes.formControl}
                    />
                  </Box>
                </Grid>
              )}
              <Grid item>
                <ButtonGroup style={{ backgroundColor: '#000' }} disableElevation size="large">
                  <Button
                    style={{
                      backgroundColor: '#000',
                      color: '#fff',
                      borderRight: '1px solid #92929D',
                    }}
                    variant="contained"
                    onClick={() => seeOriginal(certificate)}
                  >
                    <Typography variant="subtitle1">{CAPTIONS.details[store.locale]}</Typography>
                  </Button>
                  <CardOptions
                    hasBooster={hasBooster}
                    certificateType={certificate.type}
                    handleAddBooster={(e) => addBooster(e)}
                    handleUpdateRecord={() => updateChat(certificate)}
                  >
                    <ArrowDropDownIcon style={{ color: 'white' }} />
                  </CardOptions>
                </ButtonGroup>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export { CovidCard };
