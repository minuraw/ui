import React, { Fragment, createRef, useCallback, useEffect, useMemo, useState } from 'react';
import { Box, Typography, Link, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getLocale } from '../../services';

const CAPTIONS = {
  resendQuestion: {
    'en-us': `Didn't receive a code?`,
    'es-mx': '¿No recibió un código?',
  },
  resend: {
    'en-us': 'Resend.',
    'es-mx': 'Reenviar.',
  },
};

const useStyles = makeStyles((theme) => ({
  centeredContainer: {
    alignItems: 'center',
    textAlign: 'center',
  },
  leftContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
  },
  otpContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  otpTextInput: {
    width: 40,
    '& input': {
      paddingLeft: 0,
      paddingRight: 0,
      textAlign: 'center',
      zIndex: 1,
    },
    '& fieldset': {
      borderRadius: 0,
      backgroundColor: theme.palette.grey[50],
      borderColor: theme.palette.grey[200],
    },
    '&:nth-child(1) fieldset , &:nth-child(5) fieldset': {
      borderRadius: '1rem 0 0 1rem',
    },
    '&:nth-child(3) fieldset, &:nth-child(7) fieldset': {
      borderRadius: '0 1rem 1rem 0',
    },
  },
  divider: {
    height: '2px',
    width: '12px',
    margin: '0 0.8rem 0 0.8rem',
    backgroundColor: theme.palette.grey[200],
  },
  caption: {
    padding: 10,
    '& a': {
      cursor: 'pointer',
    },
  },
  disabledLink: {
    color: '#888888',
  },
  link: {
    color: theme.palette.blue.main,
  },
}));

const KEY_BACKSPACE = 8;
const KEY_DELETE = 46;
const KEY_LEFT_ARROW = 37;
const KEY_RIGHT_ARROW = 39;

const OTP_LENGTH = 6;

const OtpInput = ({ disabled = false, onSubmit = () => {}, centered }) => {
  const classes = useStyles();
  const [hasError, setHasError] = useState(false);
  const [lastSubmittedValue, setLastSubmittedValue] = useState('');
  const locale = getLocale();

  const inputRefs = useMemo(() => {
    const refs = [];
    for (let i = 0; i < OTP_LENGTH; i++) {
      refs.push(createRef());
    }
    return refs;
  });

  useEffect(() => {
    if (!disabled) {
      let hadValues = false;
      inputRefs.forEach((ref) => {
        if (ref.current.value.length > 0) {
          hadValues = true;
          ref.current.value = '';
        }
      });
      if (hadValues) {
        setHasError(true);
      }
    }
    focusInputByIndex(0);
  }, [disabled]);

  const focusInputByIndex = useCallback(
    (index) => {
      inputRefs[index].current.focus();
      inputRefs[index].current.selectionStart = 1;
    },
    [inputRefs],
  );

  const resendHandler = useCallback(() => {
    onSubmit({
      event: 'RESEND',
      value: '',
    });
  }, [onSubmit]);

  const submitHandler = useCallback(
    (otpValue) => {
      if (lastSubmittedValue === otpValue) {
        return;
      }
      setLastSubmittedValue(otpValue);
      onSubmit({
        event: 'SUBMIT',
        value: otpValue,
      });
    },
    [lastSubmittedValue, onSubmit],
  );

  const handleChange = useCallback(
    (e, index) => {
      setHasError(false);
      const inputValue = inputRefs[index].current.value.replace(/\D/g, '');
      let charIndex = 0;
      let currentIndex = index;
      for (; currentIndex < OTP_LENGTH; currentIndex++) {
        if (inputValue[charIndex]) {
          inputRefs[currentIndex].current.value = inputValue[charIndex];
          charIndex++;
        } else {
          inputRefs[currentIndex].current.value = '';
          break;
        }
      }

      if (currentIndex >= OTP_LENGTH) {
        currentIndex = OTP_LENGTH - 1;
      }
      focusInputByIndex(currentIndex);

      let finalValue = '';
      inputRefs.forEach((ref) => {
        finalValue += ref.current.value;
      });

      if (finalValue.length === OTP_LENGTH) {
        submitHandler(finalValue);
      }
    },
    [focusInputByIndex, inputRefs, submitHandler],
  );

  const handleKeyUp = useCallback(
    (e, index) => {
      if (e.keyCode === KEY_BACKSPACE || e.keyCode === KEY_DELETE) {
        inputRefs[index].current.value = '';
        const previousIndex = index > 1 ? index - 1 : 0;
        focusInputByIndex(previousIndex);
      } else if (e.keyCode === KEY_LEFT_ARROW) {
        if (index > 0) {
          focusInputByIndex(index - 1);
        }
      } else if (e.keyCode === KEY_RIGHT_ARROW) {
        if (index < OTP_LENGTH - 1) {
          focusInputByIndex(index + 1);
        }
      }
    },
    [focusInputByIndex, inputRefs],
  );

  return (
    <Box className={centered ? classes.centeredContainer : classes.leftContainer}>
      <Box className={classes.otpContainer}>
        {inputRefs.map((ref, index) => (
          <Fragment key={'otp' + index}>
            <TextField
              autoFocus={index === 0}
              disabled={disabled}
              error={hasError}
              key={'otp' + index}
              defaultValue=""
              variant="outlined"
              size="small"
              className={classes.otpTextInput}
              inputRef={ref}
              onChange={(e) => handleChange(e, index)}
              onKeyUp={(e) => handleKeyUp(e, index)}
              type="tel"
              autoComplete="one-time-code"
            />
            {index === 2 ? <div className={classes.divider} /> : null}
          </Fragment>
        ))}
      </Box>
      <Typography className={classes.caption} variant="body2">
        {CAPTIONS.resendQuestion[locale]}{' '}
        {disabled ? (
          <span className={classes.disabledLink}>{CAPTIONS.resend[locale]}</span>
        ) : (
          <Link onClick={resendHandler} className={classes.link}>
            {CAPTIONS.resend[locale]}
          </Link>
        )}
      </Typography>
    </Box>
  );
};

export { OtpInput };
