/* eslint-disable no-use-before-define */
import { useState, useEffect, useCallback, useRef, useContext } from 'react';

import { useRouter } from 'next/router';

import { MemberChat } from './MemberChat';
import { CONVERSATION_MESSAGE_TYPES, INPUT_TYPES, SOURCES } from './constants';
import { v4 as uuidv4 } from 'uuid';
import {
  getMemberChatNextQuestion,
  loadChatHistory,
  saveChatHistory,
  eventListener,
  removeEventListener,
  EVENTS,
  saveConversationMetadata,
  amendIdImages,
  getConversationMetadata,
  updateConversationMetadata,
  getSymptomBotPayload,
  getVaxYesPayload,
  getVaxYesConversationId,
  deleteVaxYesConversation,
  getVaxYesBoosterConversationInfo,
  updateVaxYesGroupCode,
  trackEventGA,
  notifyAppNotification,
  getConversationMetadataByFilters,
  reactivateGGDConversation,
} from '../../events';

import {
  getInputValidator,
  getDateDDMMYYYYString,
  logCallback,
  getMessageOption,
  getChatHeaderTime,
} from '../../utils';
import { CONVERSATION_PROVIDER_ID, PROVIDER_TO_SOURCE_MAPPING } from '../../constants/providers';
import { PROTOCOL_NAMES } from '../../constants/protocolNames';
import { getFileSizeFromBase64 } from '../Common/utils';
import { Store as GlobalStore, useToggles } from '../../context';
import { CircularLoader } from '../Loader';
import { FEATURE_TOGGLES } from '../../constants';

const GGD_CHAT_SOURCES = [
  SOURCES.VAX_YES,
  SOURCES.VAX_YES_BOOSTER,
  SOURCES.GGD_CONVERSATION,
  SOURCES.GGD_TEST_CERTIFICATES,
];

const CONVERTED_CONVERSATION_MESSAGE_TYPES = [
  CONVERSATION_MESSAGE_TYPES.QUESTION_WITH_IMAGE_TYPE_ID,
  CONVERSATION_MESSAGE_TYPES.PHARMACY_VIEW_TYPE_ID,
];

const CAPTIONS = {
  started: {
    'en-us': "Let's get started",
    'es-mx': 'Empecemos',
  },
  loading: {
    'en-us': 'Loading...',
    'es-mx': 'Cargando...',
  },
  code: {
    'en-us': 'Send me the code again.',
    'es-mx': 'Envíame el código nuevamente.',
  },
  consentsDeclined: {
    'en-us': 'Consents declined.',
    'es-mx': 'Consentimientos rechazados.',
  },
  consentsAgreed: {
    'en-us': 'Agreed to all consents.',
    'es-mx': 'De acuerdo con todos los consentimientos.',
  },
  pharmacy: {
    'en-us': 'Pharmacy selected:',
    'es-mx': 'Farmacia seleccionada:',
  },
  uploaded: {
    'en-us': 'uploaded',
    'es-mx': 'cargada',
  },
  sbThanks: {
    'en-us': 'Thank you for using SymptomBot. You did great!',
    'es-mx': 'Gracias por usar SymptomBot. ¡Lo hiciste genial!',
  },
  sbResults: {
    'en-us': 'Here are your results.',
    'es-mx': 'Aquí estan sus resultados.',
  },
  sbNone: {
    'en-us': 'None selected.',
    'es-mx': 'Ninguna seleccionada',
  },
  sbQuestion: {
    'en-us': 'Question skipped.',
    'es-mx': 'Pregunta omitida.',
  },
  sending: {
    'en-us': 'Sending images to re-verify the identity.',
    'es-mx': 'Enviando imágenes para volver a verificar la identidad.',
  },
};

const ChatHOC = ({
  signIn = () => {},
  getMemberId = () => {},
  isAuthenticated,
  getMemberProfile = () => {},
  reloadChat = () => {},
  sync,
  productId,
  groupId,
  skillId,
  skill,
  skillMode,
  messageId,
  provider,
  handleCloseDrawer = () => {},
  updateConversation = () => {},
  conversationId,
  generateConversationId = () => {},
  directLink,
  isUpdate,
  ssoData,
}) => {
  const [chatHistory, setChatHistory] = useState([]);
  const [nextChatQuestion, setNextChatQuestion] = useState({});
  const [isDataLoading, setIsDataLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [conversationMetadata, setConversationMedata] = useState(null);
  const [chatHeader, setChatHeader] = useState('');
  const [enableDelete, setEnableDelete] = useState(false);
  const [enableResume, setEnableResume] = useState(false);

  const router = useRouter();
  const chatHistoryRef = useRef(chatHistory);
  const setChatHistoryRef = useRef(setChatHistory);
  const nextQuestion = nextChatQuestion.nextQuestion || {};

  const { store, setInStore } = useContext(GlobalStore);
  const { isFeatureEnabled } = useToggles();

  const skipPaymentInClinicalFlows = isFeatureEnabled(
    FEATURE_TOGGLES.SKIP_PAYMENT_IN_CLINICAL_FLOWS,
  );

  const productIdInConversationMetadata = conversationMetadata && conversationMetadata.productId;
  // Get the product id associated with the chat
  // When a member click on a product tile, product id will be passed as a prop
  // When a member return to the chat from a previous conversation use the conversation metadata to get the product id
  const associatedProductId = productId || productIdInConversationMetadata;

  // Append additional information to the next question
  // These can be used inside components
  nextQuestion.additionalData = {
    sync,
    productId: associatedProductId,
    skillId,
    skill,
    skillMode,
    provider,
    groupId,
    ...nextChatQuestion?.latestPayload,
  };

  // TODO Handle the error case
  const handleNextQuestionLoad = (err, data) => {
    setIsDataLoading(false);

    if (err) {
      notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
      return;
    }
    try {
      const randId = uuidv4();
      data.nextQuestion.questionMsgId = randId;
      data.nextQuestion.answerMsgId = randId;
    } catch (err) {
      // do nothing
    }
    setNextChatQuestion(data);

    let lastMessage = '';
    if (Array.isArray(data?.nextQuestion?.question)) {
      lastMessage += data.nextQuestion.question[data.nextQuestion.question.length - 1];
    } else {
      lastMessage += data?.nextQuestion?.question;
    }

    if (lastMessage && !store.dashboard) {
      updateConversation(conversationId, 'Amy: ' + lastMessage);
    }
  };

  useEffect(() => {
    setIsDataLoading(true);
    initiateAuthFlow();
    // eslint-disable-next-line no-use-before-define
  }, [provider]);

  useEffect(() => {
    const listenerFnc = eventListener(
      EVENTS.CHAT_NOTIFICATION_RECEIVED,
      handleChatNotificationReceived,
    );
    return () => {
      removeEventListener(EVENTS.CHAT_NOTIFICATION_RECEIVED, listenerFnc);
    };
  }, []);

  useEffect(() => {
    chatHistoryRef.current = chatHistory;
  }, [chatHistory]);

  useEffect(() => {
    setChatHistoryRef.current = setChatHistory;
  }, [setChatHistory]);

  useEffect(() => {
    deriveChatHeader();
  }, [conversationMetadata, provider, skill, skillMode]);

  useEffect(() => {
    if (conversationMetadata) {
      const { provider, skillId, endOfConversation, mode, initiatedChat } = conversationMetadata;
      // regardless of end of interaction, enable vax yes delete
      if (provider === CONVERSATION_PROVIDER_ID.VAX_YES) {
        setEnableDelete(true);
      }

      if (!endOfConversation) {
        if (provider === CONVERSATION_PROVIDER_ID.GGD_CONVERSATION) {
          // for sync chat's mode is sync
          const isSync = mode === 'sync';
          initiateGGDChat(skillId, isSync, initiatedChat);
        } else if (provider === CONVERSATION_PROVIDER_ID.ROZIE) {
          initiateRozieChat();
        } else if (provider === CONVERSATION_PROVIDER_ID.SYMPTOM_CHECKER) {
          initiateSymptomCheckerChat();
        } else if (provider === CONVERSATION_PROVIDER_ID.VAX_YES) {
          initiateVaxYesChat();
        } else if (provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER) {
          loadVaxYesBoosterChatInformation();
        } else if (provider === CONVERSATION_PROVIDER_ID.GGD_TEST_CERTIFICATES) {
          initiateTestCertificateChat(skillId, initiatedChat);
        } else if (provider === CONVERSATION_PROVIDER_ID.GGD_CO_VAX_YES) {
          initiateCoVaxYesChat();
        }
        setEnableResume(true);
      } else {
        initiateAfterConversationEndFlow();
      }
    }
  }, [conversationMetadata]);

  const deriveChatHeader = useCallback(() => {
    let description = conversationMetadata?.intent;
    if (!description) {
      const selectedSkillDescription = `${skill}-${skillMode}`;
      description = PROTOCOL_NAMES[selectedSkillDescription];
    }

    if (!description && !provider) {
      setChatHeader(
        `${CAPTIONS.started[store.locale]} ${getChatHeaderTime(new Date().toISOString())}`,
      );
      return;
    }

    if (
      provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER ||
      provider === CONVERSATION_PROVIDER_ID.VAX_YES ||
      provider === CONVERSATION_PROVIDER_ID.GGD_CO_VAX_YES
    ) {
      description = conversationMetadata?.description || PROTOCOL_NAMES['vax-yes'];
    }

    // This is to set the header correctly at the login stage
    if (!description && provider === CONVERSATION_PROVIDER_ID.GGD_TEST_CERTIFICATES) {
      description = 'TestedYes';
    }

    if (!description && !skill) {
      setChatHeader(CAPTIONS.loading[store.locale]);
      return;
    }

    if (!description) {
      description = PROTOCOL_NAMES[skill] || 'Clinical visit';
    }
    // select the time shown in the header
    // precedence as consult scheduled time > last message > chat created time
    const conversationTimeInput =
      conversationMetadata?.scheduledStartTime ||
      conversationMetadata?.lastMessageTime ||
      conversationMetadata?.createdOn ||
      new Date().toISOString();

    const conversationTime = getChatHeaderTime(conversationTimeInput);

    setChatHeader(description + ', ' + conversationTime);
  }, [conversationMetadata, provider, skill, skillMode]);

  const handleChatNotificationReceived = useCallback((payload) => {
    if (!payload) return;
    const chatConversationId = payload.conversationId;
    const messageTypeId = payload.messageTypeId;
    const messageId = payload.messageId;
    // Check if the notification received is related to the existing chat
    if (conversationId === chatConversationId) {
      const message = {
        text: { value: payload.displayText },
        isQuestion: { value: true },
        msgId: messageId,
        dateTime: new Date().toISOString(),
        msgTypeId: parseInt(messageTypeId),
      };
      setChatHistoryRef.current((chatHistory) => {
        return [...chatHistoryRef.current, message];
      });

      // for ggd conversations once a notification is received, we want to update the WHC
      // so to a reload the chat
      if (provider === CONVERSATION_PROVIDER_ID.GGD_CONVERSATION) {
        reloadChat();
      }
    }
  }, []);

  // TODO: Format the function to include types
  const getMessageTypeId = (type) => {
    switch (type) {
      case INPUT_TYPES.EXTERNAL_VIDEO_VISIT:
        return 4; // video chat type
      case INPUT_TYPES.SYMPTOM_CHECKER_OUTPUT:
        return 7; // video chat type
      case INPUT_TYPES.VAX_YES_SUCCESS_OUTPUT:
        return 10; // video chat type
      default:
        return 1; // Text type
    }
  };

  const formatMessageToPersist = useCallback(
    (messages) => {
      const temp = [];
      const memberId = getMemberId();
      for (const x in messages) {
        if (Array.isArray(messages[x].text.value)) {
          for (const i in messages[x].text.value) {
            temp.push({
              member_id: memberId,
              channel: 'web_chat',
              conv_id: conversationId,
              aws_request_id: 'not defined yet',
              msg: messages[x].text.value[i],
              is_bot: messages[x].isQuestion.value,
              message_id: messages[x].msgId,
              message_source: messages[x].msgSource,
              message_type_id: messages[x].msgTypeId || getMessageTypeId(messages[x].type),
              isEditable: messages[x].isEditable,
            });
          }
        } else {
          temp.push({
            member_id: memberId,
            channel: 'web_chat',
            conv_id: conversationId,
            aws_request_id: 'not defined yet',
            msg: messages[x].text.value,
            is_bot: messages[x].isQuestion.value,
            message_id: messages[x].msgId,
            message_source: messages[x].msgSource,
            message_type_id: messages[x].msgTypeId || getMessageTypeId(messages[x].type),
            isEditable: messages[x].isEditable,
          });
        }
      }
      return { messages: JSON.stringify(temp) };
    },
    [conversationId, getMemberId],
  );

  const handleChatHistoryLoad = useCallback(
    (err, data, payload = null) => {
      if (err) {
        console.error('ERROR LOADING CHAT HISTORY');
        notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
      } else {
        setChatHistory((chatHistory) => {
          if (payload.revertChat) {
            // This trick is to handle vaxYes flow, all the other conversations has to re-start after clicking on a card,
            // But vaxYes conversations should be able to continue even after.
            return [...data];
          } else {
            return [...chatHistory, ...data];
          }
        });

        // Get the last message
        const lastMessage = data[data.length - 1] || {};
        const lastMessageId = lastMessage.msgId;
        // If the messageId is not defined, focus it set to the last message in the chat history
        const toBeFocusedMessageId = messageId || lastMessageId;
        // This is intended to highlight the messageId if a valid is passes
        if (toBeFocusedMessageId) {
          // A timeout without a value is set so that it will be executed after setChatHistory executed
          setTimeout(() => {
            const element = document.querySelector(`div[data-messageId='${toBeFocusedMessageId}']`);
            if (element) {
              // scroll into the view
              element.scrollIntoView();
            }
          }, 0);
        }
      }
    },
    [messageId],
  );

  const initiateRozieChat = useCallback(() => {
    setIsDataLoading(true);
    // message id is available if the member already started a conversation
    if (conversationMetadata.messageId) {
      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.ROZIE,
        userId: conversationId,
        memberProfile: getMemberProfile() || {},
      });
    } else {
      // decode the skillId to get the sync or async
      const { skillId } = conversationMetadata;
      const [selectedSkillId, syncAsync] = skillId.split('-');
      const isSync = syncAsync === 'sync';

      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.ROZIE,
        userId: conversationId,
        memberProfile: getMemberProfile() || {},
        initiateChat: true,
        skillId: selectedSkillId,
        sync: isSync,
      });
    }
  }, [conversationId, conversationMetadata, getMemberProfile]);

  const initiateGGDChat = useCallback(
    (selectedSkillId, isSync, initiateChat) => {
      setIsDataLoading(true);
      const selectedSkillDescription = `${skill}-${skillMode}`;

      if (!selectedSkillId) {
        selectedSkillId = skill; // sending skill as skill id
      }

      let intent = PROTOCOL_NAMES[selectedSkillDescription];
      if (!intent) {
        intent = PROTOCOL_NAMES[skill] || 'Clinical visit';
      }

      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.GGD_CONVERSATION,
        conversationId,
        memberProfile: getMemberProfile() || {},
        initiateChat,
        skillId: selectedSkillId,
        sync: isSync,
        skipPaymentInClinicalFlows,
      });
    },
    [
      skill,
      skillMode,
      handleNextQuestionLoad,
      conversationId,
      getMemberProfile,
      skipPaymentInClinicalFlows,
    ],
  );

  const generateLoadChatHistoryParams = useCallback(() => {
    return {
      userId: getMemberId(),
      page: 0,
      count: 100,
      conversationId,
    };
  }, [getMemberId, conversationId]);

  // This method will initiate all the flows after the conversation end has reached
  // define specifics relevant to the provider
  const initiateAfterConversationEndFlow = useCallback(() => {
    if (provider === CONVERSATION_PROVIDER_ID.VAX_YES) {
      if (groupId) {
        // Update the groupId here.
        updateVaxYesGroupCode(logCallback, {
          group_code: groupId,
        });
      }
      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.END_OF_VAX_YES,
      });
    }
    if (provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER) {
      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.END_OF_VAX_YES,
      });
    }

    if (provider === CONVERSATION_PROVIDER_ID.GGD_CONVERSATION) {
      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.END_OF_GGD_CONSULT,
        conversationId,
        hasEmergency: nextQuestion.hasEmergencyEvidence,
        ...conversationMetadata,
      });
    }
  }, [provider, conversationMetadata, groupId, handleNextQuestionLoad, conversationId]);

  const handleConversationMetadataLoad = useCallback((err, data) => {
    setIsDataLoading(false);

    // at this point conversation metadata should be available in the db
    if (err || !data) {
      console.error('Error occurred when getting conversation metadata', err);
      notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
      return;
    }

    // regardless of end of interaction, enable vax yes delete
    setEnableDelete(true);

    setConversationMedata(data);
  }, []);

  // when the conversation metadata saved, redirect to the chat with the conversation id
  const handleConversationMetadataSave = useCallback(
    (err, data) => {
      if (err) {
        notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
      } else {
        const { conversationId, provider } = data;
        const additionalData = getAdditionalRouteParams();
        // now push it to correct url
        router.replace(`/chat/${conversationId}?provider=${provider}${additionalData}`, null, {
          shallow: true,
        });
      }
    },
    [router],
  );

  const getAdditionalRouteParams = () => {
    let params = '';
    if (groupId) {
      params = `${params}&groupId=${groupId}`;
    }
    return params;
  };

  const handleConversationMetadataByFilterLoad = useCallback(
    (err, data) => {
      if (err) {
        console.error('error loading conversation metadata by filter');
        notifyAppNotification(null, {
          error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.',
        });
      } else {
        const [conversation] = data;
        // a conversation is already available
        if (conversation) {
          // restrict update to other flows expect vax yes and booster
          if (
            isUpdate &&
            (provider === CONVERSATION_PROVIDER_ID.VAX_YES ||
              provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER)
          ) {
            deleteVaxYesConversation(
              () => {
                handleConversationMetadataSave(null, conversation);
              },
              {
                conversationId: conversation.conversationId,
                deleteChatHistory: false,
              },
            );
          } else {
            handleConversationMetadataSave(null, conversation);
          }
        } else {
          // no conversations, so create a new one
          initiateANewConversation();
        }
      }
    },
    [
      handleConversationMetadataSave,
      handleDeleteChat,
      initiateANewConversation,
      isUpdate,
      provider,
    ],
  );

  const initiateANewConversation = useCallback(() => {
    let description = '';
    let selectedSkillId = '';
    let intent = '';
    let mode = null;

    const memberId = getMemberId();

    if (provider === CONVERSATION_PROVIDER_ID.ROZIE) {
      // set the sync or async in the skill id
      selectedSkillId = `${skillId}-${sync ? 'sync' : 'async'}`;
      description = 'Wheel';

      const selectedSkillDescription = `${skill}-${skillMode}`;
      intent = PROTOCOL_NAMES[selectedSkillDescription] || 'Clinical visit';
    } else if (provider === CONVERSATION_PROVIDER_ID.SYMPTOM_CHECKER) {
      description = 'SymptomBot';
      intent = 'SymptomBot';
      selectedSkillId = 'symptom-checker';
    } else if (provider === CONVERSATION_PROVIDER_ID.VAX_YES) {
      description = PROTOCOL_NAMES['vax-yes'];
      intent = 'VaxYes';
      selectedSkillId = 'VaxYes';
    } else if (provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER) {
      description = PROTOCOL_NAMES['vax-yes'];
      intent = 'VaxYesBooster';
      selectedSkillId = 'VaxYesBooster';
    } else if (provider === CONVERSATION_PROVIDER_ID.GGD_CONVERSATION) {
      const selectedSkillDescription = `${skill}-${skillMode}`;

      selectedSkillId = skill;

      description = 'Wheel';

      intent = PROTOCOL_NAMES[selectedSkillDescription] || 'Clinical visit';

      mode = sync ? 'sync' : 'async';
    } else if (provider === CONVERSATION_PROVIDER_ID.GGD_TEST_CERTIFICATES) {
      description = PROTOCOL_NAMES[skillId];
      intent = 'TestedYes';
      selectedSkillId = skillId;
    } else if (provider === CONVERSATION_PROVIDER_ID.GGD_CO_VAX_YES) {
      description = PROTOCOL_NAMES['co-vax-yes'];
      intent = 'CoVaxYes';
      selectedSkillId = 'CoVaxYes';
    }

    // this implied a new conversation
    saveConversationMetadata(handleConversationMetadataSave, {
      conversationId: generateConversationId(),
      memberId: memberId,
      description,
      productId,
      intent: intent,
      skillId: selectedSkillId,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone, // Set the timezone of the browser for now
      provider,
      mode,
    });
  }, [
    generateConversationId,
    getMemberId,
    handleConversationMetadataSave,
    productId,
    provider,
    skill,
    skillId,
    skillMode,
    sync,
  ]);

  const handleVaxYesCertificatedLoad = useCallback(
    (err, payload) => {
      const certificate = payload ? payload.certificate : null;
      if (err) {
        notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
        return null;
      } else {
        // if certificates are available, redirect to wallet
        if (certificate && certificate.certs && certificate.certs.length > 0) {
          if (groupId) {
            // Update the groupId here.
            updateVaxYesGroupCode(logCallback, {
              group_code: groupId,
            });
          }
          router.push('/dashboard/wallet/covid');
          return;
        }
        continueMemberConversationFlow();
      }
    },
    [continueMemberConversationFlow, router],
  );

  const continueMemberConversationFlow = useCallback(() => {
    const memberId = getMemberId();

    // if there is a conversation id, there is a already initiated conversation
    if (conversationId) {
      loadChatHistory(handleChatHistoryLoad, {
        ...generateLoadChatHistoryParams(),
        place: 1,
      });

      getConversationMetadata(handleConversationMetadataLoad, {
        memberId,
        conversationId,
      });
    } else {
      // vaxyes and vaxyes booster only have one conversation, there we are going check if those are available
      if (
        provider === CONVERSATION_PROVIDER_ID.VAX_YES ||
        provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER
      ) {
        getConversationMetadataByFilters(handleConversationMetadataByFilterLoad, { provider });
        return;
      }
      initiateANewConversation();
    }
  }, [
    conversationId,
    generateLoadChatHistoryParams,
    getMemberId,
    handleChatHistoryLoad,
    handleConversationMetadataByFilterLoad,
    handleConversationMetadataLoad,
    initiateANewConversation,
    provider,
  ]);

  const initiateMemberConversationFlow = useCallback(() => {
    // see if vaxyes certificates ara available if this is not an update flow
    if (directLink && provider === CONVERSATION_PROVIDER_ID.VAX_YES && !isUpdate) {
      getVaxYesConversationId(handleVaxYesCertificatedLoad, {
        formatResponse,
      });
      return;
    }
    continueMemberConversationFlow();
  }, [continueMemberConversationFlow, directLink, handleVaxYesCertificatedLoad, provider]);

  const initiateAuthFlow = useCallback(
    (place = null) => {
      if (isAuthenticated) {
        initiateMemberConversationFlow();
      } else {
        getMemberChatNextQuestion(handleNextQuestionLoad, {
          source: SOURCES.GGD_AUTH,
          memberProfile: getMemberProfile() || {},
          questions: null,
          provider,
          // pass the agreement type so that we can generically show agreement message
          meta: {
            agreementType: PROVIDER_TO_SOURCE_MAPPING[provider],
            ssoData,
            skill
          },
        });
      }
    },
    [getMemberProfile, initiateMemberConversationFlow, isAuthenticated, provider],
  );

  const handleSymptomBotConversationInit = (_, payload) => {
    getMemberChatNextQuestion(handleNextQuestionLoad, {
      source: SOURCES.SYMPTOM_CHECKER_GGD,
      memberProfile: getMemberProfile() || {},
      initiateChat: true,
      questions: null,
      handleEndOfQuestions,
      latestPayload: payload,
    });
  };

  const handleVaxYesConversationInit = (_, payload) => {
    getMemberChatNextQuestion(handleNextQuestionLoad, {
      source: SOURCES.VAX_YES,
      memberProfile: getMemberProfile() || {},
      initiateChat: true,
      questions: null,
      handleEndOfQuestions,
      latestPayload: payload,
      groupId,
    });
  };

  const handleCoVaxYesConversationInit = (_, payload) => {
    getMemberChatNextQuestion(handleNextQuestionLoad, {
      source: SOURCES.GGD_CO_VAX_YES,
      memberProfile: getMemberProfile() || {},
      initiateChat: true,
      questions: null,
      handleEndOfQuestions,
      latestPayload: payload,
      groupId,
    });
  };

  const initiateSymptomCheckerChat = useCallback(() => {
    setIsDataLoading(true);
    getSymptomBotPayload(handleSymptomBotConversationInit, {
      id: conversationId,
    });
  }, [conversationId, handleSymptomBotConversationInit]);

  const handleVaxYesInitCallBack = useCallback(
    (err, payload) => {
      const convId = payload ? payload.conversationId : conversationId;
      const endOfInteraction = payload ? payload.endOfInteraction : false;
      // const certificate = payload ? payload.certificate : null;
      if (err) {
        notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
        return null;
      } else {
        // TODO: code clean up
        // if (certificate && certificate.certs && certificate.certs.length > 0 && directLink) {
        //   router.push('/dashboard/wallet');
        //   return;
        // }
        if (convId) {
          if (groupId) {
            // Update the groupId here.
            updateVaxYesGroupCode(logCallback, {
              group_code: groupId,
            });
          }
          loadChatHistory(handleChatHistoryLoad, {
            userId: getMemberId(),
            page: 0,
            count: 100,
            conversationId,
            revertChat: true,
            place: 2,
          });
        }
        if (!endOfInteraction) {
          getVaxYesPayload(handleVaxYesConversationInit, {
            id: conversationId,
          });
        } else {
          // Initiate the end of chat flow
          initiateAfterConversationEndFlow();
        }
      }
    },
    [
      conversationMetadata,
      getMemberId,
      handleChatHistoryLoad,
      handleVaxYesConversationInit,
      initiateAfterConversationEndFlow,
      productId,
    ],
  );

  const handleCoVaxYesInitCallBack = useCallback(
    (err, payload) => {
      const convId = payload ? payload.conversationId : conversationId;
      const endOfInteraction = payload ? payload.endOfInteraction : false;

      if (err) {
        notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
        return null;
      } else {
        if (convId) {
          if (groupId) {
            // Update the groupId here.
            updateVaxYesGroupCode(logCallback, {
              group_code: groupId,
            });
          }
          loadChatHistory(handleChatHistoryLoad, {
            userId: getMemberId(),
            page: 0,
            count: 100,
            conversationId,
            revertChat: true,
            place: 2,
          });
        }
        if (!endOfInteraction) {
          getVaxYesPayload(handleCoVaxYesConversationInit, {
            id: conversationId,
          });
        } else {
          // Initiate the end of chat flow
          initiateAfterConversationEndFlow();
        }
      }
    },
    [
      conversationMetadata,
      getMemberId,
      handleChatHistoryLoad,
      handleCoVaxYesConversationInit,
      initiateAfterConversationEndFlow,
      productId,
    ],
  );

  const formatResponse = (payload, certificate) => {
    return {
      conversationId: payload && payload.length > 0 ? payload[0].conversation_id : null,
      endOfInteraction: payload && payload.length > 0 ? payload[0].end_of_conversation : null,
      certificate,
    };
  };

  const initiateVaxYesChat = useCallback(() => {
    setIsDataLoading(true);
    getVaxYesConversationId(handleVaxYesInitCallBack, {
      formatResponse,
    });
  }, [handleVaxYesInitCallBack]);

  const initiateCoVaxYesChat = useCallback(() => {
    setIsDataLoading(true);

    // TODO - get conversation id
    handleCoVaxYesInitCallBack(null, {});
  }, [handleCoVaxYesInitCallBack]);

  const handleVaxYesConversationInfoLoad = useCallback(
    (err, data) => {
      setIsDataLoading(true);
      if (err) {
        notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
        console.error('Error occurred getting vaxyes booster information');
        setIsDataLoading(false);
        return;
      }

      const latestPayload = data.context;
      const endOfConversation = data.endOfConversation;

      if (!endOfConversation) {
        setEnableResume(true);
      }

      const memberId = getMemberId();
      const memberProfile = getMemberProfile() || {};

      // Only load the next question if the end of interaction not reached
      if (!endOfConversation) {
        getMemberChatNextQuestion(handleNextQuestionLoad, {
          source: SOURCES.VAX_YES_BOOSTER,
          memberProfile,
          memberId,
          latestPayload,
          conversationId,
          handleEndOfQuestions,
        });
      } else {
        // we are done with all the data loading
        setIsDataLoading(false);
        initiateAfterConversationEndFlow();
      }
    },
    [conversationId, getMemberId, getMemberProfile, handleEndOfQuestions],
  );

  const initiateTestCertificateChat = useCallback(
    (skillId, initiatedChat) => {
      setIsDataLoading(true);
      const memberProfile = getMemberProfile() || {};
      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: SOURCES.GGD_TEST_CERTIFICATES,
        conversationId,
        initiatedChat,
        skillId,
        memberProfile,
        handleEndOfQuestions,
      });
    },
    [conversationId, handleEndOfQuestions, handleNextQuestionLoad],
  );

  const loadVaxYesBoosterChatInformation = useCallback(() => {
    setIsDataLoading(true);
    getVaxYesBoosterConversationInfo(handleVaxYesConversationInfoLoad);
  }, [handleVaxYesConversationInfoLoad]);

  const handleEndOfQuestions = useCallback(
    (source, metadata) => {
      // coming to chat view without a provider is an indication chat was initiated to login only
      // redirect to dashboard in that case
      if (source === SOURCES.GGD_AUTH && !provider) {
        router.push('/dashboard/chat');
        return;
      }
      if (source === SOURCES.VAX_YES) {
        const memberId = getMemberId();
        const messages = [];
        const len = metadata.nextQuestion.question.length;
        for (let i = 0; i < len - 2; i += 1) {
          messages.push({
            text: { value: metadata.nextQuestion.question[i] },
            isQuestion: { value: true },
            msgId: uuidv4(),
            createdOn: Date.now(),
            msgSource: SOURCES.VAX_YES,
          });
        }
        messages.push({
          text: { value: metadata.nextQuestion.question[len - 1] },
          isQuestion: { value: true },
          msgId: uuidv4(),
          createdOn: Date.now(),
          msgSource: SOURCES.VAX_YES,
          type: INPUT_TYPES.VAX_YES_SUCCESS_OUTPUT,
          msgTypeId: CONVERSATION_MESSAGE_TYPES.VAX_YES_SUCCESS_OUTPUT,
        });
        saveChatHistory(logCallback, formatMessageToPersist(messages));
        updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
        trackEventGA(null, {
          event: 'conversion',
          product: conversationMetadata?.provider || provider,
          messageOption: getMessageOption(
            conversationMetadata?.provider || provider,
            null,
            null,
            metadata.latestPayload,
          ),
        });
        setChatHistory((chatHistory) => {
          return [...chatHistory, ...messages];
        });
        // router.push(
        //   `/dashboard/chat/${conversationId}?provider=${CONVERSATION_PROVIDER_ID.VAX_YES}`,
        // );
      }
      if (source === SOURCES.VAX_YES_BOOSTER) {
        const memberId = getMemberId();
        const msg = {
          text: {
            value: Array.isArray(metadata.nextQuestion.question)
              ? metadata.nextQuestion.question[0]
              : '',
          },
          isQuestion: { value: true },
          msgId: uuidv4(),
          msgSource: SOURCES.VAX_YES_BOOSTER,
          createdOn: Date.now(),
        };
        saveChatHistory(logCallback, formatMessageToPersist([msg]));
        updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
        setChatHistory((chatHistory) => {
          return [...chatHistory, msg];
        });
        // router.push(
        //   `/dashboard/chat/${conversationId}?provider=${CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER}`,
        // );
        trackEventGA(null, {
          event: 'conversion',
          product: conversationMetadata?.provider || provider,
          messageOption: getMessageOption(
            conversationMetadata?.provider || provider,
            null,
            null,
            metadata.latestPayload,
          ),
        });
      }
      if (source === SOURCES.SYMPTOM_CHECKER_GGD) {
        const msg = {
          text: { value: metadata.nextQuestion.question },
          isQuestion: { value: true },
          msgId: uuidv4(),
          createdOn: Date.now(),
          type: INPUT_TYPES.SYMPTOM_CHECKER_OUTPUT,
        };
        const msg2 = {
          text: { value: CAPTIONS.sbThanks[store.locale] },
          isQuestion: { value: true },
          msgId: metadata.nextQuestion.id,
          createdOn: Date.now(),
          type: INPUT_TYPES.SYMPTOM_CHECKER_OUTPUT,
        };
        setChatHistory((chatHistory) => {
          return [...chatHistory, msg2];
        });
        if (isAuthenticated) {
          const memberId = getMemberId();
          updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
          saveChatHistory(logCallback, formatMessageToPersist([msg]));
          updateConversation(conversationId, CAPTIONS.sbResults[store.locale]);
        }
        trackEventGA(null, {
          event: 'conversion',
          product: conversationMetadata?.provider || provider,
          messageOption: getMessageOption(
            conversationMetadata?.provider || provider,
            null,
            null,
            metadata.latestPayload,
          ),
        });
        return metadata;
      }

      if (source === SOURCES.GGD_AUTH) {
        initiateMemberConversationFlow();
        if (!metadata.nextQuestion.authData) {
          trackEventGA(null, {
            event: 'account_creation',
            product: conversationMetadata?.provider || provider,
          });
        }
        return;
      }

      if (source === SOURCES.ROZIE) {
        // at this point save the final response
        const rozieMetadata = metadata[SOURCES.ROZIE];
        const messages = rozieMetadata.messages;

        const memberId = getMemberId();

        // Save the messages
        saveChatHistory(logCallback, formatMessageToPersist(messages));
        // Mark conversation reached the end
        updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
        setChatHistory((chatHistory) => {
          return [...chatHistory, ...messages];
        });
        updateConversation(conversationId, messages[0]?.text?.value);
      }
      if (source === SOURCES.GGD_CONVERSATION) {
        const messages = metadata.messages;
        const question = metadata.nextQuestion;
        nextQuestion.hasEmergencyEvidence = question.hasEmergencyEvidence;
        const memberId = getMemberId();

        // Save the messages
        saveChatHistory(logCallback, formatMessageToPersist(messages));
        // Mark conversation reached the end
        updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
        setChatHistory((chatHistory) => {
          return [...chatHistory, ...messages];
        });
        updateConversation(conversationId, messages[0]?.text?.value);
        trackEventGA(null, {
          event: 'conversion',
          product: conversationMetadata?.provider || provider,
          messageOption: getMessageOption(
            conversationMetadata?.provider || provider,
            null,
            null,
            metadata.latestPayload,
          ),
        });
      }
      if (source === SOURCES.GGD_TEST_CERTIFICATES) {
        const memberId = getMemberId();
        const messages = metadata.messages;
        saveChatHistory(logCallback, formatMessageToPersist(messages));
        updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
        trackEventGA(null, {
          event: 'conversion',
          product: conversationMetadata?.provider || provider,
          messageOption: getMessageOption(
            conversationMetadata?.provider || provider,
            null,
            null,
            metadata.latestPayload,
          ),
        });
        setChatHistory((chatHistory) => {
          return [...chatHistory, ...messages];
        });
      }
      if (source === SOURCES.GGD_CO_VAX_YES) {
        const memberId = getMemberId();
        const messages = [];
        const len = metadata.nextQuestion.question.length;
        for (let i = 0; i < len - 2; i += 1) {
          messages.push({
            text: { value: metadata.nextQuestion.question[i] },
            isQuestion: { value: true },
            msgId: uuidv4(),
            createdOn: Date.now(),
            msgSource: SOURCES.GGD_CO_VAX_YES,
          });
        }
        messages.push({
          text: { value: metadata.nextQuestion.question[len - 1] },
          isQuestion: { value: true },
          msgId: uuidv4(),
          createdOn: Date.now(),
          msgSource: SOURCES.GGD_CO_VAX_YES,
          type: INPUT_TYPES.VAX_YES_SUCCESS_OUTPUT,
        });
        saveChatHistory(logCallback, formatMessageToPersist(messages));
        updateConversationMetadata(null, { endOfConversation: true, memberId, conversationId });
        trackEventGA(null, {
          event: 'conversion',
          product: skill,
          messageOption: getMessageOption(skill, null, null, metadata.latestPayload),
        });
        setChatHistory((chatHistory) => {
          return [...chatHistory, ...messages];
        });
        setNextChatQuestion({});
        setIsDataLoading(false);
      }
      // Initiate end of conversation flow here
      initiateAfterConversationEndFlow();
    },
    [
      conversationId,
      formatMessageToPersist,
      getMemberId,
      initiateAfterConversationEndFlow,
      initiateMemberConversationFlow,
      isAuthenticated,
      provider,
      router,
      skill,
      updateConversation,
    ],
  );

  const loadNextQuestion = useCallback(
    (answer, question, questions, latestPayload = {}, conversationId = null, meta = {}) => {
      setIsDataLoading(true);
      getMemberChatNextQuestion(handleNextQuestionLoad, {
        source: question.source,
        userId: conversationId,
        memberProfile: getMemberProfile() || {},
        answer,
        question,
        questions,
        signIn,
        handleEndOfQuestions,
        memberId: getMemberId(),
        latestPayload,
        conversationId,
        meta,
        groupId,
        skipPaymentInClinicalFlows,
      });
    },
    [getMemberId, getMemberProfile, handleEndOfQuestions, signIn, skipPaymentInClinicalFlows],
  );

  const getConvoMessageTypeId = (msgInputType, type, question) => {
    if (question && question.display && question.display.length > 0) {
      return CONVERSATION_MESSAGE_TYPES.QUESTION_WITH_DISPLAY_TYPE_ID;
    }
    if (CONVERTED_CONVERSATION_MESSAGE_TYPES.includes(msgInputType)) {
      return CONVERSATION_MESSAGE_TYPES.TEXT_MESSAGE_TYPE_ID;
    }
    if (type === INPUT_TYPES.PHARMACY_INPUT) {
      return CONVERSATION_MESSAGE_TYPES.PHARMACY_VIEW_TYPE_ID;
    }
    return msgInputType;
  };
  const handleResponse = async (answer, question, type, eoq = false) => {
    let answerValue = answer;
    let answerMessageType = CONVERSATION_MESSAGE_TYPES.TEXT_MESSAGE_TYPE_ID;
    if (type === INPUT_TYPES.IMAGE_INPUT) {
      // need to fix rozie flow
      if (GGD_CHAT_SOURCES.includes(question.source)) {
        const imagePath = answer.location;
        answerValue = JSON.stringify({
          validatedAnswer: {
            mainMsg: question.metadata.message,
            subMsg: getFileSizeFromBase64(answer.base64String),
          },
          imagePath,
        });
        answerMessageType = CONVERSATION_MESSAGE_TYPES.IMAGE_PREVIEW_TYPE_ID;
      } else {
        answerValue = `${answer.name} uploaded`;
      }
    } else if (type === INPUT_TYPES.DATE_INPUT) {
      answer = getDateDDMMYYYYString(answer, store.locale);
      answerValue = answer;
    } else if (type === INPUT_TYPES.SINGLE_CHOICE_INPUT) {
      answerValue = answer.label;
    } else if (type === INPUT_TYPES.MULTIPLE_CHOICE_INPUT) {
      answerValue = '';
      if (answer.length > 0) {
        for (let i = 0; i < answer.length; i++) {
          if (answer[i].yes) {
            if (answerValue === '') {
              answerValue += answer[i].label;
            } else {
              answerValue += ', ' + answer[i].label;
            }
          }
        }
        if (answerValue === '') {
          answerValue = CAPTIONS.sbNone[store.locale];
        }
      } else {
        answerValue = CAPTIONS.sbQuestion[store.locale];
      }
    } else if (type === INPUT_TYPES.SYMPTOM_CHECKER_INPUT) {
      answerValue = '';
      if (answer.length > 0) {
        for (let i = 0; i < answer.length; i++) {
          if (answerValue === '') {
            answerValue += answer[i].label;
          } else {
            answerValue += ', ' + answer[i].label;
          }
        }
      }
    } else if (type === INPUT_TYPES.CONSENTS_INPUT) {
      if (answer.length === 0) {
        answerValue = CAPTIONS.consentsDeclined[store.locale];
      } else {
        answerValue = CAPTIONS.consentsAgreed[store.locale];
      }
    } else if (type === INPUT_TYPES.PHARMACY_INPUT) {
      answerValue = JSON.stringify(answer);
    } else if (type === INPUT_TYPES.APPOINTMENT_TIMESLOT_INPUT) {
      answerValue = answer.message;
    } else if (type === INPUT_TYPES.OTP_INPUT) {
      if (answer.event === 'RESEND') {
        answerValue = CAPTIONS.code[store.locale];
      } else {
        // SUBMIT default
        answerValue = answer.value;
      }
    } else if (type === INPUT_TYPES.ADDRESS_SEARCH_INPUT) {
      answerValue = answer.fullAddress;
    }

    if (type === INPUT_TYPES.PAYMENT_INPUT) {
      answerValue = answer;
    }

    if (type === INPUT_TYPES.OFF_SESSION_PAYMENT_INPUT) {
      answerValue = answer.msg;
    }

    if (type === INPUT_TYPES.YES_NO_DONT_KNOW_SELECT) {
      answerValue = answer.value;
    }

    if (type === INPUT_TYPES.AGREEMENT_INPUT) {
      answerValue = answer.label;
    }

    const questionId = question.questionMsgId;
    const answerId = question.answerMsgId;

    let questionValue = question.question;
    if (question.display && question.display.length > 0) {
      questionValue = JSON.stringify({
        text: question.question,
        display: question.display,
      });
    }
    const msg1 = {
      text: { value: questionValue },
      isQuestion: { value: true },
      msgId: questionId,
      createdOn: question.createdOn,
      msgSource: question.source,
      type,
      msgTypeId: getConvoMessageTypeId(question.msgTypeId, type, question),
    };

    const msg2 = {
      text: { value: answerValue },
      isQuestion: { value: false },
      msgId: answerId,
      msgSource: question.source,
      createdOn: Date.now(),
      type,
      msgTypeId: getConvoMessageTypeId(answerMessageType, type),
      isEditable: question.isEditable,
    };
    // TODO parse messages as an array. it can reduce the number of calls.
    if (isAuthenticated) {
      saveChatHistory(logCallback, formatMessageToPersist([msg1, msg2]));
    }

    if (type === INPUT_TYPES.IMAGE_INPUT) {
      // need to fix rozie flow
      if (GGD_CHAT_SOURCES.includes(question.source)) {
        const currentAnswer = JSON.parse(answerValue);
        msg2.text.value = JSON.stringify({
          ...currentAnswer,
          imageBase64: answer.base64String,
        });
        answerMessageType = CONVERSATION_MESSAGE_TYPES.IMAGE_PREVIEW_TYPE_ID;
        answerValue = `${answer.name} ${CAPTIONS.uploaded[store.locale]}`;
      }
    }

    const getEventType = (type) => {
      switch (type) {
        case INPUT_TYPES.PAYMENT_INPUT:
          return 'payment';
        case INPUT_TYPES.VAXYES_PAYMENT_INPUT:
          return 'payment';
        case INPUT_TYPES.OFF_SESSION_PAYMENT_INPUT:
          return 'payment';
        default:
          return 'message';
      }
    };

    trackEventGA(null, {
      event: getEventType(type),
      memberSignedIn: isAuthenticated,
      product: conversationMetadata?.provider || provider,
      messageNumber: question.questionId,
      messageOption: getMessageOption(
        conversationMetadata?.provider || provider,
        answerValue,
        nextQuestion,
        nextChatQuestion.latestPayload,
      ),
    });

    setChatHistory((chatHistory) => {
      return [...chatHistory, msg1, msg2];
    });

    updateConversation(conversationId, answerValue);

    // Save latest payload in the DynamoDB
    const questions = nextChatQuestion?.questions;
    const latestPayload = nextChatQuestion?.latestPayload;

    question.answer = answer;
    // if (question.source === SOURCES.SYMPTOM_CHECKER_INIT) {
    //   const conversationId = getConversationId();
    //   const memberId = getMemberId();
    //   if (type === INPUT_TYPES.SYMPTOM_CHECKER_INPUT) {
    //     updateConversationMetadata(null, { description: answerValue, memberId, conversationId });
    //   }
    //   questions.forEach((q) => {
    //     if (q.id === question.id) {
    //       q.answer = answer;
    //     }
    //   });
    // }
    if (!eoq) {
      loadNextQuestion(answer, question, questions, latestPayload, conversationId, {
        answerId,
        questionId,
      });
    }
  };

  const resubmitImages = (faceImage, idImage, messageId) => {
    const msg = {
      text: { value: CAPTIONS.sending[store.locale] },
      isQuestion: { value: false },
      msgId: uuidv4(),
      msgSource: SOURCES.ROZIE,
    };
    if (isAuthenticated) {
      saveChatHistory(logCallback, formatMessageToPersist([msg]));
    }
    setChatHistory((chatHistory) => {
      const temp = chatHistory.map((chat) => {
        if (chat.msgId === messageId) {
          chat.msgTypeId = 2;
        }
        return chat;
      });
      return [...temp, msg];
    });
    const memberProfile = getMemberProfile();
    const payload = {
      photos: [
        {
          base64String: faceImage.image,
          name: faceImage.name,
          type: faceImage.type,
          title: 'face',
        },
        {
          base64String: idImage.image,
          name: idImage.name,
          type: idImage.type,
          title: 'identification',
        },
      ],
      names: [
        {
          use: 'official',
          text: `${memberProfile.firstName} ${memberProfile.lastName}`,
          family: memberProfile.lastName,
          given: [memberProfile.firstName],
        },
      ],
      birthDate: memberProfile.dob,
      gender: memberProfile.gender.toLowerCase(),
      conversationId: conversationId,
      messageId: messageId,
    };
    amendIdImages(logCallback, payload);
    updateConversation(conversationId, msg.text.value);
  };

  const updateMessage = (payload) => {
    const currentQuestion = nextQuestion;
    currentQuestion.updatePrevAnswer = true;
    removeMessagesFromChatHistoryById(payload);
    loadNextQuestion(null, currentQuestion, null, nextChatQuestion.latestPayload, conversationId, {
      msgId: payload,
    });
  };

  const removeMessagesFromChatHistoryById = (msgId) => {
    const oldHistory = chatHistory;
    const newHistory = oldHistory.filter((h) => h.msgId !== msgId);
    setChatHistory(newHistory);
  };

  const redirectToChatView = useCallback(
    (chatUrlToRedirect) => {
      setLoading(true);
      const currentPath = chatUrlToRedirect || router.asPath;
      const pathParts = currentPath.split('/dashboard/');
      if (pathParts.length > 1) {
        // If the users comes from the dashboard just replace the link, it will trigger the conversation.
        const pathSections = pathParts[pathParts.length - 1].split('/');
        router.push(`/chat/${pathSections[pathSections.length - 1]}`);
      } else {
        // If user is already in the chat view, need to trigger the start of the conversation manually.
        setLoading(false);
        initiateMemberConversationFlow();
      }
    },
    [router],
  );

  const handleDeleteChat = useCallback(
    (deleteChatHistory = true) => {
      // this is currently only for vax-yes
      setChatHistory([]);
      setLoading(true);
      deleteVaxYesConversation(
        () => {
          redirectToChatView();
        },
        {
          conversationId,
          deleteChatHistory,
        },
      );
    },
    [conversationId, redirectToChatView],
  );

  const reactivateConversation = useCallback(
    (deleteChatHistory = true, triggerEvent) => {
      setChatHistory([]);
      setLoading(true);
      getVaxYesPayload(
        (_, payload) => {
          reactivateGGDConversation(
            () => {
              redirectToChatView();
            },
            {
              conversationId,
              deleteChatHistory,
              triggerEvent,
              latestPayload: payload,
            },
          );
        },
        {
          id: conversationId,
        },
      );
    },
    [conversationId, redirectToChatView],
  );

  const initiateNewChat = useCallback(() => {
    setLoading(true);
    setInStore({ dashboard: false });
    router.push(
      `/chat/start/${conversationMetadata?.productId}/${conversationMetadata?.skillId}/${conversationMetadata?.mode}`,
    );
  }, [conversationMetadata, router, setInStore]);

  if (loading) {
    return <CircularLoader GGD />;
  }

  return (
    <MemberChat
      question={nextQuestion}
      chatHistory={chatHistory}
      handleResponse={handleResponse}
      isDataLoading={isDataLoading}
      inputValidator={getInputValidator(nextQuestion.type, nextQuestion.metadata, store.locale)}
      resubmitImages={resubmitImages}
      chatHeader={chatHeader}
      reloadChat={reloadChat}
      handleDeleteChat={handleDeleteChat}
      enableDelete={enableDelete}
      enableResume={enableResume}
      handleCloseDrawer={handleCloseDrawer}
      updateMessage={updateMessage}
      provider={provider}
      redirectToChatView={redirectToChatView}
      reactivateConversation={reactivateConversation}
      conversationId={conversationId}
      initiateNewChat={initiateNewChat}
    />
  );
};

export { ChatHOC };
