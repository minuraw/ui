/* eslint-disable camelcase */
import React, { useState } from 'react';
import { Grid, TextField, Box, Link, makeStyles, Typography } from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import createDecorator from 'final-form-focus';
import { string, object } from 'yup';
import { useYupValidation } from '../../hooks';
import { COUNTRIES_ISO_CODES } from '../../constants';
import { MapboxLocationAutocomplete } from '../Common';
import { AddressCard } from '../Cards';
import { SecureSubmitButton } from './SecureSubmitButton';
import { ActionDrawerWrapper } from './ActionDrawerWrapper';

const useClasses = makeStyles((theme) => ({
  btnContainer: {
    '& .MuiGrid-container': {
      justifyContent: 'flex-end',
    },
  },
  link: {
    color: theme.palette.blue.main,
  },
}));

const focusOnErrors = createDecorator();

const validationSchema = object({
  country: string().required().trim().label('Country'),
  street: string().required().trim().label('Street'),
  city: string().required().trim().label('City'),
  state: string().required().trim().label('State'),
  zip_code: string()
    .required()
    .matches(/^[0-9]+$/, 'Zip code must be only digits')
    .min(5, 'Zip code must be exactly 5 digits')
    .max(5, 'Zip code must be exactly 5 digits')
    .label('Zip code'),
  line2: string().trim().label('Address Line 2'),
  line3: string().trim().label('Unit Number'),
});

const AddressSearch = ({
  onSubmit = () => {},
  inputAddress = {},
  placeholder = 'Search for your address',
  btnLabel = 'Next',
  disableAdditional,
  submitOnSelect = false,
}) => {
  const { validate } = useYupValidation();
  const classes = useClasses();
  // const [manualEntry, setManualEntry] = useState(false);
  const [address, setAddress] = useState(inputAddress?.country ? inputAddress : null);

  const validateForm = async (data) => {
    const errors = await validate(validationSchema)(data);

    if (errors) return errors;
    let fullAddress = data.city + ', ' + data.state + ' ' + data.zip_code + ', ' + data.country;

    if (!data.line3) {
      data.line3 = '';
    } else {
      fullAddress = data.line3 + ', ' + fullAddress;
    }
    if (!data.line2) {
      data.line2 = '';
    } else {
      fullAddress = data.line2 + ', ' + fullAddress;
    }
    fullAddress = data.street + ', ' + fullAddress;
    onSubmit({ ...data, fullAddress });
  };

  const handleUpdateLocation = (location) => {
    if (!location) return;

    let street, city, state, zip_code, country;

    location.address
      ? (street = location.address.concat(' ', location.text))
      : (street = location.text);

    location.context.forEach((i) => {
      const [context] = i.id.split('.');
      if (context === 'place') {
        city = i.text;
      } else if (context === 'region') {
        const [, theState] = i.short_code.split('-');
        state = theState;
      } else if (context === 'postcode') {
        zip_code = i.text;
      } else if (context === 'country') {
        country = i.short_code.toUpperCase();
      }
    });

    if (submitOnSelect) {
      validateForm({
        street,
        city,
        state,
        zip_code,
        country,
      });
    } else {
      setAddress({
        street,
        city,
        state,
        zip_code,
        country,
      });
    }
  };

  const resetAddress = () => {
    // setManualEntry(false);
    setAddress();
  };

  return (
    <Box data-cy="schedule-screen-patient-address">
      <ActionDrawerWrapper title="Enter your address" buttonDisabled={!address} noButton>
        {!address ? (
          <>
            <Box mb={1}>
              <MapboxLocationAutocomplete
                geocodeConfig={{
                  types: ['address'],
                  countries: [COUNTRIES_ISO_CODES.US, COUNTRIES_ISO_CODES.MX],
                }}
                onUpdateLocation={handleUpdateLocation}
                label={placeholder}
              />
            </Box>
            {/* <Box textAlign="right">
            <Link
              data-cy="schedule-screen-patient-address-cant-find"
              component="button"
              variant="body2"
              onClick={() => setManualEntry(!manualEntry)}
              className={classes.link}
              underline="none"
            >
              Can't find your address?
            </Link>
          </Box> */}
          </>
        ) : (
          <Form
            onSubmit={validateForm}
            initialValues={{ state: '', ...(address || {}) }}
            decorators={[focusOnErrors]}
            render={({ handleSubmit, submitErrors: errors = {}, values }) => (
              <form onSubmit={handleSubmit}>
                {/* <Field
                name="country"
                render={({ input }) => (
                  <FormControl margin="normal" fullWidth variant="outlined">
                    <InputLabel id="application-address-country">Country</InputLabel>
                    <Select
                      {...input}
                      native
                      labelId="application-address-country"
                      id="application-address-country"
                      label="Country"
                      fullWidth
                      inputProps={{ 'data-cy': 'input-address-country' }}
                    >
                      {[{ value: '', label: '' }, ...COUNTRIES].map((country) => (
                        <option key={`country-${country.iso_alpha_2}`} value={country.iso_alpha_2}>
                          {country.label}
                        </option>
                      ))}
                    </Select>
                    {!!errors.country && <FormHelperText error>{errors.country}</FormHelperText>}
                  </FormControl>
                )}
              /> */}
                <Grid container spacing={1}>
                  <Grid item xs={12}>
                    <AddressCard address={{ address: address }} />
                  </Grid>
                  {/* <Grid item xs={6}>
                  <Field
                    name="street"
                    render={({ input }) => (
                      <TextField
                        {...input}
                        label="Street"
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        error={!!errors.street}
                        helperText={errors.street}
                        inputProps={{
                          'data-lpignore': 'true',
                          'data-cy': 'input-street',
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    name="city"
                    render={({ input }) => (
                      <TextField
                        {...input}
                        label="City"
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        error={!!errors.city}
                        helperText={errors.city}
                        inputProps={{
                          'data-lpignore': 'true',
                          'data-cy': 'input-city',
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    name="state"
                    render={({ input }) => (
                      <FormControl margin="normal" fullWidth variant="outlined">
                        <InputLabel id="application-address-state">State</InputLabel>
                        <Select
                          {...input}
                          native
                          labelId="application-address-state"
                          id="application-address-state"
                          label="State"
                          fullWidth
                          inputProps={{ 'data-cy': 'input-address-state' }}
                        >
                          {[
                            { value: '', label: '' },
                            ...(COUNTRY_STATES[values.country] || []),
                          ].map((state) => (
                            <option key={`state-${state.value}`} value={state.value}>
                              {state.label}
                            </option>
                          ))}
                        </Select>
                        {!!errors.state && <FormHelperText error>{errors.state}</FormHelperText>}
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    name="zip_code"
                    render={({ input }) => (
                      <TextField
                        {...input}
                        label="Zip/Postal Code"
                        fullWidth
                        margin="normal"
                        type="tel"
                        variant="outlined"
                        error={!!errors.zip_code}
                        helperText={errors.zip_code}
                        inputProps={{
                          'data-lpignore': 'true',
                          'data-cy': 'input-zip_code',
                        }}
                      />
                    )}
                  />
                </Grid> */}
                  {!disableAdditional && (
                    <>
                      <Grid item xs={12}>
                        <Typography>Additional Address details</Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Field
                          name="line2"
                          render={({ input }) => (
                            <TextField
                              {...input}
                              label="Address Line 2"
                              fullWidth
                              margin="normal"
                              variant="outlined"
                              style={{ marginRight: '1rem' }}
                              inputProps={{
                                'data-lpignore': 'true',
                                'data-cy': 'input-line2',
                              }}
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Field
                          name="line3"
                          render={({ input }) => (
                            <TextField
                              {...input}
                              label="Unit Number"
                              fullWidth
                              margin="normal"
                              variant="outlined"
                              inputProps={{
                                'data-lpignore': 'true',
                                'data-cy': 'input-line3',
                              }}
                            />
                          )}
                        />
                      </Grid>
                    </>
                  )}
                </Grid>

                <Box textAlign="right">
                  <Link
                    type="button"
                    component="button"
                    variant="body2"
                    onClick={() => resetAddress()}
                    className={classes.link}
                    underline="none"
                  >
                    Search for your address
                  </Link>
                </Box>

                <Box mt={3} className={classes.btnContainer}>
                  <SecureSubmitButton label={btnLabel} onClick={handleSubmit} disabled={!address} />
                </Box>
              </form>
            )}
          />
        )}
      </ActionDrawerWrapper>
    </Box>
  );
};

export { AddressSearch };
