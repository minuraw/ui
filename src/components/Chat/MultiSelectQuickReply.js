import React, { useState, useRef } from 'react';
import { Box, Link, Typography, makeStyles } from '@material-ui/core';
import { QuickReplyButtons, GGDLoader } from '../Common';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const useClasses = makeStyles((theme) => ({
  container: {
    minHeight: '7rem',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    '& .slick-slide div': {
      outline: 'none'
    }
  },
  keyboardArrowLeftIcon: {
    cursor: 'pointer',
    position: 'absolute',
    left: '0',
  },
  skipButton: {
    backgroundColor: '#171725',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#000',
    },
  },
}));

const settings = {
  dots: false,
  infinite: false,
  arrows: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  draggable: false,
  accessibility: false,
  swipe: false,
};

const MultiSelectQuickReply = ({
  items = [{ id: 1, label: 'Sample item for multi select' }],
  options = [
    { key: 'yes', value: 'Yes' },
    { key: 'no', value: 'No' },
  ], // { key: "yes", value: "Yes" }, { key: "no", value: "No" }, { key: "idk", value: "Don't know" }
  requiredOne = true,
  isOptional = false,
  onSubmit = () => {},
}) => {
  const classes = useClasses();
  const sliderRef = useRef();

  const [sending, setSending] = useState(false)

  const [inputData, setInputData] = useState(
    items
      .filter((item) => item.label !== 'No')
      .map((item) => {
        const newItem = { ...item };
        options.forEach((option) => {
          newItem[option.key] = false;
        });
        return newItem;
      }),
  );

  const noOption = items.find((item) => item.label === 'No');

  const changeHandler = (option, item) => {
    const index = inputData.findIndex((x) => x === item);
    options.forEach((option) => {
      inputData[index][option.key] = false;
    });
    inputData[index][option.key] = true;
    setInputData([...inputData]);
  };

  const handleResponse = (index, item, option) => {
    setSlider((slider) => slider + 1);
    changeHandler(option, item);
    if (inputData.length - 1 <= index) {
      setSending(true)
      const selected = inputData.find((item) => item.yes === true);
      if (!selected && noOption && requiredOne) {
        inputData.push({ ...noOption, yes: true });
      }
      onSubmit(inputData);
    } else {
      sliderRef.current.slickNext();
    }
  };

  const [slider, setSlider] = useState(0);

  const goToPrev = () => {
    setSlider((slider) => slider - 1);
    sliderRef.current.slickPrev();
  };

  if (sending) {
    return (
      <Box display="flex" justifyContent="center">
        <GGDLoader />
      </Box>
    )
  }

  return (
    <Box className={classes.container}>
      <Slider {...settings} ref={sliderRef}>
        {inputData.map((item, index) => (
          <div key={index}>
            <Box display="flex" justifyContent="center" mb={2}>
              <Typography variant="h6" style={{ textAlign: 'center' }}>
                {item.label}
              </Typography>
            </Box>
            <Box display="flex" alignItems="center" justifyContent="center">
              <QuickReplyButtons
                item={item}
                items={options.map((option) => ({ key: option.key, label: option.value }))}
                onClick={(option) => handleResponse(index, item, option)}
              />
            </Box>
          </div>
        ))}
      </Slider>
      {slider > 0 && (
        <Box m={1}>
          <Link
            component="button"
            underline="none"
            onClick={goToPrev}
            className={classes.keyboardArrowLeftIcon}
          >
            Back
          </Link>
        </Box>
      )}
      {/* {isOptional && (
          <Box display="flex" justifyContent="center" m={1}>
            <Button
              style={{ textTransform: 'none' }}
              onClick={() => onSubmit([])}
              className={classes.skipButton}
            >
              Skip - these don't apply to me
            </Button>
          </Box>
        )} */}
    </Box>
  );
};

export { MultiSelectQuickReply };
