import React, { useContext } from 'react';
import { Store as GlobalStore } from '../../context';
import { Box, Typography, Grid } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { green, grey, red } from '@material-ui/core/colors';

const CAPTIONS = {
  yes: {
    'en-us': 'Yes',
    'es-mx': 'Si',
  },
  no: {
    'en-us': 'No',
    'es-mx': 'No',
  },
  idk: {
    'en-us': `Don't know`,
    'es-mx': 'No lo sé',
  },
};

const YesNoDontKnow = ({ onSubmit = () => {}, id = null }) => {

  const { store } = useContext(GlobalStore);

  const options = [
    { key: 'yes', value: CAPTIONS.yes[store.locale] },
    { key: 'no', value: CAPTIONS.no[store.locale] },
    { key: 'idk', value: CAPTIONS.idk[store.locale] },
  ];

  const clickHandler = (option) => {
    onSubmit({ ...option, id });
  };

  return (
    <>
      <Box>
        <Grid container justifyContent="center" spacing={2}>
          {options.map((option, optionIndex) => (
            <Grid item key={option.key + optionIndex}>
              <Box
                data-cy="dynamic_input_button"
                style={{
                  borderStyle: 'solid',
                  borderWidth: 1,
                  borderColor: grey[400],
                  borderRadius: 6,
                  cursor: 'pointer'
                }}
                width={100}
                py={2}
                onClick={() => clickHandler(option)}
              >
                <Box justifyContent="center" textAlign="center">
                  {option.key === 'yes' && (
                    <DoneIcon style={{ color: green[500] }} fontSize="large" />
                  )}
                  {option.key === 'no' && (
                    <CloseIcon style={{ color: red[500] }} fontSize="large" />
                  )}
                  {option.key === 'idk' && (
                    <ArrowForwardIcon style={{ color: grey[400] }} fontSize="large" />
                  )}
                  <Typography variant="subtitle1" style={{ color: grey[600] }}>
                    {option.value}
                  </Typography>
                </Box>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
    </>
  );
};

export { YesNoDontKnow };
