import React from 'react';
import { Card, Box, Grid, Button, makeStyles, Typography } from '@material-ui/core';
import ScheduleIcon from '@material-ui/icons/Schedule';

const useClasses = makeStyles((theme) => ({
  doseContainer: {
    padding: '1rem',
    margin: '1rem 0',
    backgroundColor: theme.palette.ggd.light,
    boxShadow: 'none',
  },
  dateContainer: {
    width: 'fit-content',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0.2rem 0.5rem',
    borderRadius: '0.5rem',
    backgroundColor: '#F1F1F5 ',
  },
  icon: {
    fontSize: '1rem',
    color: '#92929D',
    marginRight: '0.5rem',
  },
  btnContainer: {
    display: 'flex',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
    },
  },
  firstBtn: {
    marginBottom: '0.3rem',
    [theme.breakpoints.up('sm')]: {
      marginBottom: '0',
      marginRight: '0.3rem',
    },
  },
}));

const Payor = (props) => {
  const { payor, name, plan, groupId, memberId } = props.payor;

  const classes = useClasses();

  return (
    <Card className={classes.doseContainer}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: '1rem',
        }}
      >
        <Typography variant="subtitle2">{name}</Typography>
          </Box>
      <Grid container>
        <Grid item xs={6} style={{ marginBottom: '1rem' }}>
          <Box>
            <Typography variant="subtitle2" style={{ color: '#696974' }}>
              PAYOR
            </Typography>
            <Typography variant="body1">{payor}</Typography>
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Box>
            <Typography variant="subtitle2" style={{ color: '#696974' }}>
              PLAN
            </Typography>
            <Typography variant="body1">{plan}</Typography>
          </Box>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={6}>
          <Box>
            <Typography variant="subtitle2" style={{ color: '#696974' }}>
              GROUP ID
            </Typography>
            <Typography variant="body1">{groupId}</Typography>
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Box>
            <Typography variant="subtitle2" style={{ color: '#696974' }}>
              MEMBER ID
            </Typography>
            <Typography variant="body1">{memberId}</Typography>
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
};

const MedicalInsuranceCard = (props) => {
  const { certificate } = props;
  const classes = useClasses();

  const updatePayor = () => {
    return null;
  };

  const seeReport = () => {
    return null;
  };

  return (
    <>
      {certificate.payors &&
        certificate.payors.map((payor) => <Payor key={payor.id} payor={payor} />)}
      <Box style={{ marginBottom: '1rem' }}>
        <Typography variant="p">
          For enrollment or billing questions, please email us at{' '}
          <a href='mailto:provider@gogetdoc.com' rel='noreferrer' target='_blank' style={{ color: '#1E3932' }}>
            provider@gogetdoc.com
          </a>
        </Typography>
      </Box>
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <Box>
          <Typography variant="p" style={{ color: '#B5B5BE' }}>
            DATE CREATED
          </Typography>
          <Box className={classes.dateContainer}>
            <ScheduleIcon className={classes.icon} />
            <Typography variant="p" style={{ color: '#696974' }}>
              {certificate.creationDate}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.btnContainer}>
          <Button
            variant="outlined"
            size="small"
            className={classes.firstBtn}
            onClick={updatePayor}
          >
            <Typography variant="body2" style={{ padding: '0.2rem', color: '#696974' }}>
              Update
            </Typography>
          </Button>
          <Button color="primary" size="small" variant="contained" onClick={seeReport}>
            <Typography variant="body2" style={{ padding: '0.2rem', fontWeight: '600' }}>
              See Report
            </Typography>
          </Button>
        </Box>
      </Box>
    </>
  );
};

export { MedicalInsuranceCard };
