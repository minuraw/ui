import { CONVERSATION_PROVIDER_ID } from '../../constants/providers';

import SymptomCheckerAgreement from './SymptomCheckerAgreement';
import VaxYesAgreement from './VaxYesAgreement';
import CoVaxYesAgreement from './CoVaxYesAgreement';
import { GeneralVisitIntroText } from '../IntroText';

const Agreement = ({ provider }) => {
  switch (provider) {
    case CONVERSATION_PROVIDER_ID.SYMPTOM_CHECKER:
      return <SymptomCheckerAgreement />;
    case CONVERSATION_PROVIDER_ID.VAX_YES:
      return <VaxYesAgreement />;
    case CONVERSATION_PROVIDER_ID.GGD_CO_VAX_YES:
      return <CoVaxYesAgreement />;
    case CONVERSATION_PROVIDER_ID.GGD_CONVERSATION:
      return <GeneralVisitIntroText />;
    default:
      return '';
  }
};

export default Agreement;
