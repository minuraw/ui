import React, { useContext } from 'react';
import { useAuth, Store as GlobalStore } from '../../context';
import { Box, Grid, Typography, makeStyles, Link } from '@material-ui/core';

const useClasses = makeStyles({
  legalText: {
    fontSize: '0.875rem',
  },
});

const CAPTIONS = {
  hi: {
    'en-us': 'Hi',
    'es-mx': 'Hola',
  },
  this: {
    'en-us': 'this is SymptomBot from GoGetDoc.',
    'es-mx': 'soy SymptomBot de GoGetDoc.',
  },
  thisCapitalize: {
    'en-us': 'This is SymptomBot from GoGetDoc.',
    'es-mx': 'Soy SymptomBot de GoGetDoc.',
  },
  iAm: {
    'en-us': 'I am a virtual assistant to help you evaluate your symptoms.',
    'es-mx': 'Soy su asistente virtual que le ayudará a evaluar sus sintomas.',
  },
  toGet: {
    'en-us': `To get started, I'm going to gather some information about why you're here. Although I'm not a healthcare provider and will not provide a medical diagnosis, I will be able to walk you through our deep medical knowledge base and help narrow down what may be causing your symptoms, and also determine if one of our healthcare providers can assist you.`,
    'es-mx':
      'Para empezar, voy a recopilar alguna información acerca de porqué está aquí. A pesar de no ser un proveedor de servicios médicos y no proporcionar un diagnostico médico, podré acompañarlo a recorrer nuestra extensa base de conocimiento médico y ayudarlo a acotar la posible causa de sus síntomas y además determinar si alguno de nuestros proveedores de salud puede ayudarlo.',
  },
  before: {
    'en-us': 'Before proceeding, if you are having a medical emergency -',
    'es-mx': 'Antes de iniciar, si usted está experimentando una emergencia médica - ',
  },
  please: {
    'en-us': 'please stop now and call 911',
    'es-mx': 'por favor deténgase y llame al 911',
  },
  notProceed: {
    'en-us': `or your local emergency number. You should also not proceed if you are pregnant or under the age of 18.`,
    'es-mx':
      'o a la linea de emergencia local. Adicionalmente no proceda si usted se encuentra en estado de embarazo o tiene menos de 18 años.',
  },
  byClicking: {
    'en-us': `By clicking 'I Agree' you agree to the`,
    'es-mx': `Al dar click en 'Estoy de acuerdo' usted expresa estar de acuerdo con los`,
  },
  terms: {
    'en-us': 'Terms of Use',
    'es-mx': 'Términos de Uso',
  },
  and: {
    'en-us': 'and',
    'es-mx': 'y las',
  },
  privacy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Políticas de Privacidad',
  },
};

const SymptomCheckerAgreement = () => {
  const classes = useClasses();
  const { getMemberProfile } = useAuth();
  const { store } = useContext(GlobalStore);

  const memberProfile = getMemberProfile();

  const initialGreeting = memberProfile?.firstName
    ? `${CAPTIONS.hi[store.locale]} ${memberProfile.firstName}, ${CAPTIONS.this[store.locale]}`
    : CAPTIONS.thisCapitalize[store.locale]

  return (
    <Box pt={1}>
      <Grid container spacing={3}>
        <Grid item>
          <Typography>
            {initialGreeting} {CAPTIONS.iAm[store.locale]}
          </Typography>
        </Grid>
        <Grid item>
          <Typography>{CAPTIONS.toGet[store.locale]}</Typography>
        </Grid>
        <Grid item>
          <Typography color="textSecondary" className={classes.legalText}>
            {CAPTIONS.before[store.locale]} <strong>{CAPTIONS.please[store.locale]} </strong>
            {CAPTIONS.notProceed[store.locale]} {CAPTIONS.byClicking[store.locale]}{' '}
            <Link href="https://gogetdoc.com/terms" target="_blank" rel="noopener noreferrer">
              {CAPTIONS.terms[store.locale]}{' '}
            </Link>
            {CAPTIONS.and[store.locale]}{' '}
            <Link href="https://gogetdoc.com/privacy" target="_blank" rel="noopener noreferrer">
              {CAPTIONS.privacy[store.locale]}
            </Link>
            .
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default SymptomCheckerAgreement;
