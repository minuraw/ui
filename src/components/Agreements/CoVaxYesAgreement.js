import { Box, Grid, Typography, Link, makeStyles } from '@material-ui/core';
import { getLocale } from '../../services';

const CAPTIONS = {
  intro: {
    'en-us':
      'Thanks for choosing VaxYes, the easiest way to save and use your vaccine record from your phone. Let’s get started.',
    'es-mx':
      'Gracias por elegir VaxYes, la forma más fácil de guardar y usar su registro de vacunas desde su teléfono. Empecemos.',
  },
  byClicking: {
    'en-us': 'By clicking “I Agree”, you agree to our ',
    'es-mx': 'Al hacer clic en "Acepto", acepta nuestros ',
  },
  terms: {
    'en-us': 'Terms',
    'es-mx': 'términos',
  },
  and: {
    'en-us': ' and ',
    'es-mx': ' y ',
  },
  privacyPolicy: {
    'en-us': 'Privacy Policy',
    'es-mx': 'Política de privacidad',
  },
  iHereby: {
    'en-us': `. I declare that this statement about my vaccination status is true and accurate. 
                I understand that knowingly providing false information regarding my vaccination 
                status on this form may subject me to criminal penalties.`,
    'es-mx': `. Declaro que esta declaración sobre mi estado de vacunación es verdadera y precisa.
                Entiendo que proporcionar a sabiendas información falsa sobre mi vacunación
                El estado en este formulario puede someterme a sanciones penales.`,
  },
  handlingFee: {
    'en-us': `A handling fee may apply for international users. You understand if you don’t want to
              receive these messages you can reply STOP to opt out of receiving texts.`,
    'es-mx': `Es posible que se aplique una tarifa de gestión para los usuarios.
              Si no desea recibir estos mensajes, puede responder PARE para optar por no recibir mensajes de texto.`,
  },
};

const useClasses = makeStyles((theme) => ({
  legalText: {
    fontSize: '0.875rem',
  },
}));

const CoVaxYesAgreement = () => {
  const locale = getLocale();
  const classes = useClasses();

  return (
    <Box pt={1}>
      <Grid container spacing={3}>
        <Grid item>
          <Typography>{CAPTIONS.intro[locale]}</Typography>
        </Grid>
        <Grid item>
          <Typography color="textSecondary" className={classes.legalText}>
            {CAPTIONS.byClicking[locale]}
            <Link href="https://gogetdoc.com/terms" target="_blank" rel="noopener noreferrer">
              {CAPTIONS.terms[locale]}
            </Link>
            {CAPTIONS.and[locale]}
            <Link href="https://gogetdoc.com/privacy" target="_blank" rel="noopener noreferrer">
              {CAPTIONS.privacyPolicy[locale]}
            </Link>
            {CAPTIONS.iHereby[locale]}
          </Typography>
        </Grid>
        <Grid item>
          <Typography color="textSecondary" className={classes.legalText}>
            {CAPTIONS.handlingFee[locale]}
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default CoVaxYesAgreement;
