import React from 'react';

import { Box } from '@material-ui/core';

import { LOGGER } from '../../utils';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // send the error information to the DataDig
    LOGGER.error(error.message, { error, errorInfo });
  }

  render() {
    if (this.state.hasError) {
      return (
        <Box
          display="flex"
          alignItems="center"
          justifyContent="center"
          style={{ height: '100vh', color: '#26442b' }}
        >
          <h2>An error occurred. Please try again. If error persists, visit the GoGetDoc help center.</h2>
        </Box>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
