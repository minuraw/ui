import { useState, useEffect } from 'react';
import { Button, Grid, Box, Typography, useMediaQuery, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

import { isDateSame, getDateInFormat, getDate } from '../../utils';

import { getConsultTimeslots } from '../../events';
import { GGDLoader } from '../Common/GGDLoader';

const MAX_RETRY_COUNT = 3;
const START_TIME_OFFSET = 45;
const ADDITIONAL_DAYS_AHEAD = 6;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      maxWidth: '36rem',
      margin: 'auto',
    },
  },
  link: {
    cursor: 'pointer',
    color: theme.palette.blue.main,
    marginRight: '0.3rem',
  },
  timesContainer: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'nowrap',
    overflow: 'scroll',
    [theme.breakpoints.up('sm')]: {
      maxWidth: '36rem',
      margin: 'auto',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
  },
  chevronIcon: {
    position: 'absolute',
    height: '100%',
    top: 0,
    bottom: 0,
  },
  chevronLeftIcon: {
    left: 0,
  },
  chevronRightIcon: {
    right: 0,
  },
  timeButton: {
    color: '#000',
    borderColor: '#000',
    backgroundColor: '#fff',
    minWidth: '7rem',
  },
}));

const renderAppointmentTimes = (
  appointmentTimes,
  onClick,
  classes,
  pageNumber,
  setPageNumber,
  smallScreen,
) => {
  const PAGE_SIZE = 8;
  const TOTAL_PAGES = Math.ceil(appointmentTimes && appointmentTimes.length / PAGE_SIZE);

  const paginate = (array, pageNumber) => {
    return smallScreen ? array : array.slice((pageNumber - 1) * PAGE_SIZE, pageNumber * PAGE_SIZE);
  };

  return appointmentTimes ? (
    <Box className={classes.timesContainer}>
      {!smallScreen && pageNumber > 1 && (
        <KeyboardArrowLeftIcon
          className={`${classes.chevronIcon} ${classes.chevronLeftIcon}`}
          color="primary"
          onClick={() => setPageNumber(pageNumber - 1)}
          style={{ cursor: 'pointer' }}
        />
      )}
      {Array.isArray(appointmentTimes) &&
        paginate(appointmentTimes, pageNumber).map((appointmentTime, idx) => {
          return (
            <Box key={`${appointmentTime.label}-${idx}`} m={1}>
              <Button
                variant="outlined"
                onClick={() => {
                  onClick({
                    value: appointmentTime.value,
                    label: appointmentTime.label,
                  });
                }}
                className={classes.timeButton}
              >
                <Typography variant="body2" style={{ padding: '0 0.5rem', whiteSpace: 'nowrap' }}>
                  {appointmentTime.label}
                </Typography>
              </Button>
            </Box>
          );
        })}
      {!smallScreen && pageNumber < TOTAL_PAGES && (
        <KeyboardArrowRightIcon
          className={`${classes.chevronIcon} ${classes.chevronRightIcon}`}
          color="primary"
          onClick={() => setPageNumber(pageNumber + 1)}
          style={{ cursor: 'pointer' }}
        />
      )}
    </Box>
  ) : (
    <Box display="flex" justifyContent="center" m={1}>
      <Typography variant="body2" style={{ color: '#92929D' }}>
        No appointments available
      </Typography>
    </Box>
  );
};

const Appointment = ({ consultId, onClick = () => {}, isReschedule }) => {
  const classes = useStyles();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [appointmentTimeslots, setAppointmentTimeslots] = useState({});
  const [appointmentsFullyLoaded, setAppointmentFullyLoaded] = useState([]);
  const [isInitialDataLoading, setIsInitialDataLoading] = useState(true);
  const [isTimeslotsPerDayLoading, setIsTimeslotsPerDayLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [retryCount, setRetryCount] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  // Handle the initial timeslot loading
  // This contains all the timeslot for current day and metadata for another 7 days
  const handleInitialConsultTimeslotsLoad = (error, data) => {
    setIsInitialDataLoading(false);
    if (error) {
      setHasError(true);
      return;
    }
    if (data) {
      setAppointmentTimeslots(data.timeslots);
      setAppointmentFullyLoaded([data.fullyLoadedDate]);
      return;
    }
    // Not having any data at this stage means an error
    setHasError(true);
  };

  // Load the data when a day is clicked other than today
  const handleTimeslotsPerDayLoad = (_, data) => {
    // TODO handle error condition.
    setIsTimeslotsPerDayLoading(false);
    if (data) {
      // Append the data to the existing items
      setAppointmentTimeslots((timeslots) => ({ ...timeslots, ...data.timeslots }));
      // Make sure this day is marked as fully loaded such that it is not loaded again
      setAppointmentFullyLoaded((fullyLoaded) => [...fullyLoaded, data.fullyLoadedDate]);
    }
  };

  const loadInitialAppointmentTimeslots = () => {
    // Begin from 45min from current time when loading the consult times
    const startTime = new Date();
    startTime.setMinutes(startTime.getMinutes() + START_TIME_OFFSET);

    const params = {
      consultId,
      start: startTime,
      additionalTimeslotPeriod: ADDITIONAL_DAYS_AHEAD,
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone, // set the browsers timezone here
    };
    getConsultTimeslots(handleInitialConsultTimeslotsLoad, params);
  };

  useEffect(() => {
    loadInitialAppointmentTimeslots();
  }, []);

  const shouldDateBeSelected = (day) => {
    const dateStrings = Object.keys(appointmentTimeslots);
    for (let i = 0; i < dateStrings.length; i += 1) {
      if (isDateSame(dateStrings[i], day)) {
        return true;
      }
    }
    return false;
  };

  const firstWeekCalled = (day) => {
    const today = new Date(new Date().toDateString());
    const oneWeek = new Date(new Date().toDateString());
    oneWeek.setDate(oneWeek.getDate() + 7);
    return day >= today && day < oneWeek;
  };

  // Add a retry logic so that timeouts are handled
  const retryDataLoading = () => {
    setIsInitialDataLoading(true);
    setHasError(false);
    setRetryCount((count) => count + 1);
    loadInitialAppointmentTimeslots();
  };

  const handleDoNotReschedule = () => {
    onClick({
      message: 'Do not reschedule',
      abortRescheduling: true,
    });
  };

  const handleTimeSlotSelect = (timeSlot) => {
    const message = `${getDate(timeSlot.value.start)} ${timeSlot.label}`;
    onClick({
      message,
      abortRescheduling: false,
      ...timeSlot,
    });
  };

  const handleDateClick = (day) => {
    setPageNumber(1);
    // Get the date string of the selected date
    const dateString = `${day.getFullYear()}-${day.getMonth()}-${day.getDate()}`;

    // Check if the selected has fully loaded data
    const fullyLoadedDate = appointmentsFullyLoaded.find(
      (fullyLoaded) => dateString === fullyLoaded,
    );

    // If fully loaded data available, we break
    if (fullyLoadedDate) {
      setSelectedDate(day);
      return;
    }

    setIsTimeslotsPerDayLoading(true);

    // set the end day
    const endDate = new Date(day);
    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);

    const params = {
      consultId,
      start: day,
      end: endDate,
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
    };

    getConsultTimeslots(handleTimeslotsPerDayLoad, params);
    setSelectedDate(day);
  };

  if (isInitialDataLoading) {
    return (
      <Box align="center">
        <GGDLoader />
      </Box>
    );
  }

  if (hasError && retryCount < MAX_RETRY_COUNT) {
    return (
      <>
        We were unable to schedule your visit. Please try again. If error persists, visit the GoGetDoc help center.
        <Button onClick={retryDataLoading}> Retry </Button>
      </>
    );
  }

  if (hasError && retryCount >= MAX_RETRY_COUNT) {
    return (
      <>Oops something went wrong. We are unable to continue. We apologize for the inconvenience.</>
    );
  }

  const dayStyle = `
    .DayPicker-Caption {
      padding: 0 1rem;
    }
    @media (min-width: 600px) {
      .DayPicker-Caption {
        padding: 0 1.8rem;
      }
    }
    .DayPicker-Day {
      padding: 0.5rem 1.9rem;
    }
    @media (max-width: 600px) {
      .DayPicker-Day {
        padding: 0.2rem 1rem;
      }
    }
    .DayPicker-Day--today {
      color: #000;
    }
    .DayPicker-Day, .DayPicker-NavButton, .DayPicker-wrapper {
      outline: none;
    }`;

  const modifiers = {
    appointmentNotAvailableDates: firstWeekCalled,
    appointmentAvailableDates: shouldDateBeSelected,
    selectedDate,
  };

  const modifiersStyles = {
    appointmentNotAvailableDates: {
      color: '#D0021B',
    },
    appointmentAvailableDates: {
      color: '#000',
    },
    selectedDate: {
      color: '#00754A',
      fontWeight: 700,
      fontSize: '1.2rem',
      backgroundColor: 'transparent',
    },
  };

  return (
    <Grid container className={classes.root} spacing={1} justifyContent="center">
      {isReschedule && (
        <Grid item xs={12}>
          <Grid container justifyContent="flex-end">
            <Link onClick={handleDoNotReschedule} className={classes.link}>
              Do not reschedule
            </Link>
          </Grid>
        </Grid>
      )}
      <Grid item xs={12}>
        <Grid container justifyContent="center">
          <style>{dayStyle}</style>
          <DayPicker
            month={new Date()}
            modifiers={modifiers}
            modifiersStyles={modifiersStyles}
            onDayClick={handleDateClick}
            disabledDays={{ before: new Date() }}
          />
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Grid container style={{ margin: 'auto', maxWidth: smallScreen ? '20rem' : '31rem' }}>
          <Typography variant="h5" style={{ color: '#92929D', fontWeight: '400' }}>
            {getDate(selectedDate)}
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        {isTimeslotsPerDayLoading ? (
          <Box display="flex" justifyContent="center">
            <GGDLoader />
          </Box>
        ) : (
          renderAppointmentTimes(
            appointmentTimeslots[getDateInFormat(selectedDate)],
            handleTimeSlotSelect,
            classes,
            pageNumber,
            setPageNumber,
            smallScreen,
          )
        )}
      </Grid>
    </Grid>
  );
};

export default Appointment;
