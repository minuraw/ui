import EventEmitter from 'eventemitter2';

import handlers from './handler';

// Get the emitter instance
const emitter = new EventEmitter();

// Wrap the event emit by dispatch
const dispatch = (eventName, handlerFnc, payload) => {
  emitter.emit(eventName, handlerFnc, payload);
};

// Allow easy creation of events
const eventCreator = (eventName) => {
  return (handlerFnc, payload) => {
    dispatch(eventName, handlerFnc, payload);
  };
};

// Register the handler for the events
Object.keys(handlers).forEach((eventName) => {
  const handler = handlers[eventName];
  emitter.on(eventName, (handlerFnc, payload) => {
    // call the handler function
    handler(handlerFnc, payload);
  });
});

// This is used by components to listen to events such as notification from web sockets
const eventListener = (eventName, handlerFnc) => {
  // To adhere to current event format, we ignore the handler function
  const listenerFnc = (_, payload) => {
    handlerFnc(payload);
  };
  emitter.on(eventName, listenerFnc);
  return listenerFnc;
};

const removeEventListener = (eventName, listenerFnc) => {
  emitter.removeListener(eventName, listenerFnc);
};

export { eventCreator, eventListener, removeEventListener };
