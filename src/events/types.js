import EVENTS from './constant';
import { eventCreator, eventListener, removeEventListener } from './bootstrap';

const getProductCategories = eventCreator(EVENTS.GET_PRODUCT_CATEGORIES);
const getProductByCategory = eventCreator(EVENTS.GET_PRODUCTS_BY_CATEGORY);
const detectIntent = eventCreator(EVENTS.DETECT_INTENT);
const getMemberChatNextQuestion = eventCreator(EVENTS.GET_MEMBER_CHAT_NEXT_QUESTION);
const loadChatHistory = eventCreator(EVENTS.LOAD_CHAT_HISTORY);
const saveChatHistory = eventCreator(EVENTS.SAVE_CHAT_HISTORY);
const amendIdImages = eventCreator(EVENTS.AMEND_ID_IMAGES);
const getMember = eventCreator(EVENTS.GET_MEMBER);
const getProductPaymentInfo = eventCreator(EVENTS.GET_PRODUCT_PAYMENT_INFO);
const getMemberConversations = eventCreator(EVENTS.GET_MEMBER_CONVERSATIONS);
const notifyChatReceived = eventCreator(EVENTS.CHAT_NOTIFICATION_RECEIVED);
const saveConversationMetadata = eventCreator(EVENTS.SAVE_CONVERSATION_METADATA);
const updateConversationMetadata = eventCreator(EVENTS.UPDATE_CONVERSATION_METADATA);
const getConversationMetadata = eventCreator(EVENTS.GET_CONVERSATION_METADATA);
const getConsultationPrescriptionInformation = eventCreator(EVENTS.GET_CONSULT_PRESCRIPTION);
const getConsultationDiagnosisInformation = eventCreator(EVENTS.GET_CONSULT_DIAGNOSIS);
const getConsultTimeslots = eventCreator(EVENTS.GET_CONSULT_TIMESLOTS);
const getSymptomSuggestions = eventCreator(EVENTS.GET_SYMPTOM_SUGGESTIONS);
const saveSymptomBotPayload = eventCreator(EVENTS.SAVE_SYMPTOM_BOT_PAYLOAD);
const getSymptomBotPayload = eventCreator(EVENTS.GET_SYMPTOM_BOT_PAYLOAD);
const getUserImage = eventCreator(EVENTS.GET_USER_IMAGE);
const getVaxYesPayload = eventCreator(EVENTS.GET_VAX_YES_PAYLOAD);
const saveVaxYesPayload = eventCreator(EVENTS.SAVE_VAX_YES_PAYLOAD);
const deleteChatMessage = eventCreator(EVENTS.DELETE_CHAT_MESSAGE);
const getVaxYesConversationId = eventCreator(EVENTS.GET_VAX_YES_CONVERSATION_ID);
const getVaxYesBoosterCertificateInfo = eventCreator(EVENTS.GET_VAX_YES_BOOSTER_CERT_INFORMATION);
const deleteVaxYesConversation = eventCreator(EVENTS.DELETE_VAX_YES_CONVERSATION);
const getVaxYesBoosterConversationInfo = eventCreator(EVENTS.GET_VAX_YES_BOOSTER_CONVERSATION_INFO);
const getVaxYesCertificates = eventCreator(EVENTS.GET_VAX_YES_CERTIFICATES);
const updateVaxYesGroupCode = eventCreator(EVENTS.UPDATE_VAX_YES_GROUP_CODE);
const verifyVaxYesVenue = eventCreator(EVENTS.VERIFY_VAX_YES_VENUE);
const getWalletPass = eventCreator(EVENTS.GET_WALLET_PASS);
const getMemberAppointments = eventCreator(EVENTS.GET_MEMBER_APPOINTMENTS);
const getPublicVideoVisitLink = eventCreator(EVENTS.GET_PUBLIC_VIDEO_VISIT_LINK);
const trackEventGA = eventCreator(EVENTS.TRACK_EVENT_GA);
const notifyAppNotification = eventCreator(EVENTS.APP_NOTIFICATION_RECEIVED);
const getFeatureToggles = eventCreator(EVENTS.GET_FEATURE_TOGGLES);
const verifyMemberPayment = eventCreator(EVENTS.VERIFY_MEMBER_PAYMENT);
const getConversationMetadataByFilters = eventCreator(EVENTS.GET_CONVERSATION_METADATA_BY_FILTER);
const getFileUploadUrl = eventCreator(EVENTS.GET_FILE_UPLOAD_URL);
const logEvent = eventCreator(EVENTS.LOG_EVENT);
const initiateAuth = eventCreator(EVENTS.INITIATE_AUTH);
const reactivateGGDConversation = eventCreator(EVENTS.REACTIVATE_GGD_CONVERSATION);
const getSavedCards = eventCreator(EVENTS.GET_SAVED_CARDS);
const setupNewCard = eventCreator(EVENTS.SETUP_NEW_CARD);
const updateCardDetails = eventCreator(EVENTS.UPDATE_CARD_DETAILS);
const getSurveyStatus = eventCreator(EVENTS.GET_SURVEY_STATUS);
const postSurvey = eventCreator(EVENTS.POST_SURVEY);
const getCalenderLink = eventCreator(EVENTS.GET_CALENDAR_LINKS);

export {
  getProductCategories,
  getProductByCategory,
  detectIntent,
  getMemberChatNextQuestion,
  loadChatHistory,
  saveChatHistory,
  getMember,
  getProductPaymentInfo,
  getMemberConversations,
  notifyChatReceived,
  saveConversationMetadata,
  updateConversationMetadata,
  getConversationMetadata,
  EVENTS, // export this to be used with eventListener
  eventListener,
  removeEventListener,
  amendIdImages,
  getConsultationPrescriptionInformation,
  getConsultationDiagnosisInformation,
  getConsultTimeslots,
  getSymptomSuggestions,
  saveSymptomBotPayload,
  getSymptomBotPayload,
  getUserImage,
  saveVaxYesPayload,
  getVaxYesPayload,
  deleteChatMessage,
  getVaxYesConversationId,
  getVaxYesBoosterCertificateInfo,
  deleteVaxYesConversation,
  getVaxYesBoosterConversationInfo,
  getVaxYesCertificates,
  updateVaxYesGroupCode,
  verifyVaxYesVenue,
  getWalletPass,
  getMemberAppointments,
  getPublicVideoVisitLink,
  trackEventGA,
  notifyAppNotification,
  getFeatureToggles,
  verifyMemberPayment,
  getConversationMetadataByFilters,
  getFileUploadUrl,
  logEvent,
  initiateAuth,
  reactivateGGDConversation,
  getSavedCards,
  setupNewCard,
  updateCardDetails,
  getSurveyStatus,
  postSurvey,
  getCalenderLink,
};
