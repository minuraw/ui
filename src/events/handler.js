import EVENTS from './constant';
import handlers from './handlers';
import {
  getProductByCategory,
  detectIntent,
  getNextQuestion,
  getMember,
  getNextGGDQuestion,
  getNextGGDConsultQuestionAfterEndOfConversation,
  getNextCoVaxYesQuestion,
} from '../services';
import { getNextAuthQuestion } from '../services/auth';
import { SOURCES } from '../components/Chat/constants';

import { loadChatHistory, saveChat } from '../services/history';
import { getNextQuestionSymptomChecker } from '../services/symptom-checker';
import {
  getNextVaxYesQuestion,
  getNextVaxYesQuestionAfterEndOfConversation,
  getNextVaxYesBoosterQuestion,
} from '../services/vax-yes';

import { getNexTestQuestion } from '../services/test';

// Define the event handler functionality here
const handler = {
  [EVENTS.GET_PRODUCT_CATEGORIES]: async (handlerFnc, payload) => {},
  [EVENTS.GET_PRODUCTS_BY_CATEGORY]: async (handlerFnc, payload) => {
    const { categoryId } = payload;
    // When the categoryId = 0, we should load all data, hence making the categoryId undefined
    const data = await getProductByCategory(categoryId || undefined);
    handlerFnc(data);
  },
  [EVENTS.DETECT_INTENT]: async (handlerFnc, payload) => {
    const { searchText } = payload;
    try {
      const data = await detectIntent(searchText);
      handlerFnc(null, data);
    } catch (e) {
      handlerFnc(e);
    }
  },
  [EVENTS.LOAD_CHAT_HISTORY]: async (handlerFnc, payload) => {
    try {
      const response = await loadChatHistory(payload);
      handlerFnc(null, response, payload);
    } catch (err) {
      console.error('CHAT HISTORY RESPONSE ERROR ', err.message);
      handlerFnc(new Error(err), null);
    }
  },
  [EVENTS.SAVE_CHAT_HISTORY]: async (handlerFnc, payload) => {
    try {
      const response = await saveChat(payload);
      handlerFnc(null, response);
    } catch (err) {
      console.error('CHAT HISTORY RESPONSE ERROR ', err.message);
      handlerFnc(new Error(err), null);
    }
  },
  [EVENTS.GET_MEMBER_CHAT_NEXT_QUESTION]: async (handlerFnc, payload) => {
    // use the payload to determine which source should be used to load the chat.
    switch (payload.source) {
      case SOURCES.ROZIE: {
        try {
          const nextQuestion = await getNextQuestion(payload);
          // Set the question created time as now
          nextQuestion.createdOn = Date.now();
          handlerFnc(null, { nextQuestion, source: SOURCES.ROZIE });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.GGD_CONVERSATION: {
        try {
          const nextQuestion = await getNextGGDQuestion(payload);
          handlerFnc(null, { ...nextQuestion, source: SOURCES.GGD_CONVERSATION });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.GGD_AUTH: {
        try {
          const nextQuestion = await getNextAuthQuestion(payload);
          handlerFnc(null, { ...nextQuestion, source: SOURCES.GGD_AUTH });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }

      // TODO We can remove the segment between this and following commit once SymptomBot fully migrated.
      // case SOURCES.SYMPTOM_CHECKER_INIT: {
      //   const nextQuestion = await getNextQuestionSymptomChecker(payload);
      //   handlerFnc(null, { ...nextQuestion });
      //   return;
      // }
      // case SOURCES.SYMPTOM_CHECKER: {
      //   const nextQuestion = await getNextQuestionSymptomChecker(payload, true);
      //   handlerFnc(null, { ...nextQuestion });
      //   return;
      // }
      // TODO We can remove the segment between this and above commit once SymptomBot fully migrated.

      case SOURCES.SYMPTOM_CHECKER_GGD: {
        try {
          const nextQuestion = await getNextQuestionSymptomChecker(payload);
          handlerFnc(null, { ...nextQuestion, source: SOURCES.SYMPTOM_CHECKER_GGD });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.VAX_YES: {
        try {
          const nextQuestion = await getNextVaxYesQuestion(payload);
          if (!nextQuestion) return;
          handlerFnc(null, { ...nextQuestion, source: SOURCES.VAX_YES });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.END_OF_VAX_YES: {
        try {
          const nextQuestion = await getNextVaxYesQuestionAfterEndOfConversation();
          handlerFnc(null, { ...nextQuestion, source: SOURCES.END_OF_VAX_YES });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.END_OF_GGD_CONSULT: {
        try {
          const nextQuestion = await getNextGGDConsultQuestionAfterEndOfConversation(payload);
          if (nextQuestion) {
            handlerFnc(null, { ...nextQuestion, source: SOURCES.END_OF_GGD_CONSULT });
          }
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.VAX_YES_BOOSTER: {
        try {
          const nextQuestion = await getNextVaxYesBoosterQuestion(payload);
          handlerFnc(null, { ...nextQuestion, source: SOURCES.VAX_YES_BOOSTER });
          return;
        } catch (err) {
          handlerFnc(err, null);
          return;
        }
      }
      case SOURCES.GGD_TEST_CERTIFICATES: {
        try {
          const nextQuestion = await getNexTestQuestion(payload);
          handlerFnc(null, { ...nextQuestion, source: SOURCES.GGD_TEST_CERTIFICATES });
        } catch (err) {
          handlerFnc(err, null);
        }
        return;
      }
      case SOURCES.GGD_CO_VAX_YES: {
        try {
          const nextQuestion = await getNextCoVaxYesQuestion(payload);
          if (!nextQuestion) return;
          handlerFnc(null, { ...nextQuestion, source: SOURCES.VAX_YES });
          return;
        } catch (err) {
          handlerFnc(err, null);
        }
      }
    }
  },
  [EVENTS.GET_MEMBER]: async (handlerFnc, payload) => {
    try {
      const data = await getMember();
      handlerFnc(null, data);
    } catch (e) {
      handlerFnc(e);
    }
  },
  ...handlers, // Spread the handlers defined in the handlers folder. This makes it modular
};

export default handler;
