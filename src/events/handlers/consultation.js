import EVENTS from '../constant';

import {
  getConsultationDiagnosisInfo,
  getConsultationPrescriptionInfo,
  getTimeslots,
  getMemberAppointments,
  getPublicVideoVisitLink,
} from '../../services';
import { getTimeslotInfo } from '../../utils';
import { getCalenderLink } from '../../connectors/bff-connector';

const handler = {
  [EVENTS.GET_CONSULT_PRESCRIPTION]: async (handlerFnc, payload) => {
    try {
      const data = await getConsultationPrescriptionInfo(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_CONSULT_DIAGNOSIS]: async (handlerFnc, payload) => {
    try {
      const data = await getConsultationDiagnosisInfo(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_CONSULT_TIMESLOTS]: async (handlerFnc, payload) => {
    try {
      const data = await getTimeslots(payload);
      if (Array.isArray(data) && data.length > 0) {
        const { start, consultId } = payload;
        const date = new Date(start);
        const fullyLoadedDate = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
        return handlerFnc(null, { timeslots: getTimeslotInfo(data, consultId), fullyLoadedDate });
      }
      handlerFnc(null, null);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_MEMBER_APPOINTMENTS]: async (handlerFnc, payload) => {
    try {
      const data = await getMemberAppointments(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_PUBLIC_VIDEO_VISIT_LINK]: async (handlerFnc, payload) => {
    try {
      const data = await getPublicVideoVisitLink(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_CALENDAR_LINKS]: async (handlerFnc, payload) => {
    try {
      const data = await getCalenderLink(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
};

export default handler;
