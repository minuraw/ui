import EVENTS from '../constant';
import { getSurveyStatus, postSurvey } from '../../services';

const handler = {
  [EVENTS.GET_SURVEY_STATUS]: async (handlerFnc, payload) => {
    try {
      const result = await getSurveyStatus(payload);
      handlerFnc(null, result);
    } catch (err) {
      console.error('Error getting survey status');
      handlerFnc(err, null);
    }
  },
  [EVENTS.POST_SURVEY]: async (handlerFnc, payload) => {
    try {
      const result = await postSurvey(payload);
      handlerFnc(null, result);
    } catch (err) {
      console.error('Error posting survey data');
      handlerFnc(err, null);
    }
  },
};

export default handler;
