import productHandler from './products';
import conversationHandler from './conversation';
import consultationHandler from './consultation';
import symptomCheckHandler from './symptom-checker';
import googleAnalyticsHandler from './google-analytics';
import fileHandler from './file';
import walletHandler from './wallet';
import featureTogglesHandler from './feature-toggles';
import member from './member';
import common from './common';
import auth from './auth';
import survey from './survey';

const handlers = {
  ...productHandler,
  ...conversationHandler,
  ...consultationHandler,
  ...symptomCheckHandler,
  ...googleAnalyticsHandler,
  ...fileHandler,
  ...walletHandler,
  ...featureTogglesHandler,
  ...member,
  ...common,
  ...auth,
  ...survey,
};

export default handlers;
