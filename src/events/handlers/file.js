import EVENTS from '../constant';
import { getUserImage, getFileUploadUrl } from '../../services';

const handler = {
  [EVENTS.GET_USER_IMAGE]: async (handlerFnc, payload) => {
    try {
      const { imagePath } = payload;
      if (!imagePath) {
        handlerFnc(null, {});
      }
      const data = await getUserImage(payload);
      handlerFnc(null, data);
    } catch (err) {
      console.error('User image loading failed.');
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_FILE_UPLOAD_URL]: async (handlerFnc, payload) => {
    try {
      const result = await getFileUploadUrl(payload);
      handlerFnc(null, result);
    } catch (err) {
      console.error('Error getting image upload url');
      handlerFnc(err, null);
    }
  },
};

export default handler;
