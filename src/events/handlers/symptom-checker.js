import EVENTS from '../constant';
import { getSuggestedSymptoms } from '../../services';

const handler = {
  [EVENTS.GET_SYMPTOM_SUGGESTIONS]: async (handlerFnc, payload) => {
    try {
      const { searchText } = payload;
      if (!searchText) {
        handlerFnc(null, { filledWord: '', suggestedWords: [] });
      }
      const suggestedWords = await getSuggestedSymptoms(payload);
      handlerFnc(null, { filledWord: searchText, suggestedWords: suggestedWords });
    } catch (err) {
      handlerFnc(err, null);
    }
  },
};

export default handler;
