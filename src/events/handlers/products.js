import EVENTS from '../constant';

import {
  getProductPaymentInfo,
  getSavedCards,
  setupNewCard,
  updateCardDetails,
} from '../../services';

const handler = {
  [EVENTS.GET_PRODUCT_PAYMENT_INFO]: async (handlerFnc, payload) => {
    const { productId, sync, protocol, isOffSession = false, price = 0 } = payload;
    // Current use case is to show the pricing for a product
    const productsPayment = {
      isOffSession,
      items: [
        {
          productId: Number(productId),
          metadata: {
            sync,
            protocol,
          },
          price,
        },
      ],
    };
    const pricingInfo = await getProductPaymentInfo(productsPayment);
    handlerFnc(null, pricingInfo);
  },
  [EVENTS.GET_SAVED_CARDS]: async (handlerFnc, payload) => {
    try {
      const data = await getSavedCards(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.SETUP_NEW_CARD]: async (handlerFnc, payload) => {
    try {
      const data = await setupNewCard(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.UPDATE_CARD_DETAILS]: async (handlerFnc, payload) => {
    try {
      const data = await updateCardDetails(payload);
      handlerFnc(null, data);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
};

export default handler;
