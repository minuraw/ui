import { getWalletPass } from '../../connectors/bff-connector';
import EVENTS from '../constant';

const handler = {
  [EVENTS.GET_WALLET_PASS]: async (handlerFnc, payload) => {
    try {
      const walletPass = await getWalletPass(payload);
      handlerFnc(null, walletPass);
    } catch (error) {
      console.error('Could not get wallet Pass', error);
      handlerFnc(error, {});
    }
  },
};

export default handler;
