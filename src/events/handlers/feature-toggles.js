import { getFeatureToggles } from '../../connectors/bff-connector';
import EVENTS from '../constant';

const handler = {
  [EVENTS.GET_FEATURE_TOGGLES]: async (handlerFnc, payload) => {
    try {
      const { isAuthenticated, ...rest } = payload;
      const toggles = await getFeatureToggles(rest, isAuthenticated);
      handlerFnc(null, toggles);
    } catch (error) {
      console.error('Could not get feature toggles', error);
      handlerFnc(error, {});
    }
  },
};

export default handler;
