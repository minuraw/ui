import EVENTS from '../constant';

import { initiateSSO } from '../../connectors/bff-connector';

const handler = {
  [EVENTS.INITIATE_AUTH]: async (handlerFnc, payload) => {
    try {
      if (!payload.token) {
        handlerFnc(new Error('Token not found'), null);
        return;
      }
      const result = await initiateSSO(payload);
      handlerFnc(null, result);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
};

export default handler;
