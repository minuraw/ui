import EVENTS from '../constant';

import { LOGGER } from '../../utils';

const handler = {
  [EVENTS.LOG_EVENT]: async (_, payload) => {
    const { level = 'info', message = '', context = {} } = payload;

    LOGGER[level](message, context);
  },
};

export default handler;
