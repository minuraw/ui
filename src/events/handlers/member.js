import EVENTS from '../constant';
import { verifyMemberPayment } from '../../services';

const handler = {
  [EVENTS.VERIFY_MEMBER_PAYMENT]: async (_, payload) => {
    try {
      // the response is not required
      await verifyMemberPayment(payload);
    } catch (err) {
      console.error('Member payment verification failed');
    }
  },
};

export default handler;
