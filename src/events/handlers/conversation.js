import EVENTS from '../constant';

import {
  amendGGDIdImages,
  getConversations,
  saveConversationMetadata,
  updateConversationMetadata,
  getConversationMetadata,
  getVaxYesBoosterCertificateInfo,
  deleteVaxYesConversation,
  getVaxYesBoosterConversationInfo,
  getVaccineCertificates,
  reactivateGGDConversation,
} from '../../services';
import {
  getSymptomBotPayload,
  saveSymptomBotPayload,
  getVaxYesPayload,
  saveVaxYesPayload,
  deleteMessageById,
  getVYConversationId,
  getVaxYesCertificate,
} from '../../services/history';
import {
  updateVaccineCertificateGroupCode,
  verifyVenue,
  getConversationMetadataByFilters,
} from '../../connectors/bff-connector';

const handler = {
  [EVENTS.AMEND_ID_IMAGES]: async (handlerFnc, payload) => {
    const conversations = await amendGGDIdImages(payload);
    handlerFnc(null, conversations);
  },
  [EVENTS.GET_MEMBER_CONVERSATIONS]: async (handlerFnc, payload) => {
    const conversations = await getConversations();
    handlerFnc(null, conversations);
  },
  [EVENTS.SAVE_CONVERSATION_METADATA]: async (handlerFnc, payload) => {
    try {
      await saveConversationMetadata(payload);
      handlerFnc(null, payload);
    } catch (err) {
      console.error('Error in saving conversation metadata', err);
      handlerFnc(err, null);
    }
  },
  [EVENTS.UPDATE_CONVERSATION_METADATA]: async (_, payload) => {
    try {
      await updateConversationMetadata(payload);
    } catch (err) {
      console.error('Error in updating the conversation metadata', err);
    }
  },
  [EVENTS.GET_CONVERSATION_METADATA]: async (handlerFnc, payload) => {
    try {
      const conversations = await getConversationMetadata(payload);
      handlerFnc(null, conversations, payload.provider);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.SAVE_SYMPTOM_BOT_PAYLOAD]: async (handlerFnc, payload) => {
    try {
      const res = await saveSymptomBotPayload(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_SYMPTOM_BOT_PAYLOAD]: async (handlerFnc, payload) => {
    try {
      const res = await getSymptomBotPayload(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_VAX_YES_PAYLOAD]: async (handlerFnc, payload) => {
    try {
      const res = await getVaxYesPayload(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.SAVE_VAX_YES_PAYLOAD]: async (handlerFnc, payload) => {
    try {
      const res = await saveVaxYesPayload(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.DELETE_CHAT_MESSAGE]: async (handlerFnc, payload) => {
    try {
      const res = await deleteMessageById(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_VAX_YES_CONVERSATION_ID]: async (handlerFnc, payload) => {
    try {
      const res = await getVYConversationId();
      const certificate = await getVaxYesCertificate();
      handlerFnc(null, payload.formatResponse(res, certificate));
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_VAX_YES_BOOSTER_CERT_INFORMATION]: async (handlerFnc) => {
    try {
      const res = await getVaxYesBoosterCertificateInfo();
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.DELETE_VAX_YES_CONVERSATION]: async (handlerFnc, payload) => {
    try {
      const res = await deleteVaxYesConversation(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_VAX_YES_BOOSTER_CONVERSATION_INFO]: async (handlerFnc, payload) => {
    try {
      const res = await getVaxYesBoosterConversationInfo(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_VAX_YES_CERTIFICATES]: async (handlerFnc, payload) => {
    try {
      const res = await getVaccineCertificates(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.UPDATE_VAX_YES_GROUP_CODE]: async (handlerFnc, payload) => {
    try {
      const res = await updateVaccineCertificateGroupCode(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.VERIFY_VAX_YES_VENUE]: async (handlerFnc, payload) => {
    try {
      const res = await verifyVenue(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.GET_CONVERSATION_METADATA_BY_FILTER]: async (handlerFnc, payload) => {
    try {
      const res = await getConversationMetadataByFilters(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
  [EVENTS.REACTIVATE_GGD_CONVERSATION]: async (handlerFnc, payload) => {
    try {
      const res = await reactivateGGDConversation(payload);
      handlerFnc(null, res);
    } catch (err) {
      handlerFnc(err, null);
    }
  },
};

export default handler;
