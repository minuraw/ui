import EVENTS from '../constant';
import TagManager from 'react-gtm-module';

const handler = {
  [EVENTS.TRACK_EVENT_GA]: async (_, payload) => {
    const { event, memberSignedIn, product, messageNumber, messageOption  } = payload;
    TagManager.dataLayer({
      dataLayer: {
        event: event,
        memberSignedIn: memberSignedIn,
        product: product,
        messageNumber: messageNumber,
        messageOption: messageOption,
      },
    });
  },
};

export default handler;
