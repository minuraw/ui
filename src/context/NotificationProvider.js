import React, { useEffect, useCallback } from 'react';

import { useAuth } from '../context';

import { subscribeToEvent } from '../adaptors/amplify-adaptor';
import { onMessageReceived } from '../graphql/subscriptions';

import { notifyChatReceived } from '../events';

// This component does not use React context but still sits on the context folder
// This is because this component is a higher order component that handles the subscriptions in the application
const NotificationProvider = ({ children }) => {
  const { getMemberId } = useAuth();
  const memberId = getMemberId();
  const onProviderMessageReceived = (data) => {
    const providerMessage = data?.value?.data?.onMessageReceived;
    if (providerMessage) {
      // handler function is not required, passing an empty function
      // At this moment only chat related information are received therefore, notifyChatReceived event is used for everything
      notifyChatReceived(() => {}, providerMessage);
    }
  };

  const subscribeToProviderMessages = useCallback(() => {
    return subscribeToEvent(onMessageReceived, { memberId }, onProviderMessageReceived);
  }, [memberId]);

  useEffect(() => {
    let providerMessageSubscription = null;
    if (memberId) {
      providerMessageSubscription = subscribeToProviderMessages();
    }
    return function cleanup() {
      if (providerMessageSubscription) {
        providerMessageSubscription.unsubscribe();
      }
    };
  }, [memberId, subscribeToProviderMessages]);

  return <>{children}</>;
};

export { NotificationProvider };
