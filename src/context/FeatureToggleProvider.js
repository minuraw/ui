import { useRouter } from 'next/router';
import React, { useEffect, useState, useCallback } from 'react';
import { getFeatureToggles } from '../events';
import { useAuth } from './AuthProvider';

const SESSION_STORAGE_KEY_TESTER_TOKEN = 'testerToken';

const ToggleContext = React.createContext();

const FeatureToggleProvider = ({ children }) => {
  const { isAuthenticated } = useAuth();
  const router = useRouter();

  const [toggles, setToggles] = useState(null);
  const [isToggleReady, setIsToggleReady] = useState(false);
  const [toggleState, setToggleState] = useState({ level: 0, token: null });

  const initialize = useCallback(() => {
    let testerToken = router?.query?.testerToken || null;

    if (testerToken) {
      if (testerToken.toLowerCase() === 'none') {
        // remove existing token
        window.localStorage.removeItem(SESSION_STORAGE_KEY_TESTER_TOKEN);
        testerToken = null;
      } else {
        // save token
        window.localStorage.setItem(SESSION_STORAGE_KEY_TESTER_TOKEN, testerToken);
      }
    }

    if (!testerToken) {
      testerToken = window.localStorage.getItem(SESSION_STORAGE_KEY_TESTER_TOKEN) || null;
    }

    if (toggleState.level === 1 && isAuthenticated && toggleState.token === testerToken) {
      return; // no need to load again. same toggles
    }

    setIsToggleReady(false);
    let stopped = false;
    getFeatureToggles(
      (_, data) => {
        if (stopped) {
          return;
        }
        setToggles(data);
        setToggleState({
          level: isAuthenticated ? 2 : 1,
          token: testerToken,
        });
        setIsToggleReady(true);
      },
      {
        isAuthenticated,
        testerToken,
      },
    );
    return () => {
      stopped = true;
    };
  }, [isAuthenticated, router?.query?.testerToken, toggleState.level, toggleState.token]);

  useEffect(() => {
    if (!router.isReady) {
      return;
    }

    return initialize();
  }, [isAuthenticated, router]);

  const isFeatureEnabled = useCallback(
    (toggleKey) => {
      return (toggles && toggles[toggleKey]) || false;
    },
    [toggles],
  );

  return (
    <ToggleContext.Provider
      value={{
        isFeatureEnabled,
        isToggleReady,
      }}
    >
      {children}
    </ToggleContext.Provider>
  );
};

const useToggles = () => React.useContext(ToggleContext);

export { FeatureToggleProvider, useToggles };
