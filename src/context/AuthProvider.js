/* eslint-disable no-useless-escape */
import React, { useEffect, useState, useCallback, useRef } from 'react';

import { useRouter } from 'next/router';

import { getMemberProfile, clearAuthData, validateSession } from '../services/auth';
import { CircularLoader } from '../components/Loader';
import { isServerSide, getFromStorage } from '../utils';

const AuthContext = React.createContext();
const MEMBER_SESSION_INACTIVE_MAX_ALLOWED_TIME = Number(process.env.NEXT_PUBLIC_MEMBER_SESSION_INACTIVE_MAX_ALLOWED_TIME);

const AuthProvider = ({ children }) => {
  const [isAuthReady, setIsAuthReady] = useState(true);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const router = useRouter();
  // capture the last mouse down time///
  const lastMouseDownTime = useRef(Date.now());
  const SESSION_STORAGE_GROUP_ID = 'groupIdValue';

  const checkAuthStatus = useCallback(async () => {
    let isValid = false;
    try {
      isValid = await validateSession();
      setIsAuthenticated(isValid);
    } catch (e) {
      setIsAuthenticated(false);
    } finally {
      setIsAuthReady(true);
    }
  }, []);

  useEffect(() => {
    setIsAuthReady(false);
    checkAuthStatus();
  }, []);

  const getMemberId = useCallback(() => {
    return getMemberProfile()?.memberId || null;
  }, []);

  const signIn = useCallback(() => {
    setIsAuthenticated(true);
  }, [validateSession]);

  const signOut = useCallback(() => {
    setIsAuthReady(false);
    clearAuthData();
    setIsAuthenticated(false);
    const groupId = window.localStorage.getItem(SESSION_STORAGE_GROUP_ID);
    window.localStorage.removeItem(SESSION_STORAGE_GROUP_ID);
    if (groupId === 'rei') {
      window.location.href = '/rei';
    } else {
      window.location.href = '/';
    }
  }, []);

  useEffect(() => {
    const timer = setInterval(() => {
      const timeDiffSinceLastMouseDown = (Date.now() - lastMouseDownTime.current) / 60000; // get in minutes
      // if the member was inactive for the specified time, sign out from the app
      const shouldSignOut =
        timeDiffSinceLastMouseDown > MEMBER_SESSION_INACTIVE_MAX_ALLOWED_TIME &&
        isAuthenticated &&
        !getFromStorage('extend_session');
        
      if (shouldSignOut) {
        signOut();
      }
    }, (1000 * 60 * MEMBER_SESSION_INACTIVE_MAX_ALLOWED_TIME) / 2); //  do the check every half time of MEMBER_SESSION_INACTIVE_MAX_ALLOWED_TIME
    return function cleanup() {
      clearInterval(timer);
    };
  }, [router]);

  // capture the last mouse down time
  const handleMouseDown = useCallback(() => {
    lastMouseDownTime.current = Date.now();
  }, []);

  useEffect(() => {
    // global mouse down event handler to capture mouse down event
    window.addEventListener('mousedown', handleMouseDown, true);
    return function cleanup() {
      window.removeEventListener('mousedown', handleMouseDown, true);
    };
  }, [handleMouseDown]);

  return (
    <AuthContext.Provider
      value={{
        signIn,
        signOut,
        isAuthenticated,
        getMemberId,
        getMemberProfile,
      }}
    >
      {isAuthReady || isServerSide() ? children : <CircularLoader displayText="Loading.." GGD />}
    </AuthContext.Provider>
  );
};

const useAuth = () => React.useContext(AuthContext);

export { AuthProvider, useAuth };
