import React, { useState, useEffect } from 'react';

const Store = React.createContext();

const GGD_UI_STORE_KEY = 'ggd_ui_store_key';

const getFromStorage = (key) => {
  const value = window.localStorage.getItem(key);
  if (!value) return {};
  return JSON.parse(value);
};

const setToStorage = (key, value) => {
  window.localStorage.setItem(key, JSON.stringify(value));
};

const mergeToStorage = (key, value) => {
  const currentStorage = getFromStorage(key);
  setToStorage(key, { ...currentStorage, ...value });
};

const StoreProvider = ({ children }) => {
  // Read from the current session storage
  const [store, setStore] = useState({});

  // Once the component renders in client side, run the setStore
  useEffect(() => {
    setStore(getFromStorage(GGD_UI_STORE_KEY));
  }, []);

  const setInStore = (payload, persist = false) => {
    setStore((store) => ({
      ...store,
      ...payload,
    }));
    // If persist flag is used, store in the session storage
    // Number of items persisted should be minimal as it will adversly effect the performance
    if (persist) {
      mergeToStorage(GGD_UI_STORE_KEY, payload);
    }
  };

  const context = {
    store,
    setInStore,
  };
  return <Store.Provider value={context}>{children}</Store.Provider>;
};

const useStore = () => React.useContext(Store);

export { Store, StoreProvider, useStore };
