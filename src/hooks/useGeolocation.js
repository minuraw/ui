import { useCallback, useState } from 'react';
import Geocoding from '@mapbox/mapbox-sdk/services/geocoding';

const geocodingClient = Geocoding({
  accessToken: process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN,
});

const DEFAULT_CONFIG = {
  types: ['place', 'postcode'],
  countries: ['US'],
};

const useGeolocation = (config = DEFAULT_CONFIG) => {
  const [loading, setLoading] = useState(false);

  const getLocation = useCallback(
    (options = { maximumAge: 0, timeout: 5000 }) => {
      setLoading(true);

      return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
          ({ coords }) => {
            const { latitude, longitude } = coords;

            if (latitude && longitude) {
              geocodingClient
                .reverseGeocode({
                  query: [coords.longitude, coords.latitude],
                  ...config,
                })
                .send()
                .then(({ body }) => {
                  setLoading(false);
                  return resolve(body);
                });
            } else {
              setLoading(false);
            }
          },
          (error) => {
            setLoading(false);
            return reject(error);
          },
          options,
        );
      });
    },
    [config],
  );

  return { getLocation, loading };
};

export { useGeolocation };
