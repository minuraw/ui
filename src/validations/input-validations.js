const NAME_REGEX = /^[a-zA-Z .-]+$/;
const EMAIL_REGEX =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const memberNameValidation = (memberName) => {
  const validResponse = {
    valid: false,
    code: null,
  };
  if (!memberName || typeof memberName !== 'string') {
    validResponse.code = 'Invalid name value';
    return validResponse;
  }

  const sanitizeName = memberName;
  const isValidName = NAME_REGEX.test(sanitizeName);
  validResponse.valid = isValidName;
  if (!isValidName) {
    validResponse.code = 'Invalid characters in the name';
  }
  return validResponse;
};

const memberEmailValidation = (memberEmail) => {
  const validResponse = {
    valid: false,
    code: null,
  };
  if (!memberEmail || typeof memberEmail !== 'string') {
    validResponse.code = 'Invalid email value';
    return validResponse;
  }

  const sanitizeEmail = memberEmail.toLowerCase();
  const isValidEmail = EMAIL_REGEX.test(sanitizeEmail);
  validResponse.valid = isValidEmail;
  if (!isValidEmail) {
    validResponse.code = 'Invalid email';
  }
  return validResponse;
};
export { memberNameValidation, memberEmailValidation };
