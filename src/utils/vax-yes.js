const getMessageOption = (provider, answerValue, nextQuestion, latestPayload) => {
  if (nextQuestion && nextQuestion.key === 'memberPaymentInfo') {
    return { paymentMsg: answerValue };
  }
  if (provider === 4) {
    if (nextQuestion && nextQuestion.key === 'vaccineCard') {
      return { vaccineCard: answerValue };
    } else if (nextQuestion && nextQuestion.key === 'vaccineBoosterShot') {
      return {
        vaccineCard: latestPayload.vaccineCard.label,
        boosterDose: answerValue,
      };
    } else if (latestPayload && latestPayload.vaccineCard && latestPayload.vaccineBoosterShot) {
      return {
        vaccineCard: latestPayload.vaccineCard.label,
        boosterDose: latestPayload.vaccineBoosterShot.label,
      };
    } else if (latestPayload && latestPayload.vaccineCard) {
      return { vaccineCard: latestPayload.vaccineCard.label };
    }
  }
  return null;
};

export { getMessageOption };
