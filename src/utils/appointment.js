// Returns an object of appointments grouped by status
const groupAppointmentsByStatus = (appointments, derivedStatusMap = {}) => {
  if (!Array.isArray(appointments)) {
    console.warn('groupAppointmentsByStatus expects appointments to be an array');
    return {};
  }

  return appointments.reduce((prevVal, curVal) => {
    const key = derivedStatusMap[curVal.encounterDescription]
      ? derivedStatusMap[curVal.encounterDescription]
      : curVal.encounterDescription
      ? curVal.encounterDescription
      : '_unknown';

    if (prevVal[key]) {
      prevVal[key] = [...prevVal[key], curVal];
    } else {
      prevVal[key] = [curVal];
    }

    return prevVal;
  }, {});
};

export { groupAppointmentsByStatus };
