const navigateToNewChat = (router, productId, skill, sync = false) => {
  const url = '/chat/start/' + productId + '/' + skill + '/' + (sync ? 'sync' : 'async');
  router.push(url);
};

const logCallback = () => {};

export { navigateToNewChat, logCallback };
