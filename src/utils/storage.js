const setInStorage = (key, payload) => {
  if (!window) return;
  let stringifiedPayload = payload;
  if (typeof payload === 'object') {
    stringifiedPayload = JSON.stringify(payload);
  }
  // do the encoding
  window.sessionStorage.setItem(key, stringifiedPayload);
};

const getFromStorage = (key) => {
  if (!window) return;
  const stringifiedPayload = window.sessionStorage.getItem(key);
  let payload = stringifiedPayload;
  try {
    // try to parse the payload
    payload = JSON.parse(payload);
  } catch {
    // this means payload is not parsable, therefore do nothing
  }
  return payload;
};

const clearFromStorage = (key) => {
  if (!window) return;
  window.sessionStorage.removeItem(key);
};

export { setInStorage, getFromStorage, clearFromStorage };
