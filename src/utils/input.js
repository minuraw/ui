import Joi from 'joi';
import { INPUT_TYPES } from '../components/Chat/constants';
import { parsePhoneNumber, validatePhoneNumberLength } from 'libphonenumber-js';
import { COUNTRIES_ISO_CODES } from '../utils';

const MM_DD_YYYY_REGEX = /(\d{1,2})\/(\d{1,2})\/(\d{4})/;

const MM_DD_YYYY_VALIDATOR_REGEX =
  /^(((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))[-/]?[0-9]{4}|02[-/]?29[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;

const DD_MM_YYYY_VALIDATOR_REGEX =
  /^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

const stringValidator = (metadata) => {
  if (!metadata) return null;

  const { regex, regexErrorMessage, minLength, maxLength } = metadata;

  // Regex validation is handled separately
  let schema = Joi.string();
  let regexSchema = Joi.string();

  // register the schema
  if (regex) {
    regexSchema = schema.pattern(new RegExp(regex));
  }
  if (minLength) {
    schema = schema.min(+minLength);
  }
  if (maxLength) {
    schema = schema.max(+maxLength);
  }

  return (value) => {
    // In case user enters nothing, undefined is the value therefore sanitize the value to return a empty string
    const sanitizedValue = value || '';
    // do the validation
    const { error: regexError } = regexSchema.validate(sanitizedValue);
    const { error } = schema.validate(sanitizedValue);

    if (regexError) {
      return {
        valid: false,
        code: regexErrorMessage || 'Incorrect value entered',
      };
    }

    if (error) {
      const errorObject = new Error(error);
      return {
        valid: false,
        code: errorObject.message.replace('ValidationError: "value" ', ''), // remove the validation error part from the string
      };
    }
    // No errors, return the code
    return {
      valid: true,
      code: null,
    };
  };
};

const numberValidator = (metadata) => {
  if (!metadata) return null;

  const { min, max, dataType } = metadata;

  let schema = Joi.number();

  // If min set to 0, JS will consider it to be a falsy value, therefore explicit 0 check is required
  if (min || min === 0) {
    schema = schema.min(+min);
  }
  if (max || max === 0) {
    schema = schema.max(+max);
  }
  if (dataType === 'integer') {
    schema = schema.integer();
  }

  return (value) => {
    const { error } = schema.validate(value);

    if (error) {
      const errorObject = new Error(error);
      return {
        valid: false,
        code: errorObject.message.replace('ValidationError: "value" ', ''),
      };
    }
    // No error
    return {
      valid: true,
      code: null,
    };
  };
};

const phoneNumberValidator = (locale) => {
  const CAPTIONS = {
    tooShort: {
      'en-us': 'Phone number is too short.',
      'es-mx': 'El número de teléfono es muy corto.',
    },
    tooLong: {
      'en-us': 'Phone number is too long.',
      'es-mx': 'El número de teléfono es muy largo.',
    },
    invalidCountry: {
      'en-us': 'Phone number has an invalid country or area code.',
      'es-mx': 'El número de teléfono tiene un código de país o área inválido.',
    },
    notNumber: {
      'en-us': 'Phone number is not a number.',
      'es-mx': 'El número de teléfono no es un número.',
    },
    invalidLenght: {
      'en-us': 'Phone number has an invalid lenght',
      'es-mx': 'El número de teléfono tiene una longitud inválida.',
    },
    invalidPhone: {
      'en-us': 'Invalid phone number',
      'es-mx': 'Número de teléfono inválido.',
    },
  };

  return (value) => {
    const phoneNumber = `+${value.replace(/\D/g, '')}`;
    const phoneLenght = validatePhoneNumberLength(phoneNumber);

    let phoneCountry;
    if (!phoneLenght) {
      const { country } = parsePhoneNumber(phoneNumber);
      phoneCountry = country;
    }
    let code;
    if (!phoneCountry) {
      code = CAPTIONS.invalidCountry[locale];
    }
    if (phoneLenght) {
      switch (phoneLenght) {
        case 'TOO_SHORT':
          code = CAPTIONS.tooShort[locale];
          break;
        case 'TOO_LONG':
          code = CAPTIONS.tooLong[locale];
          break;
        case 'NOT_A_NUMBER':
          code = CAPTIONS.notNumber[locale];
          break;
        case 'INVALID_LENGHT':
          code = CAPTIONS.invalidLenght[locale];
          break;
        default:
          code = CAPTIONS.invalidPhone[locale];
      }
    }

    if (code) {
      return {
        valid: false,
        code: code,
      };
    }

    // No error
    return {
      valid: true,
      code: null,
    };
  };
};

const dateValidatorEN = (metadata) => {
  const schema = Joi.string().length(10).pattern(MM_DD_YYYY_VALIDATOR_REGEX).required();

  const { minDate, maxDate } = metadata || {};

  return (value) => {
    const { error } = schema.validate(value);

    if (error) {
      return {
        valid: false,
        code: 'Invalid date',
      };
    }

    // now check for minDate maxDate validation
    if (minDate) {
      const minDateObject = new Date(minDate);
      // The date selected is below the allowed date range
      if (minDateObject > new Date(value)) {
        const minDateFormatted = new Intl.DateTimeFormat('en-US').format(minDateObject);
        return {
          valid: false,
          code: `Date should be after ${minDateFormatted}`,
        };
      }
    }
    if (maxDate) {
      const maxDateObject = new Date(maxDate);
      // The date selected is above the allowed date range
      if (maxDateObject < new Date(value)) {
        const maxDateFormatted = new Intl.DateTimeFormat('en-US').format(maxDateObject);
        return {
          valid: false,
          code: `Date cannot be beyond ${maxDateFormatted}`,
        };
      }
    }
    // No error
    return {
      valid: true,
      code: null,
    };
  };
};

const dateValidatorES = (metadata) => {
  const schema = Joi.string().length(10).pattern(DD_MM_YYYY_VALIDATOR_REGEX).required();

  const { minDate, maxDate } = metadata || {};

  return (value) => {
    const { error } = schema.validate(value);

    const dateParts = value.split('/');
    value = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

    if (error) {
      return {
        valid: false,
        code: 'Fecha inválida',
      };
    }

    // now check for minDate maxDate validation
    if (minDate) {
      const minDateObject = new Date(minDate);
      // The date selected is below the allowed date range
      if (minDateObject > new Date(value)) {
        const minDateFormatted = new Intl.DateTimeFormat('es-MX').format(minDateObject);
        return {
          valid: false,
          code: `La fecha debe ser posterior al ${minDateFormatted}`,
        };
      }
    }
    if (maxDate) {
      const maxDateObject = new Date(maxDate);
      // The date selected is above the allowed date range
      if (maxDateObject < new Date(value)) {
        const maxDateFormatted = new Intl.DateTimeFormat('es-MX').format(maxDateObject);
        return {
          valid: false,
          code: `La fecha no debe ser posterior al ${maxDateFormatted}`,
        };
      }
    }
    // No error
    return {
      valid: true,
      code: null,
    };
  };
};

const getInputValidator = (type, metadata, locale) => {
  // This is text field
  if (type === INPUT_TYPES.TEXT_INPUT) {
    return stringValidator(metadata);
  }
  if (type === INPUT_TYPES.NUMBER_INPUT) {
    return numberValidator(metadata);
  }
  if (type === INPUT_TYPES.PHONE_NUMBER) {
    return phoneNumberValidator(locale);
  }
  if (type === INPUT_TYPES.DATE_INPUT) {
    if (locale === 'es-mx') {
      return dateValidatorES(metadata);
    }
    return dateValidatorEN(metadata);
  }
  return null;
};

const getWithLeadingZero = (digit) => {
  return (digit + '').padStart(2, '0');
};

const getDateDDMMYYYYString = (date, locale, delimiter = '-') => {
  if (!date) {
    return '';
  }
  if (typeof date === 'string') {
    // in this case date is passed as mm/dd/yyyy format
    const match = date.match(MM_DD_YYYY_REGEX);
    if (match) {
      // Extract each constituent
      const month = locale === 'es-mx' ? match[2] : match[1];
      const day = locale === 'es-mx' ? match[1] : match[2];
      const year = match[3];
      // return yyyy-mm-dd format
      return `${year}${delimiter}${getWithLeadingZero(month)}${delimiter}${getWithLeadingZero(
        day,
      )}`;
    }
    return '';
  }
  return (
    date.getFullYear() +
    delimiter +
    getWithLeadingZero(date.getMonth() + 1) +
    delimiter +
    getWithLeadingZero(date.getDate())
  );
};

const getDateMMDDYYYYString = (date, locale, delimiter = '/') => {
  if (!date) {
    return '';
  }
  if (typeof date === 'string') {
    // in this case date is passed as mm/dd/yyyy format
    const match = date.match(MM_DD_YYYY_REGEX);
    if (match) {
      // Extract each constituent
      const month = locale === 'es-mx' ? match[2] : match[1];
      const day = locale === 'es-mx' ? match[1] : match[2];
      const year = match[3];
      // return yyyy-mm-dd format
      return `${getWithLeadingZero(month)}${delimiter}${getWithLeadingZero(
        day,
      )}${delimiter}${year}`;
    }
    return '';
  }
  return (
    getWithLeadingZero(date.getMonth() + 1) +
    delimiter +
    getWithLeadingZero(date.getDate()) +
    delimiter +
    date.getFullYear()
  );
};

// Component used in FE will send the phone number in the following format +1 (223) 223-2323
// This util will remove +1 (country code) and other params
const formatNumberFiled = (phoneNumber) => {
  if (typeof phoneNumber !== 'string') return null;
  // First replace statement remove +1<space>, second replace removes () - ''
  return phoneNumber.replace(/^\+\d+ /, '').replace(/[^+\d]+/g, '');
};

// // This util will remove any other params than decimal to +{country code}{number}
// eg.: +1 (650) 918-6149 to +16509186149
const getFormattedPhoneNumber = (phoneNumber) => {
  if (phoneNumber) {
    return '+' + phoneNumber.replace(/\D/g, '');
  }
  return '';
};

const getCountryByPhoneNumber = (phoneNumber) => {
  try {
    const { country } = parsePhoneNumber(phoneNumber);
    return country || 'US';
  } catch (_) {}
  return 'US';
};

export {
  getInputValidator,
  getDateDDMMYYYYString,
  getDateMMDDYYYYString,
  formatNumberFiled,
  getFormattedPhoneNumber,
  getCountryByPhoneNumber,
};
