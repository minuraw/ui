import { getLocale } from '../services';

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const getTimeslotInfo = (appointments, consultId) => {
  const timeslots = {};

  appointments.forEach((appointment) => {
    // Get the time in users timezone
    const date = new Date(appointment.start);
    const dateWithTimeZone = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
    if (timeslots[dateWithTimeZone]) {
      timeslots[dateWithTimeZone].push({
        label: new Intl.DateTimeFormat('en-US', { timeStyle: 'short' }).format(date),
        value: { start: date, consultId },
      });
    } else {
      timeslots[dateWithTimeZone] = [];
      timeslots[dateWithTimeZone].push({
        label: new Intl.DateTimeFormat('en-US', { timeStyle: 'short' }).format(date),
        value: { start: date, consultId },
      });
    }
  });

  return timeslots;
};

const isDateSame = (dateString, dateObject) => {
  return (
    dateString === `${dateObject.getFullYear()}-${dateObject.getMonth()}-${dateObject.getDate()}`
  );
};

const getDateInFormat = (date) => {
  return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
};

const ordinal = (d) => {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1:
      return 'st';
    case 2:
      return 'nd';
    case 3:
      return 'rd';
    default:
      return 'th';
  }
};

const getDate = (date) => {
  const month = new Intl.DateTimeFormat('en-US', { month: 'long' }).format(date);
  const day = date.getDate();
  return `${month} ${day}${ordinal(day)}`;
};

const isToday = (someDate) => {
  const today = new Date();
  return (
    someDate.getDate() === today.getDate() &&
    someDate.getMonth() === today.getMonth() &&
    someDate.getFullYear() === today.getFullYear()
  );
};

const isTomorrow = (someDate) => {
  const tomorrow = new Date();
  // tomorrow is today + 1
  tomorrow.setDate(tomorrow.getDate() + 1);
  return (
    someDate.getDate() === tomorrow.getDate() &&
    someDate.getMonth() === tomorrow.getMonth() &&
    someDate.getFullYear() === tomorrow.getFullYear()
  );
};

const isThisYear = (someDate) => {
  const today = new Date();
  return someDate.getFullYear() === today.getFullYear();
};

// Return the chat time shown in the UI
// If today -> show only the time part
// If this year -> show only the month, day and time
// If not in this year -> show year also
const getChatDateTime = (datetime) => {
  const locale = getLocale();
  const date = new Date(datetime);
  // For today, show only the time part
  if (isToday(date)) {
    return new Intl.DateTimeFormat(locale, { timeStyle: 'short' }).format(date);
  }
  if (isThisYear(date)) {
    return new Intl.DateTimeFormat(locale, {
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(date);
  }
  return new Intl.DateTimeFormat(locale, {
    month: 'short',
    year: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }).format(date);
};

const getChatHeaderTime = (datetime) => {
  const locale = getLocale();
  const date = new Date(datetime);

  const timePart = new Intl.DateTimeFormat(locale, { timeStyle: 'short' }).format(date);
  const datePart = new Intl.DateTimeFormat(locale, { dateStyle: 'short' }).format(new Date(date));

  return `${datePart} at ${timePart}`;
};

// this converts “2021-01-01T00:00:00.000Z” => "January 01st, 2021"
const getFormattedDateFromDateStringWithoutTimeZone = (dateString, monthFormat) => {
  if (!dateString || dateString.length < 10) {
    return '';
  }
  try {
    const dateSection = dateString.split('T')[0];
    const dateParts = dateSection.split('-');
    const year = dateParts[0];
    const month =
      monthFormat === 'short'
        ? monthNames[Number(dateParts[1]) - 1].substring(0, 3)
        : monthNames[Number(dateParts[1]) - 1];
    const date = dateParts[2];
    return `${month} ${date}${ordinal(Number(date))}, ${year}`;
  } catch (err) {
    console.error('Error in date string conversion');
    return '';
  }
};

// This converts
// "English": “2021-01-07T10:20:00.000Z” => "January 01st, 2021"
// "Spanish": “2021-01-07T10:20:00.000Z” => "01 de Enero de 2021"
// "English - Short": “2021-01-07T10:20:00.000Z” => "Jan 01st, 2021"
// "English - !Short": “2021-01-07T10:20:00.000Z” => "January 01st, 2021"
// "Spanish - Short": “2021-01-07T10:20:00.000Z” => "01 de Ene. de 2021"
// "Spanish - !Short": “2021-01-07T10:20:00.000Z” => "01 de Enero de 2021"
// "English - Short - enabledTime": “2021-01-07T10:20:00.000Z” => "Jan 01st, 2021 10:20 PM"
// "English - !Short - enabledTime": “2021-01-07T10:20:00.000Z” => "January 01st, 2021 10:20 PM"
// "Spanish - Short - enabledTime": “2021-01-07T10:20:00.000Z” => "01 de Ene de 2021 22:20"
// "Spanish - !Short -  enabledTime": “2021-01-07T10:20:00.000Z” => "01 de Enero de 2021 22:20"

const getFormattedDateTimeString = (dateString, monthFormat, enabledTime) => {
  const locale = getLocale();
  if (!dateString || dateString.length < 10) {
    return '';
  }
  try {
    const date = new Date(dateString);
    const time = new Intl.DateTimeFormat(locale, { timeStyle: 'short' }).format(date);
    if (locale === 'es-mx') {
      const options = {
        year: 'numeric',
        month: monthFormat === 'short' ? 'short' : 'long',
        day: 'numeric',
        timeZone: 'UTC',
      };
      const dateES = date.toLocaleDateString('es-mx', options);
      return `${dateES}${enabledTime ? ` ${time}` : ''}`;
    }
    const dateSection = dateString.split('T')[0];
    const dateParts = dateSection.split('-');
    const year = dateParts[0];
    const month =
      monthFormat === 'short'
        ? monthNames[Number(dateParts[1]) - 1].substring(0, 3)
        : monthNames[Number(dateParts[1]) - 1];
    const day = dateParts[2];
    return `${month} ${day}${ordinal(Number(day))}, ${year}${enabledTime ? ` ${time}` : ''}`;
  } catch (err) {
    console.error('Error in date string conversion');
    return '';
  }
};

// this converts “2021-01-07T10:20:00.000Z” => "1 Jan 2021"
const getDateFromDateTimeIso = (date) => {
  if (!date) return;

  return new Intl.DateTimeFormat('en-GB', { dateStyle: 'medium' }).format(new Date(date));
};

// this converts “2021-01-07T10:20:00.000Z” => "10:20 am"
const getTimeFromDateTimeIso = (date) => {
  if (!date) return;

  return new Intl.DateTimeFormat('en-GB', { timeStyle: 'short', hour12: true }).format(
    new Date(date),
  );
};

const getHumanReadableDateTime = (date) => {
  return new Intl.DateTimeFormat('en-US', {
    dateStyle: 'short',
    timeStyle: 'short',
    hour12: true,
  }).format(new Date(date));
};

const getTimePart = (someDate) => {
  return new Intl.DateTimeFormat('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  }).format(someDate);
};

const getDatePart = (date) => new Intl.DateTimeFormat('en-US', { dateStyle: 'short' }).format(date);

const getVideoVisitInfo = (start) => {
  try {
    const startDate = new Date(start);
    const consultStartTime = getTimePart(startDate);

    let consultTimeText = `Your consult time is at ${consultStartTime} `;
    if (isToday(startDate)) {
      consultTimeText += 'today';
    } else if (isTomorrow(startDate)) {
      consultTimeText += ' tomorrow';
    } else {
      consultTimeText += getDatePart(startDate);
    }

    const consultStartTimeText = `You will be able to join the video call ten minutes before the consult time at ${consultStartTime}`;

    return {
      consultTimeText,
      consultStartTimeText,
    };
  } catch (err) {
    console.log(err);
  }
};

const getDayDateMonthTime = (dateString) => {
  if (!dateString) return;

  const fullDate = new Date(dateString);

  const day = new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(fullDate);
  const date = fullDate.getDate();
  const ordinalDate = `${date}${ordinal(date)}`;
  const month = monthNames[fullDate.getMonth()];
  const time = getTimePart(fullDate);

  return { day, date, ordinalDate, month, time };
};

export {
  getTimeslotInfo,
  isDateSame,
  getDateInFormat,
  ordinal,
  getDate,
  getChatDateTime,
  getFormattedDateFromDateStringWithoutTimeZone,
  getFormattedDateTimeString,
  getDateFromDateTimeIso,
  getTimeFromDateTimeIso,
  getHumanReadableDateTime,
  getVideoVisitInfo,
  getDayDateMonthTime,
  getChatHeaderTime
};
