import CryptoJS from 'crypto-js';

const encrypt = (string, secret) => {
  return CryptoJS.AES.encrypt(string, secret).toString();
};

const decrypt = (encryptedString, secret) => {
  const bytes = CryptoJS.AES.decrypt(encryptedString, secret);
  return bytes.toString(CryptoJS.enc.Utf8);
};

export { encrypt, decrypt };
