import { datadogLogs } from '@datadog/browser-logs';

/**
 * Application bootstrap functions
 */

const APP_NAME = 'member-frontend';
const ENV = process.env.NEXT_PUBLIC_ENV || 'development';
const DATADOG_CLIENT_TOKEN = process.env.NEXT_PUBLIC_DATADOG_CLIENT_TOKEN;
const DATADOG_SITE = process.env.NEXT_PUBLIC_DATADOG_SITE;

const init = () => {
  try {
    // DataDog log init
    datadogLogs.init({
      clientToken: DATADOG_CLIENT_TOKEN,
      site: DATADOG_SITE,
      env: ENV,
      service: APP_NAME,
      sampleRate: 100,
      forwardErrorsToLogs: ENV === 'production', // Forward the error logs in production setup
    });
  } catch (error) {
    console.error(`Error in app inti ${error}`);
  }
};

export default init;
