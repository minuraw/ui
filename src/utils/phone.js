// Formats US phone number +16509186149 to (650) 918-6149
const formatPhoneNumberUS = (str) => {
  if (!str) return;
  
  const match = str.replace(/\D/g, '').match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);

  return match && match[1] === '1' ? ['(', match[2], ') ', match[3], '-', match[4]].join('') : str;
};

export { formatPhoneNumberUS };
