
const COUNTRIES_ISO_CODES = {
  US: 'US',
  IR: 'IR',
  CU: 'CU',
  SD: 'SD',
  SY: 'SY',
  KP: 'KP',
  PR: 'PR',
  AS: 'AS',
  GU: 'GU',
  MP: 'MP',
  VI: 'VI',
};

export { COUNTRIES_ISO_CODES };
