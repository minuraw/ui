import Router from 'next/router';
import { v4 as uuidv4 } from 'uuid';

import { isServerSide } from './common';
import { getFromStorage } from './storage';

import { initiateSessionRefresh } from '../connectors/bff-connector';
import {
  getRefreshToken,
  setAccessToken,
  getMemberSessionId,
  MEMBER_SESSION_ID,
} from '../services';

const MAX_REFRESH_ATTEMPTS = 2;

const CORRELATION_ID = 'correlation-id';
const BROWSER_SESSION_ID = 'browser-session-id';

const handleSessionTimeout = () => {
  try {
    const notification = {
      error: 'Your session has been expired',
      options: { duration: -1 },
      // mark the duration as Infinity by setting it to -1
    };
    // save the notification in the session storage
    !isServerSide() && window.localStorage.setItem('notifications', JSON.stringify(notification));
    // do the redirect
    Router.push('/');
  } catch (err) {
    // error indicates service side error, reject it
  }
};

const shouldContinueToFetchRefreshToken = (attempt) => {
  if (!isServerSide()) {
    // see if the max attempts reached and it is required to extend the session
    return !!getFromStorage('extend_session') && attempt < MAX_REFRESH_ATTEMPTS;
  }
  return false;
};

const fetchRefreshToken = async () => {
  try {
    const refreshToken = getRefreshToken();
    // get the access token from refresh token
    const { access_token: accessToken } = await initiateSessionRefresh({
      refresh_token: refreshToken,
    });

    setAccessToken(accessToken);

    return true;
  } catch (err) {
    console.error(err);
    return false;
  }
};

const setBrowserSessionId = (browserSessionId) => {
  if (!isServerSide()) {
    window.sessionStorage.setItem(BROWSER_SESSION_ID, browserSessionId);
  }
};

const getBrowserSessionId = () => {
  if (!isServerSide()) {
    window.sessionStorage.getItem(BROWSER_SESSION_ID);
  }
};

const getRequestTrackingHeaders = () => {
  const headers = {};

  try {
    headers[CORRELATION_ID] = uuidv4();
    headers[MEMBER_SESSION_ID] = getMemberSessionId();
  } catch {
    // nothing to handle
  }

  return headers;
};

const getPublicRequestTrackingHeaders = () => {
  const headers = {};

  headers[CORRELATION_ID] = uuidv4();

  return headers;
};

const handleBrowserSessionId = () => {
  // see if there is a browser session id
  const browserSessionId = getBrowserSessionId();
  if (browserSessionId) return;

  // if no browser session id, set it
  setBrowserSessionId(uuidv4());
};

export {
  handleSessionTimeout,
  shouldContinueToFetchRefreshToken,
  fetchRefreshToken,
  getRequestTrackingHeaders,
  handleBrowserSessionId,
  getPublicRequestTrackingHeaders,
};
