import { CONVERSATION_PROVIDER_ID } from '../constants/providers';
import { PROTOCOL_CAP_KEYS, PROTOCOL_KEYS } from '../constants/protocolNames';

const START_SERVICES = [
  PROTOCOL_KEYS.SYMPTOM_CHECKER,
  PROTOCOL_KEYS.VAX_YES,
  PROTOCOL_KEYS.VAX_YES_BOOSTER,
  PROTOCOL_KEYS.COVID_19_TEST_CERT,
  PROTOCOL_KEYS.GGD_CO_VAX_YES,
];

const decodeChatRouteParams = (router) => {
  const { params, provider, ...queryParams } = router.query;
  const routeParams = {
    start: false,
    productId: null,
    groupId: null,
    skillId: null,
    sync: false,
    isAlreadyInitiatedChat: false,
    conversationId: null,
    messageId: null,
    skill: null,
    skillMode: 'async',
    provider: parseInt(provider, 10) || '',
    directLink: false,
    ...queryParams,
  };

  if (queryParams.update === 'true') {
    routeParams.isUpdate = true;
  } else {
    routeParams.isUpdate = false;
  }

  if (params && params.length > 0) {
    if (params[0]) {
      if (params[0] === 'start') {
        routeParams.start = true;
        if (params[1] && START_SERVICES.includes(params[1])) {
          routeParams.provider = CONVERSATION_PROVIDER_ID[PROTOCOL_CAP_KEYS[params[1]]];
          routeParams.skill = params[1];
          routeParams.skillId = params[1];
          routeParams.productId = params[2];
          routeParams.groupId = params.length > 3 ? params[3] : null;
          routeParams.directLink = true;
          return routeParams;
        }
        if (params[1] && !isNaN(params[1])) {
          routeParams.productId = parseInt(params[1], 10);
          routeParams.provider = CONVERSATION_PROVIDER_ID.ROZIE;

          if (params[2]) {
            // Attach the initial protocol as skill
            routeParams.skill = params[2];
            routeParams.skillId = 'skill_id_' + params[2] + '_protocol';
            if (params[3] && params[3] === 'sync') {
              routeParams.sync = true;
              routeParams.skillMode = 'sync';
            }

            // hardcoded to use GGD_CONVERSATION
            if (
              routeParams.skill === 'acne' ||
              routeParams.skill === 'migraine_refills' ||
              routeParams.skill === 'standard-intake-with-sb' ||
              routeParams.skill === 'urinary-tract-infection' ||
              routeParams.skill === 'birth-control' ||
              routeParams.skill === 'erectile-dysfunction' ||
              routeParams.skill === 'standard-intake'
            ) {
              routeParams.provider = CONVERSATION_PROVIDER_ID.GGD_CONVERSATION;
            }
          }
        }
      } else {
        routeParams.conversationId = params[0];
        if (params[1]) {
          routeParams.messageId = params[1];
        }
      }
    }
  }
  return routeParams;
};

const decodeWalletRouteParams = (router) => {
  const { params, provider, ...queryParams } = router.query;
  const routeParams = {
    certificateId: null,
    ...queryParams,
  };

  if (params && params.length > 0) {
    if (params[0]) {
      routeParams.certificateId = params[0];
    }
  }
  return routeParams;
};

const decodeAppointmentRouteParams = (router) => {
  const { params, provider, ...queryParams } = router.query;
  const routeParams = {
    conversationId: null,
    provider: parseInt(provider, 10) || '',
    ...queryParams,
  };

  if (params && params.length > 0) {
    if (params[0]) {
      routeParams.conversationId = params[0];
    }
  }
  return routeParams;
};

const decodeDashboardRouteParams = (router) => {
  const { section } = router.query;

  let routeParams = {
    section: null,
  };
  if (section === 'chat') {
    routeParams.section = section;
    const chatRouteParams = decodeChatRouteParams(router);
    routeParams = {
      ...routeParams,
      ...chatRouteParams,
    };
  } else if (section === 'wallet') {
    routeParams.section = section;
    const walletRouteParams = decodeWalletRouteParams(router);
    routeParams = {
      ...routeParams,
      ...walletRouteParams,
    };
  } else if (section === 'appointments') {
    routeParams.section = section;
    const appointmentRouteParams = decodeAppointmentRouteParams(router);
    routeParams = {
      ...routeParams,
      ...appointmentRouteParams,
    };
  }

  return routeParams;
};

export { decodeChatRouteParams, decodeDashboardRouteParams };
