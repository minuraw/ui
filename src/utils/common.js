const isEmptyObject = (o) => {
  if (typeof o !== 'object') return true;
  // check for the keys in the object
  return Object.keys(o).length === 0;
};

const nonEmptyArray = (arr) => {
  return Array.isArray(arr) && arr.length > 0;
};

const isServerSide = () => {
  return typeof window === 'undefined';
};

const limitText = (string, characterLimit) => {
  // Check that string parameter is a string
  if (typeof string !== 'string') return string;

  return string.length <= characterLimit ? string : string.substr(0, characterLimit) + '...';
};

export { isEmptyObject, nonEmptyArray, isServerSide, limitText };
