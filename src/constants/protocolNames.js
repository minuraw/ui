const PROTOCOL_NAMES = {
  'symptom-checker': 'SymptomBot',
  'vax-yes': 'Covid-19 Vaccine Passport',
  'acne-async': 'Acne Chat',
  'acne-sync': 'Acne Video Visit',
  'migraine_refills-sync': 'Migraine Refill Video Visit',
  'migraine_refills-async': 'Migraine Refill Chat',
  'standard-intake-sync': 'General Visit',
  'covid-19-test-cert': ' COVID-19 Test Results',
  'co-vax-yes': 'VaxYes Attestation',
  'standard-intake-with-sb-sync': 'General Visit with symptom checker.',
  'birth-control-sync': 'Birth Control Video Visit',
  'erectile-dysfunction-sync': 'Erectile Dysfunction Video Visit',
  'erectile-dysfunction-async': 'Erectile Dysfunction Chat',
  'urinary-tract-infection-sync': 'UTI Video Visit',
  'urinary-tract-infection-async': 'UTI Chat',
  'birth-control-async': 'Birth Control Chat',
};

const PROTOCOL_KEYS = {
  SYMPTOM_CHECKER: 'symptom-checker',
  VAX_YES: 'vax-yes',
  VAX_YES_BOOSTER: 'vax-yes-booster',
  COVID_19_TEST_CERT: 'covid-19-test-cert',
  GGD_CO_VAX_YES: 'co-vax-yes',
};

const PROTOCOL_CAP_KEYS = {
  'symptom-checker': 'SYMPTOM_CHECKER',
  'vax-yes': 'VAX_YES',
  'vax-yes-booster': 'VAX_YES_BOOSTER',
  'covid-19-test-cert': 'GGD_TEST_CERTIFICATES',
  'co-vax-yes': 'GGD_CO_VAX_YES',
};
export { PROTOCOL_NAMES, PROTOCOL_KEYS, PROTOCOL_CAP_KEYS };
