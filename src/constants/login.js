const LOGIN = {
  KEY: 'memberPhoneNumber',
};

const CHALLENGE = {
  KEY: 'memberOTP',
};

const VERIFY = {
  KEY: 'memberDoB',
};

const SIGN_UP = {
  KEY_FIRST_NAME: 'memberFirstName',
  KEY_LAST_NAME: 'memberLastName',
};

const GGD_AUTH = 'GGD_AUTH';

export { LOGIN, CHALLENGE, VERIFY, SIGN_UP, GGD_AUTH };
