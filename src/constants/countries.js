import React from 'react';
import { US as FlagsUS, MX as FlagsMX } from 'country-flag-icons/react/3x2';
import { CURRENCY_USD, CURRENCY_MXN } from './currencies';

const COUNTRIES_ISO_CODES = {
  US: 'US',
  MX: 'MX',
};

const COUNTRIES_CURRENCY = {
  [COUNTRIES_ISO_CODES.US]: CURRENCY_USD,
  [COUNTRIES_ISO_CODES.MX]: CURRENCY_MXN,
};

const COUNTRIES = [
  {
    iso_alpha_2: COUNTRIES_ISO_CODES.US,
    label: 'USA',
    flag_icon: <FlagsUS />,
  },
  {
    iso_alpha_2: COUNTRIES_ISO_CODES.MX,
    label: 'Mexico',
    flag_icon: <FlagsMX />,
  },
];

export { COUNTRIES, COUNTRIES_ISO_CODES, COUNTRIES_CURRENCY };
