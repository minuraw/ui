const CURRENCY_USD = {
  abbr: 'usd',
  symbol: '$',
};
const CURRENCY_MXN = {
  abbr: 'mxn',
  symbol: 'Mex$',
};

const CURRENCIES = [CURRENCY_USD, CURRENCY_MXN];

export { CURRENCIES, CURRENCY_MXN, CURRENCY_USD };
