const CONVERSATION_PROVIDER_ID = {
  ROZIE: 1,
  SYMPTOM_CHECKER: 2,
  AUTH: 3,
  VAX_YES: 4,
  VAX_YES_BOOSTER: 5,
  GGD_CONVERSATION: 6,
  GGD_TEST_CERTIFICATES: 7,
  GGD_CO_VAX_YES: 8,
};

const PROVIDER_DESCRIPTION = {
  1: 'Clinical Visit',
  2: 'SymptomBot',
  3: 'Member Login',
  4: 'VaxYes',
  5: 'VaxYes Booster',
  6: 'Clinical Visit',
  7: 'Covid-19 Test Certificate',
  8: 'VaxYes',
};

const PROVIDER_TO_SOURCE_MAPPING = {
  1: 'ROZIE',
  2: 'SYMPTOM_CHECKER',
  3: 'AUTH',
  4: 'VAX_YES',
  5: 'VAX_YES_BOOSTER',
  6: 'GGD_CONVERSATION',
  7: 'GGD_TEST_CERTIFICATES',
  8: 'GGD_CO_VAX_YES',
};

export { CONVERSATION_PROVIDER_ID, PROVIDER_DESCRIPTION, PROVIDER_TO_SOURCE_MAPPING };
