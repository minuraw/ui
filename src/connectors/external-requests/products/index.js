import {
  getProductsPayment,
  getProducts,
  getProductCategories,
  verifyMemberPayment,
} from '../../../connectors/bff-connector';

const getProductGroup = async (groupLimit) => {
  return getProducts({
    limit: groupLimit.limit,
    offset: 0,
    product_group_id: groupLimit.id,
    category_id: groupLimit.categoryId,
  });
};

export {
  getProductGroup,
  getProductsPayment,
  getProducts,
  getProductCategories,
  verifyMemberPayment,
};
