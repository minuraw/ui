import { intentDetection } from '../../../adaptors/amplify-adaptor';

const detectIntentRequest = async (searchText) => {
  const { data } = await intentDetection({ searchText });
  if (data && data.detectIntent) {
    return data.detectIntent;
  }
  throw new Error('Detect intent request failed.');
};

export { detectIntentRequest };
