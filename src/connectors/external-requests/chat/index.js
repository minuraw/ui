import {
  getConversations,
  saveMessage,
  getChatHistory as getChatHistoryBff,
  saveConversationMedata,
  updateConversationMedata,
  getConversationMedata,
  saveSymptomBotPayload,
  getSymptomBotPayload,
  getVaxYesPayload,
  saveVaxYesPayload,
  deleteMessageById,
  getVYConversationId,
  getVaxYesCertificate,
  getPublicVideoVisitLink,
} from '../../bff-connector';

const getChatHistory = async (memberId, limit, offset, conversationId) => {
  const res = await getChatHistoryBff({
    member_id: memberId,
    limit: limit,
    offset: offset,
    conversation_id: conversationId,
  });
  // TODO now you can send this as a object
  return JSON.parse(res);
};

const getMemberConversations = async () => {
  return getConversations();
};

const saveConversationMetadataInfo = (payload) => {
  return saveConversationMedata(payload);
};

const updateConversationMetadataInfo = (payload) => {
  return updateConversationMedata(payload);
};

const getConversationMetadataInfo = (payload) => {
  return getConversationMedata(payload);
};

const saveSymptomBotPayloadBff = (payload) => {
  return saveSymptomBotPayload(payload);
};

const getSymptomBotPayloadBff = (payload) => {
  return getSymptomBotPayload(payload);
};

const getVaxYesPayloadBff = (payload) => {
  return getVaxYesPayload(payload);
};

const saveVaxYesPayloadBff = (payload) => {
  return saveVaxYesPayload(payload);
};

const deleteMessageByIdBff = (payload) => {
  return deleteMessageById(payload);
};

const getVYConversationIdBff = (payload) => {
  return getVYConversationId(payload);
};

const getVaxYesCertificateBff = () => {
  return getVaxYesCertificate();
};

export {
  getChatHistory,
  saveMessage,
  getMemberConversations,
  saveConversationMetadataInfo,
  updateConversationMetadataInfo,
  getConversationMetadataInfo,
  saveSymptomBotPayloadBff,
  getSymptomBotPayloadBff,
  getVaxYesPayloadBff,
  saveVaxYesPayloadBff,
  deleteMessageByIdBff,
  getVYConversationIdBff,
  getVaxYesCertificateBff,
  getPublicVideoVisitLink
};
