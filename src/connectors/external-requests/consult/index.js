import {
  getConsultTimeslots,
  amendIdImages,
  getConsultPrescriptionInfo,
} from '../../../connectors/bff-connector';

export { getConsultTimeslots, amendIdImages, getConsultPrescriptionInfo };
