import { postEvent, uploadImage } from '../../bff-connector';

const sendMessageToRozie = async (payload) => {
  try {
    const res = await postEvent({ postEvent: payload });
    // TODO refactor this and usage - no need of having data.post
    return { data: { post: res } };
  } catch (err) {
    console.error(err);
  }
};

export { sendMessageToRozie, uploadImage };
