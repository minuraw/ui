import {
  getDiagnoseSC,
  getNextQuestion,
  getRiskFactorsSC,
  getSymptomsAutoCompleteSearch,
} from '../../bff-connector';

const getSymptomAutoComplete = (payload) => {
  return getSymptomsAutoCompleteSearch(payload);
};

const getDiagnosis = (payload) => {
  return getDiagnoseSC({ ...payload });
};

const getSymptomBotNextQuestion = (payload) => {
  return getNextQuestion({
    protocol: 'symptom-bot',
    data: payload,
  });
};

const getRiskFactor = async (payload) => {
  return getRiskFactorsSC({ ...payload });
};

export { getSymptomAutoComplete, getDiagnosis, getRiskFactor, getSymptomBotNextQuestion };
