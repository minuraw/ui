import { sendPrivateRequest, sendPublicRequest } from '../adaptors/bff-adaptor';

/** *************** Public requests *****************/

const initialQuestions = (payload) => {
  return sendPublicRequest('initialQuestions', payload);
};

const getAuthNextQuestion = (payload) => {
  return sendPublicRequest('getAuthNextQuestion', payload);
};

const getProducts = (payload) => {
  return sendPublicRequest('getProducts', payload);
};

const getProductCategories = (payload) => {
  return sendPublicRequest('getProductCategories', payload);
};

const getPublicVideoVisitLink = (payload) => {
  return sendPublicRequest('getVideoVisitLink', payload);
};

const initiateSSO = (payload) => {
  return sendPublicRequest('initiateSSO', payload);
};

const initiateLogin = (payload) => {
  return sendPublicRequest('initiateLogin', payload);
};

const initiateSessionRefresh = (payload) => {
  return sendPublicRequest('initiateSessionRefresh', payload);
};

/** *************** Private requests *****************/

const getMember = () => {
  return sendPrivateRequest('getMember');
};

const getConversations = () => {
  return sendPrivateRequest('getConversations');
};

const getPharmacies = (payload) => {
  return sendPrivateRequest('getPharmacies', payload);
};

const postEvent = (payload) => {
  return sendPrivateRequest('postEvent', payload);
};

const uploadImage = (payload) => {
  return sendPrivateRequest('uploadImage', payload);
};

const getConsultTimeslots = (payload) => {
  return sendPrivateRequest('getConsultTimeslots', payload);
};

const amendIdImages = (payload) => {
  return sendPrivateRequest('amendIdImages', payload);
};

const getChatHistory = (payload) => {
  return sendPrivateRequest('getChatHistory', payload);
};

const saveMessage = (payload) => {
  return sendPrivateRequest('saveChat', payload);
};

const getProductsPayment = (payload) => {
  return sendPrivateRequest('getProductsPayment', payload);
};

const saveConversationMedata = (payload) => {
  return sendPrivateRequest('saveConversationMetadata', payload);
};

const updateConversationMedata = (payload) => {
  return sendPrivateRequest('updateConversationMetadata', payload);
};

const getConversationMedata = (payload) => {
  return sendPrivateRequest('getConversationMetadata', payload);
};

const saveSymptomBotPayload = (payload) => {
  return sendPrivateRequest('saveSymptomCheckerConversation', payload);
};

const saveVaxYesPayload = (payload) => {
  return sendPrivateRequest('saveVaxYesConversation', payload);
};

const deleteMessageById = (payload) => {
  return sendPrivateRequest('deleteMessage', payload);
};

const getSymptomBotPayload = (payload) => {
  return sendPrivateRequest('getSymptomCheckerConversation', payload);
};

const getVaxYesPayload = (payload) => {
  return sendPrivateRequest('getVaxYesConversation', payload);
};

const getVaxYesNextQuestion = (payload) => {
  return sendPrivateRequest('getVaxYesNextQuestion', payload);
};

const getConsultPrescriptionInfo = (payload) => {
  return sendPrivateRequest('getConsultationPrescription', payload);
};

const getConsultDiagnosisInfo = (payload) => {
  return sendPrivateRequest('getConsultationDiagnosis', payload);
};

const getInitialSymptomAutoCompleteSearchSC = (payload) => {
  return sendPrivateRequest('initialSymptomAutoCompleteSearch', payload);
};

const getDiagnoseSC = (payload) => {
  return sendPrivateRequest('diagnose', payload);
};

const getRiskFactorsSC = (payload) => {
  return sendPrivateRequest('suggestRiskFactors', payload);
};

const getSymptomsAutoCompleteSearch = (payload) => {
  return sendPrivateRequest('initialSymptomAutoCompleteSearch', payload);
};

const getUserImage = (payload) => {
  return sendPrivateRequest('getUserImage', payload);
};

const getVYConversationId = (payload) => {
  return sendPrivateRequest('getVaxYesConversationId', payload);
};

const getVaxYesCertificate = (payload) => {
  return sendPrivateRequest('getVaxYesCertificate', payload);
};

const getVYBoosterCertificateInfo = () => {
  return sendPrivateRequest('getVaxYesBoosterCertificateInfo');
};

const getVaccineCertificates = () => {
  return sendPrivateRequest('getVaccineCertificates');
};

const getWalletPass = (payload) => {
  return sendPrivateRequest('getWalletPass', payload);
};

const updateVaccineCertificateGroupCode = (payload) => {
  return sendPrivateRequest('updateVaccineCertificateGroupCode', payload);
};

const verifyVenue = (payload) => {
  return sendPublicRequest('verifyVenue', payload);
};

const deleteConversation = (payload) => {
  return sendPrivateRequest('deleteConversation', payload);
};

const reactivateConversation = (payload) => {
  return sendPrivateRequest('reactivateConversation', payload);
};

const getVaxYesBoosterNextQuestion = (payload) => {
  return sendPrivateRequest('getVaxYesBoosterNextQuestion', payload);
};

const getVaxYesBoosterConversationInformation = (payload) => {
  return sendPrivateRequest('getVaxYesBoosterConversationInformation', payload);
};

const validateSession = () => {
  return sendPrivateRequest('validate');
};

const getNextQuestion = (payload) => {
  return sendPrivateRequest('getNextQuestion', payload);
};

const getMemberAppointments = (payload) => {
  return sendPrivateRequest('getMemberAppointments', payload);
};

const getVideoVisitLink = (payload) => {
  return sendPrivateRequest('getVideoVisitLink', payload);
};

const getFeatureToggles = async (payload, isAuthenticated) => {
  if (isAuthenticated) {
    return sendPrivateRequest('getFeatureToggles', payload);
  }
  return sendPublicRequest('getFeatureToggles', payload);
};

const verifyMemberPayment = (payload) => {
  return sendPrivateRequest('verifyMemberPayment', payload);
};

const getConversationMetadataByFilters = (payload) => {
  return sendPrivateRequest('getConversationMetadataByFilters', payload);
};

const getFileUploadUrl = (payload) => {
  return sendPrivateRequest('getFileUploadUrl', payload);
};

const updateMemberIdPhoto = (payload) => {
  return sendPrivateRequest('updateMemberIdPhoto', payload);
};

const getConsultInformation = (payload) => {
  return sendPrivateRequest('getConsultInformation', payload);
};

const getSavedCards = (payload) => {
  return sendPrivateRequest('getSavedCards', payload);
};

const setupNewCard = (payload) => {
  return sendPrivateRequest('setupNewCard', payload);
};

const updateCardDetails = (payload) => {
  return sendPrivateRequest('updateCardDetails', payload);
};

const getSurveyStatus = (payload) => {
  return sendPrivateRequest('getSurveyStatus', payload);
};

const postSurvey = (payload) => {
  return sendPrivateRequest('postSurvey', payload);
};

const getCalenderLink = (payload) => {
  return sendPrivateRequest('getCalenderLink', payload);
};

export {
  initialQuestions,
  getMember,
  getConversations,
  getPharmacies,
  postEvent,
  uploadImage,
  getConsultTimeslots,
  getChatHistory,
  saveMessage,
  getProductsPayment,
  updateConversationMedata,
  getConversationMedata,
  saveConversationMedata,
  amendIdImages,
  getConsultPrescriptionInfo,
  getConsultDiagnosisInfo,
  getInitialSymptomAutoCompleteSearchSC,
  getDiagnoseSC,
  getRiskFactorsSC,
  getSymptomsAutoCompleteSearch,
  saveSymptomBotPayload,
  getSymptomBotPayload,
  getUserImage,
  getVaxYesPayload,
  saveVaxYesPayload,
  getVaxYesNextQuestion,
  deleteMessageById,
  getVYConversationId,
  getVYBoosterCertificateInfo,
  deleteConversation,
  getAuthNextQuestion,
  getVaxYesBoosterNextQuestion,
  getVaxYesBoosterConversationInformation,
  getVaccineCertificates,
  getProducts,
  getProductCategories,
  validateSession,
  updateVaccineCertificateGroupCode,
  verifyVenue,
  getVaxYesCertificate,
  getNextQuestion,
  getWalletPass,
  getMemberAppointments,
  reactivateConversation,
  getPublicVideoVisitLink,
  getVideoVisitLink,
  getFeatureToggles,
  verifyMemberPayment,
  getConversationMetadataByFilters,
  getFileUploadUrl,
  updateMemberIdPhoto,
  initiateSSO,
  initiateLogin,
  getConsultInformation,
  getSavedCards,
  setupNewCard,
  updateCardDetails,
  initiateSessionRefresh,
  getSurveyStatus,
  postSurvey,
  getCalenderLink,
};
