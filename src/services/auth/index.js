import { v4 as uuidv4 } from 'uuid';

import { INPUT_TYPES, SOURCES, CONVERSATION_MESSAGE_TYPES } from '../../components/Chat/constants';
import { getAuthNextQuestion, initiateLogin } from '../../connectors/bff-connector';
import { getMember, validateAuthSession } from '../../connectors/external-requests';
import { getFormattedPhoneNumber } from '../../utils';

const SESSION_STORAGE_KEY_ACCESS_TOKEN = 'accessToken';
const SESSION_STORAGE_KEY_ACCESS_TOKEN_TEMP = 'accessTokenTemp';
const SESSION_STORAGE_KEY_REFRESH_TOKEN = 'refreshToken';

const MEMBER_SESSION_ID = 'member-session-id';

const MEMBER_AGREEMENT_SOURCES = [
  SOURCES.SYMPTOM_CHECKER,
  SOURCES.VAX_YES,
  SOURCES.GGD_CONVERSATION,
];
const GGD_CONVERSATION_AGREEMENT_SKILL_SOURCES = ['standard-intake-with-sb', 'standard-intake'];

// return the agreementType based on the chat source
// for ggd conversations, we only show the agreement for general visits
const getMemberAgreementType = (agreementType, skill) => {
  if (MEMBER_AGREEMENT_SOURCES.includes(agreementType)) {
    if (agreementType === SOURCES.GGD_CONVERSATION) {
      if (GGD_CONVERSATION_AGREEMENT_SKILL_SOURCES.includes(skill)) {
        return agreementType;
      }
      return;
    }
    return agreementType;
  }
};

const initialPayload = {
  questionId: 1,
};

const authData = {
  accessToken: null,
  memberProfile: null,
  refreshToken: null,
  memberSessionId: null,
};

const setAccessToken = (accessToken) => {
  authData.accessToken = accessToken;
  window.localStorage.setItem(SESSION_STORAGE_KEY_ACCESS_TOKEN, accessToken);
};

const setMemberProfile = (memberProfile) => {
  authData.memberProfile = memberProfile;
};

const getAccessToken = () => {
  return (
    authData.accessToken || window.localStorage.getItem(SESSION_STORAGE_KEY_ACCESS_TOKEN) || null
  );
};

const setRefreshToken = (refreshToken) => {
  authData.refreshToken = refreshToken;
  window.localStorage.setItem(SESSION_STORAGE_KEY_REFRESH_TOKEN, refreshToken);
};

const getRefreshToken = () => {
  return (
    authData.refreshToken || window.localStorage.getItem(SESSION_STORAGE_KEY_REFRESH_TOKEN) || null
  );
};

const setMemberSessionId = (memberSessionId) => {
  authData.memberSessionId = memberSessionId;
  window.localStorage.setItem(MEMBER_SESSION_ID, memberSessionId);
};

const getMemberSessionId = () => {
  return (
    authData.memberSessionId || window.localStorage.getItem(MEMBER_SESSION_ID) || null
  );
};

const getMemberProfile = () => {
  return authData.memberProfile;
};

const clearAuthData = () => {
  authData.accessToken = null;
  authData.memberProfile = null;
  window.localStorage.removeItem(SESSION_STORAGE_KEY_ACCESS_TOKEN);
};

const setTempAccessToken = (tempToken) => {
  window.localStorage.setItem(SESSION_STORAGE_KEY_ACCESS_TOKEN_TEMP, tempToken);
};

const getTempAccessToken = () => {
  return window.localStorage.getItem(SESSION_STORAGE_KEY_ACCESS_TOKEN_TEMP) || null;
};

const clearTempAccessToken = () => {
  window.localStorage.removeItem(SESSION_STORAGE_KEY_ACCESS_TOKEN_TEMP);
};

const getMetadataForInitialAuth = (meta) => {
  const { agreementType, ssoData, skill, ...other } = meta;
  const sentAgreementType = getMemberAgreementType(agreementType, skill);
  return { agreementType: sentAgreementType, ...ssoData, ...other };
};

const getNextAuthQuestion = async (payload) => {
  const { question, signIn, handleEndOfQuestions, meta = {} } = payload;

  let { latestPayload } = payload;

  /**
   * Look for a latest payload, if not exist create a bare minimum one
   * and get the initial question.
   */
  if (!latestPayload) {
    // append the meta to initial payload, this way we can inject anything under meta to the initial question without explicitly defining them
    latestPayload = { ...initialPayload, ...getMetadataForInitialAuth(meta) };
  } else if (question) {
    /**
     * If latest payload exist, update it with the current answer.
     */
    if (question.updatePrevAnswer) {
      latestPayload = revertLatestPayload(latestPayload, question);
    } else {
      latestPayload = await updateLatestPayload(latestPayload, question, meta);
    }
  }

  const nextQuestion = await getFormattedNextAuthQuestion(latestPayload);

  // save the access token if it is available
  if (nextQuestion.authData?.access_token) {
    const accessToken = nextQuestion.authData.access_token;
    const refreshToken = nextQuestion.authData.refresh_token;
    setTempAccessToken(accessToken);
    setRefreshToken(refreshToken);
  }

  // this is the first question, let's define the agreement type of message type
  if (latestPayload.questionId === 1) {
    // check if the agreement type is symptom check, in that case define the message type here
    if (getMemberAgreementType(meta.agreementType, meta.skill)) {
      nextQuestion.msgTypeId = CONVERSATION_MESSAGE_TYPES.MEMBER_AGREEMENT_TYPE;
    }
  }

  /**
   * Check if we should end the chat flow.
   */
  if (nextQuestion.endOfInteraction) {
    const tempAccessToken = getTempAccessToken();
    if (tempAccessToken) {
      setAccessToken(tempAccessToken);
      setMemberSessionId(uuidv4());
      clearTempAccessToken();
    }

    const isValid = await validateSession();
    if (isValid) {
      signIn();
    }
    return handleEndOfQuestions(nextQuestion.source, { nextQuestion });
  }
  return { nextQuestion, latestPayload };
};

const validateSession = async () => {
  try {
    const accessToken = getAccessToken();
    if (accessToken) {
      // see if there is a member profile
      const memberProfile = getMemberProfile();

      // if member profile is available, then we do not need to fetch it again
      // just validate the session
      if (memberProfile) {
        await validateAuthSession();
      } else {
        const member = await getMember();
        setMemberProfile(member);
      }
      // in either case session is validate
      return true;
    }
  } catch (e) {
    console.error('User validation error -', e.message);
  }
  return false;
};

const initiateSession = async (token, signIn) => {
  let isValid = false;
  if (token) {
    // set the token in storage
    setAccessToken(token);
    setMemberSessionId(uuidv4());
    // check the validity and sign in
    isValid = await validateSession();
    if (isValid) {
      signIn();
    }
  }
  return isValid;
};

const updateLatestPayload = async (payload, question, meta) => {
  /**
   * Update the latest payload with most recently answered question`s data.
   */
  payload.answeredQuestions = payload.answeredQuestions + 1;
  payload.questionId = question.questionId;
  payload[question.key] = manageUserInput(question);

  if (!Array.isArray(payload.questions)) {
    payload.questions = [];
  }
  payload.questions.push({
    msgId: meta.answerId,
    questionId: question.questionId,
    key: question.key,
  });
  return payload;
};

const revertLatestPayload = (payload, question) => {
  /**
   * Remove Last question from the answer list and
   * remove last answered key from the payload.
   */
  const lastQuestion = payload.questions.pop();
  if (Object.keys(payload).includes(lastQuestion?.key)) {
    delete payload[lastQuestion.key];
  }
  const prevQuestion = payload.questions.pop();

  payload.questions.push(prevQuestion);
  payload.questionId = prevQuestion ? prevQuestion.questionId : 1;

  return payload;
};

const manageUserInput = (payload) => {
  switch (payload.type) {
    case INPUT_TYPES.PHONE_NUMBER:
      return getFormattedPhoneNumber(payload.answer);
    case INPUT_TYPES.OTP_INPUT: {
      if (payload.answer.event === 'SUBMIT') {
        return payload.answer.value;
      } else {
        return 'resend';
      }
    }
    default:
      return payload.answer;
  }
};

const getFormattedNextAuthQuestion = async (latestPayload) => {
  try {
    const question = await getAuthNextQuestion(latestPayload);
    question.source = SOURCES.GGD_AUTH;
    question.createdOn = Date.now();
    return question;
  } catch (err) {
    console.error('GGD_AUTH get next question ERROR - ', err.message);
    return {};
  }
};

export {
  getNextAuthQuestion,
  getMember,
  setAccessToken,
  getAccessToken,
  getMemberProfile,
  clearAuthData,
  validateSession,
  initiateLogin,
  initiateSession,
  getRefreshToken,
  setRefreshToken,
  getMemberSessionId,
  MEMBER_SESSION_ID
};
