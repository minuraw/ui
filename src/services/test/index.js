import { v4 as uuidv4 } from 'uuid';
import { INPUT_TYPES, SOURCES } from '../../components/Chat/constants';
import {
  getNextQuestion,
  getVaxYesPayload,
  saveVaxYesPayload,
} from '../../connectors/bff-connector';
import { deleteChatMessage } from '../../events';
import { getCountryByPhoneNumber, logCallback, nonEmptyArray } from '../../utils';

const getInitialPayload = async (payload) => {
  // if the chat already initiated load the conversation information
  if (payload.initiatedChat) {
    const existingPayload = await getVaxYesPayload({
      id: payload.conversationId,
    });
    if (existingPayload && existingPayload.protocol) {
      return existingPayload;
    }
  }
  const protocol = payload.skillId;

  const initialPayload = {
    questionId: 1,
    protocol,
    ...payload.meta,
  };

  return initialPayload;
};

const getNexTestQuestion = async (payload) => {
  const { question, conversationId, handleEndOfQuestions, memberProfile, meta } = payload;

  let { latestPayload } = payload;

  /**
   * Look for a latest payload, if not exist create a bare minimum one
   * and get the initial question.
   */
  if (!latestPayload) {
    latestPayload = await getInitialPayload(payload);
  } else if (question) {
    /**
     * If latest payload exist, update it with the current answer.
     */
    if (question.updatePrevAnswer) {
      latestPayload = revertLatestPayload(latestPayload, question);
      /**
       * Delete relevant messages from the chat history.
       */
      deleteChatMessage(logCallback, { msgId: meta.msgId });
    } else {
      latestPayload = await updateLatestPayload(
        latestPayload,
        question,
        conversationId,
        meta,
        memberProfile,
      );
    }
  }

  // fire and forget saving conversation to speed up the next question
  if (conversationId) {
    saveVaxYesPayload({
      id: conversationId,
      payload: latestPayload,
    });
  }

  // if latest payload doesn't have a country, add it using phone number
  if (!latestPayload.country) {
    const phone = (nonEmptyArray(memberProfile.phone) && memberProfile.phone[0]) || null;
    latestPayload.country = getCountryByPhoneNumber(phone);
  }

  const nextQuestion = await getFormattedNextQuestion(latestPayload);
  nextQuestion.source = SOURCES.GGD_TEST_CERTIFICATES;

  /**
   * Check if we should end the chat flow.
   */
  if (nextQuestion.endOfInteraction) {
    const messages = [];

    if (nonEmptyArray(nextQuestion.question)) {
      nextQuestion.question.forEach((q) => {
        messages.push({
          text: { value: q },
          isQuestion: { value: true },
          msgId: uuidv4(),
          type: INPUT_TYPES.TEXT_INPUT,
          createdOn: Date.now(),
        });
      });
    }
    return handleEndOfQuestions(nextQuestion.source, { nextQuestion, messages });
  }
  return { nextQuestion, latestPayload };
};

const updateLatestPayload = async (payload, question, conversationId, meta, memberProfile) => {
  /**
   * Update the latest payload with most recently answered question`s data.
   */
  payload.answeredQuestions = payload.answeredQuestions + 1;
  payload.id = conversationId;
  payload.questionId = question.questionId;
  payload[question.key] = await manageUserInput(question);

  /**
   * Use msgId to verify the question to be repeated.
   */
  if (!Array.isArray(payload.questions)) {
    payload.questions = [];
  }
  payload.questions.push({
    msgId: meta.answerId,
    questionId: question.questionId,
    key: question.key,
  });
  if (question.forContext) {
    payload = { ...payload, ...question.forContext };
  }
  return payload;
};

const revertLatestPayload = (payload) => {
  /**
   * Remove Last question from the answer list and
   * remove last answered key from the payload.
   */
  const lastQuestion = payload.questions.pop();
  if (Object.keys(payload).includes(lastQuestion?.key)) {
    delete payload[lastQuestion.key];
  }
  const prevQuestion = payload.questions.pop();

  payload.questions.push(prevQuestion);
  payload.questionId = prevQuestion ? prevQuestion.questionId : 1;

  return payload;
};

const manageUserInput = async (payload) => {
  switch (payload.type) {
    case INPUT_TYPES.IMAGE_INPUT: {
      return payload.answer.location;
    }
    case INPUT_TYPES.MULTIPLE_CHOICE_INPUT: {
      return payload.answer.filter((item) => item.yes);
    }
    default:
      return payload.answer;
  }
};

const getFormattedNextQuestion = async (latestPayload) => {
  const question = await getNextQuestion({
    protocol: latestPayload.protocol,
    data: latestPayload,
  });
  question.source = SOURCES.GGD_TEST_CERTIFICATES;
  question.createdOn = Date.now();

  return question;
};

export { getNexTestQuestion };
