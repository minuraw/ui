import { detectIntentRequest } from '../../connectors/external-requests';

const detectIntent = async (searchText) => {
  const defaultIntent = 'Unknown';
  const { intent, probability, ambiguityLevel } = await detectIntentRequest(searchText);

  if (!intent) {
    return defaultIntent;
  }

  if (probability > 0.9) {
    return intent;
  } else if (
    probability > 0.8 &&
    (ambiguityLevel === 'Low' || ambiguityLevel === 'Medium' || ambiguityLevel === 'None')
  ) {
    return intent;
  } else if (probability > 0.7 && (ambiguityLevel === 'Low' || ambiguityLevel === 'None')) {
    return intent;
  } else if (probability > 0.6 && ambiguityLevel === 'None') {
    return intent;
  } else {
    return defaultIntent;
  }
};

export { detectIntent };
