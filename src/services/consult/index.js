import {
  getConsultTimeslots,
  amendIdImages,
  getConsultPrescriptionInfo,
} from '../../connectors/external-requests';
import {
  getConsultDiagnosisInfo,
  getMemberAppointments as getAppointments,
} from '../../connectors/bff-connector';

const nonEmptyArray = (arr) => {
  return Array.isArray(arr) && arr.length > 0;
};

const getTimeslots = async (params) => {
  try {
    const timeslots = await getConsultTimeslots(params);
    const allTimeslots = [];

    if (nonEmptyArray(timeslots)) {
      // Iterate over timeslots and get all the timeslots considering clinicians inside
      // TODO: Move this BFF lambda
      timeslots.forEach((timeslot) => {
        const clinicians = timeslot.clinicians;
        if (nonEmptyArray(clinicians)) {
          clinicians.forEach((clinician) => {
            allTimeslots.push({
              start: timeslot.start,
              end: timeslot.end,
              clinicianId: clinician.id,
              clinicianName: clinician.name,
            });
          });
        }
      });
    }
    return allTimeslots;
  } catch (err) {
    console.error(err);
    return null;
  }
};

const amendGGDIdImages = async (params) => {
  try {
    const res = await amendIdImages(params);
    return res;
  } catch (err) {
    console.error(err);
    return null;
  }
};

const getConsultationPrescriptionInfo = async (params) => {
  return getConsultPrescriptionInfo(params);
};

const getConsultationDiagnosisInfo = async (params) => {
  return getConsultDiagnosisInfo(params);
};

const getMemberAppointments = async (params) => {
  return getAppointments(params);
};

export {
  getTimeslots,
  amendGGDIdImages,
  getConsultationPrescriptionInfo,
  getConsultationDiagnosisInfo,
  getMemberAppointments
};
