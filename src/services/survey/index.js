import { getSurveyStatus, postSurvey } from '../../connectors/bff-connector';

export { getSurveyStatus, postSurvey };
