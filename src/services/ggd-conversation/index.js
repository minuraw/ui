import { v4 as uuidv4 } from 'uuid';
import { CONVERSATION_MESSAGE_TYPES, INPUT_TYPES, SOURCES } from '../../components/Chat/constants';
import {
  getNextQuestion,
  getVaxYesPayload,
  saveVaxYesPayload,
  updateMemberIdPhoto,
  getVideoVisitLink,
  getConsultInformation,
  deleteConversation,
  reactivateConversation,
} from '../../connectors/bff-connector';
import { deleteChatMessage } from '../../events';
import {
  getCountryByPhoneNumber,
  getFormattedPhoneNumber,
  logCallback,
  nonEmptyArray,
  isServerSide
} from '../../utils';
import * as SymptomChecker from '../symptom-checker';

const EXTERNAL_SKILLS = ['standard-intake-with-sb'];

const getProtocolBySkill = (skill) => {
  return `wheel-${skill.replaceAll('_', '-')}`; // we can remove _ after removing rozie protocols
};

const getSkillSpecificMetadata = (skill) => {
  if (EXTERNAL_SKILLS.includes(skill)) {
    // When there are multiple, use a switch here.
    return {
      useExternal: true,
      externalService: skill,
      initialEvidences: [],
      otherEvidences: [],
      demographicRiskFactorPresentIds: [],
      demographicRiskFactorAbsentIds: [],
      demographicRiskFactorUnknownIds: [],
      forSelf: true,
      answeredQuestions: 0,
      answeredList: [],
    };
  } else {
    return {};
  }
};

const getInitialPayload = async (payload) => {
  if (payload.conversationId && payload.initiateChat) {
    // Rename all the APIs to a general conversation payload
    const existingPayload = await getVaxYesPayload({
      id: payload.conversationId,
    });
    if (existingPayload && existingPayload.protocol) {
      return existingPayload;
    }
  }
  const protocol = getProtocolBySkill(payload.skillId);
  const isSyncChat = payload.sync;
  const skillSpecificMetadata = getSkillSpecificMetadata(payload.skillId);

  const initialPayload = {
    questionId: 1,
    protocol,
    isSyncChat,
    ...skillSpecificMetadata,
    age: SymptomChecker.getAgeFromBirthDate(payload.memberProfile),
    ...payload.meta,
  };

  // populate profile info
  const member = payload.memberProfile;
  const address = {
    lineOne: member.addressLineOne,
    lineTwo: member.addressLineTwo,
    lineThree: member.addressLineThree,
    city: member.city,
    postalCode: member.zip,
    state: member.stateCode,
  };

  if (address.lineOne) {
    initialPayload.address = address;
  }
  initialPayload.firstName = member.firstName;
  initialPayload.lastName = member.lastName;
  initialPayload.dob = member.dob;

  if (nonEmptyArray(member.email)) {
    initialPayload.email = member.email[0];
  }
  if (nonEmptyArray(member.phone)) {
    initialPayload.phone = member.phone[0];
  }
  if (member?.pharmacy?.name) {
    initialPayload.pharmacy = member.pharmacy;
  }
  if (member.gender) {
    initialPayload.gender = {
      label: member.gender,
      value: member.gender,
    };
  }
  initialPayload.startedAuthenticated = isServerSide()
      ? false
      : JSON.parse(window.localStorage.getItem('startedAuthenticated'));
  return initialPayload;
};

const getNextGGDQuestion = async (payload) => {
  const { question, conversationId, handleEndOfQuestions, memberProfile, meta } = payload;

  let { latestPayload } = payload;

  /**
   * Look for a latest payload, if not exist create a bare minimum one
   * and get the initial question.
   */
  if (!latestPayload) {
    latestPayload = await getInitialPayload(payload);
  } else if (question) {
    /**
     * If latest payload exist, update it with the current answer.
     */
    if (question.updatePrevAnswer) {
      latestPayload = revertLatestPayload(latestPayload, question);
      /**
       * Delete relevant messages from the chat history.
       */
      deleteChatMessage(logCallback, { msgId: meta.msgId });
    } else {
      latestPayload = await updateLatestPayload(
        latestPayload,
        question,
        conversationId,
        meta,
        memberProfile,
      );
    }
  }

  // if latest payload doesn't have a country, add it using phone number
  if (!latestPayload.country) {
    const phone = (nonEmptyArray(memberProfile.phone) && memberProfile.phone[0]) || null;
    latestPayload.country = getCountryByPhoneNumber(phone);
  }

  let nextQuestion = await getFormattedNextQuestion(latestPayload);

  // BEGIN ******** toggle skip payment
  const skipPayment = payload?.skipPaymentInClinicalFlows;

  if (skipPayment && nextQuestion.type === INPUT_TYPES.PAYMENT_INPUT) {
    latestPayload = await updateLatestPayload(
      latestPayload,
      {
        ...nextQuestion,
        answer: 'Skipped for beta users',
      },
      conversationId,
      meta,
      memberProfile,
    );
    nextQuestion = await getFormattedNextQuestion(latestPayload);
  }

  // END ******** toggle skip payment

  // fire and forget saving conversation to speed up the next question
  if (conversationId) {
    saveVaxYesPayload({
      id: conversationId,
      payload: latestPayload,
    });
  }

  /**
   * Check if we should end the chat flow.
   */
  if (nextQuestion.endOfInteraction) {
    const messages = [];

    if (nonEmptyArray(nextQuestion.question)) {
      messages.push({
        text: { value: nextQuestion.question[0] },
        isQuestion: { value: true },
        msgId: uuidv4(),
        type: INPUT_TYPES.TEXT_INPUT,
        createdOn: Date.now(),
      });
    }

    const externalVideoUrl = nextQuestion?.forContext?.confirmConsultResponse?.externalVideoUrl;
    if (externalVideoUrl) {
      messages.push({
        text: { value: externalVideoUrl },
        isQuestion: { value: true },
        msgId: uuidv4(),
        type: INPUT_TYPES.EXTERNAL_VIDEO_VISIT,
        msgTypeId: CONVERSATION_MESSAGE_TYPES.EXTERNAL_LINK_MESSAGE_TYPE_ID,
        createdOn: Date.now(),
      });
    }
    nextQuestion.hasEmergencyEvidence = !nextQuestion.showLink;
    return handleEndOfQuestions(nextQuestion.source, { nextQuestion, messages });
  }
  return { nextQuestion, latestPayload };
};

const updateLatestPayload = async (payload, question, conversationId, meta, memberProfile) => {
  /**
   * Update the latest payload with most recently answered question`s data.
   */

  if (question.service === 'SYMPTOM_CHECKER_GGD') {
    const res = SymptomChecker.updateLatestPayload(payload, question, conversationId);
    return res;
  } else {
    payload.answeredQuestions = payload.answeredQuestions + 1;
    payload.id = conversationId;
    payload.questionId = question.questionId;
    payload[question.key] = await manageUserInput(question);

    // add payment info to the payload metadata
    if (question.type === INPUT_TYPES.OFF_SESSION_PAYMENT_INPUT) {
      const metadata = payload.metadata;
      payload.metadata = {
        ...metadata,
        ...question.answer,
      };
    }

    // Add the memberId as part of the payload
    if (!payload.memberId) {
      payload.memberId = memberProfile.memberId;
    }

    /**
     * Use msgId to verify the question to be repeated.
     */
    if (!Array.isArray(payload.questions)) {
      payload.questions = [];
    }
    payload.questions.push({
      msgId: meta.answerId,
      questionId: question.questionId,
      key: question.key,
    });
    if (question.forContext) {
      payload = { ...payload, ...question.forContext };
    }

    // stop rescheduling
    if (question.type === INPUT_TYPES.APPOINTMENT_TIMESLOT_INPUT) {
      if (question?.answer?.abortRescheduling) {
        payload[question.key] = undefined;
        payload.questionId = 601;
        payload.confirmReschedule = {
          label: 'No',
          value: false,
        };
      }
    }

    return payload;
  }
};

const revertLatestPayload = (payload, question) => {
  /**
   * Remove Last question from the answer list and
   * remove last answered key from the payload.
   */

  let isSymptomCheckerGGD = question.service === 'SYMPTOM_CHECKER_GGD';

  if (payload.protocol === 'wheel-standard-intake-with-sb') {
    // specific cases for wheel-standard-intake-with-sb

    if (question?.key === 'riskFactors') {
      isSymptomCheckerGGD = false;
    } else if (question?.key === 'initialEvidences') {
      isSymptomCheckerGGD = true;
      payload.answeredList.unshift({
        prevId: 1000,
      });
    } else if (
      nonEmptyArray(question?.display) &&
      question.display.find((d) => d.type === INPUT_TYPES.SYMPTOM_CHECKER_OUTPUT)
    ) {
      isSymptomCheckerGGD = true;
    } else if (question.key === 'symptoms') {
      isSymptomCheckerGGD = true;
      payload.answeredList.push({
        name: 'skipPayloadModification',
      });
    } else if (question.key === 'syncConsultAppointmentTime' && payload.isReschedule) {
      payload.questionId = 600;
      return payload;
    }
  }

  if (isSymptomCheckerGGD) {
    const res = SymptomChecker.revertLatestPayload(payload);
    return res;
  } else {
    const lastQuestion = payload.questions.pop();
    if (Object.keys(payload).includes(lastQuestion?.key)) {
      delete payload[lastQuestion.key];
    }
    const prevQuestion = payload.questions.pop();

    payload.questions.push(prevQuestion);
    payload.questionId = prevQuestion ? prevQuestion.questionId : 1;

    return payload;
  }
};

const manageUserInput = async (payload) => {
  switch (payload.type) {
    case INPUT_TYPES.PHONE_NUMBER:
      return getFormattedPhoneNumber(payload.answer);
    case INPUT_TYPES.IMAGE_INPUT: {
      if (payload.key === 'IdImage') {
        await updateMemberIdPhoto({
          contentType: payload.answer.type,
          imagePath: payload.answer.location,
        });
      }
      return payload.answer.location;
    }
    case INPUT_TYPES.ADDRESS_SEARCH_INPUT: {
      return {
        lineOne: payload.answer.street,
        lineTwo: payload.answer.line2,
        lineThree: payload.answer.line3,
        city: payload.answer.city,
        postalCode: payload.answer.zip_code,
        state: payload.answer.state,
      };
    }
    case INPUT_TYPES.MULTIPLE_CHOICE_INPUT: {
      return payload.answer.filter((item) => item.yes);
    }
    default:
      return payload.answer;
  }
};

const getFormattedNextQuestion = async (latestPayload) => {
  try {
    const question = await getNextQuestion({
      protocol: latestPayload.protocol,
      data: latestPayload,
    });
    question.source = SOURCES.GGD_CONVERSATION;
    question.createdOn = Date.now();

    if (question.type === INPUT_TYPES.APPOINTMENT_TIMESLOT_INPUT) {
      question.answers = {
        consultId:
          question?.forContext?.createConsultResponse?.id ||
          latestPayload?.createConsultResponse?.id,
        isReschedule: latestPayload?.isReschedule,
      };
    }

    return question;
  } catch (err) {
    console.error('GGD_CONVERSATION get next question ERROR - ', err.message);
    return {};
  }
};

const getShowRescheduleCancel = ({ status }) => {
  return !!status && status !== 'Cancelled';
};

const getNextGGDConsultQuestionAfterEndOfConversation = async (payload) => {
  try {
    const { conversationId, mode, endOfConversation, isVideoVisitAvailable, hasEmergency } =
      payload;
    if (mode === 'sync') {
      // before the conversation is over, only indication we have about the sync visit is the mode
      // once the conversation is over we can use the isVideoVisitAvailable flag to determine the end of ggd consult
      const shouldLoadVideoVisitLink = endOfConversation ? isVideoVisitAvailable : true;

      // mimic an error scenario so that reschedule and cancel button will be shown
      let videoLinkResult = { error: true };

      if (shouldLoadVideoVisitLink) {
        videoLinkResult = await getVideoVisitLink({ conversationId });
      }

      const consultInfo = await getConsultInformation({ conversationId });
      return {
        nextQuestion: {
          // question: '', // Keep this empty
          // Uncomment when UI components are ready
          question: [
            "We're working on Connecting you to a provider.",
            'Here some answers to questions you may have',
          ],
          msgTypeId: CONVERSATION_MESSAGE_TYPES.END_OF_CONSULT_WHATS_HAPPENING_NOW_TYPE_ID,
          answers: {
            ...videoLinkResult,
            showRescheduleCancel: getShowRescheduleCancel(consultInfo),
          },
          type: INPUT_TYPES.END_OF_GGD_CONSULT_INPUT,
          key: 'ggd_consult_end_video',
          source: 'ggd',
          data: { ...consultInfo, mode, hasEmergency },
        },
      };
    }
    if (mode === 'async') {
      const consultInfo = await getConsultInformation({ conversationId });
      return {
        nextQuestion: {
          question: [
            "We're working on Connecting you to a provider.",
            'Here some answers to questions you may have',
          ],
          msgTypeId: CONVERSATION_MESSAGE_TYPES.END_OF_CONSULT_WHATS_HAPPENING_NOW_TYPE_ID,
          key: 'ggd_consult_end_whats_happening_now',
          source: 'ggd',
          data: { ...consultInfo, mode },
        },
      };
    }
  } catch (err) {
    console.error('Error occurred', err);
  }
};

// This removes the questions from the given payload from given question id to end.
const getTrimmedPayload = (latestPayload, questionId, nextQuestionId = null) => {
  delete latestPayload.isCancel;
  delete latestPayload.isReschedule;

  const questions = latestPayload.questions || [];
  let i = questions.length - 1;
  while (i >= 0) {
    const question = questions[i];
    if (question.questionId === questionId) {
      break;
    }

    questions.splice(i, 1);
    delete latestPayload[question.key];
    i--;
  }
  if (nextQuestionId) {
    latestPayload.questionId = nextQuestionId;
  } else {
    latestPayload.questionId = questionId;
  }

  return latestPayload;
};

const reactivateGGDConversation = async (payload) => {
  if (payload.deleteChatHistory) {
    await deleteConversation(payload); // delete from db
  }
  await reactivateConversation(payload);

  let questionId = null;
  let nextQuestionId = null;
  let extraFlags = {};

  if (payload.triggerEvent === 'CANCEL') {
    questionId = 503;
    nextQuestionId = 701;
    extraFlags = {
      isCancel: true,
    };
  } else if (payload.triggerEvent === 'RESCHEDULE') {
    questionId = 503;
    nextQuestionId = 600;
    extraFlags = {
      isReschedule: true,
    };
  }

  if (questionId) {
    const trimmedPayload = getTrimmedPayload(payload.latestPayload, questionId, nextQuestionId);
    return saveVaxYesPayload({
      id: payload.conversationId,
      payload: { ...trimmedPayload, ...extraFlags },
    });
  }
};

export {
  getNextGGDQuestion,
  getNextGGDConsultQuestionAfterEndOfConversation,
  reactivateGGDConversation,
};
