import { v4 as uuidv4 } from 'uuid';

import { CONVERSATION_MESSAGE_TYPES, INPUT_TYPES, SOURCES } from '../../components/Chat/constants';

const CONSULT_RESPONSE_MAPPING = {
  KEY: 'key',
  DATA: 'data',
  CREATE_CONSULT_RESPONSE: 'CreateConsultResponse',
  CREATE_CONSULT_STATUS: 'Status',
  EXTERNAL_URL: 'ExternalVideoUrl',
  CONSULT_FINALIZED: 'CONSULT_FINALIZED',
  PLANNED: 'planned',
};

const getAnswerFromRozieResponse = (text, endOfInteraction) => {
  const questions = [];
  let answers = null;
  let type;
  let endOfConversation = endOfInteraction;
  try {
    // try to parse the text as a json
    const jsonObject = JSON.parse(text);

    const key = jsonObject[CONSULT_RESPONSE_MAPPING.KEY];

    // If the key is consult finalization
    if (key === CONSULT_RESPONSE_MAPPING.CONSULT_FINALIZED) {
      const consultData =
        jsonObject[CONSULT_RESPONSE_MAPPING.DATA][CONSULT_RESPONSE_MAPPING.CREATE_CONSULT_RESPONSE];
      const { Status: status, ExternalVideoUrl: externalVideoUrl } = consultData;
      if (status === CONSULT_RESPONSE_MAPPING.PLANNED) {
        endOfConversation = true;
        questions.push('Consult creation is successful');
        // If an external url is available
        if (externalVideoUrl) {
          type = INPUT_TYPES.EXTERNAL_VIDEO_VISIT;
          answers = {
            link: externalVideoUrl,
            label: 'Start Visit',
            page: '/visit',
          };
        }
      }
    }
  } catch (err) {
    // failing to parse indicates text is a string
    questions.push(text);
  }
  return { questions, answers, type, endOfConversation };
};

// This method will handle all the logic related to end of Rozie flow
const handleRozieEndOfConversation = (questions, answers, handleEndOfQuestions) => {
  if (!handleEndOfQuestions) return;
  const messages = [];

  // push consult created successully message
  messages.push({
    text: { value: questions },
    isQuestion: { value: true },
    msgId: uuidv4(),
    type: INPUT_TYPES.TEXT_INPUT,
    createdOn: Date.now(),
  });
  // If answers is defined it contains link information
  if (answers) {
    messages.push({
      text: { value: answers.link },
      isQuestion: { value: true },
      msgId: uuidv4(),
      type: INPUT_TYPES.EXTERNAL_VIDEO_VISIT,
      msgTypeId: CONVERSATION_MESSAGE_TYPES.EXTERNAL_LINK_MESSAGE_TYPE_ID,
    });
  }
  // call the handle enf of question function
  handleEndOfQuestions(SOURCES.ROZIE, { [SOURCES.ROZIE]: { messages } });
};

export { getAnswerFromRozieResponse, handleRozieEndOfConversation };
