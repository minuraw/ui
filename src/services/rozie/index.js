import { INPUT_TYPES, SOURCES } from '../../components/Chat/constants';
import { sendMessageToRozie, uploadImage } from '../../connectors/external-requests';
import { getAnswerFromRozieResponse, handleRozieEndOfConversation } from './util';

const SYNC_CONSULT_TIMESLOT = 'SyncConsultAppointmentTime';

const CONSENTS = [
  { value: 'terms-of-use', label: 'Terms of Use' },
  { value: 'privacy-policy', label: 'Privacy Policy' },
  { value: 'notice-of-privacy-practices', label: 'Notice of Privacy Practices' },
  { value: 'telehealth-informed-consent', label: 'Telehealth Informed Consent' },
];

/**
 * Generate Rozie concept array
 * @param {Array} conceptMap [{key, value}]
 */
const generateConceptObject = (conceptMap) => {
  const concepts = [];
  conceptMap.forEach((concept) => {
    const { key, value } = concept;
    // If value is defined
    if (value || key === 'Patient.Address.Line2' || key === 'Patient.Address.Line3') {
      concepts.push({
        is_valid: true,
        is_checked: true,
        concept_value: {
          entity_type: key,
          entity_value: value,
        },
      });
    }
  });
  return concepts;
};

const generateRozieConceptForUserProfile = (skill_id, sync, userId, memberProfile) => {
  const {
    firstName,
    lastName,
    dob,
    gender,
    phone,
    email,
    addressLineOne,
    addressLineTwo,
    addressLineThree,
    city,
    zip,
    stateCode,
  } = memberProfile;

  let selectedEmail = null;
  if (email && Array.isArray(email) && email.length > 0) {
    selectedEmail = email[0];
  }
  let selectedPhoneNumber = null;
  if (phone && Array.isArray(phone) && phone.length > 0) {
    selectedPhoneNumber = phone[0];
  }

  const conceptMap = [
    { key: 'Patient.FirstName', value: firstName },
    { key: 'Patient.LastName', value: lastName },
    { key: 'Patient.BirthDate', value: dob },
    { key: 'Patient.BiologicalGender', value: gender },
    { key: 'Patient.Email', value: selectedEmail },
    { key: 'Patient.PhoneNumber', value: selectedPhoneNumber },
    { key: 'Patient.Address.Line1', value: addressLineOne },
    { key: 'Patient.Address.Line2', value: addressLineTwo || '' },
    { key: 'Patient.Address.Line3', value: addressLineThree || '' },
    { key: 'Patient.Address.City', value: city },
    { key: 'Patient.Address.PostalCode', value: zip },
    { key: 'Patient.Address.State', value: stateCode },
    { key: 'IsSyncChat', value: sync ? 'True' : 'False' },
  ];

  const rozieConcept = {
    user_id: userId,
    event_template: {
      event_type: 'skillconcept',
      skill: {
        skill_id,
      },
      concepts: JSON.stringify(generateConceptObject(conceptMap)),
    },
  };

  return rozieConcept;
};

const getInputTypeFromRozieType = (type) => {
  switch (type) {
    case 'TextInput':
      return INPUT_TYPES.TEXT_INPUT;
    case 'DatePicker':
      return INPUT_TYPES.DATE_INPUT;
    case 'ImagePicker':
      return INPUT_TYPES.IMAGE_INPUT;
    case 'quick_reply':
    case 'ButtonsOnly':
      return INPUT_TYPES.SINGLE_CHOICE_INPUT;
    default:
      return INPUT_TYPES.TEXT_INPUT;
  }
};

const CREATE_CONSULT_KEYS = {
  RESPONSE: 'CreateConsultResponse',
  DISQUALIFIED_RESPONSE: 'DisqualifyingResponses',
  ITEMS: 'Items',
  DATA: 'data',
  ID: 'Id',
  STATUS: 'Status',
  KEY: 'key',
  PATIENT_INSTRUCTION: 'patientInstructions',
};

const getConsultTimeslots = async (rozieQuestion) => {
  let jsonResponse = null;
  let attempt = 1;
  try {
    jsonResponse = JSON.parse(rozieQuestion.text);
    const strAttempt = rozieQuestion.metadata.ATTEMPT;
    if (!isNaN(strAttempt)) {
      attempt = parseInt(strAttempt, 10);
    }
  } catch (err) {
    console.error(err);
  }
  // Rozie text is not a json
  if (jsonResponse === null) {
    return {
      question: 'Oops something went wrong. Please try again later',
    };
  }
  // We have a JSON response so decode it
  const consultInfo = jsonResponse[CREATE_CONSULT_KEYS.DATA][CREATE_CONSULT_KEYS.RESPONSE];

  const consultStatus = consultInfo[CREATE_CONSULT_KEYS.STATUS];
  const id = consultInfo[CREATE_CONSULT_KEYS.ID];
  const disqualifyingItems =
    consultInfo[CREATE_CONSULT_KEYS.DISQUALIFIED_RESPONSE][CREATE_CONSULT_KEYS.ITEMS];

  if (consultStatus === 'finished') {
    return {
      question: ['Consultation creation complete.'],
    };
  }
  const questions = [];
  if (Array.isArray(disqualifyingItems) && disqualifyingItems.length > 0) {
    disqualifyingItems.forEach((item) => {
      questions.push(item[CREATE_CONSULT_KEYS.PATIENT_INSTRUCTION]);
    });
  }

  let question = 'Please select an appointment time.';
  if (attempt > 1) {
    question =
      'Sorry, the appointment time you selected is no longer available. Please select a new appointment time.';
  }
  questions.push(question);
  const answers = { consultId: id };

  return {
    question: questions,
    answers,
  };
};

const METADATA_KEY_MAPPING = {
  max_date: 'maxDate',
  min_date: 'minDate',
  max_num_of_images: 'maxNumberOfImage',
  val_regex: 'regex',
  val_regex_err_message: 'regexErrorMessage',
  data_type: 'dataType',
  min_value: 'min',
  max_value: 'max',
  initial_value: 'initialValue',
  max_length: 'maxLength',
  min_length: 'minLength',
};

const deriveRozieMetadata = (metadata) => {
  if (!metadata) return metadata;
  const derivedMetadata = {};
  // Iterate over metadata and map the metadata
  Object.keys(metadata).forEach((key) => {
    const mappedKey = METADATA_KEY_MAPPING[key];
    // There is a key mapping
    if (mappedKey) {
      // assign value to mapped key
      derivedMetadata[mappedKey] = metadata[key];
    } else {
      // If not assignment, keep them as is
      derivedMetadata[key] = metadata[key];
    }
  });
  return derivedMetadata;
};

const deriveQuestionFromResponse = async (res, memberProfile = null, data = null) => {
  const defaultResponses = res?.data?.post?.response_map?.responses?.default;

  if (defaultResponses && Array.isArray(defaultResponses) && defaultResponses.length > 0) {
    const rozieQuestion = defaultResponses.find(
      (r) => r.statement_type === 'question',
    )?.response_template;
    const rozieAnswer = defaultResponses.find(
      (r) => r.statement_type === 'answer',
    )?.response_template;
    const rozieQuickReply = defaultResponses.find(
      (r) => r.statement_type === 'quick_reply',
    )?.response_template;
    const rozieComment = defaultResponses.find(
      (r) => r.statement_type === 'comment',
    )?.response_template;

    if (!rozieQuestion && !rozieAnswer && !rozieComment) {
      throw new Error('No question, answer or comment received');
    }

    const question = {
      source: SOURCES.ROZIE,
      question: [],
      type: INPUT_TYPES.TEXT_INPUT,
    };
    question.endOfInteraction = res?.data?.post?.should_end_interaction || false;

    if (rozieComment) {
      question.question.push(rozieComment.text || '');
      question.key = 'rozie_comment_' + new Date().getTime();
    }

    if (rozieAnswer) {
      const {
        questions: answerAsQuestions,
        answers,
        type,
        endOfConversation,
      } = getAnswerFromRozieResponse(rozieAnswer.text || '', question.endOfInteraction);
      question.question.push(...answerAsQuestions);
      // When type is defined
      if (type && answers) {
        question.type = type;
        question.answers = answers;
      }
      question.key = 'rozie_answer_' + new Date().getTime();

      // we have reached end of conversation
      if (endOfConversation && data) {
        question.disabled = true;
        handleRozieEndOfConversation(answerAsQuestions, answers, data.handleEndOfQuestions);
        return {};
      }
    }

    if (rozieQuestion) {
      const conceptId = rozieQuestion?.metadata?.context?.concept_id;
      if (rozieQuestion.text === 'GET_PHARMACY_INFO') {
        let defaultPharmacy = false;
        if (memberProfile && memberProfile.pharmacyid) {
          defaultPharmacy = memberProfile.pharmacy;
          question.question.push(`${defaultPharmacy.name}, ${defaultPharmacy.address.line[0]},
           ${defaultPharmacy.address.state}, ${defaultPharmacy.address.city}, ${defaultPharmacy.address.postalCode}`);
          question.question.push('Do you want to continue with your default pharmacy?');
          question.type = INPUT_TYPES.SINGLE_CHOICE_INPUT;
          question.answers = [
            { value: 1, label: 'Yes' },
            { value: 2, label: 'No' },
          ];
          question.isConfirmationQuestion = true;
          question.providerResponse = res;
          question.defaultSelection = defaultPharmacy;
        } else {
          question.question.push('Please select your pharmacy');
          question.type = INPUT_TYPES.PHARMACY_INPUT;
          question.answers = true;
        }
      } else if (rozieQuestion.text === 'GET_CONSENTS') {
        question.question.push('You need to accept the following consents');
        question.type = INPUT_TYPES.CONSENTS_INPUT;
        question.answers = CONSENTS;
      } else if (rozieQuestion.text === 'GET_DELIVERY_CHANNEL') {
        // TODO remove this when this is an actual single choice question
        question.question.push('When do you want to schedule an appointment?');
        question.type = INPUT_TYPES.SINGLE_CHOICE_INPUT;
        question.isDeliveryChannelQuestion = true;
        question.answers = [
          { value: 'True', label: 'Now' },
          { value: 'False', label: 'Later' },
        ];
      } else if (rozieQuestion.text === 'GET_CONSULT_PAYMENT') {
        question.question.push('We are almost done');
        question.type = INPUT_TYPES.PAYMENT_INPUT;
        question.answers = true; // notify there is an answer type
      } else if ((conceptId || '').includes(SYNC_CONSULT_TIMESLOT)) {
        // When a sync consult is on the way, we need to fetch the schedules from the backend
        // This is where params are generated
        const { question: nextQuestion, answers } = await getConsultTimeslots(rozieQuestion);
        question.question.push(...nextQuestion);
        question.type = INPUT_TYPES.APPOINTMENT_TIMESLOT_INPUT;
        question.answers = answers;
        question.finalizeConsult = true;
      } else if (conceptId === 'Patient.Address.Line1') {
        question.question.push('Please enter your address');
        question.type = INPUT_TYPES.ADDRESS_SEARCH_INPUT;
        question.answers = {}; // if we want we can existing address here and stop sending the address when initializing
      } else {
        question.question.push(rozieQuestion.text || '');
        question.type = getInputTypeFromRozieType(rozieQuestion?.metadata?.next_input?.ui_type);
      }

      question.key = conceptId;
      question.metadata = rozieQuestion?.metadata;
      question.key = rozieQuestion?.metadata?.context?.concept_id;
      question.metadata = deriveRozieMetadata(rozieQuestion?.metadata);
    }

    if (rozieQuickReply) {
      question.type = getInputTypeFromRozieType(rozieQuickReply?.response_type);
      question.answers = rozieQuickReply.items.map((item) => {
        // we haven't consider the priority for now
        return {
          value: item.id,
          label: item.label,
        };
      });
    }

    if (
      question.type === INPUT_TYPES.SINGLE_CHOICE_INPUT &&
      rozieQuestion?.metadata?.next_input?.is_multiple_choice
    ) {
      question.type = INPUT_TYPES.MULTIPLE_CHOICE_INPUT;
    }

    if (question.type === INPUT_TYPES.IMAGE_INPUT) {
      question.answers = true;
    }

    if (!question.key) {
      question.key = 'default_' + new Date().getTime();
    }
    return question;
  }
  throw new Error('Response format error in deriveQuestionFromResponse');
};

const initializeProtocol = (skillId, sync, userId, memberProfile) => {
  const skillConceptPostEvent = generateRozieConceptForUserProfile(
    skillId,
    sync,
    userId,
    memberProfile,
  );
  return sendMessageToRozie(skillConceptPostEvent);
};

const sendTextReply = (userId, text) => {
  const postEvent = {
    user_id: userId,
    event_template: {
      event_type: 'text',
      text,
    },
  };
  return sendMessageToRozie(postEvent);
};

const sendImageReply = async (userId, imageData) => {
  try {
    // need to upload this with a proper key. Currently if user uploads images with same name, it will be replaced
    const { url } = await uploadImage(imageData);
    const postEvent = {
      user_id: userId,
      event_template: {
        event_type: 'text',
        text: url,
      },
    };
    return sendMessageToRozie(postEvent);
  } catch (error) {
    throw new Error('Image uploading failed!');
  }
};

const sendContextKeyReply = (userId, contextKeys) => {
  const postEvent = {
    user_id: userId,
    eventTemplates: contextKeys.map((item) => {
      return {
        event_type: 'contextkey',
        context_label: item.label,
        context_key: item.value,
      };
    }),
  };
  return sendMessageToRozie(postEvent);
};

const sendConceptReply = (userId, concept, conceptKey, skillId) => {
  const postEvent = {
    user_id: userId,
    event_template: {
      event_type: 'skillconcept',
      skill: {
        skill_id: skillId,
      },
      concepts: JSON.stringify([
        {
          is_valid: true,
          is_checked: true,
          concept_value: {
            entity_type: conceptKey,
            entity_object: {
              [conceptKey]: concept,
            },
          },
        },
      ]),
    },
  };
  return sendMessageToRozie(postEvent);
};

const sendAddressReply = (userId, address, skillId) => {
  const conceptMap = [
    { key: 'Patient.Address.Line1', value: address.street },
    { key: 'Patient.Address.Line2', value: address.line2 },
    { key: 'Patient.Address.Line3', value: address.line3 },
    { key: 'Patient.Address.City', value: address.city },
    { key: 'Patient.Address.PostalCode', value: address.zip_code },
    { key: 'Patient.Address.State', value: address.state },
  ];
  const postEvent = {
    user_id: userId,
    event_template: {
      event_type: 'skillconcept',
      skill: {
        skill_id: skillId,
      },
      concepts: JSON.stringify(generateConceptObject(conceptMap)),
    },
  };
  return sendMessageToRozie(postEvent);
};

const handleConsentsRejection = async (data) => {
  const question = ['We are unable to provide the service unless consent is accepted.'];
  // this is for delaying the message - otherwise order will not be guaranteed in history
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      handleRozieEndOfConversation(question, null, data.handleEndOfQuestions);
      resolve();
    }, 1000);
  });
  return {};
};

const getNextQuestion = async (data) => {
  let res = null;
  if (data.initiateChat) {
    // chat initiation
    res = await initializeProtocol(data.skillId, data.sync, data.userId, data.memberProfile);
  } else {
    if (data.question?.isConfirmationQuestion) {
      if (data?.answer?.value === 1) {
        res = await sendConceptReply(
          data.userId,
          data.question.defaultSelection,
          'Pharmacy',
          data.question?.metadata?.context?.skill_id,
        );
        return deriveQuestionFromResponse(res, data.memberProfile, data);
      } else {
        return deriveQuestionFromResponse(data?.question?.providerResponse, null, data);
      }
    }
    if (data.question?.isDeliveryChannelQuestion) {
      res = await sendTextReply(data.userId, data.answer?.value);
      return deriveQuestionFromResponse(res, data.memberProfile, data);
    }
    // finalize the consult flow
    if (data.question?.finalizeConsult) {
      const { memberId, userId } = data;
      // send memberId, conversationId, consultId and start time to Rozie
      const concept = { ...data.answer.value, memberId, conversationId: userId };
      const conceptId = data.question?.metadata?.context?.concept_id;
      const skillId = data.question?.metadata?.context?.skill_id;
      res = await sendConceptReply(userId, concept, conceptId, skillId);
      return deriveQuestionFromResponse(res, data.memberProfile, data);
    }
    // chat flow
    const inputType = data.question?.type;
    switch (inputType) {
      case INPUT_TYPES.TEXT_INPUT:
        res = await sendTextReply(data.userId, data.answer);
        break;
      case INPUT_TYPES.IMAGE_INPUT:
        res = await sendImageReply(data.userId, data.answer);
        break;
      case INPUT_TYPES.SINGLE_CHOICE_INPUT:
        res = await sendContextKeyReply(data.userId, [data.answer]);
        break;
      case INPUT_TYPES.MULTIPLE_CHOICE_INPUT:
        res = await sendContextKeyReply(data.userId, data.answer);
        break;
      case INPUT_TYPES.CONSENTS_INPUT:
        if (data.answer.length === 0) {
          return await handleConsentsRejection(data);
        }
        const consentTime = new Date().toISOString();
        const concept = {
          Items: data.answer.map((item) => {
            return {
              ConsentType: item.value,
              Version: 'latest',
              TimeOfConsent: consentTime,
            };
          }),
        };
        res = await sendConceptReply(
          data.userId,
          concept,
          'Consents',
          data.question.metadata.context.skill_id,
        );
        break;
      case INPUT_TYPES.PHARMACY_INPUT:
        res = await sendConceptReply(
          data.userId,
          data.answer,
          'Pharmacy',
          data.question.metadata.context.skill_id,
        );
        break;
      case INPUT_TYPES.ADDRESS_SEARCH_INPUT:
        res = await sendAddressReply(
          data.userId,
          data.answer,
          data.question.metadata.context.skill_id,
        );
        break;
      case INPUT_TYPES.PAYMENT_INPUT:
        res = await sendTextReply(data.userId, 'True');
        break;
      default:
        res = await sendTextReply(data.userId, data.answer);
    }
  }

  return deriveQuestionFromResponse(res, data.memberProfile, data);
};

export { getNextQuestion };
