import {
  getChatHistory,
  saveMessage,
  getMemberConversations,
  saveConversationMetadataInfo,
  updateConversationMetadataInfo,
  getConversationMetadataInfo,
  saveSymptomBotPayloadBff,
  getSymptomBotPayloadBff,
  getVaxYesPayloadBff,
  saveVaxYesPayloadBff,
  deleteMessageByIdBff,
  getVYConversationIdBff,
  getVaxYesCertificateBff,
  getPublicVideoVisitLink, 
} from '../../connectors/external-requests/chat';

const loadChatHistory = async ({ userId, page = 0, count = 100, conversationId }) => {
  if (userId) {
    try {
      const chatHistory = await getChatHistory(userId, count, page, conversationId);
      return chatHistory.response;
    } catch (e) {
      console.error('Could not load chat history - ', e.message);
      return [];
    }
  } else {
    return [];
  }
};

const saveChat = async (messages) => {
  try {
    await saveMessage(messages);
    return true;
  } catch (e) {
    console.error('Error saving message - ', e.message);
    return false;
  }
};

const getConversations = async () => {
  try {
    return getMemberConversations();
  } catch (e) {
    console.error('Error getting conversations - ', e.message);
    return false;
  }
};

const saveConversationMetadata = async (param) => {
  try {
    return saveConversationMetadataInfo(param);
  } catch (err) {
    console.error('Error saving the conversation metadata', err);
  }
};

const updateConversationMetadata = async (param) => {
  try {
    return updateConversationMetadataInfo(param);
  } catch (err) {
    console.error('Error saving the conversation metadata', err);
  }
};

const getConversationMetadata = async (param) => {
  try {
    return getConversationMetadataInfo(param);
  } catch (err) {
    console.error('Error saving the conversation metadata', err);
  }
};

const saveSymptomBotPayload = async (payload) => {
  try {
    return saveSymptomBotPayloadBff(payload);
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

const getSymptomBotPayload = async (payload) => {
  try {
    return getSymptomBotPayloadBff(payload);
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

const getVaxYesPayload = async (payload) => {
  try {
    return getVaxYesPayloadBff(payload);
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

const saveVaxYesPayload = async (payload) => {
  try {
    return saveVaxYesPayloadBff(payload);
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

const deleteMessageById = async (payload) => {
  try {
    return deleteMessageByIdBff(payload);
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

const getVYConversationId = async (payload) => {
  try {
    return getVYConversationIdBff(payload);
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

const getVaxYesCertificate = async () => {
  try {
    return getVaxYesCertificateBff();
  } catch (err) {
    console.error('Error SB payload', err);
    return null;
  }
};

module.exports = {
  loadChatHistory,
  saveChat,
  deleteMessageById,
  getConversations,
  saveConversationMetadata,
  updateConversationMetadata,
  getConversationMetadata,
  saveSymptomBotPayload,
  getSymptomBotPayload,
  getVaxYesPayload,
  saveVaxYesPayload,
  getVYConversationId,
  getVaxYesCertificate,
  getPublicVideoVisitLink
};
