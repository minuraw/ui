import { isServerSide } from "../../utils";

const SESSION_STORAGE_KEY_LOCALE = 'locale';

const localStorageData = {
  locale: null,
};

const setLocale = (locale) => {
  if (!isServerSide()) {
    window.localStorage.setItem(SESSION_STORAGE_KEY_LOCALE, locale);
    localStorageData.locale = locale;
  }
};

const getLocale = () => {
  if (localStorageData.locale) {
    return localStorageData.locale;
  }
  if (!isServerSide()) {
    localStorageData.locale =
      window.localStorage.getItem(SESSION_STORAGE_KEY_LOCALE) || 'en-us';
  }
  return localStorageData.locale || 'en-us';
};

export { setLocale, getLocale };
