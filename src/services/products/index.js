import {
  getProductGroup,
  getProductsPayment,
  getProducts,
  getProductCategories,
  verifyMemberPayment,
} from '../../connectors/external-requests';

import { getSavedCards, setupNewCard, updateCardDetails } from '../../connectors/bff-connector';

const GROUP_LIMITS = [
  {
    id: 1,
    limit: 3,
  },
  {
    id: 2,
    limit: 12,
  },
  {
    id: 3,
    limit: 4,
  },
  {
    id: 4,
    limit: 4,
  },
  {
    id: 5,
    limit: 20,
  },
];

const getProductByCategory = async (categoryId) => {
  return getAllGroups({ categoryId });
};

const getAllGroups = async (configs) => {
  const promises = [];
  GROUP_LIMITS.forEach((groupLimit) => {
    promises.push(resolveGetProductGroup({ ...groupLimit, ...configs }));
  });
  return Promise.all(promises);
};

const resolveGetProductGroup = async (groupLimit) => {
  try {
    const result = await getProductGroup(groupLimit);
    return result[0];
  } catch (err) {
    console.error(err);
    return null;
  }
};

const getProductPaymentInfo = async (productsPayment) => {
  try {
    return getProductsPayment({ productsPayment });
  } catch (err) {
    console.error(err);
    return {};
  }
};

export {
  getAllGroups,
  getProductByCategory,
  getProductsPayment,
  getProductPaymentInfo,
  getProducts,
  getProductCategories,
  verifyMemberPayment,
  getSavedCards,
  setupNewCard,
  updateCardDetails,
};
