import {
  getSymptomAutoComplete,
  getSymptomBotNextQuestion,
} from '../../connectors/external-requests';
import { deleteChatMessage, saveSymptomBotPayload } from '../../events';
import { getCountryByPhoneNumber, isServerSide, logCallback, nonEmptyArray } from '../../utils';
const { INPUT_TYPES } = require('../../components/Chat/constants');

const getSuggestedSymptoms = async (payload) => {
  try {
    const locale = isServerSide() ? 'en-us' : window.localStorage.getItem('locale') || 'en-us';
    const results = await getSymptomAutoComplete({ language: locale, ...payload });
    // Map values as required by the components
    const mappedResults = results.map((symptom) => {
      return {
        value: symptom,
        label: symptom.label,
        id: symptom.id,
      };
    });
    return mappedResults;
  } catch (err) {
    return [];
  }
};

const getNextQuestionSymptomChecker = async (payload) => {
  let { question, handleEndOfQuestions, latestPayload, memberProfile, conversationId, meta } =
    payload;
  const locale = isServerSide() ? 'en-us' : window.localStorage.getItem('locale') || 'en-us';
  if (!latestPayload) {
    const startedAuthenticated = isServerSide()
      ? false
      : JSON.parse(window.localStorage.getItem('startedAuthenticated'));

    latestPayload = {
      questionId: 1,
      language: locale,
      startedAuthenticated,
      firstName: memberProfile.firstName,
      gender: null,
      age: getAgeFromBirthDate(memberProfile),
      initialEvidences: [],
      otherEvidences: [],
      demographicRiskFactorPresentIds: [],
      demographicRiskFactorAbsentIds: [],
      demographicRiskFactorUnknownIds: [],
      forSelf: true,
      answeredQuestions: 0,
      interviewId: conversationId,
      answeredList: [],
    };
  }
  if (question) {
    if (question.updatePrevAnswer) {
      latestPayload = revertLatestPayload(latestPayload);
      /**
       * Delete relevant messages from the chat history.
       */
      deleteChatMessage(logCallback, { msgId: meta.msgId });
    } else {
      latestPayload = updateLatestPayload(latestPayload, question, conversationId);
    }
  }

  saveSymptomBotPayload(logCallback, {
    id: conversationId,
    payload: latestPayload,
  });

  latestPayload.age = getAgeFromBirthDate(memberProfile);

  // if latest payload doesn't have a country, add it using phone number
  if (!latestPayload.country) {
    const phone = (nonEmptyArray(memberProfile.phone) && memberProfile.phone[0]) || null;
    latestPayload.country = getCountryByPhoneNumber(phone);
  }

  const nextQuestion = await getSymptomBotNextQuestion(latestPayload);
  if (nextQuestion.ggdStopSCFlow) {
    return handleEndOfQuestions(nextQuestion.source, { nextQuestion });
  } else {
    return { nextQuestion, latestPayload };
  }
};

const revertLatestPayload = (latestPayload) => {
  const lastAnswered = latestPayload.answeredList.pop();
  const lastAnsweredEx = latestPayload.answeredList.pop();
  latestPayload.answeredList.push(lastAnsweredEx);
  if (lastAnswered) {
    const name = lastAnswered.name;
    const prevId = lastAnsweredEx.prevId;
    const ids = lastAnswered.ids;
    latestPayload.questionId = prevId;

    switch (name) {
      case 'forSelf': {
        delete latestPayload.forSelf;
        break;
      }
      case 'gender': {
        delete latestPayload.gender;
        break;
      }
      case 'riskFactors': {
        latestPayload.demographicRiskFactorAbsentIds = removeArrayFromArray(
          latestPayload.demographicRiskFactorAbsentIds,
          ids,
        );
        latestPayload.demographicRiskFactorPresentIds = removeArrayFromArray(
          latestPayload.demographicRiskFactorPresentIds,
          ids,
        );
        latestPayload.demographicRiskFactorUnknownIds = removeArrayFromArray(
          latestPayload.demographicRiskFactorUnknownIds,
          ids,
        );
        break;
      }
      case 'initialEvidences': {
        latestPayload.initialEvidences = removeArrayFromArrayById(
          latestPayload.initialEvidences,
          ids,
        );
        break;
      }
      case 'diagnose': {
        latestPayload.otherEvidences = removeArrayFromArrayById(latestPayload.otherEvidences, ids);
        break;
      }
    }
  }
  return latestPayload;
};

const removeArrayFromArray = (arr1, arr2) => {
  const arr = [];
  arr1.forEach((i) => {
    if (!arr2.includes(i)) {
      arr.push(i);
    }
  });
  return arr;
};

const removeArrayFromArrayById = (arr1, arr2) => {
  const arr = [];
  arr1.forEach((i) => {
    if (!arr2.includes(i.id)) {
      arr.push(i);
    }
  });
  return arr;
};

const getIndex = (arr, key) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].id === key) {
      return i;
    }
  }
  return -1;
};

const updateLatestPayload = (payload, question, conversationId) => {
  payload.answeredQuestions = payload.answeredQuestions + 1;
  payload.interviewId = conversationId;
  payload.questionId = question.questionId;
  payload = updateAnsweredList(payload, question);
  return getFormattedPersonalInformation(payload, question);
};

const updateAnsweredList = (payload, question) => {
  const ids = [];
  if (Array.isArray(question.answer)) {
    question.answer.forEach((a) => {
      ids.push(a.id);
    });
  } else {
    ids.push(question.answer.id);
  }
  payload.answeredList.push({
    ids,
    key: question.level,
    name: question.key,
    prevId: question.questionId,
  });
  return payload;
};

const getAgeFromBirthDate = (memberProfile) => {
  if (memberProfile && memberProfile.dob) {
    const dob = new Date(memberProfile.dob);
    const monthDiff = Date.now() - dob.getTime();
    const ageDt = new Date(monthDiff);
    const year = ageDt.getUTCFullYear();
    const age = Math.abs(year - 1970);
    return age;
  } else {
    return null;
  }
};

const getFormattedPersonalInformation = (temp, question) => {
  switch (question.key) {
    case 'riskFactors':
      updateRiskFactorValues(temp, question.answer);
      break;
    case 'initialEvidences':
      updateInitialSymptomValues(temp, question.answer);
      break;
    case 'diagnose':
      updateOtherSymptomValues(temp, question.answer, question.type);
      break;
    case 'memberPaymentInfo':
      temp[question.key] = question.answer;
      break;
    default:
      temp[question.key] = question.answer.value;
  }
  return temp;
};

const updateRiskFactorValues = (payload, answers) => {
  if (answers) {
    answers.forEach((ans) => {
      if (ans.yes) {
        payload.demographicRiskFactorPresentIds.push(ans.id);
      } else if (ans.no) {
        payload.demographicRiskFactorAbsentIds.push(ans.id);
      } else if (ans.idk) {
        payload.demographicRiskFactorUnknownIds.push(ans.id);
      }
    });
  } else {
    return payload;
  }
};

const updateInitialSymptomValues = (payload, answers) => {
  if (answers) {
    answers.forEach((a) => {
      payload.initialEvidences.push({
        id: a.id,
        choiceId: 'present',
      });
    });
  } else {
    return payload;
  }
};

const updateOtherSymptomValues = (payload, answers, type) => {
  if (answers) {
    if (type === INPUT_TYPES.SYMPTOM_CHECKER_INPUT) {
      answers.forEach((a) => {
        payload.initialEvidences.push({
          id: a.id,
          choiceId: 'present',
        });
      });
    } else if (Array.isArray(answers)) {
      answers.forEach((a) => {
        payload.otherEvidences.push({
          id: a.id,
          choiceId: a.yes ? 'present' : a.no ? 'absent' : 'unknown',
        });
      });
    } else {
      if (type === INPUT_TYPES.YES_NO_DONT_KNOW_SELECT) {
        payload.otherEvidences.push({
          id: answers.id,
          choiceId: answers.key === 'yes' ? 'present' : answers.key === 'no' ? 'absent' : 'unknown',
        });
      } else {
        payload.otherEvidences.push({
          id: answers.id,
          choiceId: 'present',
        });
      }
    }
  } else {
    return payload;
  }
};

export { getSuggestedSymptoms, getNextQuestionSymptomChecker, getAgeFromBirthDate, updateLatestPayload, revertLatestPayload };
