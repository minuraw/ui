// eslint-disable-next-line no-unused-vars
import { CONVERSATION_MESSAGE_TYPES, SOURCES } from '../../components/Chat/constants';
import {
  getVaxYesNextQuestion,
  deleteConversation,
  saveVaxYesPayload,
  getVYBoosterCertificateInfo,
  getVaxYesBoosterNextQuestion,
  getVaxYesBoosterConversationInformation,
  getVaccineCertificates,
  reactivateConversation,
  updateMemberIdPhoto,
} from '../../connectors/bff-connector';
import { deleteChatMessage } from '../../events';
import { getCountryByPhoneNumber, isServerSide, logCallback, nonEmptyArray } from '../../utils';
import { getLocale } from '../../services';
const { INPUT_TYPES } = require('../../components/Chat/constants');

const initialVaxYesPayload = {
  intent: 'VaxYes',
  questionId: 1,
  answeredQuestions: 0,
  questions: [],
};

const getNextVaxYesQuestion = async (payload, scFlow = false) => {
  // eslint-disable-next-line no-unused-vars
  let {
    question,
    handleEndOfQuestions,
    latestPayload,
    memberProfile,
    conversationId,
    meta,
    groupId,
  } = payload;

  /**
   * Look for a latest payload, if not exist create a bare minimum one
   * and get the initial question.
   */
  if (!latestPayload) {
    latestPayload = { ...initialVaxYesPayload };
    latestPayload.groupId = groupId;
    latestPayload.startedAuthenticated = isServerSide()
      ? false
      : JSON.parse(window.localStorage.getItem('startedAuthenticated'));
  } else if (question) {
    /**
     * If latest payload exist, update it with the current answer.
     */
    if (question.updatePrevAnswer) {
      latestPayload = revertLatestPayload(latestPayload, question);
      /**
       * Delete relevant messages from the chat history.
       */
      deleteChatMessage(logCallback, { msgId: meta.msgId });
    } else {
      latestPayload = await updateLatestPayload(
        latestPayload,
        question,
        conversationId,
        meta,
        memberProfile,
      );
    }
  }

  // if latest payload doesn't have a country, add it using phone number
  if (!latestPayload.country) {
    const phone = (nonEmptyArray(memberProfile.phone) && memberProfile.phone[0]) || null;
    latestPayload.country = getCountryByPhoneNumber(phone);
  }

  /**
   * Get next question from the BFF.
   */
  let nextQuestion = await getNextQuestion(latestPayload);

  // BEGIN ******** skip payment
  const skipPayment = memberProfile?.sso;

  if (skipPayment && nextQuestion.type === INPUT_TYPES.VAXYES_PAYMENT_INPUT) {
    latestPayload = await updateLatestPayload(
      latestPayload,
      {
        ...nextQuestion,
        answer: 'Skipped for sso users',
      },
      conversationId,
      meta,
      memberProfile,
    );
    nextQuestion = await getNextQuestion(latestPayload);
  }

  // END ******** skip payment

  // fire and forget saving conversation to speed up the next question
  if (conversationId) {
    saveVaxYesPayload({
      id: conversationId,
      payload: latestPayload,
    });
  }

  /**
   * Check if we should end the chat flow.
   */
  if (nextQuestion.endOfInteraction) {
    return handleEndOfQuestions(nextQuestion.source, { nextQuestion, latestPayload });
  }

  /**
   * Return the next question and the latest payload.
   */
  return { nextQuestion, latestPayload };
};

const getNextQuestion = async (payload) => {
  try {
    const res = await getVaxYesNextQuestion(payload);
    return formatVaxYesQuestion(res);
  } catch (err) {
    console.error('VaxYes get next question ERROR - ', err.message);
    return {};
  }
};

const formatVaxYesQuestion = (question) => {
  question.source = SOURCES.VAX_YES;
  return question;
};

const revertLatestPayload = (payload, question) => {
  /**
   * Remove Last question from the answer list and
   * remove last answered key from the payload.
   */
  const lastQuestion = payload.questions.pop();
  if (Object.keys(payload).includes(lastQuestion?.key)) {
    delete payload[lastQuestion.key];
  }
  const prevQuestion = payload.questions.pop();

  payload.questions.push(prevQuestion);
  payload.questionId = prevQuestion ? prevQuestion.questionId : 1;

  return payload;
};
const updateLatestPayload = async (payload, question, conversationId, meta, memberProfile) => {
  /**
   * Update the latest payload with most recently answered question`s data.
   */
  payload.answeredQuestions = payload.answeredQuestions + 1;
  payload.interviewId = conversationId;
  payload.questionId = question.questionId;
  payload[question.key] = await manageUserInput(question);

  // Add the memberId as part of the payload
  if (!payload.memberId) {
    payload.memberId = memberProfile.memberId;
  }

  /**
   * Use msgId to verify the question to be repeated.
   */
  if (!Array.isArray(payload.questions)) {
    payload.questions = [];
  }
  payload.questions.push({
    msgId: meta.answerId,
    questionId: question.questionId,
    key: question.key,
  });
  if (question.forContext) {
    payload = { ...payload, ...question.forContext };
  }
  return payload;
};

const manageUserInput = async (payload) => {
  /**
   * TODO Cleanup the latest payload, Eg: if there is an image to upload, upload the image to
   * the related S3 bucket and attach the reference as the answer.
   */

  switch (payload.type) {
    case INPUT_TYPES.IMAGE_INPUT: {
      if (payload.key === 'memberVerificationImage') {
        await updateMemberIdPhoto({
          contentType: payload.answer.type,
          imagePath: payload.answer.location,
        });
      }
      return payload.answer.location;
    }
    default:
      return payload.answer;
  }
};

const deleteVaxYesConversation = async (payload) => {
  if (payload.deleteChatHistory) {
    await deleteConversation(payload); // delete from db
  } else {
    await reactivateConversation(payload);
  }
  // replace with initial payload
  return saveVaxYesPayload({
    id: payload.conversationId,
    payload: { ...initialVaxYesPayload, isUpdate: true },
  });
};

const getNextVaxYesQuestionAfterEndOfConversation = () => {
  return Promise.resolve({
    nextQuestion: {
      question: '', // Keep this empty
      answers: true, // have this true so that this question is shown to have a answer only
      type: INPUT_TYPES.END_OF_VAX_YES_INPUT,
      key: 'vax_yes_end_options',
      source: 'ggd',
    },
  });
};

const getVaxYesBoosterCertificateInfo = () => {
  return getVYBoosterCertificateInfo();
};

const vaxYesBoosterInitialPayload = {
  questionId: 1,
};

// Generate the context of the chat
const generateVaxYesBoosterContext = async (payload) => {
  const { latestPayload, question, memberProfile } = payload;
  const { questionId, key, forContext } = question;

  // get the answer by examining types
  // if image type is there image should be uploaded
  const answer = await manageUserInput(question);

  // the new context generated by the answered question
  const newContext = {
    questionId,
    [key]: answer,
  };

  // Add the memberId as part of the payload
  if (!latestPayload.memberId) {
    newContext.memberId = memberProfile.memberId;
  }

  const answered = [];
  if (latestPayload.answered) {
    answered.push(...latestPayload.answered);
  }
  answered.push(newContext);

  const context = {
    ...latestPayload, // old context
    ...newContext, // newly generated context
    ...forContext, // append anything in the forContext
    answered, // all the answers given by the member. This is used to go back in the conversation
  };

  return context;
};

// We are removing the last question
const generatePreviousVaxYesBoosterContext = (context) => {
  // check if the user has answered anything
  if (nonEmptyArray(context.answered)) {
    const answered = context.answered;
    // remove the last element
    const [lastAnswer] = answered.splice(answered.length - 1, 1);
    const { key } = lastAnswer;
    // remove the answer for the last question
    delete context[key];

    const currentLast = answered[answered.length - 1];

    // get the question Id of the now last element
    const questionId = currentLast?.questionId || 1;

    return {
      ...context,
      questionId,
    };
  } else {
    // no answers imply initial state, return it
    return context;
  }
};

// handles the next question of vaxyes booster flow
const getNextVaxYesBoosterQuestion = async (payload) => {
  const { latestPayload, conversationId, memberProfile, question, handleEndOfQuestions, meta } =
    payload;
  const locale = getLocale();

  const CAPTIONS = {
    booster: {
      'en-us': 'Your vaccine booster card has been successfully created.',
      'es-mx': 'Su tarjeta de vacuna de refuerzo se ha creado con éxito.',
    },
  };

  let context = latestPayload;

  // No payload implies this is the chat init
  if (!latestPayload) {
    context = vaxYesBoosterInitialPayload;
  }
  // question will be there when user answers a question
  if (question) {
    // chat has already started

    // if the last chat message was deleted
    if (question.updatePrevAnswer) {
      context = generatePreviousVaxYesBoosterContext(context);
      deleteChatMessage(logCallback, { msgId: meta.msgId });
    } else {
      context = await generateVaxYesBoosterContext(payload);
    }
  }

  // fire and forget saving conversation to speed up the next question
  if (conversationId) {
    saveVaxYesPayload({
      id: conversationId,
      payload: context,
    });
  }

  // if latest payload doesn't have a country, add it using phone number
  if (!context.country) {
    const phone = (nonEmptyArray(memberProfile.phone) && memberProfile.phone[0]) || null;
    context.country = getCountryByPhoneNumber(phone);
  }

  // get the next question
  const nextQuestion = await getVaxYesBoosterNextQuestion(context);

  // append the source
  nextQuestion.source = SOURCES.VAX_YES_BOOSTER;

  // At the end of the chat all the end of interaction
  if (nextQuestion.endOfInteraction) {
    const endOfInteractionButtons = await getNextVaxYesQuestionAfterEndOfConversation();
    endOfInteractionButtons.question = [CAPTIONS.booster[locale]];
    handleEndOfQuestions(SOURCES.VAX_YES_BOOSTER, {
      nextQuestion: endOfInteractionButtons,
      latestPayload: context,
    });
    // making the last question empty so that it will not be rendered from the UI
    // handle end of interaction will handle the showing the last message
    return endOfInteractionButtons;
  }

  return { nextQuestion, latestPayload: context };
};

const getVaxYesBoosterConversationInfo = (payload) => {
  return getVaxYesBoosterConversationInformation(payload);
};

export {
  getNextVaxYesQuestion,
  getNextVaxYesQuestionAfterEndOfConversation,
  getVaxYesBoosterCertificateInfo,
  deleteVaxYesConversation,
  getNextVaxYesBoosterQuestion,
  getVaxYesBoosterConversationInfo,
  getVaccineCertificates,
};
