// eslint-disable-next-line no-unused-vars
import { SOURCES } from '../../components/Chat/constants';
import {
  getNextQuestion,
  deleteConversation,
  saveVaxYesPayload,
  reactivateConversation,
  updateMemberIdPhoto,
} from '../../connectors/bff-connector';
import { deleteChatMessage } from '../../events';
import { getCountryByPhoneNumber, isServerSide, logCallback, nonEmptyArray } from '../../utils';
const { INPUT_TYPES } = require('../../components/Chat/constants');

const initialVaxYesPayload = {
  intent: 'CoVaxYes',
  questionId: 1,
  answeredQuestions: 0,
  questions: [],
  protocol: 'co-vaxyes',
};

const getNextCoVaxYesQuestion = async (payload, scFlow = false) => {
  // eslint-disable-next-line no-unused-vars
  let {
    question,
    handleEndOfQuestions,
    latestPayload,
    memberProfile,
    conversationId,
    meta,
    groupId,
  } = payload;

  /**
   * Look for a latest payload, if not exist create a bare minimum one
   * and get the initial question.
   */
  if (!latestPayload) {
    latestPayload = { ...initialVaxYesPayload };
    latestPayload.groupId = groupId;
    latestPayload.startedAuthenticated = isServerSide()
      ? false
      : JSON.parse(window.localStorage.getItem('startedAuthenticated'));
  } else if (question) {
    /**
     * If latest payload exist, update it with the current answer.
     */
    if (question.updatePrevAnswer) {
      latestPayload = revertLatestPayload(latestPayload, question);
      /**
       * Delete relevant messages from the chat history.
       */
      deleteChatMessage(logCallback, { msgId: meta.msgId });
    } else {
      latestPayload = await updateLatestPayload(
        latestPayload,
        question,
        conversationId,
        meta,
        memberProfile,
      );
    }
  }

  // if latest payload doesn't have a country, add it using phone number
  if (!latestPayload.country) {
    const phone = (nonEmptyArray(memberProfile.phone) && memberProfile.phone[0]) || null;
    latestPayload.country = getCountryByPhoneNumber(phone);
  }

  /**
   * Get next question from the BFF.
   */
  let nextQuestion = await getFormattedNextQuestion(latestPayload);

  // BEGIN ******** skip payment
  const skipPayment = memberProfile?.sso;

  if (skipPayment && nextQuestion.type === INPUT_TYPES.VAXYES_PAYMENT_INPUT) {
    latestPayload = await updateLatestPayload(
      latestPayload,
      {
        ...nextQuestion,
        answer: 'Skipped for sso users',
      },
      conversationId,
      meta,
      memberProfile,
    );
    nextQuestion = await getFormattedNextQuestion(latestPayload);
  }

  // END ******** skip payment

  // fire and forget saving conversation to speed up the next question
  if (conversationId) {
    saveVaxYesPayload({
      id: conversationId,
      payload: latestPayload,
    });
  }

  /**
   * Check if we should end the chat flow.
   */
  if (nextQuestion.endOfInteraction) {
    return handleEndOfQuestions(nextQuestion.source, { nextQuestion, latestPayload });
  }

  /**
   * Return the next question and the latest payload.
   */
  return { nextQuestion, latestPayload };
};

const getFormattedNextQuestion = async (latestPayload) => {
  try {
    const question = await getNextQuestion({
      protocol: latestPayload.protocol,
      data: latestPayload,
    });
    question.source = SOURCES.GGD_CO_VAX_YES;
    question.createdOn = Date.now();

    if (question.type === INPUT_TYPES.APPOINTMENT_TIMESLOT_INPUT) {
      question.answers = {
        consultId: latestPayload.createConsultResponse.id,
      };
    }

    return question;
  } catch (err) {
    console.error('GGD_CO_VAX_YES get next question ERROR - ', err.message);
    return {};
  }
};

const revertLatestPayload = (payload, question) => {
  /**
   * Remove Last question from the answer list and
   * remove last answered key from the payload.
   */
  const lastQuestion = payload.questions.pop();
  if (Object.keys(payload).includes(lastQuestion?.key)) {
    delete payload[lastQuestion.key];
  }
  const prevQuestion = payload.questions.pop();

  payload.questions.push(prevQuestion);
  payload.questionId = prevQuestion ? prevQuestion.questionId : 1;

  return payload;
};
const updateLatestPayload = async (payload, question, conversationId, meta, memberProfile) => {
  /**
   * Update the latest payload with most recently answered question`s data.
   */
  payload.answeredQuestions = payload.answeredQuestions + 1;
  payload.interviewId = conversationId;
  payload.questionId = question.questionId;
  payload[question.key] = await manageUserInput(question);

  // Add the memberId as part of the payload
  if (!payload.memberId) {
    payload.memberId = memberProfile.memberId;
  }

  /**
   * Use msgId to verify the question to be repeated.
   */
  if (!Array.isArray(payload.questions)) {
    payload.questions = [];
  }
  payload.questions.push({
    msgId: meta.answerId,
    questionId: question.questionId,
    key: question.key,
  });
  if (question.forContext) {
    payload = { ...payload, ...question.forContext };
  }
  return payload;
};

const manageUserInput = async (payload) => {
  /**
   * TODO Cleanup the latest payload, Eg: if there is an image to upload, upload the image to
   * the related S3 bucket and attach the reference as the answer.
   */

  switch (payload.type) {
    case INPUT_TYPES.IMAGE_INPUT: {
      if (payload.key === 'memberVerificationImage') {
        await updateMemberIdPhoto({
          contentType: payload.answer.type,
          imagePath: payload.answer.location,
        });
      }
      return payload.answer.location;
    }
    default:
      return payload.answer;
  }
};

const deleteCoVaxYesConversation = async (payload) => {
  if (payload.deleteChatHistory) {
    await deleteConversation(payload); // delete from db
  } else {
    await reactivateConversation(payload);
  }
  // replace with initial payload
  return saveVaxYesPayload({
    id: payload.conversationId,
    payload: { ...initialVaxYesPayload, isUpdate: true },
  });
};

export { getNextCoVaxYesQuestion, deleteCoVaxYesConversation };
