import { memberNameValidation } from '../../validations/input-validations';

describe('Input validations tests', () => {
  describe('Member name validations', () => {
    it('should return name valid for a valid name', () => {
      expect(memberNameValidation('John').valid).toBe(true);
      expect(memberNameValidation('Sylvestor').valid).toBe(true);
      expect(memberNameValidation('Jack jr.').valid).toBe(true);
    });
    it('should return name invalid with a reason code for invalid name', () => {
      let validResponse = memberNameValidation('John345');
      expect(validResponse.valid).toBe(false);
      expect(validResponse.code).toBe('Invalid characters in the name');

      validResponse = memberNameValidation('123$4567');
      expect(validResponse.valid).toBe(false);
      expect(validResponse.code).toBe('Invalid characters in the name');
    });
    it('should return name invalid for incorrect param types', () => {
      let validResponse = memberNameValidation(null);
      expect(validResponse.valid).toBe(false);
      expect(validResponse.code).toBe('Invalid name value');

      validResponse = memberNameValidation({});
      expect(validResponse.valid).toBe(false);
      expect(validResponse.code).toBe('Invalid name value');
    });
  });
});
