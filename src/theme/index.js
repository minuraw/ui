import { createTheme } from '@material-ui/core';
import typography from './typography';
import overrides from './overrides';
import props from './props';
import palette from './palette';
import shape from './shape';
import breakpoints from './breakpoints';

const theme = createTheme({
  typography,
  overrides,
  props,
  palette,
  shape,
  breakpoints,
});

theme.typography.h1 = {
  ...theme.typography.h1,
  [theme.breakpoints.up('md')]: {
    fontSize: '3rem',
  },
};

export { theme };
