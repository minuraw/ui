/* eslint-disable no-undef */
import { getAccessToken, getLocale } from '../services';

import {
  handleSessionTimeout,
  LOGGER,
  shouldContinueToFetchRefreshToken,
  fetchRefreshToken,
  getRequestTrackingHeaders,
  getPublicRequestTrackingHeaders,
} from '../utils';

const BFF_ENDPOINT = process.env.NEXT_PUBLIC_BFF_API_HOST;

const sendPublicRequest = async (path = '', payload = {}) => {
  const requestTrackingHeaders = getPublicRequestTrackingHeaders();
  try {
    const locale = getLocale();
    const res = await fetch(BFF_ENDPOINT + '/public?' + path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        path: path,
        locale,
        ...requestTrackingHeaders,
      },
      body: JSON.stringify(payload),
    }).then((r) => r.json());
    if (res.status === 'error') {
      throw new Error(res.error);
    }
    return res.data;
  } catch (err) {
    // capture the log
    LOGGER.error('Error occurred getting response', {
      path,
      payload,
      private: false,
      error: err.message,
      ...requestTrackingHeaders
    });
    throw new Error(err);
  }
};

const sendPrivateRequest = async (path = '', payload = {}, attempt = 1) => {
  const requestTrackingHeaders = getRequestTrackingHeaders();
  try {
    const locale = getLocale();
    const response = await fetch(BFF_ENDPOINT + '/private?' + path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        path: path,
        Authorization: getAccessToken(),
        locale,
        ...requestTrackingHeaders,
      },
      body: JSON.stringify(payload),
    });
    const res = await response.json();

    // explicit check for session expiration
    if (response.status === 403) {
      // check if we need to continue to fetch the refresh token
      if (shouldContinueToFetchRefreshToken(attempt)) {
        const success = await fetchRefreshToken();
        if (success) {
          // if refresh attempt is success, call recursively to try to fetch
          return sendPrivateRequest(path, payload, attempt + 1);
        }
      }
      handleSessionTimeout();
    }
    if (res.status === 'error') {
      throw new Error(res.error);
    }
    return res.data;
  } catch (err) {
    // capture the log
    LOGGER.error('Error occurred getting response', {
      path,
      payload,
      private: false,
      error: err.message,
      ...requestTrackingHeaders
    });
    throw new Error(err);
  }
};

export { sendPublicRequest, sendPrivateRequest };
