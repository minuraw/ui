import { API } from 'aws-amplify';
import { detectIntent } from '../graphql/queries';

// import { getAccessToken } from '../services';

const intentDetection = (params) => {
  // const lambdaAuthToken = getAccessToken();
  return API.graphql({
    query: detectIntent,
    variables: params,
    authMode: 'API_KEY',
  });
};

const subscribeToEvent = (query, params, callback) => {
  // const lambdaAuthToken = getAccessToken();
  /**
   * TODO Have to add LAMDA Authorizer
   * https://docs.amplify.aws/lib/graphqlapi/authz/q/platform/js/#using-aws-appsync-sdk
   * @type {{authMode: string, variables, query}}
   */
  const payload = {
    query: query,
    variables: params,
    authMode: 'API_KEY',
  };
  return API.graphql(payload).subscribe({
    next: callback,
  });
};

export { intentDetection, subscribeToEvent };
