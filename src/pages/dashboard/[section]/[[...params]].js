import { Header, MemberViewMobile, MemberViewDesktop } from '../../../components/Layout';
import { useRouter } from 'next/router';
import { decodeDashboardRouteParams } from '../../../utils';
import { useAuth } from '../../../context';
import { useCallback, useEffect, useState } from 'react';
import { CircularLoader } from '../../../components/Loader';
import { useMediaQuery, Box } from '@material-ui/core';
import {
  eventListener,
  EVENTS,
  getMemberConversations,
  getVaxYesCertificates,
  getMemberAppointments,
  notifyAppNotification,
} from '../../../events';
import { PROTOCOL_KEYS } from '../../../constants/protocolNames';
import { getVaxYesPayload } from '../../../connectors/bff-connector';
import { reactivateGGDConversation } from '../../../services';
import { CONVERSATION_PROVIDER_ID } from '../../../constants/providers';

const TABS = [
  {
    id: 'wallet',
    label: {
      'en-us': 'Wallet',
      'es-mx': 'Cartera',
    },
    disabled: false,
  },
  {
    id: 'chat',
    label: {
      'en-us': 'Chats',
      'es-mx': 'Conversaciones',
    },
    disabled: false,
  },
  {
    id: 'appointments',
    label: {
      'en-us': 'Appointments',
      'es-mx': 'Citas',
    },
    disabled: false,
  },
];

const tabHistory = {
  chat: null,
  appointments: null,
  wallet: null,
};

const getPreviousUrl = (tab) => {
  return tabHistory[tab];
};

const setPreviousUrl = (tab, url) => {
  tabHistory[tab] = url;
};

const appointmentComparator = (appointmentA, appointmentB) => {
  const a = new Date(appointmentA.scheduledStartTime);
  const b = new Date(appointmentB.scheduledStartTime);
  if (a < b) {
    return -1;
  }
  if (a > b) {
    return 1;
  }
  return 0;
};

const Dashboard = () => {
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  const { isAuthenticated, getMemberProfile } = useAuth();
  const router = useRouter();

  const [routerParams, setRouterParams] = useState(null);
  const [isDataLoading, setIsDataLoading] = useState(true);
  const [mainLoading, setIsMainLoading] = useState(false);
  const [conversations, setConversations] = useState(null);
  const [walletCertificates, setWalletCertificates] = useState(null);
  const [appointments, setAppointments] = useState(null);
  const [selectedAppointment, setSelectedAppointment] = useState(null);

  const updateRouterParameters = (params) => {
    if (params) {
      if (params.section === null) {
        params.section = 'chat'; // default dashboard page to chat
      }
      setRouterParams(params);
    }
  };

  useEffect(() => {
    if (router.isReady) {
      if (isAuthenticated) {
        const routeParams = decodeDashboardRouteParams(router);
        updateRouterParameters(routeParams);
        setIsDataLoading(true);
      } else {
        router.replace('/');
      }
    }
  }, [router]);

  useEffect(() => {
    if (routerParams) {
      handleInitialTabLoad(routerParams.section);
    }
  }, [routerParams]);

  const redirectTo = useCallback(
    (url) => {
      router.push(url, undefined, { shallow: true });
    },
    [router],
  );

  const handleChangeConversation = useCallback(
    (conversation) => {
      const { conversationId, provider } = conversation;
      // avoid re-render current chat.
      if (routerParams.conversationId === conversationId) {
        return;
      }
      const url = `/dashboard/chat/${conversationId}?provider=${provider}`;
      setPreviousUrl('chat', url);
      redirectTo(url);
    },
    [redirectTo, routerParams],
  );

  const handleChangeAppointment = useCallback(
    (appointment) => {
      const { conversationId, provider } = appointment;
      setSelectedAppointment(appointment);
      // avoid re-render current chat.
      if (routerParams.conversationId === conversationId) {
        return;
      }
      const url = `/dashboard/appointments/${conversationId}?provider=${provider}`;
      setPreviousUrl('appointments', url);
      redirectTo(url);
    },
    [redirectTo, routerParams],
  );

  const handleChangeWalletCertificate = useCallback(
    (certificate) => {
      const { id } = certificate;
      // avoid re-render current one
      if (routerParams.certificateId === id) {
        return;
      }
      const url = `/dashboard/wallet/${id}`;
      setPreviousUrl('wallet', url);
      redirectTo(url);
    },
    [redirectTo, routerParams],
  );

  const loadConversations = useCallback(
    (error, conversations) => {
      if (!error) {
        setConversations(
          conversations.map((conversation, index) => {
            if (conversation.messageTypeId === 3) {
              conversation.message = 'Id image verification failed!';
            } else if (conversation.messageTypeId === 4) {
              conversation.message = 'Start visit.';
            } else if (conversation.messageTypeId === 5) {
              conversation.message = 'Your prescription received!';
            } else if (conversation.messageTypeId === 6) {
              conversation.message = 'Diagnosis information received!';
            } else if (conversation.messageTypeId === 7) {
              conversation.message = 'Here are your results.';
            } else if (conversation.messageTypeId === 8 || conversation.messageTypeId === 9) {
              try {
                conversation.message = JSON.parse(conversation.message).validatedAnswer.mainMsg;
              } catch {
                console.log('Invalid message type');
                conversation.message = 'Input validated';
              }
            } else if (conversation.messageTypeId === 12) {
              try {
                const pharmacy = JSON.parse(conversation.message);
                conversation.message = pharmacy.name;
              } catch (err) {
                console.error('Error occurred', err);
                conversation.message = 'Pharmacy selected';
              }
            }
            if (!conversation.endOfConversation) {
              conversation.message = 'You: ' + conversation.message;
            }
            return conversation;
          }),
        );

        // mark the first chat as loaded when comes to /dashboard/chat
        if (!routerParams.start && !routerParams.conversationId && conversations.length > 0) {
          handleChangeConversation(conversations[0]);
        } else {
          setPreviousUrl('chat', router.asPath);
        }
      }
      setIsDataLoading(false);
    },
    [routerParams, handleChangeConversation, router],
  );

  const handleLoadCertificates = useCallback(
    (_, data) => {
      if (!data || !data.certs || !Array.isArray(data.certs)) {
        console.error('Error in getVaxYesCertificates');
        setWalletCertificates([]);
        return setIsDataLoading(false);
      }

      setWalletCertificates(data.certs);

      // mark the first cert as loaded when comes to /dashboard/wallet
      if (!routerParams.certificateId && data.certs.length > 0) {
        handleChangeWalletCertificate(data.certs[0]);
      } else {
        setPreviousUrl('wallet', router.asPath);
      }
      setIsDataLoading(false);
    },
    [handleChangeWalletCertificate, router, routerParams],
  );

  const handleMemberAppointmentLoad = useCallback(
    (_, data) => {
      if (!data || !Array.isArray(data)) {
        console.error('Error in getMemberAppointments');
        setAppointments([]);
        return setIsDataLoading(false);
      }

      // organize appointments
      const upcomingAppointments = data.filter((a) => a.encounterDescription === 'Planned');
      const pastAppointments = data.filter((a) => a.encounterDescription !== 'Planned');

      upcomingAppointments.sort(appointmentComparator);
      pastAppointments.sort(appointmentComparator);
      const allAppointments = upcomingAppointments;
      for (let i = pastAppointments.length - 1; i >= 0; i--) {
        allAppointments.push(pastAppointments[i]);
      }
      setAppointments(allAppointments);

      if (allAppointments.length > 0) {
        if (!routerParams.conversationId) {
          handleChangeAppointment(allAppointments[0]);
        } else {
          const currentAppointment = allAppointments.find(
            (a) => a.conversationId === routerParams.conversationId,
          );
          if (currentAppointment) {
            setSelectedAppointment(currentAppointment);
          }
          setPreviousUrl('appointments', router.asPath);
        }
      }

      setIsDataLoading(false);
    },
    [handleChangeAppointment, router, routerParams],
  );

  const handleInitialTabLoad = useCallback(
    (tab) => {
      if (tab === 'chat') {
        if (conversations) {
          if (!routerParams.conversationId && conversations.length > 0) {
            handleChangeConversation(conversations[0]);
          } else {
            setIsDataLoading(false);
          }
        } else {
          getMemberConversations(loadConversations);
        }
      } else if (tab === 'wallet') {
        if (walletCertificates) {
          if (!routerParams.certificateId && walletCertificates.length > 0) {
            handleChangeWalletCertificate(walletCertificates[0]);
          } else {
            setIsDataLoading(false);
          }
        } else {
          getVaxYesCertificates(handleLoadCertificates);
        }
      } else if (tab === 'appointments') {
        if (appointments) {
          if (!routerParams.conversationId && appointments.length > 0) {
            handleChangeAppointment(appointments[0]);
          } else {
            setIsDataLoading(false);
          }
        } else {
          // load video appointments
          getMemberAppointments(handleMemberAppointmentLoad, { type: 'video' });
        }
      } else {
        setIsDataLoading(false);
      }
    },
    [
      appointments,
      conversations,
      handleChangeAppointment,
      handleChangeConversation,
      handleChangeWalletCertificate,
      handleLoadCertificates,
      handleMemberAppointmentLoad,
      loadConversations,
      routerParams,
      walletCertificates,
    ],
  );

  const handleChatNotificationReceived = (payload) => {
    if (!payload) return;
    const chatConversationId = payload.eventType;
    updateConversation(chatConversationId, payload.displayText);
  };

  useEffect(() => {
    eventListener(EVENTS.CHAT_NOTIFICATION_RECEIVED, handleChatNotificationReceived);
  }, []);

  const updateConversation = useCallback((conversationId, message) => {
    setConversations((conversations) => {
      const matchingConversationIndex = conversations.findIndex(
        (conversation) => conversation.conversationId === conversationId,
      );
      if (matchingConversationIndex !== -1 && message) {
        const chatConversation = conversations[matchingConversationIndex];
        // Remove the element from the array
        conversations.splice(matchingConversationIndex, 1);
        // The date should be coming from the backend
        const newChatConversation = {
          ...chatConversation,
          message,
          date: new Date(),
        };
        // Add it to the first element
        conversations.splice(0, 0, newChatConversation);
      }
      return [...conversations];
    });
  }, []);

  const handleTabChange = useCallback(
    (tab) => {
      const previousUrl = getPreviousUrl(tab);
      if (previousUrl) {
        redirectTo(previousUrl);
      } else {
        redirectTo(`/dashboard/${tab}`);
      }
    },
    [redirectTo],
  );

  const updateChat = useCallback(
    (certificate) => {
      setIsMainLoading(true);
      if (certificate?.id === 'covid') {
        const url = `/chat/start/${PROTOCOL_KEYS.VAX_YES}/5?update=true`;
        redirectTo(url);
      } else if (certificate?.id === 'booster') {
        const url = `/chat/start/${PROTOCOL_KEYS.VAX_YES_BOOSTER}/5?update=true`;
        redirectTo(url);
      }
    },
    [redirectTo],
  );

  const reactivateChat = useCallback(
    async (conversationId, triggerEvent) => {
      setIsMainLoading(true);
      const existingPayload = await getVaxYesPayload({
        id: conversationId,
      });

      if (existingPayload) {
        const payload = {
          conversationId,
          triggerEvent,
          deleteChatHistory: false,
          latestPayload: existingPayload,
        };

        await reactivateGGDConversation(payload);
        const url = `/chat/${conversationId}?provider=${CONVERSATION_PROVIDER_ID.GGD_CONVERSATION}`;
        redirectTo(url);
        return;
      }
      notifyAppNotification(null, { error: 'An error occurred. Please try again. If error persists, visit the GoGetDoc help center.' });
    },
    [redirectTo],
  );

  return (
    <Box>
      <Header headerType="chat" />
      {routerParams && !mainLoading ? (
        smallScreen ? (
          <MemberViewMobile
            memberProfile={getMemberProfile() || {}}
            routerParams={routerParams}
            handleChangeConversation={handleChangeConversation}
            tabData={TABS}
            conversations={conversations}
            updateConversation={updateConversation}
            handleTabChange={handleTabChange}
            walletCertificates={walletCertificates}
            loading={isDataLoading}
            handleChangeWalletCertificate={handleChangeWalletCertificate}
            appointments={appointments}
            selectedAppointment={selectedAppointment}
            handleChangeAppointment={handleChangeAppointment}
            updateChat={updateChat}
            reactivateChat={reactivateChat}
          />
        ) : (
          <MemberViewDesktop
            memberProfile={getMemberProfile() || {}}
            routerParams={routerParams}
            handleChangeConversation={handleChangeConversation}
            tabData={TABS}
            conversations={conversations}
            updateConversation={updateConversation}
            handleTabChange={handleTabChange}
            walletCertificates={walletCertificates}
            loading={isDataLoading}
            handleChangeWalletCertificate={handleChangeWalletCertificate}
            appointments={appointments}
            selectedAppointment={selectedAppointment}
            handleChangeAppointment={handleChangeAppointment}
            updateChat={updateChat}
            reactivateChat={reactivateChat}
          />
        )
      ) : (
        <CircularLoader displayText="Loading.." GGD />
      )}
    </Box>
  );
};

export default Dashboard;
