import Head from 'next/head';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import { theme } from '../theme';
import '@fontsource/poppins/600.css';
import '@fontsource/poppins/500.css';
import '@fontsource/roboto/400.css';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import Amplify from 'aws-amplify';
import { SnackbarProvider } from 'notistack';

import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';

import {
  StoreProvider,
  NotificationProvider,
  AuthProvider,
  FeatureToggleProvider,
} from '../context';

import ErrorBoundary from '../components/ErrorBoundary';

import Notifications from '../components/Notifications';

import TagManager from 'react-gtm-module';

import init from '../utils/app-init';

if (process.browser && process.env.NEXT_PUBLIC_GTM) {
  TagManager.initialize({ gtmId: process.env.NEXT_PUBLIC_GTM });
}

const myAppConfig = {
  aws_appsync_graphqlEndpoint: process.env.NEXT_PUBLIC_GRAPHQL_URL,
  aws_appsync_region: process.env.NEXT_PUBLIC_REGION,
  aws_appsync_authenticationType: process.env.NEXT_PUBLIC_AUTHENTICATION_TYPE,
  aws_appsync_apiKey: process.env.NEXT_PUBLIC_GRAPHQL_API_KEY,
};

Amplify.configure({ ...myAppConfig, ssr: true });

const stripePromise = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY);

// initiate app init functions
init();

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="https://ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com/images/favicons/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="https://ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com/images/favicons/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="https://ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com/images/favicons/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest.json" />
        <link
          rel="mask-icon"
          href="https://ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com/images/favicons/safari-pinned-tab.svg"
          color="#00754A"
        />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
      </Head>

      <ErrorBoundary>
        <StoreProvider>
          <Notifications />
          <AuthProvider>
            <FeatureToggleProvider>
              <NotificationProvider>
                <Elements stripe={stripePromise}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <ThemeProvider theme={theme}>
                      <SnackbarProvider maxSnack={3} disableWindowBlurListener>
                        <CssBaseline>
                          <Component {...pageProps} />
                        </CssBaseline>
                      </SnackbarProvider>
                    </ThemeProvider>
                  </MuiPickersUtilsProvider>
                </Elements>
              </NotificationProvider>
            </FeatureToggleProvider>
          </AuthProvider>
        </StoreProvider>
      </ErrorBoundary>
    </>
  );
}

export default MyApp;
