import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { Conversation } from '../components/Chat/Conversation';
import { CircularLoader } from '../components/Loader';

import { useAuth } from '../context';

const Login = () => {

  const router = useRouter();

  const { isAuthenticated } = useAuth();
  const [authReady, setAuthReady] = useState(false);

  useEffect(() => {
    // user is authenticated, login in not required
    // redirect to dash board
    if (isAuthenticated) {
      router.push('/dashboard/chat');
    } else {
      // now show the chat to login
      setAuthReady(true);
    }
  }, []);

  if (!authReady) {
    return <CircularLoader displayText="Loading ..." GGD/>;
  }
  return <Conversation />;
};

export default Login;
