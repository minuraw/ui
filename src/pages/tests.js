import { DynamicProductCard } from '../components/Cards';
import { Typography, Box, Grid } from '@material-ui/core';
import { SectionContainer } from '../components/Common';
import Pagination from '@material-ui/lab/Pagination';
import { useCallback, useState } from 'react';
import { Header, Footer } from '../components/Layout';
import { getProducts } from '../services';

const PAGE_SIZE = 12;

const filterProductsForPage = (products, page) => {
  if (!products) {
    return [];
  }
  const firstIndex = (page - 1) * PAGE_SIZE;
  const lastIndex = page * PAGE_SIZE;
  return products.filter((p, i) => i >= firstIndex && i < lastIndex);
};

const getPageCount = (products) => {
  return Math.ceil(products.length / PAGE_SIZE);
};

const Tests = ({ products }) => {
  const [currentProducts, setCurrentProducts] = useState(() => {
    return filterProductsForPage(products, 1);
  });
  const onPageChange = useCallback(
    (event, page) => {
      setCurrentProducts(filterProductsForPage(products, page));
    },
    [products],
  );

  if (!products) return null;

  return (
    <>
      <Header />
      <SectionContainer maxWidth="sm">
        <Box textAlign="center">
          <Typography variant="h1" component="h1">
            All Providers
          </Typography>
        </Box>
      </SectionContainer>
      <SectionContainer maxWidth="lg" color>
        <Grid container spacing={2}>
          {currentProducts.map((product, index) => (
            <Grid item xs={12} sm={4} md={3} key={index}>
              <DynamicProductCard product={product} />
            </Grid>
          ))}
        </Grid>
        <Grid justifyContent="flex-end" container>
          <Grid item>
            <Box my={4}>
              <Pagination
                onChange={onPageChange}
                count={getPageCount(products)}
                color="primary"
                defaultPage={1}
              />
            </Box>
          </Grid>
        </Grid>
      </SectionContainer>
      <SectionContainer color maxWidth="lg">
        <Footer />
      </SectionContainer>
    </>
  );
};

export async function getStaticProps() {
  const productData = await getProducts({
    limit: null,
    offset: 0,
    product_group_id: 3,
  });
  const products = productData[0].products;
  return {
    props: {
      products: products && Array.isArray(products) && products.length > 0 ? products : null,
    },
    revalidate: 900, // 15 min cache
  };
}

export default Tests;
