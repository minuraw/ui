import { useRouter } from 'next/router';
import { Box } from '@material-ui/core';
import { SpecificProductCard } from '../../components/Cards/SpecificProductCard';

const Product = () => {
  const router = useRouter();
  const { productGroupId, productId } = router.query;
  return (
    <Box p={2}>
      <SpecificProductCard productGroupId={productGroupId} productId={productId} powered/>
    </Box>
  );
};

export default Product;
