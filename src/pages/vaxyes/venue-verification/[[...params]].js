import React, { useState } from 'react';
import Image from 'next/image';
import { LoginWrapper } from '../../../components/Auth';
import { Form, Field } from 'react-final-form';
import NumberFormat from 'react-number-format';
import { LogoVaxYes } from '../../../components/Logo';
import { Paper, Box, Typography, Button, TextField, Link } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { CircularLoader } from '../../../components/Loader';
import { useRouter } from 'next/router';
import { verifyVaxYesVenue } from '../../../events';

// const MOCK_DATA = {
//   verification_level: 'COVID-19 - Level 1',
//   certificate_images: [
//     'https://images.dev-vaxyes-api.gogetdoc.com/179/vax_image_1634147682178.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAYR3GVIS2CFBNAVE7%2F20211024%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20211024T160913Z&X-Amz-Expires=60&X-Amz-Signature=f37e77a3a0302ef60753508ed44f494a9b5136da8cd44656147a319f3af9338b&X-Amz-SignedHeaders=host',
//     'https://images.dev-vaxyes-api.gogetdoc.com/179/vax_image_1634149112423.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAYR3GVIS2CFBNAVE7%2F20211024%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20211024T160913Z&X-Amz-Expires=60&X-Amz-Signature=81783ae84cea841e2a1eff527f8da75f6fc6ef0f4e15ce3971bce93ac8153b1c&X-Amz-SignedHeaders=host',
//   ],
// };

const Params = () => {
  const [certificate, setCertificate] = useState();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  // eslint-disable-next-line no-unused-vars
  const [errorMsg, setErrorMsg] = useState(false);

  const reGenerateToken = (params) => {
    let token = '';
    if (Array.isArray(params)) {
      token = params.join('/');
    }
    return token;
  };

  const handleVenueVerification = (err, data) => {
    setLoading(false);
    if (err) {
      setErrorMsg(true);
    } else {
      setCertificate(data);
    }
  };

  const onSubmit = ({ dob }) => {
    const { params } = router.query;
    const token = reGenerateToken(params);
    setLoading(true);
    verifyVaxYesVenue(handleVenueVerification, {
      query: token,
      dob,
    });
    // For testing purposes only to see how component renders if there's data returned from API
    // setCertificate(MOCK_DATA);
  };

  const userCertificateVerified =
    certificate?.verification_level && certificate?.certificate_images;

  return (
    <LoginWrapper>
      <Paper>
        <Box p={2}>
          <Box my={2} textAlign="center">
            <LogoVaxYes />
          </Box>
          {loading ? (
            <CircularLoader displayText="Loading.." GGD/>
          ) : (
            <>
              {userCertificateVerified ? (
                <>
                  <Box my={2}>
                    <Alert severity="success">
                      <AlertTitle>Verification {certificate.verification_level}</AlertTitle>
                      Go to{' '}
                      <Link
                        href="https://www.gogetdoc.com/vaxyes"
                        target="_blank"
                        rel="noreferrer"
                        underline="always"
                      >
                        gogetdoc.com/vaxyes
                      </Link>{' '}
                      for more details
                    </Alert>
                  </Box>
                  <Box>
                    {certificate.certificate_images.map((img, idx) => {
                      return (
                        <Box mb={1} key={idx}>
                          <Image
                            src={img}
                            alt="Vaccine Card"
                            width={460}
                            height={260}
                            objectFit="contain"
                          />
                        </Box>
                      );
                    })}
                  </Box>
                </>
              ) : (
                <>
                  <Typography gutterBottom align="center">
                    Please enter the member’s date of birth to verify VaxYes verification level.
                  </Typography>
                  {errorMsg && (
                    <Box>
                      <Alert severity="error">
                        There was a problem verifying your details. Please try again.
                      </Alert>
                    </Box>
                  )}
                  <Form
                    onSubmit={onSubmit}
                    render={({ handleSubmit, submitErrors: errors = {} }) => (
                      <form onSubmit={handleSubmit}>
                        <Field
                          name="dob"
                          render={({ input }) => (
                            <NumberFormat
                              {...input}
                              placeholder="MM/DD/YYYY"
                              format="##/##/####"
                              label="Verify Date of Birth"
                              fullWidth
                              size="small"
                              variant="outlined"
                              margin="normal"
                              customInput={TextField}
                            />
                          )}
                        />
                        <Box mt={2}>
                          <Button type="submit" color="primary" fullWidth>
                            Submit
                          </Button>
                        </Box>
                      </form>
                    )}
                  />
                </>
              )}
            </>
          )}
        </Box>
      </Paper>
    </LoginWrapper>
  );
};

export default Params;
