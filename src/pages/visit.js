import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { Box, Grid, useMediaQuery, Typography } from '@material-ui/core';
// import { Header } from '../components/Layout';
import { ChatHeader } from '../components/Chat/ChatHeader';
import { LogoIconGGD } from '../components/Logo';
import { MessageGroup, UserAnswerGroup } from '../components/Common';
import { GGDLoader } from '../components/Common/GGDLoader';
import { getVideoVisitInfo } from '../utils';
import { getPublicVideoVisitLink } from '../events';
import { stripHtml } from 'string-strip-html';

const renderError = (err) => {
  try {
    const { consultTimeText, consultStartTimeText } = getVideoVisitInfo(parseInt(err.start));
    if (err.error) {
      if (new Date() > new Date(parseInt(err.end))) {
        return (
          <Box display="flex" justifyContent="center" alignItems="center">
            <Typography variant="subtitle1">Video Visit time expired.</Typography>
          </Box>
        );
      } else {
        return (
          <Box display="flex" flexDirection="column" alignItems="center">
            <Box>
              <h2>{consultTimeText}</h2>
            </Box>
            <Box>
              <h3>{consultStartTimeText}</h3>
            </Box>
          </Box>
        );
      }
    }
  } catch (err) {
    console.log(err);
  }
  return (
    <>
      <h2>
        We were unable to access your visit. Please use the correct link. If error persists, visit the GoGetDoc help center. 
      </h2>
    </>
  );
};

const renderVideoVisit = (data = {}, isJoined, smallScreen) => {
  const { link, conversation } = data;
  return (
    link && (
      <Grid container>
        <Grid item xs={12} md={isJoined ? 8 : 12}>
          <Box style={{ height: smallScreen && isJoined ? '70vh' : '100%' }}>
            <iframe
              src={link}
              title="Your visit"
              style={{ width: '100%', height: '100%' }}
              allow="camera; microphone"
              frameBorder="0"
            />
          </Box>
        </Grid>
        {isJoined && (
          <Grid item xs={12} md={4}>
            <Box style={{ height: smallScreen ? '30vh' : '100vh', overflow: 'scroll' }}>
              <ChatHeader
                avatarUrl={{ url: '', image: <LogoIconGGD color="#fff" height="30px" /> }}
                chatHeader="Patient intake history"
              />
              <Box p={2}>
              {conversation?.length > 0 ?
                conversation.map((message) => {
                  if (message.is_bot) {
                    return RenderTextMessage(message.msg, message.id, message.created_on);
                  } else {
                    return renderUserAnswer(
                      message.msg,
                      message.id,
                      message.created_on,
                      message.message_type_id,
                    );
                  }
                })
                :
                <Box display="flex" alignItems="center" justifyContent="center">
                  <Typography variant="body2">No chat history</Typography>
                </Box>
              }
              </Box>
            </Box>
          </Grid>
        )}
      </Grid>
    )
  );
};

const RenderTextMessage = (message, msgId, chatTime) => {
  return (
    <Box my={2}>
      <MessageGroup
        messages={[message]}
        messageId={msgId}
        messageTime={chatTime}
      />
    </Box>
  );
};

const renderUserAnswer = (
  answer,
  msgId,
  createdOn,
  msgTypeId,
) => {

  return (
    <UserAnswerGroup
      alignRight
      msgTypeId={msgTypeId}
      messages={[stripHtml(answer || '').result]}
      messageId={msgId}
      messageTime={createdOn}
    />
  );
};

const Visit = () => {
  const router = useRouter();

  const { token } = router.query;

  const [isDataLoading, setIsDataLoading] = useState(false);
  const [error, setError] = useState(null);
  const [videoVisit, setVideoVisit] = useState({});
  const [ isJoined, setIsJoined ] = useState(false);
  const smallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));

  useEffect(
    () =>
      window.addEventListener('message', (event) => {
        // IMPORTANT: check the origin of the data!
        if (event.origin.startsWith(process.env.NEXT_PUBLIC_VIDEO_VISIT_URL)) {
          // The data was sent from your site.
          // Data sent with postMessage is stored in event.data:
          setIsJoined(event.data.join);
        }
      }),
    [],
  );

  const handleVideoLinkLoad = (err, data) => {
    setIsDataLoading(false);
    if (err || data.error) {
      setError(err || data);
    } else {
      setVideoVisit(data);
    }
  };

  useEffect(() => {
    setIsDataLoading(true);
    if (router.isReady) {
      getPublicVideoVisitLink(handleVideoLinkLoad, { token });
    }
  }, [router, token]);

  if (isDataLoading) {
    return <Box display="flex" alignItems="center" justifyContent="center" height='100vh'><GGDLoader /></Box>;
  }

  return (
    <>
      {/* <Header /> */}
      <Grid container justifyContent="center" style={{ width: '100vw', height: '100vh' }}>
        {error ? renderError(error) : renderVideoVisit(videoVisit, isJoined, smallScreen)}
      </Grid>
    </>
  );
};

export default Visit;
