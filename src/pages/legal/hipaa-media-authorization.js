import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
    maxWidth: 1024,
    margin: 'auto',
    padding: 32,
  },
});

const HIPAAMediaAuthorization = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <br />
      <Typography variant="h6" gutterBottom align="center">
        Health Insurance Portability and Accountability Act - Authorization
      </Typography>
      <br />
      <Typography variant="h6" gutterBottom>
        Authorization
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        GoGetDoc, LLC (“GoGetDoc”) offers services, including but not limited to, helping you find
        and learn about healthcare options, utilizing healthcare tools, and accessing digital
        healthcare for certain conditions. As part of providing the GoGetDoc services, GoGetDoc may
        collect, use, share, and exchange your health history and other health related information
        with Your Healthcare Providers. Under a federal law called the Health Insurance Portability
        and Accountability Act (“HIPAA”), health and health-related information may be considered
        “protected health information” or “PHI” if such information is received from or on behalf of
        Your Healthcare Providers.
      </Typography>
      <br />

      <Typography variant="h6" gutterBottom>
        Safeguards for PHI
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        HIPAA protects the privacy and security of your PHI by limiting the uses and disclosures of
        PHI by healthcare providers and health plans (called “Covered Entities”) as well as
        companies, like GoGetDoc, that provide certain types of assistance to Covered Entities
        (called “Business Associates”). Under certain circumstances described in HIPAA, an
        individual needs to sign an Authorization form before a Covered Entity, like Your Healthcare
        Provider(s), can disclose protected health information to a third party.
      </Typography>
      <br />

      <Typography variant="h6" gutterBottom>
        Non-Protected Health Information
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        As a condition of creating your GoGetDoc account, you are required to read and agree to
        GoGetDoc’s Terms of Service. GoGetDoc’s Terms of Service explains how GoGetDoc processes and
        shares information received from you that is not covered by HIPAA (“Non-PHI”). This
        authorization has no effect on, or application to, GoGetDoc’s treatment of any of your
        Non-PHI information.
      </Typography>
      <br />

      <Typography variant="h6" gutterBottom>
        Your PHI Authorization
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        The purpose of this GoGetDoc Authorization (“Authorization”) is to request your written
        permission to allow GoGetDoc to use and disclose your PHI in the operation of GoGetDoc’s
        business and to disclose your PHI to Your Healthcare Providers. Your PHI includes, but is
        not limited to your medical history, drug history, and doctor history. By e-signing this
        Authorization, you give GoGetDoc permission to retain your PHI and to use same as necessary
        in the performance of GoGetDoc’s Services.
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        Such use includes, but is not limited to, using your PHI for the following:
      </Typography>
      <ul>
        <li>enable and customize your use of the GoGetDoc Services;</li>
        <li>provide you alerts of other GoGetDoc Services;</li>
        <li>
          notify you regarding providers or healthcare tools we think you may be interested in
          learning more about;
        </li>
        <li>
          share information with you regarding services, products or resources about which we think
          you may be interested in learning more;
        </li>
        <li>provide you with updates and information about the GoGetDoc Services;</li>
        <li>market to you about GoGetDoc and third party products and services;</li>
        <li>conduct analysis for GoGetDoc’s business purposes;</li>
        <li>support development of the GoGetDoc Services;</li>
        <li>
          and create de-identified information and then use and disclose this information in any way
          permitted by law, including to third parties in connection with their commercial and
          marketing efforts.
        </li>
      </ul>
      <Typography variant="body1" gutterBottom>
        You also agree that GoGetDoc can disclose your PHI to:
      </Typography>
      <ul>
        <li>third parties assisting GoGetDoc with any of the uses described above;</li>
        <li>
          Your Healthcare Providers to enable them to refer you to, and make appointments with,
          other providers on your behalf, or to perform an analysis on potential health issues or
          treatments, provided that you choose to use the applicable GoGetDoc Service;
        </li>
        <li>a third party as part of a potential merger, sale or acquisition of GoGetDoc;</li>
        <li>
          our business partners who assist us by performing core services (such as hosting, billing,
          fulfillment, or data storage and security) related to the operation or provision of our
          services, even when GoGetDoc is no longer working on behalf of Your Healthcare Providers;
        </li>
        <li>a provider of medical services, in the event of an emergency;</li>
        <li>
          and organizations that collect, aggregate and organize your information so they can make
          same more easily accessible to your providers.
        </li>
      </ul>
      <Typography variant="h6" gutterBottom>
        Re-disclosure
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        If GoGetDoc discloses your PHI to a third party, except for a covered entity with its own
        HIPAA obligations and documentation, GoGetDoc will require that the person or entity
        receiving your PHI agrees to only use and disclose your PHI to carry out its specific
        business obligations to GoGetDoc or for the permitted purpose of the disclosure (as
        described above). GoGetDoc cannot, however, guarantee that any such person or entity to
        which GoGetDoc discloses your PHI or other information will not re-disclose your PHI in ways
        that you or we did not intend or permit.
      </Typography>
      <br />

      <Typography variant="h6" gutterBottom>
        Expiration and Revocation of Authorization
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        Your Authorization remains in effect until GoGetDoc receives a written notice of revocation
        from You.
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        YOU CAN CHANGE YOUR MIND AND REVOKE THIS AUTHORIZATION AT ANY TIME AND FOR ANY (OR NO)
        REASON.
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        If you wish to revoke this Authorization, you must notify GoGetDoc by submitting a
        revocation in writing through email at support@GoGetDoc.com. Your decision not to execute
        this Authorization or to revoke it at any time will affect your ability to use certain
        components of the GoGetDoc services. A Revocation of Authorization is effective once
        GoGetDoc receives the revocation. However, the revocation does not have any effect on
        GoGetDoc’s prior actions taken in reliance on the Authorization before revocation. Your
        revocation of this Authorization to GoGetDoc does not effect any authorization You may have
        entered with third parties, including Your Healthcare Providers.
      </Typography>
      <br />
    </div>
  );
};

export default HIPAAMediaAuthorization;
