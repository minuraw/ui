import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
    maxWidth: 1024,
    margin: 'auto',
    padding: 32,
  },
});

const TelehealthInformedConsent = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <br />
      <Typography variant="h6" gutterBottom align="center">
        Informed Consent
      </Typography>
      <br />
      <Typography variant="subtitle1" gutterBottom>
        This is a legal and binding document between you and Wheel Health, Inc. on behalf of itself,
        its subsidiaries, and its affiliated professional entities (collectively, “Wheel,” “We,”
        “Our”, or “Us”). Read it carefully before clicking “I accept”. This document is provided to
        you as part of your engagement with GoGetDoc, LLC (&quot;GoGetDoc&quot;), which has
        contracted with Wheel to provide you access to the services described below.
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        By clicking “I accept”, you hereby consent to receive health care services from licensed
        health care providers contracted with Wheel Care, LLC or Wheel-affiliated professional
        entities (in either case, “Wheel-affiliated Providers”) who are located at sites remote from
        you to provide consultative services to you. The receipt of health care services from a
        Wheel-affiliated Provider via <a href="https://gogetdoc.com">https://gogetdoc.com</a> (the
        “Services”) is a type of “telemedicine” or “telehealth” service.
      </Typography>
      <br />
      <Typography variant="h6" gutterBottom>
        Description of Services
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        Wheel-affiliated Providers may include behavioral health or primary care practitioners,
        nurse practitioners, physician’s assistants, specialists, and/or subspecialists. The name,
        credentials, and specialty or subspeciality of your Wheel-affiliated Provider will be
        disclosed to you before, during, or after the Services are provided. In some cases,
        telemedicine visits may not be the most appropriate way for you to seek medical care and
        treatment. For example, certain medical conditions may require an in-person procedure, more
        urgent attention, or a health care provider other than your health care provider using the
        Services.
      </Typography>
      <ul>
        <li>
          We may ask you a series of initial questions to help you determine whether a telemedicine
          visit is appropriate for you. Based on your responses to these questions, we may determine
          that a telemedicine visit may not be appropriate for the particular issue for which you
          are seeking a telemedicine visit or for other reasons related to your health status. In
          such a case: (i) you will receive an alert notifying you that you will be unable to use
          the Services for the particular issue you submitted; (ii) your request for a telemedicine
          visit will not be submitted to your Wheel-affiliated Provider; (iii) your Wheel-affiliated
          Provider will not receive any of the information that you submitted; and (iv) you will
          need to seek any needed care in another way. Your Wheel-affiliated Provider may, following
          submission of a telemedicine visit request, determine that your diagnosis or treatment
          requires an in-person office visit or is otherwise not appropriately addressed through use
          of the Services. In such a case, your Wheel-affiliated Provider may notify you that you
          will be unable to use the Services for the particular issue you submitted and provide
          additional information regarding next steps. Your Wheel-affiliated Provider is solely
          responsible for providing you any such notification, whether through the Services or by
          some other means.
        </li>
        <li>
          We will use store-and-forward technology, audio-only consultations, and/or audio-video
          consultations to provide the Services. To ensure privacy and confidentiality of the
          Services, we use industry standard security measures. The Services may, but not
          necessarily will, result in a new prescription, refilling an existing prescription,
          patient education, non-prescriptive recommendations, or a recommendation to seek follow-up
          care in-person or through a different provider.
        </li>
        <li>
          Wheel will have no responsibility or liability for your Wheel-affiliated Provider’s delay
          or failure to respond to a telemedicine visit request, to notify you that your
          telemedicine visit cannot be completed, or to provide you with next steps or follow-up
          information, or for any care, medical advice or treatment provided by your Provider.
        </li>
        <li>
          While your account is with GoGetDoc, LLC, the Services and clinical care are provided as
          indicated herein.
        </li>
      </ul>
      <br />
      <Typography variant="h6" gutterBottom>
        Benefits and Risks
      </Typography>
      <br />
      <Typography variant="body1" gutterBottom>
        Your use of the Services may have the following possible benefits:
      </Typography>
      <ul>
        <li>
          Making it easier and more efficient for you to seek medical care and treatment for the
          conditions treated by the applicable health care provider;
        </li>
        <li>
          Allowing you to seek medical care and treatment by your Wheel-affiliated Provider at times
          that are convenient for you; and
        </li>
        <li>
          Enabling you to communicate with your Wheel-affiliated Provider without the necessity of
          an in-office appointment.
        </li>
      </ul>
      <Typography variant="body1" gutterBottom>
        As with any medical procedure, there are potential risks associated with the use of
        telemedicine or telehealth services, which may include, without limitation, the following:
      </Typography>
      <ul>
        <li>
          The information transmitted to your Wheel-affiliated Provider may not be sufficient (e.g.,
          poor resolution of images) to allow your Wheel-affiliated Provider to make an appropriate
          medical decision;
        </li>
        <li>
          Your Wheel-affiliated Provider’s inability to conduct certain tests or assess vital signs
          in-person may in some cases prevent the provider from providing a diagnosis or treatment
          or from identifying the need for emergency medical care or treatment for you;
        </li>
        <li>
          Your Wheel-affiliated Provider may not be able to provide medical treatment for your
          particular condition and you may be required to seek alternative health care or emergency
          care services;
        </li>
        <li>
          Delays in medical evaluation/treatment or a failure to obtain needed treatment could occur
          due to unavailability of your Wheel-affiliated Provider, deficiencies or failures of the
          technology or electronic equipment used, a transmission delay or failure, issues with the
          internet or other communications means, or for other reasons;
        </li>
        <li>
          The electronic systems, public networks, or security protocols or safeguards used in the
          Services could fail, causing a breach of privacy of your medical or other information;
        </li>
        <li>
          Your Wheel-affiliated Provider’s diagnosis and treatment options, especially pertaining to
          certain prescriptions, may be limited;{' '}
        </li>
        <li>
          Lack of access to your medical records or ability to perform an in-person examination,
          which could result in negative health outcomes (e.g., adverse drug interactions, allergic
          reactions).{' '}
        </li>
      </ul>
      <Typography variant="body1" gutterBottom>
        Please note that your Wheel-affiliated Provider may charge you, your health insurance
        provider, or both (as applicable), for telemedicine visits. You agree that you are: (i)
        fully responsible for payment for all fees billed to you related to telemedicine visits; and
        (ii) responsible for asking any questions that you may have about and fully understanding
        your financial responsibility before using the Services. Please contact your
        Wheel-affiliated Provider or insurance provider for any questions regarding fees, billing,
        or insurance coverage for telemedicine visits.
      </Typography>
      <br />
      <Typography variant="h6" gutterBottom>
        By clicking “I accept”, you also represent and warrant the following:
      </Typography>
      <ul>
        <li>
          Your Wheel-affiliated Provider has discussed the use of telemedicine services with you,
          including the benefits and risks of such use and the alternatives to the use of the
          Services, and you have provided consent to your Wheel-affiliated Provider for the use of
          the Services.
        </li>
        <li>
          You understand that you have the right to access your medical information created during
          use of the Services or to have the medical information forwarded to a third-party or
          alternative provider. Wheel will not forward any personally identifiable information to
          third-parties or other providers without your written consent.
        </li>
        <li>You understand Wheel may use third-party vendors to provide the Services.</li>
        <li>
          You understand that the use of the Services involves electronic communication of your
          personal medical information to your Wheel-affiliated Providers who may be located in
          other areas, including outside of the state in which you reside, and that the electronic
          systems, public networks, or security protocols or safeguards used in the Services could
          fail, causing a breach of privacy of your medical or other information. You agree to hold
          Wheel harmless for any information lost due to technical failures.
        </li>
        <li>
          You understand that, despite the privacy risks associated with the Services, all federal
          and state laws, rules, and regulations regarding privacy and confidentiality will apply to
          the Services, including HIPAA.
        </li>
        <li>
          You understand that it is your duty to provide your Wheel-affiliated Provider truthful,
          accurate, and complete information, including all relevant information regarding care that
          you may have received or may be receiving from other health care providers or outside of
          the Services. You also understand that if you are uncomfortable with receiving the
          Services or the method in which the Services are provided, you should inform your
          Wheel-affiliated Provider.
        </li>
        <li>
          You understand that your use of the Services will create a provider-patient relationship
          with your Wheel-affiliated Provider and that he or she may consult with other providers
          during the course of your treatment. However, you understand that your Wheel-affiliated
          Provider is not your primary care provider.
        </li>
        <li>
          You understand that your Wheel-affiliated Provider may determine that your condition is
          not suitable for diagnosis or treatment using the Services, or may fail to respond
          promptly or ever to your request for a telemedicine service, and that you may need to seek
          medical care and treatment from your Wheel-affiliated Provider, a specialist, or other
          health care provider outside of the Services.
        </li>
        <li>
          You understand that in the event of an emergency or an adverse reaction to treatment, you
          should dial 911 to receive appropriate follow-up care. In the event of technology failure
          or your Wheel-affiliated Provider determining you need see another provider or make an
          in-person appointment, he or she will provide you with next steps or follow-up
          information.
        </li>
        <li>
          You understand the risks and benefits of the Services and its use in the medical care and
          treatment provided to you by your Wheel-affiliated Provider. You also understand that you
          may refuse or withdraw from care at any time, and that your refusal or withdrawal will not
          affect your ability to receive care in the future.
        </li>
        <li>
          You understand that failure to comply with the terms of this document may result in the
          termination of the provider-patient relationship created through use of the Services.
        </li>
        <li>
          No potential benefits from the use of the Services, care provided via the Services, or
          specific results can be guaranteed. Your condition may not be cured or improved, and in
          some cases, may get worse.
        </li>
      </ul>
      <br />
      <Typography variant="h6" gutterBottom>
        Acknowledgement
      </Typography>
      <br />
      <Typography variant="subtitle1" gutterBottom>
        By clicking “I accept”, you (a) further certify that you are the patient, or that you are
        duly authorized by the patient as the patient’s representative or legal guardian, (b)
        acknowledge and accept the risks identified above and the terms associated with the receipt
        of clinical services via the Services, and (c) give your informed consent to receive
        clinical services under the terms described herein.
      </Typography>
      <br />
    </div>
  );
};

export default TelehealthInformedConsent;
