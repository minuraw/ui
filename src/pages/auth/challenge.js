import React, { useState, useContext } from 'react';
import { Grid, Paper, Box, Typography, makeStyles } from '@material-ui/core';
import { useAuth, Store as GlobalStore } from '../../context';
import { useRouter } from 'next/router';
import { LoginWrapper, LoginForm } from '../../components/Auth';
import { INPUT_TYPES } from '../../components/Chat/constants';
import { GGDLoader } from '../../components/Common';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

import { initiateLogin } from '../../services/auth';

import { CHALLENGE, GGD_AUTH } from '../../constants/login';

import { setInStorage, getFromStorage } from '../../utils';

const CAPTIONS = {
  confirm: {
    'en-us': `Now we'll confirm a few details.`,
    'es-mx': 'Ahora confirmaremos algunos detalles.',
  },
  please: {
    'en-us': 'Please enter the 6-digit verification code sent to your phone number.',
    'es-mx': 'Ingrese el código de verificación de 6 dígitos enviado a su número de teléfono',
  },
  error: {
    'en-us': 'The code you entered is invalid. Please enter the code that we just sent you.',
    'es-mx': 'El código que ingresó no es válido. Introduzca el código que le acabamos de enviar.',
  },
};

const useClasses = makeStyles((theme) => ({
  containerError: {
    '& .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline':
      {
        borderColor: '#f44336',
      },
  },
  arrow: {
    cursor: 'pointer',
    position: 'absolute',
    left: '0.5rem',
    top: '1.5rem',
  },
}));

const Challenge = () => {
  const { isAuthenticated } = useAuth();
  const router = useRouter();
  const { store } = useContext(GlobalStore);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const classes = useClasses();

  const onSubmit = async (data) => {
    const { session } = getFromStorage(GGD_AUTH);

    setLoading(true);

    const payload = {
      stage: 'challenge',
      data: {
        [CHALLENGE.KEY]: data.value,
        session,
      },
    };
    if (data.event === 'RESEND') {
      payload.stage = 'resend';
      payload.data = {
        session,
      };
    }
    try {
      const response = await initiateLogin(payload);

      // successfully resent, prompt the user
      if (data.event === 'RESEND') {
        setError(null);
        setLoading(false);
        return;
      }
      // Failure in the challenge
      // handle it properly
      if (response.success === false) {
        setError(CAPTIONS.error[store.locale]);
        // handle error
        setLoading(false);
        return;
      }
      router.push('/auth/verify');
      setInStorage(GGD_AUTH, { session: response.session });
      return;
    } catch {
      // handle error
    }
    setLoading(false);
  };

  if (isAuthenticated) {
    router.push('/member-chat');
    return '';
  }

  return (
    <LoginWrapper>
      <Grid item xs={12}>
        <Paper>
          <Box py={3} px={2} style={{ position: 'relative' }}>
            <Box px={2} textAlign="center">
              <Typography variant="h5" gutterBottom>
                {CAPTIONS.confirm[store.locale]}
              </Typography>
              <Typography variant="body1" style={{ color: '#a0a0a0' }}>
                {CAPTIONS.please[store.locale]}
              </Typography>
            </Box>
            {loading ? (
              <Box align="center">
                <GGDLoader />
              </Box>
            ) : (
              <Box className={error && classes.containerError}>
                <KeyboardArrowLeftIcon
                  color="primary"
                  onClick={() => router.push('/auth/login')}
                  className={classes.arrow}
                />
                <LoginForm
                  data-cy="login_form_otp"
                  inputControl="OTP"
                  inputType={INPUT_TYPES.OTP_INPUT}
                  handleResponse={onSubmit}
                />
                {error && (
                  <Typography variant="body2" color="error" align="center">
                    {error}
                  </Typography>
                )}
              </Box>
            )}
          </Box>
        </Paper>
      </Grid>
    </LoginWrapper>
  );
};

export default Challenge;
