import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { LoginWrapper, LoginForm } from '../../components/Auth';
import { Grid, Paper, Box, Typography, makeStyles } from '@material-ui/core';
import { Store as GlobalStore, useAuth } from '../../context';
import { INPUT_TYPES } from '../../components/Chat/constants';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

import { initiateLogin, initiateSession, setRefreshToken } from '../../services/auth';

import { VERIFY, GGD_AUTH } from '../../constants/login';

import { clearFromStorage, getFromStorage, getDateMMDDYYYYString } from '../../utils';

const CAPTIONS = {
  security: {
    'en-us': 'For your security please confirm your date of birth.',
    'es-mx': 'Por su seguridad por favor confirme su fecha de nacimiento.',
  },
  continue: {
    'en-us': 'Continue',
    'es-mx': 'Continuar',
  },
  cantLogin: {
    'en-us': `Can't login?`,
    'es-mx': '¿No puede iniciar sesión?',
  },
  signUp: {
    'en-us': 'Sign up for new user?',
    'es-mx': '¿Registrarse como usuario nuevo?',
  },
  memberNotFound: {
    'en-us': 'To continue, please confirm your login information.',
    'es-mx': 'Para continuar, confirme su información de inicio de sesión.',
  },
  errorInMemberRegistration: {
    'en-us': 'To continue, please confirm your login information.',
    'es-mx': 'Para continuar, confirme su información de inicio de sesión.',
  },
};

const useClasses = makeStyles((theme) => ({
  arrow: {
    cursor: 'pointer',
    position: 'absolute',
    left: '0.5rem',
    top: '1.5rem',
  },
}));

const Verify = () => {
  const router = useRouter();
  const { store } = useContext(GlobalStore);
  const classes = useClasses();
  const { signIn } = useAuth();

  const handleResponse = async (value) => {
    const answer = getDateMMDDYYYYString(value, store.locale);
    try {
      const { session } = getFromStorage(GGD_AUTH);

      const payload = {
        stage: 'verify',
        data: {
          session,
          [VERIFY.KEY]: answer,
        },
      };
      const response = await initiateLogin(payload);
      // see if it is success response
      if (response.success === false) {
        throw new Error(CAPTIONS.errorInMemberRegistration[store.locale]);
      }
      // see if the member found
      if (response.member_found === false) {
        throw new Error(CAPTIONS.memberNotFound[store.locale]);
      }
      const isValid = await initiateSession(response?.access_token, signIn);
      if (isValid) {
        setRefreshToken(response.refresh_token);
        clearFromStorage(GGD_AUTH);
        router.push('/dashboard');
      } else {
        // show the necessary error
      }
    } catch (err) {
      const message =
        err.message === CAPTIONS.memberNotFound[store.locale]
          ? err.message
          : CAPTIONS.errorInMemberRegistration[store.locale];
      throw new Error(message);
    }
  };

  return (
    <LoginWrapper>
      <Grid item xs={12}>
        <Paper>
          <Box py={3} px={2} style={{ position: 'relative' }}>
            <Box px={2}>
              <Typography variant="h5" align="center">
                {CAPTIONS.security[store.locale]}
              </Typography>
            </Box>
            <LoginForm
              inputControl="DOB"
              inputType={INPUT_TYPES.DATE_INPUT}
              handleResponse={handleResponse}
            />
            <KeyboardArrowLeftIcon
              color="primary"
              onClick={() => router.push('/auth/login')}
              className={classes.arrow}
            />
            {/* <Box py={3}>
              <Divider />
            </Box>
            <Grid container justifyContent="center" spacing={2}>
              <Grid item>
                <strong>
                  <ButtonLink
                    weight="600"
                    label={CAPTIONS.cantLogin[store.locale]}
                    color="primary"
                  />
                </strong>
              </Grid>
              <Grid item>
                <strong>
                  <ButtonLink weight="600" label={CAPTIONS.signUp[store.locale]} color="primary" />
                </strong>
              </Grid>
            </Grid> */}
          </Box>
        </Paper>
      </Grid>
    </LoginWrapper>
  );
};

export default Verify;
