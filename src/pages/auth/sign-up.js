import { useState, useContext } from 'react';
import { Store as GlobalStore, useAuth } from '../../context';
import { Form, Field } from 'react-final-form';
import { LoginWrapper } from '../../components/Auth';
import {
  Grid,
  Paper,
  Box,
  Typography,
  TextField,
  Button,
  CircularProgress,
  makeStyles,
} from '@material-ui/core';

import { useRouter } from 'next/router';

import { getNextAuthQuestion } from '../../services/auth';

import { LOGIN, VERIFY, SIGN_UP } from '../../constants/login';

const CAPTIONS = {
  introText: {
    'en-us': 'Please enter your first name and last name.',
    'es-mx': 'Ingrese su nombre y apellido.',
  },
  continue: {
    'en-us': 'Continue',
    'es-mx': 'Continuar',
  },
  firstName: {
    'en-us': 'First name',
    'es-mx': 'Nombre de pila',
  },
  lastName: {
    'en-us': 'Last name',
    'es-mx': 'Apellido',
  },
};

const useClasses = makeStyles(() => ({
  input: {
    fontSize: '1.3rem',
    fontWeight: 500,
  },
}));

const SignUp = () => {
  const { store } = useContext(GlobalStore);
  const classes = useClasses();

  const router = useRouter();

  const { signIn } = useAuth();

  const [loading, setLoading] = useState(false);

  const handleSignIn = (nextQuestion) => {
    // if the first name is asked again, there is an error somewhere
    if (nextQuestion.key === SIGN_UP.KEY_FIRST_NAME) {
      setLoading(false);
      throw new Error('Something went wrong. Please try again.');
    }
    router.push('/dashboard');
  };

  const onSubmit = async (data) => {
    setLoading(true);
    const { first_name: firstName, last_name: lastName } = data;
    const { memberPhoneNumber, memberDoB } = router.query;
    // we are directly using the conversation engine here
    const question = {
      questionId: 22,
      answer: lastName,
      key: SIGN_UP.KEY_LAST_NAME,
    };
    // get the next question
    await getNextAuthQuestion({
      question,
      latestPayload: {
        [LOGIN.KEY]: memberPhoneNumber,
        [VERIFY.KEY]: memberDoB,
        [SIGN_UP.KEY_FIRST_NAME]: firstName,
      },
      signIn,
      handleEndOfQuestions: (_, { nextQuestion }) => handleSignIn(nextQuestion),
    });
  };

  return (
    <LoginWrapper>
      <Grid item xs={12}>
        <Paper>
          <Box py={3} px={2}>
            <Box px={2} mb={2}>
              <Typography variant="h5" align="center">
                {CAPTIONS.introText[store.locale]}
              </Typography>
            </Box>
            <Form
              onSubmit={onSubmit}
              render={({ handleSubmit }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <Grid container spacing={2} direction="column">
                      <Grid item xs>
                        <Field
                          name="first_name"
                          render={({ input }) => (
                            <TextField
                              {...input}
                              InputProps={{
                                className: classes.input,
                              }}
                              placeholder={CAPTIONS.firstName[store.locale]}
                              fullWidth
                              margin="none"
                              variant="outlined"
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs>
                        <Field
                          name="last_name"
                          render={({ input }) => (
                            <TextField
                              {...input}
                              InputProps={{
                                className: classes.input,
                              }}
                              fullWidth
                              margin="none"
                              variant="outlined"
                              placeholder={CAPTIONS.lastName[store.locale]}
                            />
                          )}
                        />
                      </Grid>
                    </Grid>
                    <Box mt={2}>
                      <Button type="submit" color="primary" fullWidth disabled={loading}>
                        {loading ? (
                          <CircularProgress color="inherit" size={24} />
                        ) : (
                          CAPTIONS.continue[store.locale]
                        )}
                      </Button>
                    </Box>
                  </form>
                );
              }}
            />
          </Box>
        </Paper>
      </Grid>
    </LoginWrapper>
  );
};

export default SignUp;
