import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { Box, Paper, Typography, Grid, Link } from '@material-ui/core';
import { useAuth, Store as GlobalStore } from '../../context';
import { LoginWrapper, LoginForm } from '../../components/Auth';
import { INPUT_TYPES } from '../../components/Chat/constants';

import { initiateLogin } from '../../services/auth';
import { getFormattedPhoneNumber, setInStorage } from '../../utils';

import { LOGIN, GGD_AUTH } from '../../constants/login';

const CAPTIONS = {
  getStarted: {
    'en-us': 'Get started by entering your phone number',
    'es-mx': 'Comience ingresando su número de teléfono',
  },
  agree: {
    'en-us': 'By clicking "continue", you agree to GoGetDoc',
    'es-mx': 'Al hacer clic en "continuar", acepta los',
  },
  terms: {
    'en-us': 'Terms of Service',
    'es-mx': 'Términos de Servicios',
  },
  agree2: {
    'en-us': 'and acknowledge you have read the',
    'es-mx': 'de GoGetDoc Inc y reconoce que ha leído las',
  },
  policy: {
    'en-us': 'Privacy Policy.',
    'es-mx': 'Póliticas de Privacidad.',
  },
  agree3: {
    'en-us':
      'You also consent to receive calls or SMS messages, including by automated dialer, from GoGetDoc Inc and its affiliates to the number you provide for informational purposes including your appointments and results. A handling fee may apply for international users. You understand that you may opt out by texting',
    'es-mx':
      'También acepta recibir llamadas o mensajes SMS, incluso mediante un marcador automático, de GoGetDoc Inc y sus afiliados al número que proporcione con fines informativos, incluidas sus citas y resultados. Es posible que se aplique una tarifa de gestión para los usuarios internacionales. Usted comprende que puede optar por no participar enviando un mensaje de texto con',
  },
  stop: {
    'en-us': 'STOP',
    'es-mx': 'STOP',
  },
  to: {
    'en-us': 'to 877-280-2151',
    'es-mx': 'al 877-280-2151',
  },
  continue: {
    'en-us': 'Continue',
    'es-mx': 'Continuar',
  },
  cantLogin: {
    'en-us': `Can't login?`,
    'es-mx': '¿No puede iniciar sesión?',
  },
  signUp: {
    'en-us': 'Sign up for new user?',
    'es-mx': '¿Registrarse como usuario nuevo?',
  },
};

const Login = () => {
  const { isAuthenticated } = useAuth();
  const router = useRouter();
  const { store } = useContext(GlobalStore);

  const handleResponse = async (value) => {
    try {
      const phoneNumber = getFormattedPhoneNumber(value);
      const payload = {
        stage: 'login',
        data: {
          [LOGIN.KEY]: phoneNumber,
        },
      };

      const { session } = await initiateLogin(payload);

      // go to next view if the opt has been sent to the member
      router.push('/auth/challenge');
      // save the session in storage
      setInStorage(GGD_AUTH, { session });
    } catch (err) {
      throw new Error('Error in sending verification code. Please try again.');
    }
  };

  if (isAuthenticated) {
    router.push('/member-chat');
    return '';
  }

  return (
    <LoginWrapper>
      <Grid item xs={12}>
        <Paper>
          <Box py={3} px={2}>
            <Box px={2}>
              <Typography variant="h5" align="center">
                {CAPTIONS.getStarted[store.locale]}
              </Typography>
            </Box>
            <LoginForm
              inputControl="PHONE_NUMBER"
              inputType={INPUT_TYPES.PHONE_NUMBER}
              handleResponse={handleResponse}
            />
            <Box mt={3}>
              <Typography
                variant="body2"
                align="center"
                style={{ fontSize: '0.75rem', color: '#757575' }}
              >
                {CAPTIONS.agree[store.locale]}{' '}
                <Link href="https://gogetdoc.com/terms" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.terms[store.locale]}
                </Link>{' '}
                {CAPTIONS.agree2[store.locale]}{' '}
                <Link href="https://gogetdoc.com/privacy" target="_blank" rel="noopener noreferrer">
                  {CAPTIONS.policy[store.locale]}
                </Link>{' '}
                {CAPTIONS.agree3[store.locale]} <strong>{CAPTIONS.stop[store.locale]}</strong>{' '}
                {CAPTIONS.to[store.locale]}
              </Typography>
            </Box>
            {/* <Box py={3}>
              <Divider />
            </Box>
            <Grid container justifyContent="center" spacing={2}>
              <Grid item>
                <strong>
                  <ButtonLink
                    weight="600"
                    label={CAPTIONS.cantLogin[store.locale]}
                    color="primary"
                  />
                </strong>
              </Grid>
              <Grid item>
                <strong>
                  <ButtonLink weight="600" label={CAPTIONS.signUp[store.locale]} color="primary" />
                </strong>
              </Grid>
            </Grid> */}
          </Box>
        </Paper>
      </Grid>
    </LoginWrapper>
  );
};

export default Login;
