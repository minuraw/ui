import { useRouter } from 'next/router';
import { useEffect } from 'react';

const DashboardRedirect = () => {
  const router = useRouter();

  useEffect(() => {
    router.replace('/dashboard/chat');
  });

  return '';
};

export default DashboardRedirect;
