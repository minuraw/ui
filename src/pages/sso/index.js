import { useEffect, useState, useCallback } from 'react';
import { useRouter } from 'next/router';

import { Box } from '@material-ui/core';

import { GGDLoader } from '../../components/Common';

import { initiateAuth } from '../../events';

import { setAccessToken, validateSession } from '../../services/auth';

import { useAuth } from '../../context';

const SSO = () => {
  const router = useRouter();

  const { signIn } = useAuth();

  const [error, setError] = useState(null);

  const handleInitiateAuthLoad = useCallback(
    async (err, data) => {
      if (err) {
        setError(err);
        return;
      }
      // handle the auth token load
      const { accessToken } = data;
      if (!accessToken) {
        setError(new Error('No access token'));
        return;
      }
      setAccessToken(accessToken);

      // see if the user is validated
      const isValid = await validateSession();
      if (isValid) {
        signIn();
      }

      // Assume that relay as a path
      const { relayState } = router.query;
      let redirectPath = '/';
      if (relayState) {
        redirectPath = relayState;
      }
      router.push(redirectPath);
    },
    [router, signIn],
  );

  useEffect(() => {
    if (router.isReady) {
      const { token } = router.query;
      initiateAuth(handleInitiateAuthLoad, { token });
    }
  }, [handleInitiateAuthLoad, router.isReady, router.query]);

  if (error) {
    return (
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        style={{ height: '100vh', color: '#26442b' }}
      >
        <h2>Something went wrong. Please refresh the page.</h2>
      </Box>
    );
  }

  return <GGDLoader />;
};

export default SSO;
