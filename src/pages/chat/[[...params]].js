import { Conversation } from '../../components/Chat';
import { Header } from '../../components/Layout';
import { useRouter } from 'next/router';
import { decodeChatRouteParams } from '../../utils';
import { useAuth, useToggles } from '../../context';
import { useEffect, useState } from 'react';
import { CircularLoader } from '../../components/Loader';
import { CONVERSATION_PROVIDER_ID } from '../../constants/providers';
import { FEATURE_TOGGLES } from '../../constants';

const PROVIDERS = [
  CONVERSATION_PROVIDER_ID.SYMPTOM_CHECKER,
  CONVERSATION_PROVIDER_ID.VAX_YES,
  CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER,
];

// TODO - implement and move this to a separate location - may need api calls to validate skillId
const isValidProduct = (productId, skillId) => {
  return true;
};

const Chat = () => {
  const { isAuthenticated } = useAuth();
  const router = useRouter();
  const [routerParams, setRouterParams] = useState(null);
  const { isFeatureEnabled, isToggleReady } = useToggles();

  useEffect(() => {
    // this way we know that the relevant page loaded with or without authentication
    // here the session storage is used instead of the Store Provider to avoid unnecessary rendering when ths store changes
    window.localStorage.setItem('startedAuthenticated', JSON.stringify(isAuthenticated));
    return function cleanup() {
      window.localStorage.removeItem('startedAuthenticated');
    };
  }, []);

  useEffect(() => {
    if (router.isReady && isToggleReady) {
      if (isAuthenticated) {
        // router.replace('/dashboard' + router.asPath);
        // return;
      }
      const routParams = decodeChatRouteParams(router);
      // set groupid to redirect REI users in logout
      window.localStorage.setItem('groupIdValue', routParams.groupId);

      const enableVaxYesConversation = isFeatureEnabled(
        FEATURE_TOGGLES.ENABLE_VAX_YES_CONVERSATION,
      );
      const enableSymptomBotConversation = isFeatureEnabled(
        FEATURE_TOGGLES.ENABLE_SYMPTOM_BOT_CONVERSATION,
      );
      const enableAllConversation = isFeatureEnabled(FEATURE_TOGGLES.ENABLE_ALL_CONVERSATIONS);

      const provider = routParams?.provider;
      if (
        enableAllConversation ||
        ((provider === CONVERSATION_PROVIDER_ID.VAX_YES ||
          provider === CONVERSATION_PROVIDER_ID.VAX_YES_BOOSTER ||
          provider === CONVERSATION_PROVIDER_ID.GGD_CO_VAX_YES) &&
          enableVaxYesConversation) ||
        (provider === CONVERSATION_PROVIDER_ID.SYMPTOM_CHECKER && enableSymptomBotConversation) ||
        provider === CONVERSATION_PROVIDER_ID.GGD_TEST_CERTIFICATES
      ) {
        updateRouterParameters(routParams);
      } else {
        window.location.href = 'https://gogetdoc.com';
      }
    }
  }, [router, isToggleReady]);

  const updateRouterParameters = (params) => {
    if (
      params &&
      params.start &&
      params.skillId &&
      params.productId &&
      isValidProduct(params.productId, params.skillId)
    ) {
      setRouterParams(params);
    } else if (params && params.conversationId) {
      // If the conversationId present in the params save it
      setRouterParams(params);
    } else if (params && PROVIDERS.includes(params.provider)) {
      setRouterParams(params);
    } else {
      router.replace('/');
    }
  };

  // as long the skill does not change, don't render this
  const getConversationKey = () => {
    const { conversationId, skillId } = routerParams;
    return conversationId || skillId;
  };

  const ssoData = {
    sso: false,
  };

  // identify whether this is a sso user or not
  if (routerParams?.groupId && routerParams?.nameId && routerParams?.groupId === 'rei') {
    ssoData.sso = true;
    ssoData.nameId = routerParams?.nameId;
    ssoData.sessionIndex = routerParams?.sessionIndex;
    ssoData.memberFirstName = routerParams?.firstName;
    ssoData.memberLastName = routerParams?.lastName;
  }

  return (
    <>
      <Header headerType="chat" />
      {routerParams ? (
        <Conversation
          key={getConversationKey()}
          sync={routerParams.sync}
          productId={routerParams.productId}
          groupId={routerParams.groupId}
          skillId={routerParams.skillId}
          skill={routerParams.skill}
          skillMode={routerParams.skillMode}
          conversationId={routerParams.conversationId}
          messageId={routerParams.messageId}
          provider={routerParams.provider}
          isAlreadyInitiatedChat={routerParams.isAlreadyInitiatedChat}
          directLink={routerParams.directLink}
          isUpdate={routerParams.isUpdate}
          ssoData={ssoData}
        />
      ) : (
        <CircularLoader displayText="Loading.." GGD />
      )}
    </>
  );
};

export default Chat;
