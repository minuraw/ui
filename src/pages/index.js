/* eslint-disable no-undef */
import { useState, useContext, useRef, useEffect } from 'react';
import {
  Clinical,
  Utility,
  Store,
  Footer,
  ModernCare,
  MoreCare,
  CustomTabs,
  Header,
  MarketingPartners,
} from '../components/Layout';
import { Typography, Box, useMediaQuery } from '@material-ui/core';
import { IntentSearch, SectionContainer } from '../components/Common';
import { getProductByCategory } from '../events';
import { Store as GlobalStore, useToggles } from '../context';
import { getProductCategories, getProducts, getLocale } from '../services';
import { FEATURE_TOGGLES } from '../constants';
import { CircularLoader } from '../components/Loader';

const CAPTIONS = {
  title: {
    'en-us': 'Modern Healthcare Starts Here',
    'es-mx': 'La atención médica moderna comienza aquí',
  },
};

const GROUP_LIMITS = [
  {
    id: 1,
    limit: 3,
  },
  {
    id: 2,
    limit: 12,
  },
  {
    id: 3,
    limit: 4,
  },
  {
    id: 4,
    limit: 4,
  },
  {
    id: 5,
    limit: 20,
  },
];

const Home = ({ productGroups, categories }) => {
  const { store, setInStore } = useContext(GlobalStore);
  const { isFeatureEnabled, isToggleReady } = useToggles();

  const sectionTop = useRef();

  const [productsVisible, setProductsVisible] = useState(1);

  const isLargeScreen = useMediaQuery((theme) => theme.breakpoints.up('sm'));

  // At navigations, next will load data from server
  // We are checking if data is loaded in context if so show them if not show the data from server
  const validProductGroupData = store.productGroups || productGroups;

  const [productGroupData, setProductGroupData] = useState(validProductGroupData);
  const [isDataLoading, setIsDataLoading] = useState(false);

  const [largeScreen, setLargeScreen] = useState(true);
  const [showAllCards, setShowAllCards] = useState(true);
  const [showVaxYesCard, setShowVaxYesCard] = useState(true);
  const [isHidden, setIsHidden] = useState(true);
  const locale = getLocale();

  useEffect(() => {
    setLargeScreen(isLargeScreen);
    setIsDataLoading(true);
    if (isToggleReady) {
      if (!isFeatureEnabled(FEATURE_TOGGLES.SHOW_MAIN_PAGE)) {
        window.location.href = 'https://gogetdoc.com';
        return;
      }
      setShowAllCards(isFeatureEnabled(FEATURE_TOGGLES.SHOW_ALL_CARDS));
      setShowVaxYesCard(isFeatureEnabled(FEATURE_TOGGLES.SHOW_VAX_YES_CARD));
      setIsDataLoading(false);
      setIsHidden(false);
    }
  }, [isFeatureEnabled, isLargeScreen, isToggleReady]);

  // This is called once the products in a certain category is loaded
  const handleLoadProductByCategory = (data) => {
    setIsDataLoading(false);
    // Save items in context
    setInStore({ productGroups: data });
    setProductGroupData(data);
  };

  // Hanlder when a category is selected by user
  const handleCategorySelect = (categoryId) => {
    setIsDataLoading(true);
    getProductByCategory(handleLoadProductByCategory, { categoryId });
  };

  if (!productGroupData) return null;

  // Show a loader
  if (isDataLoading) return <CircularLoader GGD />;

  // Scroll content into view when tabs are clicked
  const scrollContent = () => {
    sectionTop.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  };

  if (showVaxYesCard && !showAllCards) {
    // filter only vaxyes card
    productGroupData[1].products = productGroupData[1].products.filter((p) => p.id === 5);
  }

  return (
    <Box style={{ visibility: isHidden ? 'hidden' : 'visible' }}>
      <Header />
      {/* Title and search bar */}
      <SectionContainer maxWidth="sm">
        <Box textAlign="center">
          <Typography variant="h1" component="h1">
            {CAPTIONS.title[locale]}
          </Typography>
        </Box>
        <Box mt={4}>
          <IntentSearch suggestions />
        </Box>
        {largeScreen && (
          <Box mt={3}>
            <MarketingPartners />
          </Box>
        )}
      </SectionContainer>

      <Box ref={sectionTop}>
        {showAllCards && productGroupData[0] && (largeScreen || productsVisible - 1 === 0) && (
          <Clinical
            productGroup={productGroupData[0]}
            filterCategories={categories}
            handleCategorySelect={handleCategorySelect}
          />
        )}
        {(showAllCards || showVaxYesCard) &&
          productGroupData[1] &&
          (largeScreen || productsVisible - 1 === 1) && (
            <Utility productGroup={productGroupData[1]} />
          )}
        {showAllCards && productGroupData[2] && (largeScreen || productsVisible - 1 === 2) && (
          <Store productGroup={productGroupData[2]} />
        )}
        {showAllCards && productGroupData[3] && (largeScreen || productsVisible - 1 === 3) && (
          <ModernCare productGroup={productGroupData[3]} />
        )}
        {showAllCards && productGroupData[4] && (largeScreen || productsVisible - 1 === 4) && (
          <MoreCare productGroup={productGroupData[4]} />
        )}
      </Box>

      <SectionContainer color maxWidth="lg">
        <Footer />
      </SectionContainer>
      {!largeScreen && (
        <CustomTabs
          setProductsVisible={setProductsVisible}
          tabData={productGroupData}
          handleClick={scrollContent}
        />
      )}
    </Box>
  );
};

export async function getStaticProps() {
  const productGroups = await Promise.all(
    GROUP_LIMITS.map(async (groupLimit) => {
      const productData = await getProducts({
        limit: groupLimit.limit,
        offset: 0,
        product_group_id: groupLimit.id,
      });
      return productData[0];
    }),
  );

  const categories = await getProductCategories();

  return {
    props: {
      productGroups:
        productGroups && Array.isArray(productGroups) && productGroups.length > 0
          ? productGroups
          : null,
      categories,
    },
    revalidate: 900, // 15 min cache
  };
}

export default Home;
