import { groupAppointmentsByStatus } from '../src/utils';
import { describe, it, expect } from '@jest/globals';

describe('groupAppointmentsByStatus', () => {
  it('Returns an empty object when passed an empty object', () => {
    const result = groupAppointmentsByStatus({});
    expect(result).toEqual({});
  });

  it('Returns an empty object when passed an empty array', () => {
    const result = groupAppointmentsByStatus([]);
    expect(result).toEqual({});
  });

  it('Returns an object for valid object', () => {
    const result = groupAppointmentsByStatus([{ encounterDescription: 'cancelled' }]);
    expect(result).toEqual({
      cancelled: [
        {
          encounterDescription: 'cancelled',
        },
      ],
    });
  });
});
