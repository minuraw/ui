module.exports = {
  basePath: '',
  reactStrictMode: false,
  images: {
    domains: [
      'images.dev-vaxyes-api.gogetdoc.com',
      'ggd-static-resource-bucket-dev.s3.us-east-2.amazonaws.com',
      'ggd-tele-consult-api-attachments-dev.s3.us-east-2.amazonaws.com',
    ],
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
};
